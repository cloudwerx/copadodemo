/**
* @name         LandingComponent
* @author       Girish
* @date         5-3-21
* @description  This component holds tiles to all the internal tools
**/
import { LightningElement, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getUserType from '@salesforce/apex/LandingComponentController.getUserType';
export default class LandingComponent extends NavigationMixin(LightningElement) {


   /*  page refs variables */
    @track onboardingPageRef;
    @track skillAnalyzerPageRef;
    @track candidateEvaluatorPageRef;
    @track certificationPageRef;
    @track processPageRef;
    @track attendancePageRef;
    @track onbordingListPageRef;
    
  

    @track contactId;
    @track isOnboardingComplete;
    @track userType;
    @track type;

    connectedCallback() {

        this.contactId = new URL(window.location.href).searchParams.get('recordId');
        /* this.isUserAuthenticated = new URL(window.location.href).searchParams.get('Authenticated');
        this.isOnboardingComplete = new URL(window.location.href).searchParams.get('onboardingComplete');
        this.type = new URL(window.location.href).searchParams.get('type'); */
   
    }

    showToast(toastTitle, toastMessage, toastVarient) {
        const evt = new ShowToastEvent({
            title: toastTitle,
            message : toastMessage,
            variant: toastVarient,
        });
        this.dispatchEvent(evt);
    }
    
    navigateToOnboarding(event) {
        /* console.log('inside navigateToOnboarding');
        console.log('type :' + this.type); */

        getUserType({loggedInUserId : this.contactId
        }).then(res=>{
            this.type = res;
            if(this.type == 'Employee') { // if empoyee, used single alphabet instead of "employee" to avoid url manipulating
            //console.log('inside if');
            this.onboardingPageRef = {
                type: 'comm__namedPage',
                attributes: {
                    name: 'Onboarding__c'
                },
                state : {
                    // onboardingComplete : this.isOnboardingComplete,
                    recordId : this.contactId,
                    Authenticated : 'true'
                }
            };
            //console.log('home page reference: ' + JSON.stringify(this.homePageRef));
            this[NavigationMixin.Navigate](this.onboardingPageRef);
        } else if(this.type == 'Employer') {// if employer
            
            this.onbordingListPageRef = {
                type: 'comm__namedPage',
                attributes: {
                    name: 'OnboardingEmployeeList__c'
                },
                state : {
                    recordId : this.contactId
                    // Authenticated : 'true'
                }
            }; 
            //console.log('home page reference: ' + JSON.stringify(this.onbordingListPageRef));
            this[NavigationMixin.Navigate](this.onbordingListPageRef);
        }
        }).catch(err => {
            //console.log(err);
        });

        
        
    }

    navigateToCertsTracker(){
        this.certificationPageRef = {
            type: 'comm__namedPage',
            attributes: {
                name: 'Certification_Tracker__c'
            },
            state : {
                recordId : this.contactId
            }
        }; 
        //console.log('home page reference: ' + JSON.stringify(this.certificationPageRef));
        this[NavigationMixin.Navigate](this.certificationPageRef);
    }

    navigateToSkillAnalyzer() {
        this.showToast('Coming soon...', 'This tool is not available at the moment, please check back later', 'info');
        this.skillAnalyzerPageRef = {
            type: 'comm__namedPage',
            attributes: {
                name: ''
            },
            state : {
            }
        }; 
        //console.log('home page reference: ' + JSON.stringify(this.skillAnalyzerPageRef));
        this[NavigationMixin.Navigate](this.skillAnalyzerPageRef);
    }
    navigateToProcessInsight() {
        this.showToast('Coming soon...', 'This tool is not available at the moment, please check back later', 'info');
        this.processPageRef = {
            type: 'comm__namedPage',
            attributes: {
                name: ''
            },
            state : {
            }
        }; 
        //console.log('home page reference: ' + JSON.stringify(this.processPageRef));
        this[NavigationMixin.Navigate](this.processPageRef);
    }

    navigateToAttendanceApp() {
        this.showToast('Coming soon...', 'This tool is not available at the moment, please check back later', 'info');
        this.attendancePageRef = {
            type: 'comm__namedPage',
            attributes: {
                name: ''
            },
            state : {
            }
        }; 
        //console.log('home page reference: ' + JSON.stringify(this.attendancePageRef));
        this[NavigationMixin.Navigate](this.attendancePageRef);
    }

    navigateToCandidateEvaluator() {
        this.showToast('Coming soon...', 'This tool is not available at the moment, please check back later', 'info');
    }
    
}