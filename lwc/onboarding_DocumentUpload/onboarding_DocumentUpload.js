/************************************************************************************************
* @Name         onboarding_DocumentUpload
* @Author       Yash Bhalerao
* @Date         19-01-2021
* @Description  Custom file upload component
* @TestClass    
*************************************************************************************************/

import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import saveFile from '@salesforce/apex/OnboardingUIController.saveFile';
import sendSlackMessage from '@salesforce/apex/OnboardingUIController.sendSlackMessage';
//import { registerListener } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';

import {subscribe,unsubscribe,MessageContext,publish} from 'lightning/messageService';
import filterContactsMC from '@salesforce/messageChannel/FilterContactsMessageChannel__c';
import newMC from '@salesforce/messageChannel/NewMessageChannel__c';

import PartnerCommunityGuide from '@salesforce/resourceUrl/CloudwerxLogoOnboarding';

const columns = [
    {label: 'Title', fieldName: 'Title'}
];

export default class Onboarding_DocumentUpload extends LightningElement 
{

    @track cloudwerxManual = PartnerCommunityGuide;

    subscription = null;

    @wire(MessageContext)
    messageContext;

    @track contactfromlogin = {};
    
    @wire(CurrentPageReference) pageRef;

    @api acceptedFileFormats;
    @api fileUploaded;
    @api slackMessage;
    @api informationForm;
    @api isComplete;


    handleUploadFinished() {
        this.dispatchEvent(new CustomEvent('uploadfinished', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                data: { recordId: this.recordId }
            }
        }));
        this.dispatchEvent(new ShowToastEvent({
            title: 'Completed',
            message: 'File has been uploaded',
        }));
    }


    @api recordId;
    @track columns = columns;
    @track data;
    @track fileName = '';

    @track UploadFile = 'Upload File';
    @track SlackMessageButtonText = 'Send Message';
    @track informationFormButtonText = 'Employee Info';

    @track showLoadingSpinner = false;
    @track isTrue = false;
    selectedRecords;
    filesUploaded = [];
    file;
    fileContents;
    fileReader;
    content;
    MAX_FILE_SIZE = 1500000;

    @track isModalOpen = false;
    @track isSlackModalOpen = false;
    @track isInformationModalOpen = false;
    
    @track dataList = []

    @api showButton;

    @track isSlackMsgSent = false;

    connectedCallback() 
    {
        if (!this.subscription) {
            this.subscription = subscribe(
                this.messageContext,
                filterContactsMC,
                (message) => this.handleFilterKeySubmit(message)
            );
        }
    }

    disconnectedCallback() {
        unsubscribe(this.subscription);
        this.subscription = null;
    }

    handleFilterKeySubmit(message)
    {
        //console.log('message : ' + message);
        this.contactfromlogin = message;
        //console.log('this.contactfromlogin : ' + this.contactfromlogin);
    }

    // getting file 
    handleFilesChange(event) {
        if(event.target.files.length > 0) {
            this.filesUploaded = event.target.files;
            this.fileName = event.target.files[0].name;
        }
    }

    handleSave() {
        if(this.filesUploaded.length > 0) {
            this.uploadHelper();
        }
        else {
            this.fileName = 'Please select file to upload!';
        }
    }

    uploadHelper() {
        this.file = this.filesUploaded[0];
       if (this.file.size > this.MAX_FILE_SIZE) {
            //window.console.log('File Size is to long');
            return ;
        }
        this.showLoadingSpinner = true;
        // create a FileReader object 
        this.fileReader= new FileReader();
        // set onload function of FileReader object  
        this.fileReader.onloadend = (() => {
            this.fileContents = this.fileReader.result;
            let base64 = 'base64,';
            this.content = this.fileContents.indexOf(base64) + base64.length;
            this.fileContents = this.fileContents.substring(this.content);
            
            // call the uploadProcess method 
            this.saveToFile();
        });
    
        this.fileReader.readAsDataURL(this.file);
    }

    // Calling apex class to insert the file
    saveToFile() 
    {
        //console.log('this.contactfromlogin.Id : ' + this.contactfromlogin.Id);
        saveFile({ idParent: this.recordId, strFileName: this.file.name, base64Data: encodeURIComponent(this.fileContents), contactId : this.contactfromlogin.Id})
        .then(result => {
            window.console.log('result ====> ' +result);
            // refreshing the datatable
            //this.getRelatedFiles();

            this.fileName = this.fileName + ' - Uploaded Successfully';
            this.UploadFile = 'Upload File';
            //this.isComplete = true;
            this.showLoadingSpinner = false;

            // Showing Success message after file insert
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success!',
                    message: this.file.name + ' - Uploaded Successfully',
                    variant: 'success',
                }),
            );

        })
        .catch(error => {
            // Showing errors if any while inserting the files
            //window.console.log(error);
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error while uploading File',
                    message: error.message,
                    variant: 'error',
                }),
            );
        });
    }

    openModal() {
        // to open modal set isModalOpen tarck value as true
        this.isModalOpen = true;
    }
    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.isModalOpen = false;
    }
    submitDetails() {
        // to close modal set isModalOpen tarck value as false
        //Add your code to call apex method or do some processing
        this.isModalOpen = false;
    }

    //Slack Modal

    @track slackMessageText;

    openSlackModal() {
        // to open modal set isModalOpen tarck value as true
        this.isSlackModalOpen = true;
    }
    closeSlackModal() {
        // to close modal set isModalOpen tarck value as false
        this.isSlackModalOpen = false;
    }
    submitSlackDetails() {
        // to close modal set isModalOpen tarck value as false
        //Add your code to call apex method or do some processing
        this.isSlackModalOpen = false;
    }

    handleTextChange(event)
    {
        if(event.target.value.length > 0)
        {
            this.slackMessageText = event.target.value;
        }
    }

    handleTextSave(event)
    {
        this.sendMessage();
        this.isSlackModalOpen = false;
    }

    @track responseFromApex;

    sendMessage()
    {
        //console.log('this.slackMessageText : ' + this.slackMessageText);
        //console.log('this.contactfromlogin.Id : ' + this.contactfromlogin.Id);
        
        sendSlackMessage({slackMessage : this.slackMessageText, contactId : this.contactfromlogin.Id}).then(response =>{
            this.responseFromApex = response;
            this.isComplete = true;
            this.SlackMessageButtonText = 'Message sent';
            const toastEvent = new ShowToastEvent({
                title : 'Message sent successfully.',
                variant : 'success',
            });
            this.dispatchEvent(toastEvent);
            }).catch(error => {
            //console.log('Error - Cannot send message');
            });
    }

    openInformationModal() {
        // to open modal set isModalOpen tarck value as true
        this.isInformationModalOpen = true;
    }
    closeInformationModal() {
        // to close modal set isModalOpen tarck value as false
        this.isInformationModalOpen = false;
    }
    submitInformationDetails() {
        // to close modal set isModalOpen tarck value as false
        //Add your code to call apex method or do some processing
        this.isInformationModalOpen = false;
        publish(this.messageContext, newMC, null);
        this.isComplete = true;
    }
}