/**
* @name         Onboarding_login
* @author       Girish Baviskar
* @date         15-1-2021
* @description  Login component for onboarding.
**/
import { LightningElement, track, wire } from 'lwc';
import generateAndEmailOtp from '@salesforce/apex/Onboarding_loginController.generateAndEmailOtp';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import CloudwerxLogoOnboarding from '@salesforce/resourceUrl/CloudwerxLogoOnboarding';
import { NavigationMixin } from 'lightning/navigation';
import { CurrentPageReference } from 'lightning/navigation';
export default class Onboarding_login extends NavigationMixin(LightningElement)  {

    @track email;
    @track showSendOtpSection = true;
    @track showLoginSection = false;
    
    @track data = [];
    @track receivedOtp ;
    @track contact = {};
    @track inputOtp;
    @track cloudwerxLogoURL = CloudwerxLogoOnboarding;
    @track isOnboardingComplete;

    @wire(CurrentPageReference)
    pageRef;
    pageRefUrl;
    @track homePageUrl;
    @track onbordingListPageRef;
    @track homePageRef;
    connectedCallback() {
        // Generate a URL to a User record page
        
         this[NavigationMixin.GenerateUrl](this.pageRef)
            .then(url => this.pageRefUrl = url); 
        //console.log('home page url' + this.pageRefUrl);
        //console.log('currnet page ref : ' + JSON.stringify(this.pageRef));
    }
    
    EmailInputChangeHandler(event) {
        this.email = event.target.value;
    }

    OtpInputChangeHandler(event) {
        this.inputOtp = event.target.value;
    }

    //method to show toast.
    showToast(toastTitle, toastMessage, toastVarient) {
        const evt = new ShowToastEvent({
            title: toastTitle,
            message : toastMessage,
            variant: toastVarient,
        });
        this.dispatchEvent(evt);
    }
    
    //validates input and if all good, generates otp. 
    sendOtpClickHandler() {
        //console.log('inside sendOtpClickHandler');
        if(this.email != null) {
            if(this.email.includes('cloudwerx' )) {
                this.showSendOtpSection = false;
                this.showLoginSection = true;
                //console.log('inside sendOtpClickHandler()');
                this.sendOtp();
            } else {
                this.showToast('Not Cloudwerx Email', 'Please enter cloudwerx Email address', 'info');
            }
        } else {
             // alert('Please enter Your Official Email id');
             this.showToast('Email Not Entered', 'Please enter email address below ', 'info');
        }
        
    }

    sendOtp() {
        //console.log('inside sendOtp()');
        //that = this;
        generateAndEmailOtp({officialEmailId : this.email}).then(res =>{
            this.receivedOtp = res.otp;
            this.contact = res.objContact;
            //console.log('Onboarding_Feedback_Rating__c' + res.objContact.Onboarding_Feedback_Rating__c);
            if(res.objContact.Is_Employee_Onboarding_Complete__c == false) {
                //console.log('inside if');
                this.isOnboardingComplete = 'false';
                //console.log('isOnboardingComplete ' + this.isOnboardingComplete);
            } else {
                //console.log('inside else');
                this.isOnboardingComplete = 'true';
            }
            
            //console.log('inside then of send opt contact ID:'+ this.contact.Id);
            this.showToast('OTP Sent', 'Please check your cloudwerx Email address', 'success');
        }).catch(err => {
            //alert('Opps. That Email id Does not exists with us');
            //console.log('email : ' + this.email);
            this.showLoginSection  = false;
            this.showSendOtpSection = true;
            this.showToast('Email Not Found', 'Enter email issued by cloudwerx', 'info');
            //console.log('inside catch of sendOtp');
            //console.log(err)
        });
    }

    backClickHandler(){
        this.showSendOtpSection = true;
        this.showLoginSection = false;   
    }
    loginClickHandler(event){
        this.validateOtp();
    }

    validateOtp() {
        //console.log('inputotp : ' + this.inputOtp + 'receivedOtp : '+ this.receivedOtp);
        if(this.inputOtp) {
            if(this.inputOtp == this.receivedOtp) {
                this.showToast('Login Successful', 'Welcome to onboarding', 'success');
                this.navigateToNextPage();

                
                /* this[NavigationMixin.GenerateUrl](this.homePageRef)
                .then(url => this.homePageUrl = url); */

                // Generate a URL to a User record page
                /* this[NavigationMixin.GenerateUrl]({
                    type: 'standard__recordPage',
                    attributes: {
                        recordId: 'a120T000000I83iQAC',
                        actionName: 'view',
                    },
                }).then(url => {
                    this.homePageUrl = url;
                });
                //console.log('home page url : ' + this.homePageUrl); */
               
                
            } else {
                this.showToast('Incorrect OTP', 'Please Enter OTP you received on Email ID', 'error');
                //alert('Please Enter OTP you received on Email ID');
            }
        } else {
            //alert('Please Enter OTP you received on Email ID');
            this.showToast('OTP Not found', 'Please Enter OTP', 'info');
        }
        
        
    }
    
    //Navigates to next page after successfull login.
    navigateToNextPage() {
       
        //console.log('inside navigateToNextPage');
        //console.log('emp  type : ' + this.contact.Employee_Type__c);
        //console.log('isOnboardingComplete :' + this.isOnboardingComplete);
        if(this.contact.Employee_Type__c == 'Employee') {
            //console.log('inside if');
            this.homePageRef = {
                type: 'standard__namedPage',
                attributes: {
                    pageName: 'home'
                },
                state : {
                    onboardingComplete : this.isOnboardingComplete,
                    Authenticated : 'true',
                    recordId : this.contact.Id
                }
            };
            //console.log('home page reference: ' + JSON.stringify(this.homePageRef));
            this[NavigationMixin.Navigate](this.homePageRef);
        } else if(this.contact.Employee_Type__c == 'Employer') {
            //console.log('inside else if');
            this.onbordingListPageRef = {
                type: 'comm__namedPage',
                attributes: {
                    name: 'onboardingEmployeeList__c'
                },
                state : {
                    recordId : this.contact.Id,
                    Authenticated : 'true'
                
                }
            };
            //console.log('home page reference: ' + JSON.stringify(this.onbordingListPageRef));
            this[NavigationMixin.Navigate](this.onbordingListPageRef);
        }
        
    }
    
    

    navigateToHomePage() {
        
    }
}