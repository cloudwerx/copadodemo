/**
* @name         Onboarding_EmployeeList
* @author       Girish Baviskar
* @date         21-1-2021
* @description  This component shows list of employeers who are currently going through onboarding process.
**/
import { LightningElement, track, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import CloudwerxLogoOnboarding from '@salesforce/resourceUrl/CloudwerxLogoOnboarding';
import { CurrentPageReference } from 'lightning/navigation';
import getCurrentOnboardingEmployees from '@salesforce/apex/Onboarding_EmployeeListController.getCurrentOnboardingEmployees';
const columns = ['Name', 'Work Location', 'Date Of Joining'];
export default class Onboarding_EmployeeList extends NavigationMixin(LightningElement)  {
    @track data = [];
    requestingContactId = new URL(window.location.href).searchParams.get('recordId'); //contact who is requesting list of onboaridng employees
    @track onboardingPageRef;   
    @track showSpinner = true; 
    @track cloudwerxLogoURL = CloudwerxLogoOnboarding; // static resource 
    @track showTable = true;

    @wire(CurrentPageReference)
    pageRef;
    connectedCallback(){
        
        
        //console.log('inside connected callback() of employee list');
        //console.log('current page ref: ' + JSON.stringify(this.pageRef) ); 
        //gets all the employees list who are going through onboarding process.
        getCurrentOnboardingEmployees({requestingContactId : this.requestingContactId
        }).then(res =>{
            this.showSpinner = false;
            if(res != null) {
                this.data = res;
                this.showTable = true;
                //console.log('Result : ' + res);
            } else {
                this.showTable = false;
            }
            
        }).catch(err => {
            //console.log(err);
        });


    }
    hrefClick() {
        //console.log('inside hrefClick');
    }

    backClickHandler() {
        window.history.back();
    }

    logoutClickHandler() {
        //console.log('inside logoutclickHandler');
        this.loginPageRef = {
            type: 'standard__namedPage',
            attributes: {
                pageName: 'home'
            },
            state : {
            }
        };
        //console.log('home page reference: ' + JSON.stringify(this.onbordingListPageRef));
        this[NavigationMixin.Navigate](this.loginPageRef);
    }

    onClickNameHandler(event) {
        //console.log('inside onClickNameHandler');
        var contactId = event.target.dataset.id;
        //console.log( 'contact id is'+ contactId);
        this.onboardingPageRef = {
            type: 'comm__namedPage',
            attributes: {
                name: 'Onboarding__c'
            },
            state : {
                recordId : contactId,
                Authenticated : 'true',
                userType : 'Employer'
            }
        
        };
        this[NavigationMixin.Navigate](this.onboardingPageRef);
    } 

    logoutClickHandler() {
         //console.log('inside logoutclickHandler'); 
        this.loginPageRef = {
            type: 'standard__namedPage',
            attributes: {
                pageName: 'home'
            },
            state : {
            }
        };
        //console.log('home page reference: ' + JSON.stringify(this.onbordingListPageRef));
        this[NavigationMixin.Navigate](this.loginPageRef);
    }
}