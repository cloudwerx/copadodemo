<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Create/Verify Contact is Cloudwerx Sales</label>
    <protected>false</protected>
    <values>
        <field>AssignedToId__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description_of_Task__c</field>
        <value xsi:type="xsd:string">Create &amp; Verify contact in Salesforce org</value>
    </values>
    <values>
        <field>Detail_Button__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Employee_Role__c</field>
        <value xsi:type="xsd:string">Pune Common</value>
    </values>
    <values>
        <field>Employer_Comments__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Help_Doc_ID__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Help_Doc_URL__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>IsRequired__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Is_Downloadable__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Is_Information_Form_required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Is_Slack_msg_Required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Is_Upload_Required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Office_Location__c</field>
        <value xsi:type="xsd:string">Pune</value>
    </values>
    <values>
        <field>Subject_of_Task__c</field>
        <value xsi:type="xsd:string">Create/Verify Contact is Cloudwerx Salesforce org</value>
    </values>
    <values>
        <field>Task_Sequence_Number__c</field>
        <value xsi:type="xsd:double">7.0</value>
    </values>
    <values>
        <field>Type_of_Action__c</field>
        <value xsi:type="xsd:string">Upload file</value>
    </values>
    <values>
        <field>Type_of_Document__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type_of_Task__c</field>
        <value xsi:type="xsd:string">Employer</value>
    </values>
</CustomMetadata>
