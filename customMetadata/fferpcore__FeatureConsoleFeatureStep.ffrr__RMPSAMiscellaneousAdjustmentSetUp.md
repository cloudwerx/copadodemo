<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Configure RM for use with PSA Misc. Adj.</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Configuration__c</field>
        <value xsi:type="xsd:string">{
                &quot;pse__Miscellaneous_Adjustment__c.ffrrtemplate__c&quot;: {
                    &quot;Custom Field Operation&quot;: {
                        &quot;enable&quot;: &quot;Add&quot;,
                        &quot;disable&quot;: &quot;Remove&quot;
                    },
                    &quot;Custom Field Type&quot;: &quot;Lookup&quot;,
                    &quot;Custom Field Label&quot;: &quot;Actuals Template&quot;,
                    &quot;Custom Field Reference To&quot;: &quot;ffrr__Template__c&quot;,
                    &quot;Custom Field Relationship Name&quot;: &quot;MiscellaneousAdjustment&quot;
                },
                &quot;ffrr__RevenueRecognitionTransactionLine__c.ffrrMiscellaneousAdjustment__c&quot;: {
                    &quot;Custom Field Operation&quot;: {
                        &quot;enable&quot;: &quot;Add&quot;,
                        &quot;disable&quot;: &quot;Remove&quot;
                    },
                    &quot;Custom Field Type&quot;: &quot;Lookup&quot;,
                    &quot;Custom Field Label&quot;: &quot;Miscellaneous Adjustment&quot;,
                    &quot;Custom Field Reference To&quot;: &quot;pse__Miscellaneous_Adjustment__c&quot;,
                    &quot;Custom Field Relationship Name&quot;: &quot;RevenueRecognitionTransactionLines&quot;
                },
                &quot;ffrr__RevenueForecastTransactionLine__c.ffrrMiscellaneousAdjustment__c&quot;: {
                    &quot;Custom Field Operation&quot;: {
                        &quot;enable&quot;: &quot;Add&quot;,
                        &quot;disable&quot;: &quot;Remove&quot;
                    },
                    &quot;Custom Field Type&quot;: &quot;Lookup&quot;,
                    &quot;Custom Field Label&quot;: &quot;Miscellaneous Adjustment&quot;,
                    &quot;Custom Field Reference To&quot;: &quot;pse__Miscellaneous_Adjustment__c&quot;,
                    &quot;Custom Field Relationship Name&quot;: &quot;RevenueForecastTransactionLines&quot;
                },
                &quot;pse__Miscellaneous_Adjustment__c.ffrrPercentComplete__c&quot;: {
                    &quot;Custom Field Operation&quot;: {
                        &quot;enable&quot;: &quot;Add&quot;,
                        &quot;disable&quot;: &quot;Remove&quot;
                    },
                    &quot;Custom Field Type&quot;: &quot;Percent&quot;,
                    &quot;Custom Field Precision&quot;: &quot;8&quot;,
                    &quot;Custom Field Scale&quot;: &quot;2&quot;,
                    &quot;Custom Field Label&quot;: &quot;% Complete&quot;
                },
                &quot;pse__Miscellaneous_Adjustment__c.ffrrStartDate__c&quot;: {
                    &quot;Custom Field Operation&quot;: {
                        &quot;enable&quot;: &quot;Add&quot;,
                        &quot;disable&quot;: &quot;Remove&quot;
                    },
                    &quot;Custom Field Type&quot;: &quot;Date&quot;,
                    &quot;Custom Field Label&quot;: &quot;Start Date&quot;
                },
                &quot;pse__Miscellaneous_Adjustment__c.ffrrIncludeInRevenueRecognition__c&quot;: {
                    &quot;Custom Field Operation&quot;: {
                        &quot;enable&quot;: &quot;Add&quot;,
                        &quot;disable&quot;: &quot;Remove&quot;
                    },
                    &quot;Custom Field Type&quot;: &quot;Checkbox&quot;,
                    &quot;Custom Field Label&quot;: &quot;Include In Revenue Recognition&quot;,
                    &quot;Custom Field Default Value&quot;: &quot;true&quot;
                }
            }</value>
    </values>
    <values>
        <field>fferpcore__DisableRevertAction__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>fferpcore__Feature__c</field>
        <value xsi:type="xsd:string">ffrr__RMPSASmartStartStep</value>
    </values>
    <values>
        <field>fferpcore__IsOptional__c</field>
        <value xsi:type="xsd:string">Optional</value>
    </values>
    <values>
        <field>fferpcore__StepDescription__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>fferpcore__StepNumber__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
    <values>
        <field>fferpcore__StepType__c</field>
        <value xsi:type="xsd:string">Custom Field</value>
    </values>
</CustomMetadata>
