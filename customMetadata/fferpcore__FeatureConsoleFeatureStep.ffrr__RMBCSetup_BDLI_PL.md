<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Update Billing Document Line Page Layout</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Configuration__c</field>
        <value xsi:type="xsd:string">{
        &quot;name&quot;: &quot;fferpcore__BillingDocumentLineItem__c-ffbc__Billing Document Line Item Layout - Lightning&quot;,
        &quot;sections&quot;: {
          &quot;Revenue Management&quot;: {
            &quot;operation&quot;: &quot;Add&quot;,
            &quot;anchor&quot;: &quot;System Information&quot;,
            &quot;detail&quot;: &quot;true&quot;,
            &quot;edit&quot;: &quot;true&quot;,
            &quot;style&quot;: &quot;TwoColumnsTopToBottom&quot;
            }
          },
        &quot;fields&quot;: {
          &quot;ffrrIncludeInRevRec__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;Revenue Management__0&quot;,
            &quot;behaviour&quot;: &quot;Readonly&quot;
          },
          &quot;ffrrtemplate__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrrIncludeInRevRec__c&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrrRecognizedToDate__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;Revenue Management__1&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrrRevenueRecognitionComplete__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrrRecognizedToDate__c&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          }
        }
      }</value>
    </values>
    <values>
        <field>fferpcore__DisableRevertAction__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>fferpcore__Feature__c</field>
        <value xsi:type="xsd:string">ffrr__RMBCSetUp</value>
    </values>
    <values>
        <field>fferpcore__IsOptional__c</field>
        <value xsi:type="xsd:string">Optional</value>
    </values>
    <values>
        <field>fferpcore__StepDescription__c</field>
        <value xsi:type="xsd:string">ffrr__RMBCLayoutBillingDocumentLineItemFeatureStepDescription</value>
    </values>
    <values>
        <field>fferpcore__StepNumber__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
    <values>
        <field>fferpcore__StepType__c</field>
        <value xsi:type="xsd:string">Page Layout</value>
    </values>
</CustomMetadata>
