<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>slack pune settings</label>
    <protected>false</protected>
    <values>
        <field>Bot_Named_Credentials__c</field>
        <value xsi:type="xsd:string">Slack_Pune_Bot_Credential</value>
    </values>
    <values>
        <field>Channels__c</field>
        <value xsi:type="xsd:string">CH1G9SW9Y,CMRSHGLBE,CKN8VBBBJ</value>
    </values>
    <values>
        <field>Employers_Channel__c</field>
        <value xsi:type="xsd:string">D01LVASFAKA</value>
    </values>
    <values>
        <field>End_Of_Day_Time_Employee__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>End_Of_Day_Time_Employer__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>Introductory_Message_Channel__c</field>
        <value xsi:type="xsd:string">CH1G9SW9Y</value>
    </values>
    <values>
        <field>Notify_Employee__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Notify_Employer__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Office_Location__c</field>
        <value xsi:type="xsd:string">Pune Office</value>
    </values>
    <values>
        <field>Reminder_Message_To_Employee__c</field>
        <value xsi:type="xsd:string">Hi firstName,
Your onboarding tasks are still pending. Please update the tasks as soon as possible.</value>
    </values>
    <values>
        <field>Reminder_Message_To_Employer__c</field>
        <value xsi:type="xsd:string">Hi @channel,
The onboarding tasks for Employee Name: firstName lastName are still pending. Please complete those as soon as possible.</value>
    </values>
    <values>
        <field>Slack_Workspace_URI__c</field>
        <value xsi:type="xsd:string">cloudwerxspace.slack.com</value>
    </values>
    <values>
        <field>Task_Complete_Message_To_Employee__c</field>
        <value xsi:type="xsd:string">Hi firstName, your Onboarding steps from cloudwerx are completed :tada:.</value>
    </values>
    <values>
        <field>Task_Complete_Message_To_Employer__c</field>
        <value xsi:type="xsd:string">Hi @channel, I&apos;ve completed all the onboarding steps :tada:.</value>
    </values>
    <values>
        <field>User_Named_Credentials__c</field>
        <value xsi:type="xsd:string">Slack_Pune_User_Credential</value>
    </values>
</CustomMetadata>
