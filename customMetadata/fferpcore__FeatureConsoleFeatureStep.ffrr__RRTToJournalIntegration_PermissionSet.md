<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Create Permission Set for RRT to JNL Int</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Configuration__c</field>
        <value xsi:type="xsd:string">{
				&quot;RRTToJournalIntegrationPermission&quot;:
				{
					&quot;Write Permission Set Label&quot;: &quot;RRT to Journal Integration&quot;,
					&quot;Write Permission Set Description&quot;: &quot;Permission set for creating Accounting Journals from Revenue Recognition Transactions via the RRT to Journal Integrations.&quot;,
					&quot;objects&quot;: {
							&quot;ffrr__RevenueRecognitionTransaction__c&quot;: {
									&quot;enable&quot;: &quot;RU&quot;,
									&quot;disable&quot;: &quot;&quot;
							},
							&quot;ffrr__RevenueRecognitionTransactionLine__c&quot;: {
									&quot;enable&quot;: &quot;RU&quot;,
									&quot;disable&quot;: &quot;&quot;
							},
							&quot;c2g__CodaJournal__c&quot;: {
									&quot;enable&quot;: &quot;CRU&quot;,
									&quot;disable&quot;: &quot;&quot;
							},
							&quot;c2g__CodaJournalLineItem__c&quot;: {
									&quot;enable&quot;: &quot;CRU&quot;,
									&quot;disable&quot;: &quot;&quot;
							}
					},
					&quot;fields&quot;: {
							&quot;ffrr__RevenueRecognitionTransaction__c.link_FFA_id__c&quot;: {
								&quot;enable&quot;: &quot;R&quot;,
								&quot;disable&quot;: &quot;&quot;
							},
							&quot;ffrr__RevenueRecognitionTransaction__c.link_FFA_state__c&quot;: {
								&quot;enable&quot;: &quot;R&quot;,
								&quot;disable&quot;: &quot;&quot;
							},
							&quot;ffrr__RevenueRecognitionTransaction__c.link_FFA_errors__c&quot;: {
								&quot;enable&quot;: &quot;R&quot;,
								&quot;disable&quot;: &quot;&quot;
							},
							&quot;ffrr__RevenueRecognitionTransaction__c.JournalStatus__c&quot;: {
								&quot;enable&quot;: &quot;R&quot;,
								&quot;disable&quot;: &quot;&quot;
							},
							&quot;ffrr__RevenueRecognitionTransaction__c.JournalDate__c&quot;: {
								&quot;enable&quot;: &quot;R&quot;,
								&quot;disable&quot;: &quot;&quot;
							},
							&quot;ffrr__RevenueRecognitionTransactionLine__c.link_FFA_id__c&quot;: {
								&quot;enable&quot;: &quot;R&quot;,
								&quot;disable&quot;: &quot;&quot;
							},
							&quot;ffrr__RevenueRecognitionTransactionLine__c.link_FFA_state__c&quot;: {
								&quot;enable&quot;: &quot;R&quot;,
								&quot;disable&quot;: &quot;&quot;
							},
							&quot;ffrr__RevenueRecognitionTransactionLine__c.link_FFA_errors__c&quot;: {
								&quot;enable&quot;: &quot;R&quot;,
								&quot;disable&quot;: &quot;&quot;
							},
							&quot;c2g__CodaJournal__c.link_FFRR_id__c&quot;: {
								&quot;enable&quot;: &quot;R&quot;,
								&quot;disable&quot;: &quot;&quot;
							},
								 &quot;c2g__CodaJournal__c.link_FFRR_state__c&quot;: {
								&quot;enable&quot;: &quot;R&quot;,
								&quot;disable&quot;: &quot;&quot;
							},
							&quot;c2g__CodaJournal__c.link_FFRR_errors__c&quot;: {
								&quot;enable&quot;: &quot;R&quot;,
								&quot;disable&quot;: &quot;&quot;
							},
							&quot;c2g__CodaJournalLineItem__c.link_FFRR_id__c&quot;: {
								&quot;enable&quot;: &quot;R&quot;,
								&quot;disable&quot;: &quot;&quot;
								},
							&quot;c2g__CodaJournalLineItem__c.link_FFRR_state__c&quot;: {
								&quot;enable&quot;: &quot;R&quot;,
								&quot;disable&quot;: &quot;&quot;
							},
							&quot;c2g__CodaJournalLineItem__c.link_FFRR_errors__c&quot;: {
								&quot;enable&quot;: &quot;R&quot;,
								&quot;disable&quot;: &quot;&quot;
							}
					},
					&quot;pages&quot;: {
							&quot;ffrr__RRTCreateJournals&quot;: {
									&quot;enable&quot;: &quot;true&quot;,
									&quot;disable&quot;: &quot;false&quot;
							}
						}
				}
			}</value>
    </values>
    <values>
        <field>fferpcore__DisableRevertAction__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>fferpcore__Feature__c</field>
        <value xsi:type="xsd:string">ffrr__RRTToJournalIntegration</value>
    </values>
    <values>
        <field>fferpcore__IsOptional__c</field>
        <value xsi:type="xsd:string">Optional</value>
    </values>
    <values>
        <field>fferpcore__StepDescription__c</field>
        <value xsi:type="xsd:string">ffrr__RRTToJournalIntegrationPermissionSetDescription</value>
    </values>
    <values>
        <field>fferpcore__StepNumber__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
    <values>
        <field>fferpcore__StepType__c</field>
        <value xsi:type="xsd:string">Write Permission Set</value>
    </values>
</CustomMetadata>
