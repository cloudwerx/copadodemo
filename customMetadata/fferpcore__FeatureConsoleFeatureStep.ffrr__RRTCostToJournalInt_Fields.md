<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Add Fields for RRT-JNL Int Cost</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Configuration__c</field>
        <value xsi:type="xsd:string">{
			&quot;ffrr__RevenueRecognitionTransaction__c.link_CostJournal_id__c&quot; : {
				&quot;Custom Field Operation&quot; : {
					&quot;enable&quot; : &quot;Add&quot;
				},
				&quot;Custom Field Type&quot; : &quot;Lookup&quot;,
				&quot;Custom Field Label&quot; : &quot;Cost Journal&quot;,
				&quot;Custom Field Reference To&quot; : &quot;c2g__CodaJournal__c&quot;,
				&quot;Custom Field Relationship Name&quot; : &quot;RevRecTransactionsForCost&quot;
			},
			&quot;ffrr__RevenueRecognitionTransaction__c.link_CostJournal_state__c&quot; : {
				&quot;Custom Field Operation&quot; : {
					&quot;enable&quot; : &quot;Add&quot;
				},
				&quot;Custom Field Type&quot;: &quot;Text&quot;,
				&quot;Custom Field Label&quot;: &quot;Cost Journal Creation State&quot;,
				&quot;Custom Field Description&quot;: &quot;Indicates whether the journal was created successfully by the RRT to Journal Integration for Cost.&quot;,
				&quot;Custom Field Inline Help Text&quot;: &quot;Indicates whether the journal was created successfully by the RRT to Journal Integration for Cost.&quot;,
				&quot;Custom Field Length&quot;: &quot;255&quot;
			},
			&quot;ffrr__RevenueRecognitionTransaction__c.link_CostJournal_errors__c&quot; : {
				&quot;Custom Field Operation&quot; : {
					&quot;enable&quot; : &quot;Add&quot;
				},
				&quot;Custom Field Type&quot;: &quot;TextArea&quot;,
				&quot;Custom Field Label&quot;: &quot;Cost Journal Creation Error&quot;,
				&quot;Custom Field Description&quot;: &quot;Errors that occurred when creating the journal by the RRT to Journal Integration for Cost.&quot;,
				&quot;Custom Field Inline Help Text&quot;: &quot;Errors that occurred when creating the journal by the RRT to Journal Integration for Cost.&quot;
			},
			&quot;ffrr__RevenueRecognitionTransactionLine__c.link_CostJournal_id__c&quot; : {
				&quot;Custom Field Operation&quot; : {
					&quot;enable&quot; : &quot;Add&quot;
				},
				&quot;Custom Field Type&quot; : &quot;Lookup&quot;,
				&quot;Custom Field Label&quot; : &quot;Cost Journal Line&quot;,
				&quot;Custom Field Reference To&quot; : &quot;c2g__CodaJournalLineItem__c&quot;,
				&quot;Custom Field Relationship Name&quot; : &quot;RevRecTransactionLinesForCost&quot;
			},
			&quot;ffrr__RevenueRecognitionTransactionLine__c.link_CostJournal_state__c&quot; : {
				&quot;Custom Field Operation&quot; : {
					&quot;enable&quot; : &quot;Add&quot;
				},
				&quot;Custom Field Type&quot;: &quot;Text&quot;,
				&quot;Custom Field Label&quot;: &quot;Cost Journal Line Creation State&quot;,
				&quot;Custom Field Description&quot;: &quot;Indicates whether the journal line was created successfully by the RRT to Journal Integration for Cost.&quot;,
				&quot;Custom Field Inline Help Text&quot;: &quot;Indicates whether the journal line was created successfully by the RRT to Journal Integration for Cost.&quot;,
				&quot;Custom Field Length&quot;: &quot;255&quot;
			},
			&quot;ffrr__RevenueRecognitionTransactionLine__c.link_CostJournal_errors__c&quot; : {
				&quot;Custom Field Operation&quot; : {
					&quot;enable&quot; : &quot;Add&quot;
				},
				&quot;Custom Field Type&quot;: &quot;TextArea&quot;,
				&quot;Custom Field Label&quot;: &quot;Cost Journal Line Creation Error&quot;,
				&quot;Custom Field Description&quot;: &quot;Errors that occurred when creating the journal line by the RRT to Journal Integration for Cost.&quot;,
				&quot;Custom Field Inline Help Text&quot;: &quot;Errors that occurred when creating the journal line by the RRT to Journal Integration for Cost.&quot;
			}
		}</value>
    </values>
    <values>
        <field>fferpcore__DisableRevertAction__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>fferpcore__Feature__c</field>
        <value xsi:type="xsd:string">ffrr__RRTCostToJournalInt</value>
    </values>
    <values>
        <field>fferpcore__IsOptional__c</field>
        <value xsi:type="xsd:string">Required</value>
    </values>
    <values>
        <field>fferpcore__StepDescription__c</field>
        <value xsi:type="xsd:string">ffrr__RRTCostToJournalIntFieldFeatureStepDescription</value>
    </values>
    <values>
        <field>fferpcore__StepNumber__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>fferpcore__StepType__c</field>
        <value xsi:type="xsd:string">Custom Field</value>
    </values>
</CustomMetadata>
