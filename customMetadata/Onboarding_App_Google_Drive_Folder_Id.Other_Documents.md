<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Other Documents</label>
    <protected>false</protected>
    <values>
        <field>Folder_Id__c</field>
        <value xsi:type="xsd:string">17prAJ4KKfUIC-vRz9VzCxTRAjYU_ieWq</value>
    </values>
    <values>
        <field>Folder_Name__c</field>
        <value xsi:type="xsd:string">Other Documents</value>
    </values>
</CustomMetadata>
