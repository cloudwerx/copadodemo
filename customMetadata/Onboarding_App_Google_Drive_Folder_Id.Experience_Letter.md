<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Experience Letter</label>
    <protected>false</protected>
    <values>
        <field>Folder_Id__c</field>
        <value xsi:type="xsd:string">1x4pr6TsYzj1U11n28Ay59XeKZycDBD7V</value>
    </values>
    <values>
        <field>Folder_Name__c</field>
        <value xsi:type="xsd:string">Experience Letter</value>
    </values>
</CustomMetadata>
