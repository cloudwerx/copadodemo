<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>2</label>
    <protected>false</protected>
    <values>
        <field>Payment_Method__c</field>
        <value xsi:type="xsd:string">International Cards/AMEX/Corporate Cards/Diners</value>
    </values>
    <values>
        <field>Transaction_Fee__c</field>
        <value xsi:type="xsd:double">3.36</value>
    </values>
</CustomMetadata>
