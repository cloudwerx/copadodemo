<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SP18 RC Revenue Alloc Section anchor</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__FeatureStep__c</field>
        <value xsi:type="xsd:string">ffrr__SP18_RC_PL</value>
    </values>
    <values>
        <field>fferpcore__ParameterName__c</field>
        <value xsi:type="xsd:string">Revenue Allocation</value>
    </values>
    <values>
        <field>fferpcore__ParameterType__c</field>
        <value xsi:type="xsd:string">Page Layout Section Anchor Section</value>
    </values>
    <values>
        <field>fferpcore__ParameterValueOnDisable__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>fferpcore__ParameterValueOnEnable__c</field>
        <value xsi:type="xsd:string">Information</value>
    </values>
</CustomMetadata>
