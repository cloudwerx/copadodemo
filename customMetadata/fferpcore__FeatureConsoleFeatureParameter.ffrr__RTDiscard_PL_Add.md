<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RTDiscard Add Button to Layout</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__FeatureStep__c</field>
        <value xsi:type="xsd:string">ffrr__RTDiscard_PL</value>
    </values>
    <values>
        <field>fferpcore__ParameterName__c</field>
        <value xsi:type="xsd:string">ffrr__DetailDiscard</value>
    </values>
    <values>
        <field>fferpcore__ParameterType__c</field>
        <value xsi:type="xsd:string">Page Layout Custom Button Operation</value>
    </values>
    <values>
        <field>fferpcore__ParameterValueOnDisable__c</field>
        <value xsi:type="xsd:string">Remove</value>
    </values>
    <values>
        <field>fferpcore__ParameterValueOnEnable__c</field>
        <value xsi:type="xsd:string">Add</value>
    </values>
</CustomMetadata>
