<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Revenue Cloud Connector</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__EnabledByDefault__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>fferpcore__EnforceStepOrder__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>fferpcore__FeatureDescription__c</field>
        <value xsi:type="xsd:string">ffrr__RevenueCloudSetUpFeatureDescription</value>
    </values>
    <values>
        <field>fferpcore__HelpPageParams__c</field>
        <value xsi:type="xsd:string">topicid=1047&amp;prod=330&amp;version=2021.1</value>
    </values>
    <values>
        <field>fferpcore__HelpPage__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>fferpcore__ToggleAttributeWhenLicensed__c</field>
        <value xsi:type="xsd:string">On and off</value>
    </values>
    <values>
        <field>fferpcore__ToggleAttribute__c</field>
        <value xsi:type="xsd:string">On only</value>
    </values>
</CustomMetadata>
