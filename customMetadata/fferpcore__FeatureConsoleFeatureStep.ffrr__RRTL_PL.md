<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Update RRTL Page Layout</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Configuration__c</field>
        <value xsi:type="xsd:string">{
                &quot;name&quot;: &quot;ffrr__RevenueRecognitionTransactionLine__c-ffrr__Revenue Recognition Transaction Line Layout&quot;,
                &quot;sections&quot;: {
                    &quot;Currencies&quot;: {
                        &quot;operation&quot;: &quot;Add&quot;,
                        &quot;anchor&quot;: &quot;Record Information&quot;,
                        &quot;detail&quot;: &quot;true&quot;,
                        &quot;edit&quot;: &quot;true&quot;,
                        &quot;style&quot;: &quot;TwoColumnsTopToBottom&quot;
                    }
                },
                &quot;fields&quot;: {
                    &quot;ffrr__AmountAmortizedHome__c&quot;: {
                        &quot;operation&quot;: {
                            &quot;enable&quot;: &quot;Add&quot;,
                            &quot;disable&quot;: &quot;Remove&quot;
                        },
                        &quot;anchor&quot;: &quot;ffrr__AmountAmortized__c&quot;,
                        &quot;behaviour&quot;: &quot;Readonly&quot;
                    },
                    &quot;ffrr__AmountAmortizedDual__c&quot;: {
                        &quot;operation&quot;: {
                            &quot;enable&quot;: &quot;Add&quot;,
                            &quot;disable&quot;: &quot;Remove&quot;
                        },
                        &quot;anchor&quot;: &quot;ffrr__AmountAmortizedHome__c&quot;,
                        &quot;behaviour&quot;: &quot;Readonly&quot;
                    },
                    &quot;ffrr__AmountRecognizedHome__c&quot;: {
                        &quot;operation&quot;: {
                            &quot;enable&quot;: &quot;Add&quot;,
                            &quot;disable&quot;: &quot;Remove&quot;
                        },
                        &quot;anchor&quot;: &quot;ffrr__AmountRecognized__c&quot;,
                        &quot;behaviour&quot;: &quot;Readonly&quot;
                    },
                    &quot;ffrr__AmountRecognizedDual__c&quot;: {
                        &quot;operation&quot;: {
                            &quot;enable&quot;: &quot;Add&quot;,
                            &quot;disable&quot;: &quot;Remove&quot;
                        },
                        &quot;anchor&quot;: &quot;ffrr__AmountRecognizedHome__c&quot;,
                        &quot;behaviour&quot;: &quot;Readonly&quot;
                    },
                    &quot;ffrr__Currency__c&quot;: {
                        &quot;operation&quot;: {
                            &quot;enable&quot;: &quot;Add&quot;,
                            &quot;disable&quot;: &quot;Remove&quot;
                        },
                        &quot;anchor&quot;: &quot;Currencies__0&quot;,
                        &quot;behaviour&quot;: &quot;Readonly&quot;
                    },
                    &quot;ffrr__DualCurrency__c&quot;: {
                        &quot;operation&quot;: {
                            &quot;enable&quot;: &quot;Add&quot;,
                            &quot;disable&quot;: &quot;Remove&quot;
                        },
                        &quot;anchor&quot;: &quot;ffrr__Currency__c&quot;,
                        &quot;behaviour&quot;: &quot;Readonly&quot;
                    },
                    &quot;ffrr__HomeCurrency__c&quot;: {
                        &quot;operation&quot;: {
                            &quot;enable&quot;: &quot;Add&quot;,
                            &quot;disable&quot;: &quot;Remove&quot;
                        },
                        &quot;anchor&quot;: &quot;ffrr__DualCurrency__c&quot;,
                        &quot;behaviour&quot;: &quot;Readonly&quot;
                    },
                    &quot;ffrr__DocumentCurrencyRate__c&quot;: {
                        &quot;operation&quot;: {
                            &quot;enable&quot;: &quot;Add&quot;,
                            &quot;disable&quot;: &quot;Remove&quot;
                        },
                        &quot;anchor&quot;: &quot;Currencies__1&quot;,
                        &quot;behaviour&quot;: &quot;Readonly&quot;
                    },
                    &quot;ffrr__DualCurrencyRate__c&quot;: {
                        &quot;operation&quot;: {
                            &quot;enable&quot;: &quot;Add&quot;,
                            &quot;disable&quot;: &quot;Remove&quot;
                        },
                        &quot;anchor&quot;: &quot;ffrr__DocumentCurrencyRate__c&quot;,
                        &quot;behaviour&quot;: &quot;Readonly&quot;
                    }
                }
            }</value>
    </values>
    <values>
        <field>fferpcore__DisableRevertAction__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>fferpcore__Feature__c</field>
        <value xsi:type="xsd:string">ffrr__RTCurrencies</value>
    </values>
    <values>
        <field>fferpcore__IsOptional__c</field>
        <value xsi:type="xsd:string">Optional</value>
    </values>
    <values>
        <field>fferpcore__StepDescription__c</field>
        <value xsi:type="xsd:string">ffrr__RRTLLayoutFeatureStepDescription</value>
    </values>
    <values>
        <field>fferpcore__StepNumber__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>fferpcore__StepType__c</field>
        <value xsi:type="xsd:string">Page Layout</value>
    </values>
</CustomMetadata>
