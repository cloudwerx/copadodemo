<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Configure RFTL - Internal Use Only</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Configuration__c</field>
        <value xsi:type="xsd:string">{
                &quot;ffrr__RevenueForecastTransactionLine__c.ffrrCurrencyValue__c&quot;: {
                    &quot;Custom Field Operation&quot;: {
                        &quot;enable&quot;: &quot;Add&quot;,
                        &quot;disable&quot;: &quot;Remove&quot;
                    },
                    &quot;Custom Field Type&quot;: &quot;Currency&quot;,
                    &quot;Custom Field Precision&quot;: &quot;18&quot;,
                    &quot;Custom Field Scale&quot;: &quot;2&quot;,
                    &quot;Custom Field Label&quot;: &quot;Value&quot;,
                    &quot;Custom Field Formula&quot;: &quot;ffrr__Value__c&quot;
                }
            }</value>
    </values>
    <values>
        <field>fferpcore__DisableRevertAction__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>fferpcore__Feature__c</field>
        <value xsi:type="xsd:string">ffrr__RMPSASmartStartStep</value>
    </values>
    <values>
        <field>fferpcore__IsOptional__c</field>
        <value xsi:type="xsd:string">Optional</value>
    </values>
    <values>
        <field>fferpcore__StepDescription__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>fferpcore__StepNumber__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
    <values>
        <field>fferpcore__StepType__c</field>
        <value xsi:type="xsd:string">Custom Field</value>
    </values>
</CustomMetadata>
