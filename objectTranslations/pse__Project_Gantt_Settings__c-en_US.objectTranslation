<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <help><!-- When a project is locked for editing, this setting determines whether users (who have permission to edit tasks on the Project Task Gantt) are given the option to break the lock and force the other user into read-only mode. --></help>
        <label><!-- Always Allow Breaking Locks --></label>
        <name>pse__Break_Current_Locks__c</name>
    </fields>
    <fields>
        <help><!-- Defines the API name of the field set, on the project task object, that contains the custom columns to show on Project Task Gantt. When the setting is blank, the Gantt Editable Custom Columns field set is used. --></help>
        <label><!-- Columns Field Set --></label>
        <name>pse__Columns_Field_Set__c</name>
    </fields>
    <fields>
        <help><!-- If selected, records in Project Task Gantt are not loaded in a buffer.  The default is deselected. This setting should only be changed after consulting FinancialForce.com Customer Support. --></help>
        <label><!-- Disable Buffered Rendering --></label>
        <name>pse__Disable_Buffered_Rendering__c</name>
    </fields>
    <fields>
        <help><!-- If selected the resolve dependency button is deactivated when the project task dependencies are synchronized. If deselected, the button remains active when project task dependencies do not need to be synchronized. The default value is deselected. --></help>
        <label><!-- Disable Resolve Dependency Btn on Sync --></label>
        <name>pse__Disable_Resolve_Dependency_Btn_On_Sync__c</name>
    </fields>
    <fields>
        <help><!-- Determines the number of rows to render above the current view in Project Task Gantt. The default is 20. This setting should only be changed after consulting FinancialForce.com Customer Support. --></help>
        <label><!-- Leading Buffer Zone --></label>
        <name>pse__Leading_Buffer_Zone__c</name>
    </fields>
    <fields>
        <help><!-- API name of a field set used to display a custom tooltip for Milestones. If defined, this setting replaces the default Milestone tooltip. --></help>
        <label><!-- Milestone Tooltip Field Set --></label>
        <name>pse__Milestone_Tooltip__c</name>
    </fields>
    <fields>
        <help><!-- API name of a field set used to display a custom tooltip for Project Task Assignments. If defined, this setting replaces the default Project Task Assignment tooltip. --></help>
        <label><!-- PTA Tooltip Field Set --></label>
        <name>pse__PTA_Tooltip__c</name>
    </fields>
    <fields>
        <help><!-- Determines levels of Gantt edit access for the Project Manager. Values are &quot;Full&quot; (default) - can edit created projects, &quot;Read&quot;- has only read-only access, or &quot;None&quot; - access determined by Permission Controls. --></help>
        <label><!-- Project Manager Access --></label>
        <name>pse__Project_Manager_Access__c</name>
    </fields>
    <fields>
        <help><!-- API name of a field set used to display a custom tooltip for Projects. If defined, this setting replaces the default Project tooltip. --></help>
        <label><!-- Project Tooltip Field Set --></label>
        <name>pse__Project_Tooltip__c</name>
    </fields>
    <fields>
        <help><!-- The API name of a field set on a project task containing fields that are not editable in Project Task Gantt. --></help>
        <label><!-- Read Only Field Set --></label>
        <name>pse__Read_Only_Field_Set__c</name>
    </fields>
    <fields>
        <help><!-- If selected, customizations to width, order, and visibility of columns in the Project Task Gantt will be remembered per user. --></help>
        <label><!-- Save Column Preferences --></label>
        <name>pse__Save_Column_Prefs__c</name>
    </fields>
    <fields>
        <help><!-- If selected, the Create Resource Demand button is shown on the Project Task Gantt toolbar. Use to staff the project plan through the Create Resource Demand button from Project Task Assignments page. --></help>
        <label><!-- Show &quot;Create Resource Demand&quot; button --></label>
        <name>pse__Show_Generate_RRs__c</name>
    </fields>
    <fields>
        <help><!-- Determines if the hours complete percentage shows in the task bar display in the Schedule pane. This setting does not control the display of the %Hrs button in the Schedule pane. --></help>
        <label><!-- Show Hours Completion % in Task Bar --></label>
        <name>pse__Show_Hours_Completion_Percent_Bar__c</name>
    </fields>
    <fields>
        <help><!-- Determines if the points complete percentage shows in the task bar display in the Schedule pane. This setting is selected by default. This setting does not control the display of the %Pts button in the Schedule pane. --></help>
        <label><!-- Show Points Completion % in Task Bar --></label>
        <name>pse__Show_Points_Completion_Percent_Bar__c</name>
    </fields>
    <fields>
        <help><!-- API name of a field set used to display a custom tooltip for Project Tasks. If defined, this setting replaces the default Project Task tooltip. --></help>
        <label><!-- Task Tooltip Field Set --></label>
        <name>pse__Task_Tooltip__c</name>
    </fields>
    <fields>
        <help><!-- Determines the number of rows to render below the current view in Project Task Gantt. The default is 10. This setting should only be changed after consulting FinancialForce.com Customer Support. --></help>
        <label><!-- Trailing Buffer Zone --></label>
        <name>pse__Trailing_Buffer_Zone__c</name>
    </fields>
</CustomObjectTranslation>
