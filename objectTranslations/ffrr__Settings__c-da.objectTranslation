<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <article>Definite</article>
        <plural>false</plural>
        <value>Indstillinger</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>false</plural>
        <value>Indstillinger</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>false</plural>
        <value>Indstillinger</value>
    </caseValues>
    <caseValues>
        <article>Definite</article>
        <plural>true</plural>
        <value>Indstillinger</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>true</plural>
        <value>Indstillinger</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>true</plural>
        <value>Indstillinger</value>
    </caseValues>
    <fields>
        <help>API-navn på feltet på kildeobjektet, der indeholder oplysninger, der kan vises om en post. Ofte vil dette være den almindelige Salesforce-konto, men det kan være enhver tekst, picklist, opslag, nummer, autonummer eller formelfelt.</help>
        <label><!-- Display Information --></label>
        <name>ffrr__AccountName__c</name>
    </fields>
    <fields>
        <help>API-navn på feltet på kildeobjektet, der angiver, om det er aktivt. Hvis du vil inkludere poster i indtægtsstyring, når et felt har en bestemt værdi, skal du angive feltnavnet i Active Field, værdien i Active Value og indstille Include Active Value to True.</help>
        <label><!-- Active Field --></label>
        <name>ffrr__ActiveField__c</name>
    </fields>
    <fields>
        <help>Værdi på det aktive felt, der angiver, om kildeoptegnelsen er aktiv.</help>
        <label><!-- Active Value --></label>
        <name>ffrr__ActiveValue__c</name>
    </fields>
    <fields>
        <help>API-navnet på opslagsfeltet (til kildeobjektet for denne indstilling), som din administrator har tilføjet til Actuals vs Forecast Line-objektet. Dette felt er obligatorisk, når muligheden for at bruge Actual vs Forecast Feature er aktiveret.</help>
        <label><!-- Actual vs Forecast Line Lookup --></label>
        <name>ffrr__ActualVsForecastRelationship__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Nummer eller Valuta på kildeobjektet, der gemmer det beløb, der afskrives til dato. Hvis befolkningen er populær, opdateres værdien på kildeoptegnelsen automatisk hver gang en indtægtsgenkendelsestransaktion er indgået.</help>
        <label><!-- Amortized To Date Value --></label>
        <name>ffrr__AmortizedToDateValue__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, opslaget eller picklisten, der repræsenterer rapporteringsværdien 1. Dette kan enten være et felt på kildeobjektet, eller ved hjælp af opslagsrelationer kan du henvise til et felt på et objekt, der er knyttet sammen med en sti på op til fem opslag.</help>
        <label><!-- Analysis Item 1 --></label>
        <name>ffrr__AnalysisItem1__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, opslag eller picklist, der repræsenterer rapporteringsværdi 2. Dette kan enten være et felt på kildeobjektet, eller ved hjælp af opslagsrelationer kan du henvise til et felt på et objekt, der er knyttet sammen med en sti på op til fem opslag.</help>
        <label><!-- Analysis Item 2 --></label>
        <name>ffrr__AnalysisItem2__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, opslaget eller picklisten, der repræsenterer rapporteringsværdi 3. Dette kan enten være et felt på kildeobjektet, eller ved hjælp af opslagsrelationer kan du henvise til et felt på et objekt, der er knyttet sammen med en sti på op til fem opslag.</help>
        <label><!-- Analysis Item 3 --></label>
        <name>ffrr__AnalysisItem3__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, opslaget eller picklisten, der repræsenterer rapporteringsværdi 4. Dette kan enten være et felt på kildeobjektet, eller ved hjælp af opslagsrelationer kan du henvise til et felt på et objekt, der er knyttet sammen med en sti på op til fem opslag.</help>
        <label><!-- Analysis Item 4 --></label>
        <name>ffrr__AnalysisItem4__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, opslaget eller picklisten på kildeobjektet, der repræsenterer den balance, GLA, som indtægtsjournalerne er bogført til.</help>
        <label><!-- Balance Sheet GLA --></label>
        <name>ffrr__BalanceSheetAccount__c</name>
    </fields>
    <fields>
        <label><!-- Billed To Date (Reserved) --></label>
        <name>ffrr__BilledToDate__c</name>
    </fields>
    <fields>
        <label><!-- Chain ID --></label>
        <name>ffrr__ChainID__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, opslaget eller picklisten, der repræsenterer virksomheden. Dette kan enten være et felt på kildeobjektet, eller ved hjælp af opslagrelationer kan du henvise til et felt på et objekt, der er knyttet sammen med en sti på op til fem opslag.</help>
        <label><!-- Company --></label>
        <name>ffrr__Company__c</name>
    </fields>
    <fields>
        <help>API-navn på feltet på kildeobjektet, der angiver dets færdiggørelsesstatus. Hvis der oprettes en skabelon til leverbar type til denne indstilling, vil indtægter blive genkendt, når kildeoptegnelsen er afsluttet.</help>
        <label><!-- Completed Field --></label>
        <name>ffrr__CompletedField__c</name>
    </fields>
    <fields>
        <help>Værdi på det aktive felt, der angiver, om kildeoptegnelsen er afsluttet. Hvis der oprettes en skabelon til leverbar type til denne indstilling, vil indtægter blive genkendt, når kildeoptegnelsen er afsluttet.</help>
        <label><!-- Completed Value --></label>
        <name>ffrr__CompletedValue__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, opslaget eller picklisten på kildeobjektet, der repræsenterer den balance, GLA, som omkostningsdagbøgerne er bogført til.</help>
        <label><!-- Balance Sheet GLA (Cost) --></label>
        <name>ffrr__CostBalanceSheetAccount__c</name>
    </fields>
    <fields>
        <help>API navn på tekstfeltet, opslag eller valgliste på kildeobjektet, der repræsenterer den specifikke del af dit kort over konti, hvor dine ledelseskonti er underanalyseret.</help>
        <label><!-- Cost Center --></label>
        <name>ffrr__CostCenter__c</name>
    </fields>
    <fields>
        <help>API navn på tekstfeltet, opslag eller valgliste på kildeobjektet, der repræsenterer den specifikke del af dit kort over konti, hvor dine ledelseskonti er underanalyseret.</help>
        <label><!-- Cost Center (Cost) --></label>
        <name>ffrr__CostCostCenter__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, opslaget eller picklisten på kildeobjektet, der repræsenterer resultatopgørelsen GLA, hvortil omkostningsblade er bogført.</help>
        <label><!-- Income Statement GLA (Cost) --></label>
        <name>ffrr__CostIncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>API navn på feltet på kildeobjektet, der repræsenterer den hastighed, hvormed omkostningsenheder opkræves.</help>
        <label><!-- Cost Rate --></label>
        <name>ffrr__CostRate__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Nummer på kildeobjektet, der repræsenterer det samlede antal opladede omkostninger. Omkostningsenhedernes værdi kan være det samlede antal timer eller dage arbejdet på et projekt for eksempel.</help>
        <label><!-- Total Cost Units --></label>
        <name>ffrr__CostTotalUnits__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Tekst, Opslag eller Picklist på kildeobjektet, der repræsenterer valutaen for kildeposten.</help>
        <label><!-- Document Currency --></label>
        <name>ffrr__Currency__c</name>
    </fields>
    <fields>
        <help>Feltmappedefinitionen Populært på Performance Obligation Line Items som standard, når de forbinder dem med et Source Record og disse indstillinger.</help>
        <label><!-- Default Field Mapping Definition --></label>
        <name>ffrr__DefaultFieldMappingDefinition__c</name>
        <relationshipLabel><!-- Settings --></relationshipLabel>
    </fields>
    <fields>
        <help>API-navn på feltet Number på kildeobjektet, der gemmer den hidtidige omsætning i dobbelt valuta.</help>
        <label><!-- FOR FUTURE USE Deferred Rev (Dual) --></label>
        <name>ffrr__DeferredRevenueToDateDualCurrency__c</name>
    </fields>
    <fields>
        <help>API-navn på feltet Nummer på kildeobjektet, der gemmer den hidtidige omsætning, i hjemvaluta.</help>
        <label><!-- FOR FUTURE USE Deferred Rev (Home) --></label>
        <name>ffrr__DeferredRevenueToDateHomeCurrency__c</name>
    </fields>
    <fields>
        <help>API-navn på feltet Number på kildeobjektet, der gemmer den hidtidige omsætning i rapporteringsvaluta.</help>
        <label><!-- FOR FUTURE USE Deferred Rev (Reporting) --></label>
        <name>ffrr__DeferredRevenueToDateReportingCurrency__c</name>
    </fields>
    <fields>
        <help>API-navn på feltet Number på kildeobjektet, der gemmer den hidtidige omsætning i dokumentvaluta.</help>
        <label><!-- FOR FUTURE USE Deferred Rev --></label>
        <name>ffrr__DeferredRevenueToDate__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Tekst eller Lang tekstområde på kildeobjektet, der indeholder en beskrivelse.</help>
        <label><!-- Description --></label>
        <name>ffrr__Description__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Dobbelt eller Valuta på kildeobjektet, der repræsenterer dokumentet til boligvalutakurs.</help>
        <label><!-- Document Rate --></label>
        <name>ffrr__DocumentCurrencyRate__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Dobbelt eller Valuta på kildeobjektet, der repræsenterer regnskabsvirksomhedens hjemsted for dobbelt valutakurs.</help>
        <label><!-- Dual Rate --></label>
        <name>ffrr__DualCurrencyRate__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Tekst, Opslag eller Picklist på kildeobjektet, der repræsenterer regnskabsvirksomhedens dobbelte valuta. Dobbelt valuta er typisk en virksomhedsvaluta, der anvendes til rapporteringsformål.</help>
        <label><!-- Dual Currency --></label>
        <name>ffrr__DualCurrency__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Date eller DateTime på kildeobjektet, der repræsenterer dets slutdato.</help>
        <label><!-- End Date/Deliverable Date --></label>
        <name>ffrr__EndDate__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Filter 1 --></label>
        <name>ffrr__Filter1__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Filter 2 --></label>
        <name>ffrr__Filter2__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Filter 3 --></label>
        <name>ffrr__Filter3__c</name>
    </fields>
    <fields>
        <help>Vælg for at angive, at den tilsvarende værdi er en fast kode, ikke et API-navn på det relaterede kildeobjekt. Disse afkrydsningsfelter kan indstilles uafhængigt af hinanden.</help>
        <label><!-- Fixed Balance Sheet --></label>
        <name>ffrr__FixedBalanceSheetAccountCode__c</name>
    </fields>
    <fields>
        <help>Vælg for at angive, at den tilsvarende værdi er en fast kode, ikke et API-navn på det relaterede kildeobjekt. Disse afkrydsningsfelter kan indstilles uafhængigt af hinanden.</help>
        <label><!-- Fixed Balance Sheet (Cost) --></label>
        <name>ffrr__FixedCostBalanceSheetAccountCode__c</name>
    </fields>
    <fields>
        <help>Vælg for at angive, at den tilsvarende værdi er en fast kode, ikke et API-navn på det relaterede kildeobjekt. Disse afkrydsningsfelter kan indstilles uafhængigt af hinanden.</help>
        <label><!-- Fixed Cost Center --></label>
        <name>ffrr__FixedCostCenterCode__c</name>
    </fields>
    <fields>
        <help>Vælg for at angive, at den tilsvarende værdi er en fast kode, ikke et API-navn på det relaterede kildeobjekt. Disse afkrydsningsfelter kan indstilles uafhængigt af hinanden.</help>
        <label><!-- Fixed Cost Center (Cost) --></label>
        <name>ffrr__FixedCostCostCenterCode__c</name>
    </fields>
    <fields>
        <help>Vælg for at angive, at den tilsvarende værdi er en fast kode, ikke et API-navn på det relaterede kildeobjekt. Disse afkrydsningsfelter kan indstilles uafhængigt af hinanden.</help>
        <label><!-- Fixed Income Statement (Cost) --></label>
        <name>ffrr__FixedCostIncomeStatementAccountCode__c</name>
    </fields>
    <fields>
        <help>Vælg for at angive, at den tilsvarende værdi er en fast kode, ikke et API-navn på det relaterede kildeobjekt. Disse afkrydsningsfelter kan indstilles uafhængigt af hinanden.</help>
        <label><!-- Fixed Income Statement --></label>
        <name>ffrr__FixedIncomeStatementAccountCode__c</name>
    </fields>
    <fields>
        <help>Vælg for at angive, at den tilsvarende værdi er en fast kode, ikke et API-navn på det relaterede kildeobjekt. Disse afkrydsningsfelter kan indstilles uafhængigt af hinanden.</help>
        <label><!-- FOR FUTURE USE Fixed True-up (Dual) --></label>
        <name>ffrr__FixedTrueUpDualAccountCode__c</name>
    </fields>
    <fields>
        <help>Vælg for at angive, at den tilsvarende værdi er en fast kode, ikke et API-navn på det relaterede kildeobjekt. Disse afkrydsningsfelter kan indstilles uafhængigt af hinanden.</help>
        <label><!-- FOR FUTURE USE Fixed True-up (Home) --></label>
        <name>ffrr__FixedTrueUpHomeAccountCode__c</name>
    </fields>
    <fields>
        <help>API-navnet på opslagsfeltet (til kildeobjektet på dette indstillingsniveau), som din administrator har tilføjet til indtægtsoversigten Transaktionsobjekt. Dette skal være befolket, hvis indstillingstypen er Prognose, og indstillingsniveauet er Primær.</help>
        <label><!-- Revenue Forecast Transaction Lookup --></label>
        <name>ffrr__ForecastHeaderPrimaryRelationship__c</name>
    </fields>
    <fields>
        <help>API-navnet på opslagsfeltet (til kildeobjektet på dette indstillingsniveau), som din administrator har tilføjet til indtægtsoversigten Transaktionslinjeobjekt. Dette skal være befolket, hvis indstillingstypen er Prognose.</help>
        <label><!-- Revenue Forecast Transaction Line Lookup --></label>
        <name>ffrr__ForecastTransactionLineRelationship__c</name>
    </fields>
    <fields>
        <label><!-- Group Name --></label>
        <name>ffrr__GroupName__c</name>
    </fields>
    <fields>
        <label><!-- Group --></label>
        <name>ffrr__Group__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, opslaget eller picklisten på kildeobjektet, der repræsenterer den specifikke komponent, du gerne vil gruppere efter, til genkend alle processen.</help>
        <label><!-- Grouped By --></label>
        <name>ffrr__GroupedBy__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Tekst, opslag eller valgliste på kildeobjektet, der repræsenterer regnskabsvirksomhedens hjemland. Hjem valuta er selskabets vigtigste arbejde, og rapportering, valuta.</help>
        <label><!-- Home Currency --></label>
        <name>ffrr__HomeCurrency__c</name>
    </fields>
    <fields>
        <help>Inkluder eller ekskluder kildeoptegnelser med værdien defineret i feltet Aktivt værdi.</help>
        <label><!-- Include Active Value --></label>
        <name>ffrr__IncludeActiveValue__c</name>
        <picklistValues>
            <masterLabel>Exclude</masterLabel>
            <translation>Udelukke</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Include</masterLabel>
            <translation>Omfatte</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Inkluder eller ekskluder kildeoptegnelser med værdien defineret i feltet Afsluttet værdi.</help>
        <label><!-- Include Completed Value --></label>
        <name>ffrr__IncludeCompletedValue__c</name>
        <picklistValues>
            <masterLabel>Exclude</masterLabel>
            <translation>Udelukke</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Include</masterLabel>
            <translation>Omfatte</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, opslaget eller picklisten på kildeobjektet, der repræsenterer resultatopgørelsen GLA, som indtægtsskemaerne er bogført til.</help>
        <label><!-- Income Statement GLA --></label>
        <name>ffrr__IncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>API navn på objektet, der fungerer som datakilde på dette niveau. Dette kan ikke ændre sig, når indstillingen er brugt på en skabelon. For ikke-primære indstillinger skal objektet være i et hoveddetaljer eller opslagsrelation med objektet i dets forælderindstilling.</help>
        <label><!-- Object --></label>
        <name>ffrr__Object__c</name>
    </fields>
    <fields>
        <help>API-navnet på opslagsfeltet (til kildeobjektet for denne indstilling), som din administrator har tilføjet til objektobjektet Obligationslinjepost.</help>
        <label><!-- POLI Source Lookup --></label>
        <name>ffrr__POLISourceField__c</name>
    </fields>
    <fields>
        <help>API-navne på opslagsfelter på kildeobjektet beskrevet af denne indstillingspost eller linket til kildeobjektet ved en forholdssti på op til 5 opslag, hvis værdier udfylder indtægtsplanplanopslag. Når du angiver flere felter, skal de være kommaseparerede og i samme rækkefølge som felterne i indtægtsplanlinjens opslag.</help>
        <label><!-- Parent Relationship Paths --></label>
        <name>ffrr__ParentRelationshipPaths__c</name>
    </fields>
    <fields>
        <help>Vælg forældningsindstillingerne (indstillingerne for kildeobjektet et niveau over dette i hierarkiet).</help>
        <label><!-- Parent Settings --></label>
        <name>ffrr__ParentSettings__c</name>
        <relationshipLabel><!-- ParentSettings --></relationshipLabel>
    </fields>
    <fields>
        <help>API navn på feltet Antal eller Procent på kildeobjektet, der repræsenterer, hvordan fuldført kildeposten er i procentvise vilkår.</help>
        <label><!-- % Complete --></label>
        <name>ffrr__PercentageComplete__c</name>
    </fields>
    <fields>
        <help>Indtast API-navnet på opslagningsfeltet til kildeobjektet i overordnet indstilling.</help>
        <label><!-- Parent Lookup --></label>
        <name>ffrr__PrimaryRelationship__c</name>
    </fields>
    <fields>
        <help>API-navn på den søgning, der repræsenterer produktet. Dette kan enten være et opslag på kildeobjektet, eller ved hjælp af opslagforhold kan du henvise til et opslag på et objekt, der er knyttet sammen med en sti på op til fem opslag.</help>
        <label><!-- Product --></label>
        <name>ffrr__Product__c</name>
    </fields>
    <fields>
        <help>API-navn på feltet på kildeobjektet, der repræsenterer den hastighed, hvormed enhederne opkræves.</help>
        <label><!-- Rate --></label>
        <name>ffrr__Rate__c</name>
    </fields>
    <fields>
        <help>API-navn på feltet Number på kildeobjektet, der gemmer den hidtidige indtægt, i dobbelt valuta. Hvis den er udfyldt, opdateres værdien på kildeposten automatisk hver gang der foretages en indtægtsgenkendelsestransaktion.</help>
        <label><!-- FOR FUTURE USE Rec To Date Value (Dual) --></label>
        <name>ffrr__RecognizedToDateValueDual__c</name>
    </fields>
    <fields>
        <help>API-navn for feltet Nummer på kildeobjektet, der gemmer den hidtidige indtægt, der er anerkendt i hjemvaluta. Hvis den er udfyldt, opdateres værdien på kildeposten automatisk hver gang der foretages en indtægtsgenkendelsestransaktion.</help>
        <label><!-- FOR FUTURE USE Rec To Date Value (Home) --></label>
        <name>ffrr__RecognizedToDateValueHome__c</name>
    </fields>
    <fields>
        <help>API-navn på feltet Nummer eller Valuta på kildeobjektet, der lagrer mængden af ​​indtægter, der er anerkendt til dato. Hvis befolkningen er populær, opdateres værdien på kildeoptegnelsen automatisk hver gang en indtægtsgenkendelsestransaktion er indgået.</help>
        <label><!-- Recognized To Date Value --></label>
        <name>ffrr__RecognizedToDateValue__c</name>
    </fields>
    <fields>
        <help>API-navnet på det boolske felt på kildeobjektet, der angiver alle indtægter, er fuldt anerkendt, og alle omkostninger afskrives fuldt ud. Hvis det er befolket, aktiveres kildens postkasse automatisk, når indtægterne er fuldt anerkendte og afskrives fuldt ud.</help>
        <label><!-- Fully Recognized and Amortized --></label>
        <name>ffrr__RevenueRecognitionCompleted__c</name>
    </fields>
    <fields>
        <help>API-navne på opslagsfelter (til det kildeobjekt, der er beskrevet af denne indstillingspost eller relaterede objekter), som din administrator har føjet til Revenue Schedule Line-objektet. Disse felter er udfyldt fra de opslag, der er identificeret i forældrerelationsstierne. Når du angiver flere felter, skal de være kommaseparerede og i samme rækkefølge som felterne i forældrerelationsstierne.</help>
        <label><!-- Revenue Schedule Line Lookups --></label>
        <name>ffrr__RevenueScheduleLineLookups__c</name>
    </fields>
    <fields>
        <help>API-navn på opslagsfeltet (til kildeobjektet på dette indstillingsniveau), som din administrator har tilføjet til indtægtsplanobjektet.</help>
        <label><!-- Revenue Schedule Lookup --></label>
        <name>ffrr__RevenueScheduleSourceLookup__c</name>
    </fields>
    <fields>
        <help>API navn på feltet på kildeobjektet, der lagrer den frittstående salgspris for en præstationsforpligtelses linjepost.</help>
        <label><!-- SSP --></label>
        <name>ffrr__SSP__c</name>
    </fields>
    <fields>
        <help>API-navn på den opslag, der repræsenterer Salesforce-kontoen. Dette kan enten være et opslag på kildeobjektet, eller ved hjælp af opslagforhold kan du henvise til et opslag på et objekt, der er knyttet sammen med en sti på op til fem opslag.</help>
        <label><!-- Account --></label>
        <name>ffrr__SalesforceAccount__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Setting Uniqueness --></label>
        <name>ffrr__SettingUniqueness__c</name>
    </fields>
    <fields>
        <help>Vælg niveauet for denne kildeobjektindstillingsoptegnelse.</help>
        <label><!-- Settings Level --></label>
        <name>ffrr__SettingsLevel__c</name>
        <picklistValues>
            <masterLabel>Level 2</masterLabel>
            <translation>Niveau 2</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Level 3</masterLabel>
            <translation>Niveau 3</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Level 4</masterLabel>
            <translation>Niveau 4</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Primary</masterLabel>
            <translation>Primær</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Den type proces, som denne indstillingsoptegnelse vil blive brugt til: Aktuel, Prognose eller begge dele. Hvis mappingsne er ens for både faktiske og prognoseprocesser, skal du oprette en indstillingsoptegnelse til brug for begge.</help>
        <label><!-- Settings Type --></label>
        <name>ffrr__SettingsType__c</name>
        <picklistValues>
            <masterLabel>Actual</masterLabel>
            <translation>Faktiske</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Forecast</masterLabel>
            <translation>Vejrudsigt</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>API navn på feltet Date eller DateTime på kildeobjektet, der repræsenterer startdatoen.</help>
        <label><!-- Start Date --></label>
        <name>ffrr__StartDate__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Nummer eller Valuta på kildeobjektet, der repræsenterer den samlede pris.</help>
        <label><!-- Total Cost --></label>
        <name>ffrr__TotalCost__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Nummer eller Valuta på kildeobjektet, der repræsenterer den samlede indtægt.</help>
        <label><!-- Total Revenue --></label>
        <name>ffrr__TotalRevenue__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Nummer på kildeobjektet, der repræsenterer det samlede antal opladede enheder. Den samlede enheds værdi kan være antallet af timer eller dage arbejdet på et projekt for eksempel.</help>
        <label><!-- Total Units --></label>
        <name>ffrr__TotalUnits__c</name>
    </fields>
    <fields>
        <help>API-navnet på opslagsfeltet (til kildeobjektet på dette indstillingsniveau), som din administrator har tilføjet til genstanden Revenue Recognition Transaction Line. Dette skal være befolket, hvis indstillingstypen er faktisk.</help>
        <label><!-- Revenue Recognition Txn Line Lookup --></label>
        <name>ffrr__TransactionLineRelationship__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, opslag eller pickliste på det kildeobjekt, der repræsenterer den GLA, der skal bruges til oprettelsesværdier i dobbelt valuta.</help>
        <label><!-- FOR FUTURE USE True-Up GLA (Dual) --></label>
        <name>ffrr__TrueUpDualAccount__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, opslag eller pickliste på det kildeobjekt, der repræsenterer GLA, der skal bruges til oprettelsesværdier i hjemmevaluta.</help>
        <label><!-- FOR FUTURE USE True-Up GLA (Home) --></label>
        <name>ffrr__TrueUpHomeAccount__c</name>
    </fields>
    <fields>
        <help>Anvendes denne indstilling i indtægtsaftaler.</help>
        <label><!-- Use in Revenue Contract --></label>
        <name>ffrr__UseInRevenueContract__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Antal eller Procent på kildeobjektet, der repræsenterer VSOE (Vendor Specific Objective Evidence) procent.</help>
        <label><!-- VSOE % --></label>
        <name>ffrr__VSOEPercent__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Nummer eller Valuta på kildeobjektet, der repræsenterer VSOE (Vendor Specific Objective Evidence) -raten.</help>
        <label><!-- VSOE Rate --></label>
        <name>ffrr__VSOERate__c</name>
    </fields>
    <fields>
        <help>Typen af ​​behandling af denne indstillingspost vil blive brugt til: Omsætning, Omkostning eller begge dele.</help>
        <label><!-- Value Type --></label>
        <name>ffrr__ValueType__c</name>
        <picklistValues>
            <masterLabel>Cost</masterLabel>
            <translation>Koste</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Revenue</masterLabel>
            <translation>Indtægter</translation>
        </picklistValues>
    </fields>
    <gender>Feminine</gender>
    <nameFieldLabel>Indstillinger Navn</nameFieldLabel>
    <webLinks>
        <label><!-- CreateMappings --></label>
        <name>ffrr__CreateMappings</name>
    </webLinks>
    <webLinks>
        <label><!-- CreateMappingsList --></label>
        <name>ffrr__CreateMappingsList</name>
    </webLinks>
    <webLinks>
        <label><!-- DeleteSettings --></label>
        <name>ffrr__DeleteSettings</name>
    </webLinks>
</CustomObjectTranslation>
