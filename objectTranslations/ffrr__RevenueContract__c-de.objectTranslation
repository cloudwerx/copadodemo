<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <caseType>Nominative</caseType>
        <plural>false</plural>
        <value>Erlösvertrag</value>
    </caseValues>
    <caseValues>
        <caseType>Nominative</caseType>
        <plural>true</plural>
        <value>Umsatzverträge</value>
    </caseValues>
    <caseValues>
        <caseType>Accusative</caseType>
        <plural>false</plural>
        <value>Erlösvertrag</value>
    </caseValues>
    <caseValues>
        <caseType>Accusative</caseType>
        <plural>true</plural>
        <value>Umsatzverträge</value>
    </caseValues>
    <caseValues>
        <caseType>Genitive</caseType>
        <plural>false</plural>
        <value>Erlösvertrag</value>
    </caseValues>
    <caseValues>
        <caseType>Genitive</caseType>
        <plural>true</plural>
        <value>Umsatzverträge</value>
    </caseValues>
    <caseValues>
        <caseType>Dative</caseType>
        <plural>false</plural>
        <value>Erlösvertrag</value>
    </caseValues>
    <caseValues>
        <caseType>Dative</caseType>
        <plural>true</plural>
        <value>Umsatzverträge</value>
    </caseValues>
    <fields>
        <help>Informationen zum Revenue Contract. Oft ist dies der Kontoname, aber es können beliebige Informationen sein.</help>
        <label><!-- Display Information --></label>
        <name>ffrr__AccountName__c</name>
    </fields>
    <fields>
        <label><!-- Account --></label>
        <name>ffrr__Account__c</name>
        <relationshipLabel><!-- Revenue Contracts --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Active --></label>
        <name>ffrr__Active__c</name>
    </fields>
    <fields>
        <help>Gibt an, ob die Zuordnung für diesen Erlösvertrag erfolgreich ausgeführt werden kann.</help>
        <label><!-- Ready for Allocation --></label>
        <name>ffrr__Allocatable__c</name>
    </fields>
    <fields>
        <help>Das Verhältnis, das zur Zuordnung von Einnahmen zu verknüpften Leistungsverpflichtungen verwendet wird, bei denen der zugeordnete Umsatz nicht überschrieben wurde.</help>
        <label><!-- Allocation Ratio --></label>
        <name>ffrr__AllocationRatio__c</name>
    </fields>
    <fields>
        <help>Zeigt an, ob der Umsatz für diesen Vertrag vollständig zugewiesen wurde oder neu zugewiesen werden muss.</help>
        <label><!-- Allocation Status --></label>
        <name>ffrr__AllocationStatus__c</name>
    </fields>
    <fields>
        <help>Nachschlagen zum Firmenobjekt.</help>
        <label><!-- Company --></label>
        <name>ffrr__Company__c</name>
        <relationshipLabel><!-- Revenue Contracts --></relationshipLabel>
    </fields>
    <fields>
        <help>Anzahl der Dezimalstellen in der Datensatzwährung.</help>
        <label><!-- Currency Decimal Places --></label>
        <name>ffrr__CurrencyDP__c</name>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>ffrr__Description__c</name>
    </fields>
    <fields>
        <label><!-- End Date --></label>
        <name>ffrr__EndDate__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Leistungsverpflichtungen in Bezug auf Einnahmen mit mindestens einem Null-SSP-Wert.</help>
        <label><!-- Null SSP Count --></label>
        <name>ffrr__NullSSPCount__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der verknüpften Leistungsverpflichtungen, für die ein Wert für den zugewiesenen Umsatz eingegeben wurde.</help>
        <label><!-- PO Allocated Revenue Override Count --></label>
        <name>ffrr__POAllocatedRevenueOverrideCount__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Leistungsverpflichtungen für diesen Vertrag.</help>
        <label><!-- Performance Obligations Count --></label>
        <name>ffrr__PerformanceObligationsCount__c</name>
    </fields>
    <fields>
        <help>Gibt an, ob der Umsatz für diesen Vertrag erfolgreich zugewiesen wurde.</help>
        <label><!-- Revenue Allocated --></label>
        <name>ffrr__RevenueAllocated__c</name>
    </fields>
    <fields>
        <help>Dieses Feld sollte nur ausgefüllt werden, wenn der Umsatz des Vertrags auf Vertragsebene definiert ist und nicht aus den Leistungsverpflichtungen berechnet wird. Wenn dieser Wert ausgefüllt wird, wird er im Feld Umsatz verwendet.</help>
        <label><!-- Revenue Override --></label>
        <name>ffrr__RevenueOverride__c</name>
    </fields>
    <fields>
        <label><!-- Fully Recognized and Amortized --></label>
        <name>ffrr__RevenueRecognitionComplete__c</name>
    </fields>
    <fields>
        <help>Der Wert, der den Leistungsverpflichtungen zugeordnet wird. Verwendet den Gesamtumsatz oder die Umsatzüberschreibung, wenn sie gefüllt ist.</help>
        <label><!-- Revenue --></label>
        <name>ffrr__Revenue__c</name>
    </fields>
    <fields>
        <label><!-- Start Date --></label>
        <name>ffrr__StartDate__c</name>
    </fields>
    <fields>
        <help>Summe der zugewiesenen Einnahmen Überschreibt die Werte aller verknüpften Leistungsverpflichtungen.</help>
        <label><!-- Total Allocated Revenue Override --></label>
        <name>ffrr__TotalAllocatedRevenueOverride__c</name>
    </fields>
    <fields>
        <help>Summe der zugewiesenen Erlöswerte aller verknüpften Leistungsverpflichtungen</help>
        <label><!-- Total Allocated Revenue --></label>
        <name>ffrr__TotalAllocatedRevenue__c</name>
    </fields>
    <fields>
        <help>Summe der Amortized to Date-Werte aller verknüpften Leistungsverpflichtungen.</help>
        <label><!-- Total Amortized To Date --></label>
        <name>ffrr__TotalAmortizedToDate__c</name>
    </fields>
    <fields>
        <help>Summe der Kostenwerte aller verknüpften Leistungsverpflichtungen</help>
        <label><!-- Total Cost --></label>
        <name>ffrr__TotalCost__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der verknüpften Leistungsverpflichtungen, bei denen die Felder &quot;Zuordnungsrate&quot; und &quot;Zugewiesene Einnahmenüberschreibung&quot; null sind.</help>
        <label><!-- Null Allocation Ratio Count --></label>
        <name>ffrr__TotalNullAllocationRatioCount__c</name>
    </fields>
    <fields>
        <help>Summe der Anerkannten Werte aus allen verknüpften Leistungsverpflichtungen.</help>
        <label><!-- Total Recognized To Date --></label>
        <name>ffrr__TotalRecognizedToDate__c</name>
    </fields>
    <fields>
        <help>Summe der Erlöswerte aller verknüpften Leistungsverpflichtungen</help>
        <label><!-- Total Revenue --></label>
        <name>ffrr__TotalRevenue__c</name>
    </fields>
    <fields>
        <help>Summe der SSP-Werte für verknüpfte Leistungsverpflichtungen, bei denen der zugeordnete Umsatz nicht überschrieben wurde.</help>
        <label><!-- Total SSP for Allocation --></label>
        <name>ffrr__TotalSSPForAllocation__c</name>
    </fields>
    <fields>
        <help>Summe der Standalone-Verkaufspreise aus allen verknüpften Leistungsverpflichtungen.</help>
        <label><!-- Total SSP --></label>
        <name>ffrr__TotalSSP__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Leistungsverpflichtungen in Bezug auf Einnahmen, bei denen SSP Null ist.</help>
        <label><!-- Zero SSP Count --></label>
        <name>ffrr__ZeroSSPCount__c</name>
    </fields>
    <fields>
        <label><!-- Template --></label>
        <name>ffrr__ffrrtemplate__c</name>
        <relationshipLabel><!-- Revenue Contracts --></relationshipLabel>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Umsatzvertragsnummer</nameFieldLabel>
    <webLinks>
        <label><!-- AllocateRevenue --></label>
        <name>ffrr__AllocateRevenue</name>
    </webLinks>
    <webLinks>
        <label><!-- ListAllocateRevenue --></label>
        <name>ffrr__ListAllocateRevenue</name>
    </webLinks>
    <webLinks>
        <label><!-- ListUpdatePerformanceObligations --></label>
        <name>ffrr__ListUpdatePerformanceObligations</name>
    </webLinks>
    <webLinks>
        <label><!-- ManageObligations --></label>
        <name>ffrr__ManageObligations</name>
    </webLinks>
    <webLinks>
        <label><!-- UpdatePerformanceObligations --></label>
        <name>ffrr__UpdatePerformanceObligations</name>
    </webLinks>
</CustomObjectTranslation>
