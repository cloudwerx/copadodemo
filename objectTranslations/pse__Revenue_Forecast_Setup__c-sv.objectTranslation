<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <article>Definite</article>
        <plural>false</plural>
        <value>Prognosinställning</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>false</plural>
        <value>Prognosinställning</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>false</plural>
        <value>Prognosinställning</value>
    </caseValues>
    <caseValues>
        <article>Definite</article>
        <plural>true</plural>
        <value>Prognosinställning</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>true</plural>
        <value>Prognosinställning</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>true</plural>
        <value>Prognosinställning</value>
    </caseValues>
    <fields>
        <help>Indikerar inställningsposten för inkomstprognos som används för beräkningar av intäktsprognoser. Du kan bara ha en aktiv postprognos för inställningar.</help>
        <label><!-- Active --></label>
        <name>pse__Active__c</name>
    </fields>
    <fields>
        <help>API-namn för fältuppsättningen på det detaljobjekt för intäktsprognosversion som innehåller projekt-, möjlighet- och milstolpsfält som du vill använda som anpassade filter på sidan Granska prognosversion.</help>
        <label><!-- Custom Filter Field Set --></label>
        <name>pse__Custom_Filter_Field_Set__c</name>
    </fields>
    <fields>
        <help>Om det väljs utesluts möjligheterna från inkomstprognosversionerna.</help>
        <label><!-- Exclude Opportunities for Versions --></label>
        <name>pse__Exclude_Opportunities_For_Versions__c</name>
    </fields>
    <fields>
        <help>Om det väljs utesluts möjligheter från planerade jobb för inkomster.</help>
        <label><!-- Exclude Opportunities --></label>
        <name>pse__Exclude_Opportunities__c</name>
    </fields>
    <fields>
        <help>Om det väljs används inte sannolikheter för möjlighetsintäkter.</help>
        <label><!-- Exclude Probability from Opportunities --></label>
        <name>pse__Exclude_Opportunity_Probabilities__c</name>
    </fields>
    <fields>
        <help>Välj för att utesluta intäkter relaterade till begärda resursbegäranden för projekt och milstolpar.</help>
        <label><!-- Exclude Resource Requests on Project --></label>
        <name>pse__Exclude_Resource_Requests_On_Project__c</name>
    </fields>
    <fields>
        <help>Om det väljs ingår inte längre fakturering av intäktsredovisning från integrationen med Revenue Management i beräkningarna av intäktsprognosen.</help>
        <label><!-- Exclude Revenue Recognition Actuals --></label>
        <name>pse__Exclude_Revenue_Recognition_Actuals__c</name>
    </fields>
    <fields>
        <help>Om det väljs utesluts eventuella icke planerade intäkter på projekt eller milstolpar från inkomstprognosversionerna.</help>
        <label><!-- Exclude Unscheduled Revenue for Versions --></label>
        <name>pse__Exclude_Unscheduled_Revenue_For_Versions__c</name>
    </fields>
    <fields>
        <help>Antal poster som ska behandlas samtidigt, t.ex. tidskort och utgifter.</help>
        <label><!-- Forecast Factor Batch Size --></label>
        <name>pse__Forecast_Factor_Batch_Size__c</name>
    </fields>
    <fields>
        <help>API-namn för fältuppsättningen på Milestone-objektet som styr de svävaruppgifter som visas för en milstolpe i rutan för uppdelning av prognoser. Om du inte anger en fältuppsättning gäller standarduppgifterna för svävar.</help>
        <label><!-- Hover Details Field Set on Milestone --></label>
        <name>pse__Hover_Details_Field_Set_On_Milestone__c</name>
    </fields>
    <fields>
        <help>API-namn för fältuppsättningen på Opportunity-objektet som styr de svävardetaljer som visas för en möjlighet i Prognosfördelningsnätet. Om du inte anger en fältuppsättning gäller standarduppgifterna för svävar.</help>
        <label><!-- Hover Details Field Set on Opportunity --></label>
        <name>pse__Hover_Details_Field_Set_On_Opportunity__c</name>
    </fields>
    <fields>
        <help>API-namn för fältuppsättningen på projektobjektet som styr de svävaruppgifter som visas för ett projekt i rutan för uppdelning av prognoser. Om du inte anger en fältuppsättning gäller standarduppgifterna för svävar.</help>
        <label><!-- Hover Details Field Set on Project --></label>
        <name>pse__Hover_Details_Field_Set_On_Project__c</name>
    </fields>
    <fields>
        <help>Om det väljs används tröskelvärdet för bästa fall för prognoser för intäktsintäkter.</help>
        <label><!-- Include Best Case --></label>
        <name>pse__Include_Best_Case__c</name>
    </fields>
    <fields>
        <help>Om det väljs, tillämpas tröskelvärdena på prognoserna för inkomstintäkter.</help>
        <label><!-- Include Worst Case --></label>
        <name>pse__Include_Worst_Case__c</name>
    </fields>
    <fields>
        <help>Antalet möjligheter och möjlighetsrader som ska behandlas samtidigt.</help>
        <label><!-- Opportunity Batch Size --></label>
        <name>pse__Opportunity_Batch_Size__c</name>
    </fields>
    <fields>
        <help>Minsta procentsannolikhet för en möjlighet att inkluderas i bästa fall.</help>
        <label><!-- Opportunity Best Case Threshold (%) --></label>
        <name>pse__Opportunity_Best_Case_Threshold__c</name>
    </fields>
    <fields>
        <help>Den lägsta procentsatsen för en möjlighet att inkluderas i det förväntade scenariot.</help>
        <label><!-- Opportunity Expected Threshold (%) --></label>
        <name>pse__Opportunity_Expected_Case_Threshold__c</name>
    </fields>
    <fields>
        <help>Den lägsta procentsatsen för en möjlighet att inkluderas i värsta fall.</help>
        <label><!-- Opportunity Worst Case Threshold (%) --></label>
        <name>pse__Opportunity_Worst_Case_Threshold__c</name>
    </fields>
    <fields>
        <help>Antal projekt som ska behandlas samtidigt.</help>
        <label><!-- Project Batch Size --></label>
        <name>pse__Project_Batch_Size__c</name>
    </fields>
    <fields>
        <help>Ange API-namnet på fältet på Opportunity-objektet som du vill använda som förväntat slutdatum för projektet.</help>
        <label><!-- Expected Project End Date Field on Opp --></label>
        <name>pse__Project_End_Date_on_Opportunity__c</name>
    </fields>
    <fields>
        <help>Ange API-namnet på fältet på Opportunity-objektet du vill använda som förväntat projektdatum.</help>
        <label><!-- Expected Project Start Date Field on Opp --></label>
        <name>pse__Project_Start_Date_on_Opportunity__c</name>
    </fields>
    <fields>
        <help>Om valt när du inte använder integrationen mellan PSA och intäktshantering, behåller intäktsprognoser faktiska resultat från föregående prognoskörning under en stängd tidsperiod. Resterande väntande intäkter flyttas till nästa öppna tidsperiod.</help>
        <label><!-- Retain Pending Revenue in Closed Periods --></label>
        <name>pse__Retain_Pending_Revenue_In_Closed_Periods__c</name>
    </fields>
    <fields>
        <help>Välj API-namnet på fältet på projektobjektet du vill använda för att beräkna värdet i fältet% timmar slutfört för erkännande. Vid behov kan du skapa ditt eget fält och lägga till det i picklistan.</help>
        <label><!-- Total Hours Field on Project --></label>
        <name>pse__Total_Hours_Field_on_Project__c</name>
        <picklistValues>
            <masterLabel>pse__Estimated_Hours_at_Completion__c</masterLabel>
            <translation>pse__Estimated_Hours_at_Completion__c</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>pse__Planned_Hours__c</masterLabel>
            <translation>pse__Planned_Hours__c</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Antal versionsdetaljer som ska behandlas samtidigt.</help>
        <label><!-- Version Detail Batch Size --></label>
        <name>pse__Version_Detail_Batch_Size__c</name>
    </fields>
    <fields>
        <help>Prognosperioden täcks av intäktsprognosen.</help>
        <label><!-- Version Forecast Period --></label>
        <name>pse__Version_Forecast_Period__c</name>
        <picklistValues>
            <masterLabel>Current Quarter</masterLabel>
            <translation>Nuvarande kvartal</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Current Year</masterLabel>
            <translation>Nuvarande år</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Current and Next Quarter</masterLabel>
            <translation>Nuvarande och nästa kvartal</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rolling 12 Months</masterLabel>
            <translation>Rullande 12 månader</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rolling 3 Months</masterLabel>
            <translation>Rullande 3 månader</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rolling 6 Months</masterLabel>
            <translation>Rullande 6 månader</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Objektet som projekt och möjligheter grupperas under på sidan Review Forecast Version.</help>
        <label><!-- Object for Version Grouping --></label>
        <name>pse__Version_Grouping_Primary__c</name>
        <picklistValues>
            <masterLabel>Group</masterLabel>
            <translation>Grupp</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Practice</masterLabel>
            <translation>Öva</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Region</masterLabel>
            <translation>Område</translation>
        </picklistValues>
    </fields>
    <gender>Feminine</gender>
    <nameFieldLabel>Inställningsnamn för intäkter</nameFieldLabel>
</CustomObjectTranslation>
