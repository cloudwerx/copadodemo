<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <article>Definite</article>
        <plural>false</plural>
        <value>Team</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>false</plural>
        <value>Team</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>false</plural>
        <value>Team</value>
    </caseValues>
    <caseValues>
        <article>Definite</article>
        <plural>true</plural>
        <value>Lag</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>true</plural>
        <value>Lag</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>true</plural>
        <value>Lag</value>
    </caseValues>
    <fields>
        <help>Når det er sant, sendes timeplanen til alle gruppemedlemmer når du klikker Send-knappen.</help>
        <label><!-- All Team Members --></label>
        <name>pse__All_Team_Members__c</name>
    </fields>
    <fields>
        <help>Når det er sant, sendes en varsling til alle gruppemedlemmer med redigeringstilgang når en gruppeplan byttes.</help>
        <label><!-- Anyone with Edit Access --></label>
        <name>pse__Anyone_With_Edit_Access__c</name>
    </fields>
    <fields>
        <help>Beskrivelsen for laget.</help>
        <label><!-- Description --></label>
        <name>pse__Description__c</name>
    </fields>
    <fields>
        <help>Velg å la gruppemedlemmene bytte skift på tilhørende lagplan. Dette feltet er deaktivert som standard.</help>
        <label><!-- Enable Swapping --></label>
        <name>pse__Enable_Swapping__c</name>
    </fields>
    <fields>
        <help>Når det er sant, sendes et varsel til det nylig tildelte teammedlemmet når en gruppeplan blir byttet.</help>
        <label><!-- Currently Assigned Team Member --></label>
        <name>pse__Now_Assigned__c</name>
    </fields>
    <fields>
        <help>Når det er sant, sendes et varsel til det tidligere tildelte teammedlemmet når en gruppeplan blir byttet.</help>
        <label><!-- Previously Assigned Team Member --></label>
        <name>pse__Previously_Assigned__c</name>
    </fields>
    <fields>
        <help>Prosjektet er knyttet til teamet.</help>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Teams --></relationshipLabel>
    </fields>
    <fields>
        <help>Angir egendefinert e-postmal som skal brukes til å sende lagplaner.</help>
        <label><!-- Custom Template Name (Schedule) --></label>
        <name>pse__Schedule_Custom_Template_Name__c</name>
    </fields>
    <fields>
        <help>Angir e-postadresser for å motta gruppeplanen. Verdien er en kommaseparert liste over e-postadresser.</help>
        <label><!-- Other Email Addresses (Schedule) --></label>
        <name>pse__Schedule_Other_Email_Addresses__c</name>
    </fields>
    <fields>
        <help>Når det er sant, sendes timeplanen til e-postadresser som er angitt i feltet Andre e-postadresser når du klikker Send-knappen.</help>
        <label><!-- Send Email to Others (Schedule) --></label>
        <name>pse__Schedule_Send_Email_To_Others__c</name>
    </fields>
    <fields>
        <help>Når det er sant, bruker timeplanen e-postmalen som er definert i feltet Tilpasset malnavn (Planlegging).</help>
        <label><!-- Use Custom Template (Schedule) --></label>
        <name>pse__Schedule_Use_Custom_Template__c</name>
    </fields>
    <fields>
        <help>Angir egendefinert e-postmal som skal brukes til å sende varslingsvarsler for gruppeplan.</help>
        <label><!-- Custom Template Name (Swap) --></label>
        <name>pse__Swap_Custom_Template_Name__c</name>
    </fields>
    <fields>
        <help>Angir e-postadresser for å motta varsling når en holdtidsplan byttes. Verdien er en kommaseparert liste over e-postadresser.</help>
        <label><!-- Other Email Addresses (Swap) --></label>
        <name>pse__Swap_Other_Email_Addresses__c</name>
    </fields>
    <fields>
        <help>Når det er sant, sendes en varsling til alle e-postadresser som er definert i feltet Send e-post til andre (bytte) når en gruppeplan byttes.</help>
        <label><!-- Send Email to Others (Swap) --></label>
        <name>pse__Swap_Send_Email_To_Others__c</name>
    </fields>
    <fields>
        <help>Når det er sant, bruker swap notification emailen e-postmalen som er definert i feltet Tilpasset malnavn (Bytt).</help>
        <label><!-- Use Custom Template (Swap) --></label>
        <name>pse__Swap_Use_Custom_Template__c</name>
    </fields>
    <fields>
        <help>Resursen som eier laget.</help>
        <label><!-- Team Owner --></label>
        <name>pse__Team_Owner__c</name>
        <relationshipLabel><!-- Team --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Time Zone --></label>
        <name>pse__Time_Zone__c</name>
        <picklistValues>
            <masterLabel>Africa/Algiers</masterLabel>
            <translation>Africa / Algiers</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Africa/Cairo</masterLabel>
            <translation>Afrika / Kairo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Africa/Casablanca</masterLabel>
            <translation>Afrika / Casablanca</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Africa/Johannesburg</masterLabel>
            <translation>Afrika / Johannesburg</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Africa/Nairobi</masterLabel>
            <translation>Africa / Nairobi</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Adak</masterLabel>
            <translation>America / Adak</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Anchorage</masterLabel>
            <translation>America / Anchorage</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Argentina/Buenos_Aires</masterLabel>
            <translation>Amerika / Argentina / Buenos_Aires</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Bogota</masterLabel>
            <translation>America / Bogota</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Caracas</masterLabel>
            <translation>America / Caracas</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Chicago</masterLabel>
            <translation>America / Chicago</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Denver</masterLabel>
            <translation>Amerika / Denver</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/El_Salvador</masterLabel>
            <translation>America / El_Salvador</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Halifax</masterLabel>
            <translation>America / Halifax</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Indiana/Indianapolis</masterLabel>
            <translation>America / Indiana / Indianapolis</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Lima</masterLabel>
            <translation>Amerika / Lima</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Los_Angeles</masterLabel>
            <translation>America / Los_Angeles</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Mazatlan</masterLabel>
            <translation>America / Mazatlan</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Mexico_City</masterLabel>
            <translation>America / Mexico_City</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/New_York</masterLabel>
            <translation>Amerika / New_York</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Panama</masterLabel>
            <translation>America / Panama</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Phoenix</masterLabel>
            <translation>America / Phoenix</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Puerto_Rico</masterLabel>
            <translation>America / Puerto_Rico</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Santiago</masterLabel>
            <translation>America / Santiago</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Sao_Paulo</masterLabel>
            <translation>America / Sao_Paulo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Scoresbysund</masterLabel>
            <translation>America / Scoresbysund</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/St_Johns</masterLabel>
            <translation>America / St_Johns</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Tijuana</masterLabel>
            <translation>America / Tijuana</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Baghdad</masterLabel>
            <translation>Asia / Baghdad</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Baku</masterLabel>
            <translation>Asia / Baku</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Bangkok</masterLabel>
            <translation>Asia / Bangkok</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Beirut</masterLabel>
            <translation>Asia / Beirut</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Colombo</masterLabel>
            <translation>Asia / Colombo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Dhaka</masterLabel>
            <translation>Asia / Dhaka</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Dubai</masterLabel>
            <translation>Asia / Dubai</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Ho_Chi_Minh</masterLabel>
            <translation>Asia / Ho_Chi_Minh</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Hong_Kong</masterLabel>
            <translation>Asia / Hong_Kong</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Jakarta</masterLabel>
            <translation>Asia / Jakarta</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Jerusalem</masterLabel>
            <translation>Asia / Jerusalem</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Kabul</masterLabel>
            <translation>Asia / Kabul</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Kamchatka</masterLabel>
            <translation>Asia / Kamchatka</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Karachi</masterLabel>
            <translation>Asia / Karachi</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Kathmandu</masterLabel>
            <translation>Asia / Kathmandu</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Kolkata</masterLabel>
            <translation>Asia / Kolkata</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Kuala_Lumpur</masterLabel>
            <translation>Asia / Kuala_Lumpur</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Kuwait</masterLabel>
            <translation>Asia / Kuwait</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Manila</masterLabel>
            <translation>Asia / Manila</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Rangoon</masterLabel>
            <translation>Asia / Rangoon</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Riyadh</masterLabel>
            <translation>Asia / Riyadh</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Seoul</masterLabel>
            <translation>Asia / Seoul</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Shanghai</masterLabel>
            <translation>Asia / Shanghai</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Singapore</masterLabel>
            <translation>Asia / Singapore</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Taipei</masterLabel>
            <translation>Asia / Taipei</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Tashkent</masterLabel>
            <translation>Asia / Tashkent</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Tbilisi</masterLabel>
            <translation>Asia / Tbilisi</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Tehran</masterLabel>
            <translation>Asia / Teheran</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Tokyo</masterLabel>
            <translation>Asia / Tokyo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Yekaterinburg</masterLabel>
            <translation>Asia / Jekaterinburg</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Yerevan</masterLabel>
            <translation>Asia / Yerevan</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Atlantic/Azores</masterLabel>
            <translation>Atlanterhavet / Azorene</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Atlantic/Bermuda</masterLabel>
            <translation>Atlantic / Bermuda</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Atlantic/Cape_Verde</masterLabel>
            <translation>Atlantic / Cape_Verde</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Atlantic/South_Georgia</masterLabel>
            <translation>Atlantic / South_Georgia</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Australia/Adelaide</masterLabel>
            <translation>Australia / Adelaide</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Australia/Brisbane</masterLabel>
            <translation>Australia / Brisbane</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Australia/Darwin</masterLabel>
            <translation>Australia / Darwin</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Australia/Lord_Howe</masterLabel>
            <translation>Australia / Lord_Howe</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Australia/Perth</masterLabel>
            <translation>Australia / Perth</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Australia/Sydney</masterLabel>
            <translation>Australia / Sydney</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Amsterdam</masterLabel>
            <translation>Europa / Amsterdam</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Athens</masterLabel>
            <translation>Europa / Athen</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Berlin</masterLabel>
            <translation>Europe / Berlin</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Brussels</masterLabel>
            <translation>Europa / Brussel</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Bucharest</masterLabel>
            <translation>Europa / Bucuresti</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Dublin</masterLabel>
            <translation>Europa / Dublin</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Helsinki</masterLabel>
            <translation>Europa / Helsinki</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Istanbul</masterLabel>
            <translation>Europa / Istanbul</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Lisbon</masterLabel>
            <translation>Europa / Lisboa</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/London</masterLabel>
            <translation>Europe / London</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Minsk</masterLabel>
            <translation>Europa / Minsk</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Moscow</masterLabel>
            <translation>Europa / Moskva</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Paris</masterLabel>
            <translation>Europa / Paris</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Prague</masterLabel>
            <translation>Europa / Praha</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Rome</masterLabel>
            <translation>Europa / Roma</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>GMT</masterLabel>
            <translation>GMT</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Auckland</masterLabel>
            <translation>Pacific / Auckland</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Chatham</masterLabel>
            <translation>Pacific / Chatham</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Enderbury</masterLabel>
            <translation>Pacific / Enderbury</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Fiji</masterLabel>
            <translation>Pacific / Fiji</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Gambier</masterLabel>
            <translation>Stillehavet / Gambier</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Guadalcanal</masterLabel>
            <translation>Pacific / Guadalcanal</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Honolulu</masterLabel>
            <translation>Pacific / Honolulu</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Kiritimati</masterLabel>
            <translation>Pacific / Kiritimati</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Marquesas</masterLabel>
            <translation>Pacific / Marquesas</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Niue</masterLabel>
            <translation>Stillehavet / Niue</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Norfolk</masterLabel>
            <translation>Pacific / Norfolk</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Pago_Pago</masterLabel>
            <translation>Pacific / Pago_Pago</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Pitcairn</masterLabel>
            <translation>Pacific / Pitcairn</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Tongatapu</masterLabel>
            <translation>Pacific / Tongatapu</translation>
        </picklistValues>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Lag navn</nameFieldLabel>
    <validationRules>
        <errorMessage><!-- A project must be active to associate it with a team. --></errorMessage>
        <name>pse__Active_Project</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- One or both of the email address fields do not contain a value. --></errorMessage>
        <name>pse__Other_Email_Address_Fields_are_Blank</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- One or both of the email address fields contains one or more invalid email addresses. --></errorMessage>
        <name>pse__Validate_Other_Email_Address_Fields</name>
    </validationRules>
    <webLinks>
        <label><!-- Manage_Team --></label>
        <name>pse__Manage_Team</name>
    </webLinks>
    <webLinks>
        <label><!-- Open_Team_Scheduler --></label>
        <name>pse__Open_Team_Scheduler</name>
    </webLinks>
</CustomObjectTranslation>
