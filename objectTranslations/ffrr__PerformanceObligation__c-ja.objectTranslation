<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>履行義務</value>
    </caseValues>
    <fields>
        <help>履行義務に関する情報。多くの場合、これはアカウント名になりますが、どのような情報でもかまいません。</help>
        <label><!-- Display Information --></label>
        <name>ffrr__AccountName__c</name>
    </fields>
    <fields>
        <help>ソースレコードがアクティブかどうかを示します。</help>
        <label><!-- Active --></label>
        <name>ffrr__Active__c</name>
    </fields>
    <fields>
        <help>履行義務の累積丸め誤差により発生した収益と収益契約の配分収益との差額による調整。</help>
        <label><!-- Allocated Revenue Rounding Adjustment --></label>
        <name>ffrr__AllocatedRevenueAdjustment__c</name>
    </fields>
    <fields>
        <help>ここに別の値を入力して、この履行義務に割り当てられた計算金額を上書きします。</help>
        <label><!-- Allocated Revenue Override --></label>
        <name>ffrr__AllocatedRevenueOverride__c</name>
    </fields>
    <fields>
        <help>収益配分計算によってこの履行義務に配分された金額。この値は収益認識に使用されます。</help>
        <label><!-- Allocated Revenue --></label>
        <name>ffrr__AllocatedRevenue__c</name>
    </fields>
    <fields>
        <help>収益契約全体で収益を配分するために使用される比率。</help>
        <label><!-- Allocation Ratio --></label>
        <name>ffrr__AllocationRatio__c</name>
    </fields>
    <fields>
        <help>この履行義務の収益が完全に割り当てられているか、または再割り当てが必要かを示します。</help>
        <label><!-- Allocation Status --></label>
        <name>ffrr__AllocationStatus__c</name>
    </fields>
    <fields>
        <label><!-- Amortized to Date --></label>
        <name>ffrr__AmortizedToDate__c</name>
    </fields>
    <fields>
        <label><!-- Balance Sheet GLA --></label>
        <name>ffrr__BalanceSheetAccount__c</name>
    </fields>
    <fields>
        <help>ソースレコードが完成しているかどうかを示します。</help>
        <label><!-- Completed --></label>
        <name>ffrr__Completed__c</name>
    </fields>
    <fields>
        <help>原価パフォーマンス債務管理明細の管理履行義務のフィールドに入力するために使用されます。</help>
        <label><!-- Controlling POLI (Cost) --></label>
        <name>ffrr__ControllingCostPOLI__c</name>
        <relationshipLabel><!-- Controlling POLIs (Cost) --></relationshipLabel>
    </fields>
    <fields>
        <help>収益実績債務管理ライン明細の管理履行義務のフィールドに入力するために使用されます。</help>
        <label><!-- Controlling POLI (Revenue) --></label>
        <name>ffrr__ControllingPOLI__c</name>
        <relationshipLabel><!-- Controlling POLIs (Revenue) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Balance Sheet GLA (Cost) --></label>
        <name>ffrr__CostBalanceSheetAccount__c</name>
    </fields>
    <fields>
        <label><!-- Cost Center --></label>
        <name>ffrr__CostCenter__c</name>
    </fields>
    <fields>
        <label><!-- Cost Center (Cost) --></label>
        <name>ffrr__CostCostCenter__c</name>
    </fields>
    <fields>
        <label><!-- Income Statement GLA (Cost) --></label>
        <name>ffrr__CostIncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>この履行義務の費用</help>
        <label><!-- Cost --></label>
        <name>ffrr__Cost__c</name>
    </fields>
    <fields>
        <help>レコードの通貨の小数点以下の桁数。</help>
        <label><!-- Currency Decimal Places --></label>
        <name>ffrr__CurrencyDP__c</name>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>ffrr__Description__c</name>
    </fields>
    <fields>
        <label><!-- End Date --></label>
        <name>ffrr__EndDate__c</name>
    </fields>
    <fields>
        <help>この履行義務が収益に関連することを示します。</help>
        <label><!-- Has Revenue --></label>
        <name>ffrr__HasRevenue__c</name>
    </fields>
    <fields>
        <label><!-- Income Statement GLA --></label>
        <name>ffrr__IncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>SSP値がnullの収益に関連する履行義務の明細の数。</help>
        <label><!-- Null SSP Count --></label>
        <name>ffrr__NullSSPCount__c</name>
    </fields>
    <fields>
        <label><!-- % Complete --></label>
        <name>ffrr__PercentComplete__c</name>
    </fields>
    <fields>
        <help>収入が割り当てられ、ソースレコードがアクティブであるかどうかがチェックされます。チェックマークを付けないと、このレコードを収益認識に含めることができません。</help>
        <label><!-- Ready for Revenue Recognition --></label>
        <name>ffrr__ReadyForRevenueRecognition__c</name>
    </fields>
    <fields>
        <label><!-- Recognized to Date --></label>
        <name>ffrr__RecognizedToDate__c</name>
    </fields>
    <fields>
        <label><!-- Revenue Contract --></label>
        <name>ffrr__RevenueContract__c</name>
        <relationshipLabel><!-- Performance Obligations --></relationshipLabel>
    </fields>
    <fields>
        <help>収益に関連する履行義務明細の数。</help>
        <label><!-- Revenue Count --></label>
        <name>ffrr__RevenueCount__c</name>
    </fields>
    <fields>
        <help>設定すると、この履行義務のすべての収益が完全に認識され、すべての費用が完全に償却されることを示します。</help>
        <label><!-- Fully Recognized and Amortized --></label>
        <name>ffrr__RevenueRecognitionComplete__c</name>
    </fields>
    <fields>
        <help>この履行義務の収益</help>
        <label><!-- Revenue --></label>
        <name>ffrr__Revenue__c</name>
    </fields>
    <fields>
        <help>ここに別の値を入力して、ソースレコードから[独立販売価格]を上書きします。</help>
        <label><!-- SSP Override --></label>
        <name>ffrr__SSPOverride__c</name>
    </fields>
    <fields>
        <help>この履行義務に使用される独立販売価格。 Total SSP、またはSSP Overrideが設定されている場合はSSP Overrideを使用します。</help>
        <label><!-- SSP --></label>
        <name>ffrr__SSP__c</name>
    </fields>
    <fields>
        <label><!-- Start Date --></label>
        <name>ffrr__StartDate__c</name>
    </fields>
    <fields>
        <help>リンクされているすべての履行義務明細からの独立販売価格の合計。</help>
        <label><!-- Total SSP --></label>
        <name>ffrr__TotalSSP__c</name>
    </fields>
    <fields>
        <label><!-- Template --></label>
        <name>ffrr__ffrrTemplate__c</name>
        <relationshipLabel><!-- Performance Obligations --></relationshipLabel>
    </fields>
    <nameFieldLabel>履行義務番号</nameFieldLabel>
    <webLinks>
        <label><!-- AllocateRevenue --></label>
        <name>ffrr__AllocateRevenue</name>
    </webLinks>
    <webLinks>
        <label><!-- Update --></label>
        <name>ffrr__Update</name>
    </webLinks>
</CustomObjectTranslation>
