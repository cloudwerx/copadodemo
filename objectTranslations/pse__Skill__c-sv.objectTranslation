<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <article>Definite</article>
        <plural>false</plural>
        <value>Skicklighet / Certifiering</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>false</plural>
        <value>Skicklighet / Certifiering</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>false</plural>
        <value>Skicklighet / Certifiering</value>
    </caseValues>
    <caseValues>
        <article>Definite</article>
        <plural>true</plural>
        <value>Färdigheter och certifieringar</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>true</plural>
        <value>Färdigheter och certifieringar</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>true</plural>
        <value>Färdigheter och certifieringar</value>
    </caseValues>
    <fields>
        <help>Organisation eller institution som beviljar denna certifiering.</help>
        <label><!-- Certification Source --></label>
        <name>pse__Certification_Source__c</name>
        <picklistValues>
            <masterLabel>AWS</masterLabel>
            <translation><!-- AWS --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Google</masterLabel>
            <translation>Google</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Jira</masterLabel>
            <translation><!-- Jira --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Mulesoft</masterLabel>
            <translation><!-- Mulesoft --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Salesforce</masterLabel>
            <translation><!-- Salesforce --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UiPath</masterLabel>
            <translation><!-- UiPath --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Textbeskrivning av denna färdighet eller certifiering.</help>
        <label><!-- Description --></label>
        <name>pse__Description__c</name>
    </fields>
    <fields>
        <help>ID för att identifiera denna färdighet i ett externt system.</help>
        <label><!-- External ID --></label>
        <name>pse__External_Id__c</name>
    </fields>
    <fields>
        <help>Picklist av certifieringsgrupper.</help>
        <label><!-- Group --></label>
        <name>pse__Group__c</name>
        <picklistValues>
            <masterLabel>Automation</masterLabel>
            <translation><!-- Automation --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Functional Skills</masterLabel>
            <translation><!-- Functional Skills --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Industry Knowledge</masterLabel>
            <translation><!-- Industry Knowledge --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Integration</masterLabel>
            <translation><!-- Integration --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Product Knowledge</masterLabel>
            <translation><!-- Product Knowledge --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Professional Certification</masterLabel>
            <translation><!-- Professional Certification --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Salesforce</masterLabel>
            <translation><!-- Salesforce --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Soft Skills</masterLabel>
            <translation><!-- Soft Skills --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Technical Certification</masterLabel>
            <translation><!-- Technical Certification --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Technical Skills</masterLabel>
            <translation><!-- Technical Skills --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Hierarchy Key Path --></label>
        <name>pse__Hierarchy_Key_Path__c</name>
    </fields>
    <fields>
        <help>Indikerar nivån inom kompetenshierarkin.</help>
        <label><!-- Hierarchy Level --></label>
        <name>pse__Hierarchy_Level__c</name>
    </fields>
    <fields>
        <help>Banfält för auto-genererade förknippade förfader namn på skickligheten i hierarkin.</help>
        <label><!-- Hierarchy Path --></label>
        <name>pse__Hierarchy_Path__c</name>
    </fields>
    <fields>
        <help>Uppslag till föräldern i kompetenshierarkin. Föräldrar måste ha en posttyp av kategori.</help>
        <label><!-- Parent Category --></label>
        <name>pse__Parent_Category__c</name>
        <relationshipLabel><!-- Category Children --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Parent Hierarchy Level --></label>
        <name>pse__Parent_Hierarchy_Level__c</name>
    </fields>
    <fields>
        <label><!-- Parent Record Type --></label>
        <name>pse__Parent_Record_Type__c</name>
    </fields>
    <fields>
        <label><!-- Record Type Name --></label>
        <name>pse__Record_Type_Name__c</name>
    </fields>
    <fields>
        <help>Borttagen. Se Record Type istället.</help>
        <label><!-- Skill Or Certification --></label>
        <name>pse__Skill_Or_Certification__c</name>
        <picklistValues>
            <masterLabel>Certification</masterLabel>
            <translation>Certifiering</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Skill</masterLabel>
            <translation>Skicklighet</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Sats av skicklighetstyper. Används vid gruppering av kompetenskapacitetrapporter.</help>
        <label><!-- Type --></label>
        <name>pse__Type__c</name>
        <picklistValues>
            <masterLabel>TBA</masterLabel>
            <translation><!-- TBA --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Det unika kompetensnamnet.</help>
        <label><!-- Unique Name --></label>
        <name>pse__Unique_Name__c</name>
    </fields>
    <gender>Feminine</gender>
    <nameFieldLabel>Färdighet / Certifieringsnamn</nameFieldLabel>
    <recordTypes>
        <description><!-- A level in a hierarchy. Skills and certifications can belong to a category. --></description>
        <label><!-- Category --></label>
        <name>pse__Category</name>
    </recordTypes>
    <recordTypes>
        <description><!-- A document relating to a status or a level of achievement. --></description>
        <label><!-- Certification --></label>
        <name>pse__Certification</name>
    </recordTypes>
    <recordTypes>
        <description><!-- An ability, proficiency, or expertise, in a subject area. --></description>
        <label><!-- Skill --></label>
        <name>pse__Skill</name>
    </recordTypes>
    <webLinks>
        <label><!-- CascadeDelete --></label>
        <name>pse__CascadeDelete</name>
    </webLinks>
</CustomObjectTranslation>
