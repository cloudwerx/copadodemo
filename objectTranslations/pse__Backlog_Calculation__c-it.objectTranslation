<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Calcolo del backlog</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Calcoli del backlog</value>
    </caseValues>
    <fields>
        <help>Un campo interno utilizzato da PSE - non destinato alla visualizzazione per gli utenti.</help>
        <label><!-- Batch Id --></label>
        <name>pse__Batch_Id__c</name>
    </fields>
    <fields>
        <help>Indica se calcolare il backlog per i progetti contenuti nella regione, nella pratica o nel gruppo.</help>
        <label><!-- Calculate Project Backlog --></label>
        <name>pse__Calculate_Project_Backlog__c</name>
    </fields>
    <fields>
        <help>Aggiorna i record di regione, pratica, gruppo o progetto con i valori direttamente dai record di dettaglio del backlog, oltre a creare record di oggetti Backlog Calculation e Backlog Detail.</help>
        <label><!-- Copy Fields for Current Time Period --></label>
        <name>pse__Copy_Fields_for_Current_Time_Period__c</name>
    </fields>
    <fields>
        <help>La data di fine per i calcoli del backlog. Se non viene specificata una data di fine, verranno elaborati tutti i record dalla data di inizio. Questo è raccomandato quando si calcola il backlog non schedulato.</help>
        <label><!-- End Date --></label>
        <name>pse__End_Date__c</name>
    </fields>
    <fields>
        <label><!-- Error Details --></label>
        <name>pse__Error_Details__c</name>
    </fields>
    <fields>
        <label><!-- Group --></label>
        <name>pse__Group__c</name>
        <relationshipLabel><!-- Backlog Calculations --></relationshipLabel>
    </fields>
    <fields>
        <help>Indica se calcolare il backlog per tutti i sottolivelli della regione selezionata, della pratica o del gruppo. Questo campo viene ignorato se il backlog è calcolato solo per una risorsa.</help>
        <label><!-- Include Sublevels --></label>
        <name>pse__Include_Sublevels__c</name>
    </fields>
    <fields>
        <label><!-- Is Report Master --></label>
        <name>pse__Is_Report_Master__c</name>
    </fields>
    <fields>
        <label><!-- Post Process Batch Id --></label>
        <name>pse__Post_Process_Batch_Id__c</name>
    </fields>
    <fields>
        <label><!-- Practice --></label>
        <name>pse__Practice__c</name>
        <relationshipLabel><!-- Backlog Calculations --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Backlog Calculations --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Region --></label>
        <name>pse__Region__c</name>
        <relationshipLabel><!-- Backlog Calculations --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Resource --></label>
        <name>pse__Resource__c</name>
        <relationshipLabel><!-- Backlog Calculations --></relationshipLabel>
    </fields>
    <fields>
        <help>Sovrascrivi i record di dettagli di backlog esistenti per le stesse regioni, pratiche, gruppi, progetti e periodi di tempo associati. Si consiglia di selezionare questa opzione per evitare l’uso di spazio non necessario.</help>
        <label><!-- Reuse Detail Objects --></label>
        <name>pse__Reuse_Detail_Objects__c</name>
    </fields>
    <fields>
        <help>L&apos;impostazione di questo valore imposterà automaticamente il campo della data di inizio</help>
        <label><!-- Start Calculating From --></label>
        <name>pse__Start_Calculating_From__c</name>
        <picklistValues>
            <masterLabel>Start of next week</masterLabel>
            <translation>Inizio della prossima settimana</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Start of this week</masterLabel>
            <translation>Inizio di questa settimana</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Today</masterLabel>
            <translation>Oggi</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Start Date --></label>
        <name>pse__Start_Date__c</name>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>pse__Status__c</name>
        <picklistValues>
            <masterLabel>Complete</masterLabel>
            <translation>Completare</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Error Occurred</masterLabel>
            <translation>Errore</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>In Progress</masterLabel>
            <translation>In corso</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Not Started</masterLabel>
            <translation>Non iniziato</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Post Processing</masterLabel>
            <translation>Post produzione</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Time Period Types --></label>
        <name>pse__Time_Period_Types__c</name>
        <picklistValues>
            <masterLabel>Month</masterLabel>
            <translation>Mese</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Other</masterLabel>
            <translation>Altro</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Perpetual</masterLabel>
            <translation>Perpetuo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Quarter</masterLabel>
            <translation>Trimestre</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Week</masterLabel>
            <translation>Settimana</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Year</masterLabel>
            <translation>Anno</translation>
        </picklistValues>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Nome di calcolo del backlog</nameFieldLabel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <errorMessage><!-- End date cannot occur before start date --></errorMessage>
        <name>pse__check_end_date</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Only one of Region, Practice, Group, or Project may be specified. --></errorMessage>
        <name>pse__only_one_source_allowed</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- One of Region, Practice, Group, or Project is required --></errorMessage>
        <name>pse__source_required</name>
    </validationRules>
    <webLinks>
        <label><!-- Calculate_Backlog --></label>
        <name>pse__Calculate_Backlog</name>
    </webLinks>
</CustomObjectTranslation>
