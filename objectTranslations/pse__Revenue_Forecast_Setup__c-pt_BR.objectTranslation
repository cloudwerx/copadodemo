<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Configuração da previsão de receita</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Configuração da previsão de receita</value>
    </caseValues>
    <fields>
        <help>Indica o registro de configuração de previsão de receita usado para cálculos de previsão de receita. Você só pode ter um registro de configuração de previsão de receita ativo.</help>
        <label><!-- Active --></label>
        <name>pse__Active__c</name>
    </fields>
    <fields>
        <help>Nome da API do campo definido no objeto Detalhe da versão da previsão de receita que contém os campos do projeto, oportunidade e marco que você deseja usar como filtros personalizados na página Revisar versão da previsão.</help>
        <label><!-- Custom Filter Field Set --></label>
        <name>pse__Custom_Filter_Field_Set__c</name>
    </fields>
    <fields>
        <help>Se selecionado, as oportunidades são excluídas das versões de previsão de receita.</help>
        <label><!-- Exclude Opportunities for Versions --></label>
        <name>pse__Exclude_Opportunities_For_Versions__c</name>
    </fields>
    <fields>
        <help>Se selecionado, as oportunidades são excluídas dos trabalhos agendados de previsão de receita.</help>
        <label><!-- Exclude Opportunities --></label>
        <name>pse__Exclude_Opportunities__c</name>
    </fields>
    <fields>
        <help>Se selecionado, as probabilidades não são aplicadas às previsões de receita da oportunidade.</help>
        <label><!-- Exclude Probability from Opportunities --></label>
        <name>pse__Exclude_Opportunity_Probabilities__c</name>
    </fields>
    <fields>
        <help>Selecione para excluir a receita relacionada a solicitações de recursos retidos em projetos e marcos.</help>
        <label><!-- Exclude Resource Requests on Project --></label>
        <name>pse__Exclude_Resource_Requests_On_Project__c</name>
    </fields>
    <fields>
        <help>Se selecionado, os reais de reconhecimento de receita da integração com o Gerenciamento de receita não são mais incluídos nos cálculos de previsão de receita.</help>
        <label><!-- Exclude Revenue Recognition Actuals --></label>
        <name>pse__Exclude_Revenue_Recognition_Actuals__c</name>
    </fields>
    <fields>
        <help>Se selecionado, qualquer receita não programada em projetos ou marcos é excluída das versões de previsão de receita.</help>
        <label><!-- Exclude Unscheduled Revenue for Versions --></label>
        <name>pse__Exclude_Unscheduled_Revenue_For_Versions__c</name>
    </fields>
    <fields>
        <help>Número de registros a serem processados ao mesmo tempo, como cartões de ponto e despesas.</help>
        <label><!-- Forecast Factor Batch Size --></label>
        <name>pse__Forecast_Factor_Batch_Size__c</name>
    </fields>
    <fields>
        <help>Nome da API do campo configurado no objeto Milestone que controla os detalhes de foco exibidos para um marco na grade Detalhamento da Previsão. Se você não inserir um conjunto de campos, os detalhes de foco padrão serão aplicados.</help>
        <label><!-- Hover Details Field Set on Milestone --></label>
        <name>pse__Hover_Details_Field_Set_On_Milestone__c</name>
    </fields>
    <fields>
        <help>Nome da API do campo definido no objeto Oportunidade que controla os detalhes de foco exibidos para uma oportunidade na grade Detalhamento da Previsão. Se você não inserir um conjunto de campos, os detalhes de foco padrão serão aplicados.</help>
        <label><!-- Hover Details Field Set on Opportunity --></label>
        <name>pse__Hover_Details_Field_Set_On_Opportunity__c</name>
    </fields>
    <fields>
        <help>Nome da API do campo definido no objeto Projeto que controla os detalhes de foco exibidos para um projeto na grade Detalhamento da Previsão. Se você não inserir um conjunto de campos, os detalhes de foco padrão serão aplicados.</help>
        <label><!-- Hover Details Field Set on Project --></label>
        <name>pse__Hover_Details_Field_Set_On_Project__c</name>
    </fields>
    <fields>
        <help>Se selecionado, o limite do melhor caso é aplicado às previsões de receita da oportunidade.</help>
        <label><!-- Include Best Case --></label>
        <name>pse__Include_Best_Case__c</name>
    </fields>
    <fields>
        <help>Se selecionado, o limite do pior caso é aplicado às previsões de receita da oportunidade.</help>
        <label><!-- Include Worst Case --></label>
        <name>pse__Include_Worst_Case__c</name>
    </fields>
    <fields>
        <help>Número de oportunidades e itens de linha de oportunidade a serem processados ao mesmo tempo.</help>
        <label><!-- Opportunity Batch Size --></label>
        <name>pse__Opportunity_Batch_Size__c</name>
    </fields>
    <fields>
        <help>A probabilidade de porcentagem mínima para uma oportunidade ser incluída no melhor cenário.</help>
        <label><!-- Opportunity Best Case Threshold (%) --></label>
        <name>pse__Opportunity_Best_Case_Threshold__c</name>
    </fields>
    <fields>
        <help>A probabilidade de porcentagem mínima para uma oportunidade ser incluída no cenário esperado.</help>
        <label><!-- Opportunity Expected Threshold (%) --></label>
        <name>pse__Opportunity_Expected_Case_Threshold__c</name>
    </fields>
    <fields>
        <help>A probabilidade de porcentagem mínima para uma oportunidade ser incluída no pior cenário.</help>
        <label><!-- Opportunity Worst Case Threshold (%) --></label>
        <name>pse__Opportunity_Worst_Case_Threshold__c</name>
    </fields>
    <fields>
        <help>Número de projetos a serem processados ao mesmo tempo.</help>
        <label><!-- Project Batch Size --></label>
        <name>pse__Project_Batch_Size__c</name>
    </fields>
    <fields>
        <help>Insira o nome da API do campo no objeto Oportunidade que deseja usar como a data de término esperada do projeto.</help>
        <label><!-- Expected Project End Date Field on Opp --></label>
        <name>pse__Project_End_Date_on_Opportunity__c</name>
    </fields>
    <fields>
        <help>Insira o nome da API do campo no objeto Oportunidade que deseja usar como a data de início do projeto esperada.</help>
        <label><!-- Expected Project Start Date Field on Opp --></label>
        <name>pse__Project_Start_Date_on_Opportunity__c</name>
    </fields>
    <fields>
        <help>Se selecionado quando você não está usando a integração entre o PSA e o gerenciamento de receita, a previsão de receita retém os valores reais da execução de previsão anterior para qualquer período fechado. Qualquer receita pendente restante é movida para o próximo período de tempo aberto.</help>
        <label><!-- Retain Pending Revenue in Closed Periods --></label>
        <name>pse__Retain_Pending_Revenue_In_Closed_Periods__c</name>
    </fields>
    <fields>
        <help>Selecione o nome da API do campo no objeto Projeto que deseja usar para calcular o valor no campo% de horas concluídas para reconhecimento. Se necessário, você pode criar seu próprio campo e adicioná-lo à lista de seleção.</help>
        <label><!-- Total Hours Field on Project --></label>
        <name>pse__Total_Hours_Field_on_Project__c</name>
        <picklistValues>
            <masterLabel>pse__Estimated_Hours_at_Completion__c</masterLabel>
            <translation>pse__Estimated_Hours_at_Completion__c</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>pse__Planned_Hours__c</masterLabel>
            <translation>pse__Planned_Hours__c</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Número de registros de detalhes da versão a serem processados ao mesmo tempo.</help>
        <label><!-- Version Detail Batch Size --></label>
        <name>pse__Version_Detail_Batch_Size__c</name>
    </fields>
    <fields>
        <help>O período de previsão coberto pela versão de previsão de receita.</help>
        <label><!-- Version Forecast Period --></label>
        <name>pse__Version_Forecast_Period__c</name>
        <picklistValues>
            <masterLabel>Current Quarter</masterLabel>
            <translation>Trimestre Atual</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Current Year</masterLabel>
            <translation>Ano atual</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Current and Next Quarter</masterLabel>
            <translation>Trimestre atual e próximo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rolling 12 Months</masterLabel>
            <translation>Rolando 12 meses</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rolling 3 Months</masterLabel>
            <translation>Rolando 3 meses</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rolling 6 Months</masterLabel>
            <translation>Rolando 6 meses</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>O objeto sob o qual os projetos e oportunidades são agrupados na página Revisar versão da previsão.</help>
        <label><!-- Object for Version Grouping --></label>
        <name>pse__Version_Grouping_Primary__c</name>
        <picklistValues>
            <masterLabel>Group</masterLabel>
            <translation>Grupo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Practice</masterLabel>
            <translation>Prática</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Region</masterLabel>
            <translation>Região</translation>
        </picklistValues>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Nome da configuração da previsão de rece</nameFieldLabel>
</CustomObjectTranslation>
