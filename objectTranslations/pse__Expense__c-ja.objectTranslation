<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>費用</value>
    </caseValues>
    <fieldSets>
        <label><!-- Expense Columns For Combine Attachment PDF --></label>
        <name>pse__ExpenseColumnsForCombineAttachmentPDF</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Columns For Combine Attachment Page --></label>
        <name>pse__ExpenseColumnsForCombineAttachmentPage</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Header Row Editable Columns --></label>
        <name>pse__ExpenseHeaderRowEditableColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Header Row Read Only Columns --></label>
        <name>pse__ExpenseHeaderRowReadOnlyColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Notes Fields Editable Columns --></label>
        <name>pse__ExpenseNotesFieldsEditableColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Notes Fields Read Only Columns --></label>
        <name>pse__ExpenseNotesFieldsReadOnlyColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Mobile Expenses: Additional Fields in New and Edit Mode --></label>
        <name>pse__Mobile_Expenses_Additional_Fields</name>
    </fieldSets>
    <fields>
        <label><!-- Expense Notification --></label>
        <name>FFX_Expense_Notification__c</name>
    </fields>
    <fields>
        <help>チェックマークを付けると、[Include In Financials]がチェックされていても、管理者は、経費のプロジェクト、リソース、通貨、または日付の編集を含め、経費に「グローバル」な変更を加えることができます。構成要件：実績計算モードを「スケジュール済み」に設定する必要があります。</help>
        <label><!-- Admin Global Edit --></label>
        <name>pse__Admin_Global_Edit__c</name>
    </fields>
    <fields>
        <label><!-- Amount To Bill --></label>
        <name>pse__Amount_To_Bill__c</name>
    </fields>
    <fields>
        <label><!-- Amount To Reimburse --></label>
        <name>pse__Amount_To_Reimburse__c</name>
    </fields>
    <fields>
        <label><!-- Amount --></label>
        <name>pse__Amount__c</name>
    </fields>
    <fields>
        <help>フィールドは、経費に適用される経費率を保持します。</help>
        <label><!-- Applied Expense Rate --></label>
        <name>pse__Applied_Expense_Rate__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <help>このチェックボックスは、経費が承認されたときにチェックする必要があります。これは、その親の経費報告書（ある場合）が承認されているかどうかに基づいて行う必要があります。</help>
        <label><!-- Approved --></label>
        <name>pse__Approved__c</name>
    </fields>
    <fields>
        <label><!-- Approved for Billing --></label>
        <name>pse__Approved_for_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Approved for Vendor Payment --></label>
        <name>pse__Approved_for_Vendor_Payment__c</name>
    </fields>
    <fields>
        <label><!-- Assignment --></label>
        <name>pse__Assignment__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <help>経費の添付ファイルが経費行から経費報告書に移動されたかどうかを示すために構成によって自動的にチェックされます。</help>
        <label><!-- Attachments moved to ER --></label>
        <name>pse__Attachments_moved_to_ER__c</name>
    </fields>
    <fields>
        <help>監査ノートの履歴を保存します。ユーザーがタイムカードUIを使用してプロジェクトまたは割り当てを変更し、監査記録が255文字を超えると、古い監査記録がここに移動されて追加されます。</help>
        <label><!-- Audit Notes History --></label>
        <name>pse__Audit_Notes_History__c</name>
    </fields>
    <fields>
        <label><!-- Audit Notes --></label>
        <name>pse__Audit_Notes__c</name>
    </fields>
    <fields>
        <label><!-- Bill Date --></label>
        <name>pse__Bill_Date__c</name>
    </fields>
    <fields>
        <label><!-- Bill Transaction --></label>
        <name>pse__Bill_Transaction__c</name>
        <relationshipLabel><!-- Expense (Bill Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <help>この計算式は、請求可能な費用については金額（ある場合は非請求可能な発生金額と税金を差し引いたもの）、発生しない通貨では請求できない費用についてはゼロ（ゼロ）に等しくなります。</help>
        <label><!-- Billable Amount --></label>
        <name>pse__Billable_Amount__c</name>
    </fields>
    <fields>
        <help>プロジェクトからのデフォルトで、費用の請求金額を増やすためのプロジェクト通貨での定額料金。計算は、この一定の経費手数料額の前に任意の割合の経費手数料を適用します。</help>
        <label><!-- Billable Fee Flat Amount --></label>
        <name>pse__Billable_Fee_Flat_Amount__c</name>
    </fields>
    <fields>
        <help>プロジェクト通貨と一緒にデフォルトでプロジェクトから請求される費用の請求金額を増やすためのプロジェクト通貨でのフラット料金を示すテキスト式。計算は、この一定の経費手数料額の前に任意の割合の経費手数料を適用します。</help>
        <label><!-- Billable Fee Flat --></label>
        <name>pse__Billable_Fee_Flat__c</name>
    </fields>
    <fields>
        <help>プロジェクトからデフォルトで支払われる費用の請求金額を増やすための手数料の割合。 0％は増加なし（デフォルト）を意味し、100％は倍増を意味します。この計算では、一定の経費手数料の前に、経費の割合*が適用されます。</help>
        <label><!-- Billable Fee Percentage --></label>
        <name>pse__Billable_Fee_Percentage__c</name>
    </fields>
    <fields>
        <label><!-- Billable --></label>
        <name>pse__Billable__c</name>
    </fields>
    <fields>
        <label><!-- Billed --></label>
        <name>pse__Billed__c</name>
    </fields>
    <fields>
        <label><!-- Billing Amount (Pre-Fee Subtotal) --></label>
        <name>pse__Billing_Amount_Pre_Fee_Subtotal__c</name>
    </fields>
    <fields>
        <label><!-- Billing Amount --></label>
        <name>pse__Billing_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Billing Currency --></label>
        <name>pse__Billing_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Invoiced --></label>
        <name>pse__Billing_Event_Invoiced__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Item --></label>
        <name>pse__Billing_Event_Item__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Billing Event Released --></label>
        <name>pse__Billing_Event_Released__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Status --></label>
        <name>pse__Billing_Event_Status__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event --></label>
        <name>pse__Billing_Event__c</name>
    </fields>
    <fields>
        <label><!-- Billing Hold --></label>
        <name>pse__Billing_Hold__c</name>
    </fields>
    <fields>
        <label><!-- Cost Transaction --></label>
        <name>pse__Cost_Transaction__c</name>
        <relationshipLabel><!-- Expense (Cost Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>pse__Description__c</name>
    </fields>
    <fields>
        <help>自動マイレージ払い戻しのために移動した距離。</help>
        <label><!-- Distance --></label>
        <name>pse__Distance__c</name>
    </fields>
    <fields>
        <help>経費が、請求イベント生成に適した状態にあることを示します（請求承認フラグは含まれません。これは、グローバル構成ごとにも必要な場合があります）。</help>
        <label><!-- Eligible for Billing --></label>
        <name>pse__Eligible_for_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Exchange Rate (Billing Currency) --></label>
        <name>pse__Exchange_Rate_Billing_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Exchange Rate (Incurred Currency) --></label>
        <name>pse__Exchange_Rate_Incurred_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Exchange Rate (Reimbursement Currency) --></label>
        <name>pse__Exchange_Rate_Reimbursement_Currency__c</name>
    </fields>
    <fields>
        <help>設定されると、このフィールドは、経費を負担するリソースによって経験される発生率と償還率の間の*相対*為替レートを設定します。</help>
        <label><!-- Exchange Rate (Resource-Defined) --></label>
        <name>pse__Exchange_Rate_Resource_Defined__c</name>
    </fields>
    <fields>
        <help>チェックした場合、このビジネス記録に請求しないでください。 Billing Holdと同じ効果ですが、Billing Generationからの永久的な除外を反映することを目的としています。</help>
        <label><!-- Exclude from Billing --></label>
        <name>pse__Exclude_from_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Expense Date --></label>
        <name>pse__Expense_Date__c</name>
    </fields>
    <fields>
        <label><!-- Expense Report --></label>
        <name>pse__Expense_Report__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Expense Split Parent --></label>
        <name>pse__Expense_Split_Parent__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Include In Financials --></label>
        <name>pse__Include_In_Financials__c</name>
    </fields>
    <fields>
        <help>請求可能な経費をチェックした場合、これは[未払税額]フィールドで指定された経費金額の一部が請求対象ではないことを意味します。 （これは[請求対象外発生金額]フィールドの金額とは別に行われます。）</help>
        <label><!-- Incurred Tax Non-Billable --></label>
        <name>pse__Incurred_Tax_Non_Billable__c</name>
    </fields>
    <fields>
        <help>発生した税額</help>
        <label><!-- Incurred Tax --></label>
        <name>pse__Incurred_Tax__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Date --></label>
        <name>pse__Invoice_Date__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Number --></label>
        <name>pse__Invoice_Number__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Transaction --></label>
        <name>pse__Invoice_Transaction__c</name>
        <relationshipLabel><!-- Expense (Invoice Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Invoiced --></label>
        <name>pse__Invoiced__c</name>
    </fields>
    <fields>
        <help>経費報告が送信された場合にユーザーが経費明細への添付を必要とする場合、このフィールドはUIに表示されます。このフィールドがtrueに設定されている場合、設定はtrueに設定されていますが、ユーザーは添付ファイルを追加する必要はありません。</help>
        <label><!-- Lost Receipt --></label>
        <name>pse__Lost_Receipt__c</name>
    </fields>
    <fields>
        <label><!-- Milestone --></label>
        <name>pse__Milestone__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Mobile Expense Reference ID --></label>
        <name>pse__Mobile_Expense_Reference_ID__c</name>
    </fields>
    <fields>
        <help>この計算式は、請求対象の費用に対する請求対象外の金額と請求対象外の発生した金額および税金（ゼロから費用額までのいずれか）に相当します。</help>
        <label><!-- Non-Billable Amount --></label>
        <name>pse__Non_Billable_Amount__c</name>
    </fields>
    <fields>
        <help>請求可能な経費に指定されている場合は、経費の請求可能額を計算するために経費額（発生通貨）から差し引かれます。分値ゼロ、最大金額費用額。デフォルト値：ゼロまたはnull請求対象外費用には影響しません。</help>
        <label><!-- Non-Billable Incurred Amount --></label>
        <name>pse__Non_Billable_Incurred_Amount__c</name>
    </fields>
    <fields>
        <help>この計算式では、追加の非請求対象未払い金額に、非請求対象未収税（追加の非請求対象未払い金額）を追加して、最低0.00と最高費用額を計算します。</help>
        <label><!-- Non-Billable Incurred Subtotal --></label>
        <name>pse__Non_Billable_Incurred_Subtotal__c</name>
    </fields>
    <fields>
        <help>このチェックボックスがチェックされていると、経費の払い戻し金額は発生した金額に関係なくゼロになります。</help>
        <label><!-- Non-Reimbursable --></label>
        <name>pse__Non_Reimbursible__c</name>
    </fields>
    <fields>
        <label><!-- Notes --></label>
        <name>pse__Notes__c</name>
    </fields>
    <fields>
        <label><!-- Override Group Currency Code --></label>
        <name>pse__Override_Group_Currency_Code__c</name>
    </fields>
    <fields>
        <help>プロジェクトが別のグループに属していても、経費取引のグループ実績へのロールアップ先のグループをオーバーライドします。通常、経費の取引は「フォロー」ルールに基づいてそのプロジェクトまたはリソースのグループにロールアップされます。</help>
        <label><!-- Override Group --></label>
        <name>pse__Override_Group__c</name>
        <relationshipLabel><!-- Override Group For Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Practice Currency Code --></label>
        <name>pse__Override_Practice_Currency_Code__c</name>
    </fields>
    <fields>
        <help>プロジェクトが別のプラクティスに属している場合でも、経費取引がプラクティス実績にロールアップするプラクティスをオーバーライドします。通常、経費のトランザクションは、「フォロー」ルールに基づいて、そのプロジェクトまたはリソースのプラクティスにロールアップされます。</help>
        <label><!-- Override Practice --></label>
        <name>pse__Override_Practice__c</name>
        <relationshipLabel><!-- Override Practice For Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Rate (Billing Currency) --></label>
        <name>pse__Override_Rate_Billing_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Override Rate (Incurred Currency) --></label>
        <name>pse__Override_Rate_Incurred_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Override Rate (Reimbursement Currency) --></label>
        <name>pse__Override_Rate_Reimbursement_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Override Region Currency Code --></label>
        <name>pse__Override_Region_Currency_Code__c</name>
    </fields>
    <fields>
        <help>プロジェクトが別の地域にある場合でも、地域取引のために経費取引が積み上げられる地域をオーバーライドします。通常、経費の取引は「フォロー」ルールに基づいてそのプロジェクトまたはリソースの地域にロールアップされます。</help>
        <label><!-- Override Region --></label>
        <name>pse__Override_Region__c</name>
        <relationshipLabel><!-- Override Region For Expenses --></relationshipLabel>
    </fields>
    <fields>
        <help>プロジェクト方法論へのルックアップ</help>
        <label><!-- Project Methodology --></label>
        <name>pse__Project_Methodology__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <help>プロジェクトフェーズへのルックアップ</help>
        <label><!-- Project Phase --></label>
        <name>pse__Project_Phase__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Rate Unit --></label>
        <name>pse__Rate_Unit__c</name>
        <picklistValues>
            <masterLabel>Kilometer</masterLabel>
            <translation>キロメートル</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Mile</masterLabel>
            <translation>マイル</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>収益予測のためにこのレコードに適用する認識方法。</help>
        <label><!-- Recognition Method --></label>
        <name>pse__Recognition_Method__c</name>
    </fields>
    <fields>
        <help>このフィールドには、後で仕入先請求書で使用するために、システムの（上書きされていない）レートを使用してプロジェクト（請求）通貨に変換された[返済額]の数値が格納されます。</help>
        <label><!-- Reimbursement Amount In Project Currency --></label>
        <name>pse__Reimbursement_Amount_In_Project_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Reimbursement Amount --></label>
        <name>pse__Reimbursement_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Reimbursement Currency --></label>
        <name>pse__Reimbursement_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Resource --></label>
        <name>pse__Resource__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Revenue Transaction --></label>
        <name>pse__Revenue_Transaction__c</name>
        <relationshipLabel><!-- Expense (Revenue Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Split Expense --></label>
        <name>pse__Split_Expense__c</name>
    </fields>
    <fields>
        <label><!-- Split Notes --></label>
        <name>pse__Split_Notes__c</name>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>pse__Status__c</name>
        <picklistValues>
            <masterLabel>Approved</masterLabel>
            <translation>承認済み</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Draft</masterLabel>
            <translation>ドラフト</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rejected</masterLabel>
            <translation>拒否されました</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Submitted</masterLabel>
            <translation>送信済み</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Submitted --></label>
        <name>pse__Submitted__c</name>
    </fields>
    <fields>
        <help>このチェックボックスはシステム用です。これは、将来のメソッド内で実行されるタイムカード分割の挿入/更新と併せてチェックすることを目的としており、それ以降の将来のメソッドが呼び出されないようにします。</help>
        <label><!-- Synchronous Update Required --></label>
        <name>pse__Synchronous_Update_Required__c</name>
    </fields>
    <fields>
        <help>費用に含まれているオプションの税の種類</help>
        <label><!-- Tax Type --></label>
        <name>pse__Tax_Type__c</name>
        <picklistValues>
            <masterLabel>GST</masterLabel>
            <translation>GST</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>VAT 1</masterLabel>
            <translation>VAT 1</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>VAT 2</masterLabel>
            <translation>VAT 2</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>第三者費用アプリケーションからの費用ID。</help>
        <label><!-- Third-Party Expenses App Expense ID --></label>
        <name>pse__Third_Party_Expenses_App_Expense_ID__c</name>
    </fields>
    <fields>
        <label><!-- Type --></label>
        <name>pse__Type__c</name>
        <picklistValues>
            <masterLabel>Airfare</masterLabel>
            <translation>航空運賃</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Auto Mileage</masterLabel>
            <translation>オートマイレージ</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Business Meals</masterLabel>
            <translation>ビジネスミール</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Car Rental</masterLabel>
            <translation>レンタカー</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Employee Relations</masterLabel>
            <translation>従業員との関係</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Gasoline</masterLabel>
            <translation>ガソリン</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>General and Admin Expenses</masterLabel>
            <translation>一般管理費</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>IT Equipment and Services</masterLabel>
            <translation>IT機器とサービス</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Lodging (Room and Tax)</masterLabel>
            <translation>宿泊（部屋と税）</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Miscellaneous</masterLabel>
            <translation>その他</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Office Supplies and Services</masterLabel>
            <translation>事務用品およびサービス</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Personal Meals</masterLabel>
            <translation>個人的な食事</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Phone</masterLabel>
            <translation>電話</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Postage and Shipping</masterLabel>
            <translation>送料と発送</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Recruiting</masterLabel>
            <translation>募集</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Subway</masterLabel>
            <translation>地下鉄</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Taxi</masterLabel>
            <translation>タクシー</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Vendor Invoice Item --></label>
        <name>pse__Vendor_Invoice_Item__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <nameFieldLabel>経費番号</nameFieldLabel>
    <sharingReasons>
        <label><!-- PSE Approver Share --></label>
        <name>pse__PSE_Approver_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE Member Share --></label>
        <name>pse__PSE_Member_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE PM Share --></label>
        <name>pse__PSE_PM_Share</name>
    </sharingReasons>
    <validationRules>
        <errorMessage><!-- Assignment Project must match Expense Project. --></errorMessage>
        <name>pse__Expense_Asgnmt_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Assignment Resource must match Expense Resource. --></errorMessage>
        <name>pse__Expense_Asgnmt_Resource_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Expense Line Project must match Expense Report Project. --></errorMessage>
        <name>pse__Expense_ER_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Expense Line Resource must match Expense Report Resource. --></errorMessage>
        <name>pse__Expense_ER_Resource_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- For an Expense Line to be marked as Billed, it must also be marked as Included in Financials and Approved. --></errorMessage>
        <name>pse__Expense_Line_Billing_Requires_Inclusion</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- For an Expense Line to be marked as Invoiced, it must also be marked as Billed. --></errorMessage>
        <name>pse__Expense_Line_Invoicing_Requires_Billing</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- An Expense Line may not be submitted or included in financials unless it belongs to an Expense Report. --></errorMessage>
        <name>pse__Expense_Line_Needs_Parent_For_Submit</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Expense Line&apos;s Project field value may not be updated once set (unless parent Expense Report is changed, Admin Global Edit is checked, Audit Notes are provided, and Actuals Calculation Mode is Scheduled). --></errorMessage>
        <name>pse__Expense_Line_Project_is_Constant</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Expense Line&apos;s Expense Report may not be changed if it is submitted or included in financials. --></errorMessage>
        <name>pse__Expense_Line_Report_Fixed_If_Submitted</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Expense Line&apos;s Resource field value may not be blank and may not be updated once set (unless Admin Global Edit is checked, Audit Notes are provided, and Actuals Calculation Mode is Scheduled). --></errorMessage>
        <name>pse__Expense_Line_Resource_is_Constant</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- An Expense may only be marked as Billable if its Project is Billable and its Expense Report and Assignment, if any, are Billable. --></errorMessage>
        <name>pse__Expense_May_Not_Be_Billable</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Milestone Project must match Expense Project. --></errorMessage>
        <name>pse__Expense_Milestone_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Expense Report is required --></errorMessage>
        <name>pse__Expense_Requires_Expense_Report</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- If provided, the incurred tax amount for an Expense must not be less than zero or greater than the Expense amount itself. --></errorMessage>
        <name>pse__Incurred_Tax_Amount_Invalid</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Invoice Number or Date is not allowed on record unless it is invoiced --></errorMessage>
        <name>pse__Invoiced</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- If provided, the non-billable incurred amount for an Expense must not be less than zero or greater than the Expense amount itself. --></errorMessage>
        <name>pse__Non_Billable_Incurred_Amount_Invalid</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The non-billable incurred subtotal for an Expense (any non-billable amount plus any non-billable tax) must not be greater than the Expense amount itself. --></errorMessage>
        <name>pse__Non_Billable_Subtotal_Invalid</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Resource-defined Exchange Rate can only be set if Incurred and Resource currencies are different.  If the currencies are the same, a relative rate of 1.0 is implied. --></errorMessage>
        <name>pse__Res_Defined_Rate_Diff_Currencies_Only</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Resource-defined Exchange Rate must either be greater than 0.0 (zero) or null/empty. --></errorMessage>
        <name>pse__Res_Defined_Rate_may_not_be_Negative</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The vendor invoice cannot change once set. --></errorMessage>
        <name>pse__Vendor_Invoice_Constant</name>
    </validationRules>
    <webLinks>
        <label><!-- Admin_Edit --></label>
        <name>pse__Admin_Edit</name>
    </webLinks>
    <webLinks>
        <label><!-- Clear_Billing_Data --></label>
        <name>pse__Clear_Billing_Data</name>
    </webLinks>
    <webLinks>
        <label><!-- Edit_Expenses --></label>
        <name>pse__Edit_Expenses</name>
    </webLinks>
    <webLinks>
        <label><!-- Multiple_Expense_Entry_UI --></label>
        <name>pse__Multiple_Expense_Entry_UI</name>
    </webLinks>
    <webLinks>
        <label><!-- Ready_to_Submit --></label>
        <name>pse__Ready_to_Submit</name>
    </webLinks>
</CustomObjectTranslation>
