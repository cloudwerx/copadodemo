<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Linea di transazione Riconoscimento dei ricavi</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Linee di transazione di riconoscimento dei ricavi</value>
    </caseValues>
    <fieldSets>
        <label><!-- Summarization Fields --></label>
        <name>ffrr__Summarization_Fields</name>
    </fieldSets>
    <fields>
        <label><!-- GLA Type --></label>
        <name>ffrr__AccountType__c</name>
    </fields>
    <fields>
        <label><!-- GLA Revenue --></label>
        <name>ffrr__Account__c</name>
    </fields>
    <fields>
        <label><!-- Actual vs. Forecast Record --></label>
        <name>ffrr__ActualVsForecastRelationship__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Amortized Opening Balance Dual --></label>
        <name>ffrr__AmortizedOpeningBalanceDual__c</name>
    </fields>
    <fields>
        <label><!-- Amortized Opening Balance Home --></label>
        <name>ffrr__AmortizedOpeningBalanceHome__c</name>
    </fields>
    <fields>
        <help>Importo copiato dal campo Ammortizzato alla data di questo record sorgente prima dell&apos;esistenza di eventuali transazioni di riconoscimento delle entrate. In genere viene impostato un saldo di apertura durante la migrazione dei dati in Revenue Management.</help>
        <label><!-- Amortized Opening Balance --></label>
        <name>ffrr__AmortizedOpeningBalance__c</name>
    </fields>
    <fields>
        <help>L’importo cumulativo ammortizzato fino ad oggi, nella valuta del documento, per il record di origine in questa riga della transazione.</help>
        <label><!-- Amortized To Date --></label>
        <name>ffrr__AmortizedToDate__c</name>
    </fields>
    <fields>
        <help>Ammontare ammortizzato in doppia valuta.</help>
        <label><!-- Amount Amortized (Dual) --></label>
        <name>ffrr__AmountAmortizedDual__c</name>
    </fields>
    <fields>
        <help>Ammontare ammortizzato in valuta nazionale.</help>
        <label><!-- Amount Amortized (Home) --></label>
        <name>ffrr__AmountAmortizedHome__c</name>
    </fields>
    <fields>
        <label><!-- Amount Amortized --></label>
        <name>ffrr__AmountAmortized__c</name>
    </fields>
    <fields>
        <help>Importo riconosciuto in doppia valuta</help>
        <label><!-- Amount Recognized (Dual) --></label>
        <name>ffrr__AmountRecognizedDual__c</name>
    </fields>
    <fields>
        <help>Importo riconosciuto nella valuta locale</help>
        <label><!-- Amount Recognized (Home) --></label>
        <name>ffrr__AmountRecognizedHome__c</name>
    </fields>
    <fields>
        <label><!-- Amount Recognized --></label>
        <name>ffrr__AmountRecognized__c</name>
    </fields>
    <fields>
        <help>Valore del rapporto 1.</help>
        <label><!-- Analysis Item 1 --></label>
        <name>ffrr__AnalysisItem1__c</name>
    </fields>
    <fields>
        <help>Valore del rapporto 2.</help>
        <label><!-- Analysis Item 2 --></label>
        <name>ffrr__AnalysisItem2__c</name>
    </fields>
    <fields>
        <help>Valore del rapporto 3.</help>
        <label><!-- Analysis Item 3 --></label>
        <name>ffrr__AnalysisItem3__c</name>
    </fields>
    <fields>
        <help>Valore del rapporto 4.</help>
        <label><!-- Analysis Item 4 --></label>
        <name>ffrr__AnalysisItem4__c</name>
    </fields>
    <fields>
        <label><!-- Company --></label>
        <name>ffrr__Company__c</name>
    </fields>
    <fields>
        <label><!-- GLA Cost --></label>
        <name>ffrr__CostAccount__c</name>
    </fields>
    <fields>
        <label><!-- Cost Amended --></label>
        <name>ffrr__CostAmended__c</name>
    </fields>
    <fields>
        <label><!-- Cost Center --></label>
        <name>ffrr__CostCenter__c</name>
    </fields>
    <fields>
        <label><!-- Cost Center (Cost) --></label>
        <name>ffrr__CostCostCenter__c</name>
    </fields>
    <fields>
        <label><!-- Document Currency --></label>
        <name>ffrr__Currency__c</name>
    </fields>
    <fields>
        <help>Il tasso di cambio che converte dalla valuta del documento alla valuta nazionale.</help>
        <label><!-- Document Rate --></label>
        <name>ffrr__DocumentCurrencyRate__c</name>
    </fields>
    <fields>
        <help>Il tasso di cambio che converte dalla valuta nazionale alla doppia valuta.</help>
        <label><!-- Dual Rate --></label>
        <name>ffrr__DualCurrencyRate__c</name>
    </fields>
    <fields>
        <help>La doppia valuta.</help>
        <label><!-- Dual Currency --></label>
        <name>ffrr__DualCurrency__c</name>
    </fields>
    <fields>
        <help>La valuta locale</help>
        <label><!-- Home Currency --></label>
        <name>ffrr__HomeCurrency__c</name>
    </fields>
    <fields>
        <label><!-- Internal Amount (Amortized) --></label>
        <name>ffrr__InternalAmortizedAmount__c</name>
    </fields>
    <fields>
        <label><!-- Internal Amount --></label>
        <name>ffrr__InternalAmount__c</name>
    </fields>
    <fields>
        <help>L&apos;ammontare ammortizzato con il segno invertito.</help>
        <label><!-- Journal Amount (Amortized) --></label>
        <name>ffrr__JournalAmortizedAmount__c</name>
    </fields>
    <fields>
        <help>L&apos;importo ammortizzato in doppia valuta, con il segno invertito.</help>
        <label><!-- Journal Amount Dual (Amortized) --></label>
        <name>ffrr__JournalAmortizedDualAmount__c</name>
    </fields>
    <fields>
        <help>L&apos;ammontare ammortizzato nella valuta locale, con il segno invertito.</help>
        <label><!-- Journal Amount Home (Amortized) --></label>
        <name>ffrr__JournalAmortizedHomeAmount__c</name>
    </fields>
    <fields>
        <help>L&apos;importo riconosciuto in doppia valuta, con il segno invertito.</help>
        <label><!-- Journal Amount Dual --></label>
        <name>ffrr__JournalAmountDual__c</name>
    </fields>
    <fields>
        <help>L&apos;importo riconosciuto nella valuta locale, con il segno invertito.</help>
        <label><!-- Journal Amount Home --></label>
        <name>ffrr__JournalAmountHome__c</name>
    </fields>
    <fields>
        <help>L&apos;importo riconosciuto con il segno invertito.</help>
        <label><!-- Journal Amount --></label>
        <name>ffrr__JournalAmount__c</name>
    </fields>
    <fields>
        <label><!-- Level 3 Object Record Name --></label>
        <name>ffrr__Level3ObjectRecordName__c</name>
    </fields>
    <fields>
        <label><!-- Level 4 Object Record Name --></label>
        <name>ffrr__Level4ObjectRecordName__c</name>
    </fields>
    <fields>
        <label><!-- Performance Obligation --></label>
        <name>ffrr__PerformanceObligation__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Period --></label>
        <name>ffrr__Period__c</name>
    </fields>
    <fields>
        <label><!-- Primary Object Record Name --></label>
        <name>ffrr__PrimaryObjectRecordName__c</name>
    </fields>
    <fields>
        <help>Cerca il prodotto nella riga della transazione.</help>
        <label><!-- Product --></label>
        <name>ffrr__Product__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Recognized Date --></label>
        <name>ffrr__RecognizedDate__c</name>
    </fields>
    <fields>
        <label><!-- Recognized Opening Balance Dual --></label>
        <name>ffrr__RecognizedOpeningBalanceDual__c</name>
    </fields>
    <fields>
        <label><!-- Recognized Opening Balance Home --></label>
        <name>ffrr__RecognizedOpeningBalanceHome__c</name>
    </fields>
    <fields>
        <help>Importo copiato dal campo Riconosciuto alla data di questo record di origine prima dell&apos;esistenza di eventuali transazioni di riconoscimento delle entrate. In genere viene impostato un saldo di apertura durante la migrazione dei dati in Revenue Management.</help>
        <label><!-- Recognized Opening Balance --></label>
        <name>ffrr__RecognizedOpeningBalance__c</name>
    </fields>
    <fields>
        <label><!-- Recognized Record ID --></label>
        <name>ffrr__RecognizedRecordID__c</name>
    </fields>
    <fields>
        <help>L’importo cumulativo riconosciuto fino ad oggi, nella valuta del documento, per il record di origine in questa riga della transazione.</help>
        <label><!-- Recognized To Date --></label>
        <name>ffrr__RecognizedToDate__c</name>
    </fields>
    <fields>
        <label><!-- Revenue Amended --></label>
        <name>ffrr__RevenueAmended__c</name>
    </fields>
    <fields>
        <label><!-- Revenue Contract --></label>
        <name>ffrr__RevenueContract__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Revenue Recognition Transaction --></label>
        <name>ffrr__RevenueRecognitionMaster__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <fields>
        <help>Cerca l’account Salesforce nella riga della transazione.</help>
        <label><!-- Account --></label>
        <name>ffrr__SalesforceAccount__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Level 2 Object Record Name --></label>
        <name>ffrr__SecondaryObjectRecordName__c</name>
    </fields>
    <fields>
        <label><!-- Template --></label>
        <name>ffrr__Template__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Numero di riga della transazione</nameFieldLabel>
    <startsWith>Vowel</startsWith>
</CustomObjectTranslation>
