<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <label><!-- Additional Fields --></label>
        <name>pse__Additional_Fields__c</name>
    </fields>
    <fields>
        <help><!-- If selected,users are able to save task time records with zero hours. The default value is false. --></help>
        <label><!-- Allow Zero Entry --></label>
        <name>pse__Allow_Zero_Entry__c</name>
    </fields>
    <fields>
        <help><!-- If true the project or assignment can be changed as long as the timecard &apos;s status is one of the &quot;timecard-edit-status-values/Timecard_edit_status_values__c&quot; values. --></help>
        <label><!-- Assignment project editable after save --></label>
        <name>pse__Assignment_project_editable_after_save__c</name>
    </fields>
    <fields>
        <help><!-- The fields to use as tooltip for assignments. Fully-qualified (with namespace if applicable), case-sensitive field names are required --></help>
        <label><!-- Assignment tooltip fields --></label>
        <name>pse__Assignment_tooltip_fields__c</name>
    </fields>
    <fields>
        <help><!-- If the Billable field is configured to be displayed, determines whether it should be displayed to the left or the right of Hours in the timecard header line of the Timecard UI. --></help>
        <label><!-- Billable Header Field Position Is Left --></label>
        <name>pse__Billable_Header_Field_Position_Is_Left__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether timecards with exactly the same combination of week, project, assignment, resource, billable, project, methodology, location, travel are combined on the Timecard Entry page. --></help>
        <label><!-- Combine Similar Timecards --></label>
        <name>pse__Combine_Similar_Timecards__c</name>
    </fields>
    <fields>
        <help><!-- If checked Additional Fields will be copied from previous week timecards. --></help>
        <label><!-- Copy AdditionalFields From Previous Week --></label>
        <name>pse__Copy_AdditionalFields_From_Previous_Week__c</name>
    </fields>
    <fields>
        <help><!-- If checked ETC field  will be copied from previous week timecards --></help>
        <label><!-- Copy ETC From Previous Week --></label>
        <name>pse__Copy_ETC_From_Previous_Week__c</name>
    </fields>
    <fields>
        <help><!-- If checked Hour fields like Monday Hours, Tuesday Hours etc  will be copied from previous week timecards --></help>
        <label><!-- Copy Hours From Previous Week --></label>
        <name>pse__Copy_Hours_From_Previous_Week__c</name>
    </fields>
    <fields>
        <help><!-- If checked Location Information fields like Location Mon etc  will be copied from previous week timecards --></help>
        <label><!-- Copy Locations From Previous Week --></label>
        <name>pse__Copy_Locations_From_Previous_Week__c</name>
    </fields>
    <fields>
        <help><!-- If checked Methodology field  will be copied from previous week timecards --></help>
        <label><!-- Copy Methodology From Previous Week --></label>
        <name>pse__Copy_Methodology_From_Previous_Week__c</name>
    </fields>
    <fields>
        <help><!-- If checked Milestones will be copied from previous week&apos;s timecards --></help>
        <label><!-- Copy Milestone From Previous Week --></label>
        <name>pse__Copy_Milestone_From_Previous_Week__c</name>
    </fields>
    <fields>
        <help><!-- If checked Notes will be copied from previous week timecards --></help>
        <label><!-- Copy Notes From Previous Week --></label>
        <name>pse__Copy_Notes_From_Previous_Week__c</name>
    </fields>
    <fields>
        <help><!-- If checked Phase field  will be copied from previous week timecards --></help>
        <label><!-- Copy Phase From Previous Week --></label>
        <name>pse__Copy_Phase_From_Previous_Week__c</name>
    </fields>
    <fields>
        <help><!-- If set to true, the Copy From Schedule functionality populates the Notes fields for each day on the Timecard if it finds Assignment Daily Notes child records on the Assignment for that day. (default false) --></help>
        <label><!-- Copy Schedule Assignment Daily Notes --></label>
        <name>pse__Copy_Schedule_Assignment_Daily_Notes__c</name>
    </fields>
    <fields>
        <help><!-- When selected, task hours are copied from the previous week&apos;s timecard. --></help>
        <label><!-- Copy Task Hours From Previous Week --></label>
        <name>pse__Copy_Task_Hours_From_Previous_Week__c</name>
    </fields>
    <fields>
        <help><!-- When selected, task information is copied from the previous week&apos;s timecard. --></help>
        <label><!-- Copy Tasks From Previous Week --></label>
        <name>pse__Copy_Tasks_From_Previous_Week__c</name>
    </fields>
    <fields>
        <help><!-- If checked Travel Information fields like Travel Week, Travel Mon etc  will be copied from previous week timecards --></help>
        <label><!-- Copy Travel From Previous Week --></label>
        <name>pse__Copy_Travel_From_Previous_Week__c</name>
    </fields>
    <fields>
        <help><!-- When true and the Resource Lookup popup on the new timecard UI returns only one result, the Resource Lookup popup is automatically closed and the resource field is set to returned resource. --></help>
        <label><!-- Default Single Resource Search Result --></label>
        <name>pse__Default_Single_Resource_Search_Result__c</name>
    </fields>
    <fields>
        <label><!-- Default Week Offset --></label>
        <name>pse__Default_Week_Offset__c</name>
    </fields>
    <fields>
        <help><!-- If true, billable drop down field on timecard ui will be shown as disabled i.e non-editable by user . --></help>
        <label><!-- Default billable --></label>
        <name>pse__Default_billable__c</name>
    </fields>
    <fields>
        <help><!-- TC UI cache the all assigned assignments/project along with the milestones while it loads. This may lead to salesforce&apos;s viewstate limit, if user have many assignments/projects. Setting this config value to true will minimize this cache. --></help>
        <label><!-- Disable cache --></label>
        <name>pse__Disable_cache__c</name>
    </fields>
    <fields>
        <help><!-- These many empty timecard lines will be added, when add lines button will be clicked. --></help>
        <label><!-- Empty lines to append on add lines --></label>
        <name>pse__Empty_lines_to_append_on_add_lines__c</name>
    </fields>
    <fields>
        <help><!-- This is the minimum no of lines that will be always available for new timecard entry. --></help>
        <label><!-- Empty timecard lines to append by def --></label>
        <name>pse__Empty_timecard_lines_to_append_by_def__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether the billable column on the Timecard Entry page is hidden. When true, the column is hidden and the billable checkbox defaults to selected. The default value is false. --></help>
        <label><!-- Hide Billable Column --></label>
        <name>pse__Hide_Billable_Column__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether the Copy From Previous Week button on the new timecard UI is hidden. The default value is false. --></help>
        <label><!-- Hide Copy From Previous Week Button --></label>
        <name>pse__Hide_Copy_From_Previous_Week_Button__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether the Copy Schedules button on the new timecard UI is hidden. The default value is false. --></help>
        <label><!-- Hide Copy Schedules Button --></label>
        <name>pse__Hide_Copy_Schedules_Button__c</name>
    </fields>
    <fields>
        <help><!-- When true, the Expand Tasks or Collapse Tasks link is hidden in the Time Entry page. The default value is false. --></help>
        <label><!-- Hide Expand Tasks --></label>
        <name>pse__Hide_Expand_Tasks__c</name>
    </fields>
    <fields>
        <help><!-- When true, the Notes column that normally appears on the Timecard Entry page is hidden. The default value is false. --></help>
        <label><!-- Hide Notes Column --></label>
        <name>pse__Hide_Notes_Column__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether the Save button on the new timecard UI is hidden. The default value is false. --></help>
        <label><!-- Hide Save Button --></label>
        <name>pse__Hide_Save_Button__c</name>
    </fields>
    <fields>
        <help><!-- When true, the &apos;All Tasks&apos; and &apos;My Tasks&apos; buttons are hidden on the Add Tasks popup in the Time Entry page. The default value is false. --></help>
        <label><!-- Hide Show All Tasks/My Tasks --></label>
        <name>pse__Hide_Show_All_Tasks_My_Tasks__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether the Submit button on the new timecard UI is hidden. The default value is false. --></help>
        <label><!-- Hide Submit Button --></label>
        <name>pse__Hide_Submit_Button__c</name>
    </fields>
    <fields>
        <help><!-- If selected and Task Time Entry Mode is set to Calculated,  only one row of task hours is displayed. The default value of the checkbox is clear. --></help>
        <label><!-- Hide Timecard Hours in Calculated Mode --></label>
        <name>pse__Hide_Timecard_Hours_in_Calculated_Mode__c</name>
    </fields>
    <fields>
        <help><!-- If true and Task Time Entry Mode is Current, the View or Edit Details links shown when hovering over a timecard line are hidden. If true and Task Time Entry Mode is not Current, the Notes tab is hidden on the links. The default value is false --></help>
        <label><!-- Hide View Edit Details Link --></label>
        <name>pse__Hide_View_Edit_Details_Link__c</name>
    </fields>
    <fields>
        <help><!-- The given field/config is used to color-code the date-column-heading for a Work Calendar Holiday for the selected Resource on the Timecard UI. --></help>
        <label><!-- Holiday Color Code --></label>
        <name>pse__Holiday_Color_Code__c</name>
    </fields>
    <fields>
        <help><!-- Determines the pixel width of timecard hours cells in the Timecard V15 Entry page. --></help>
        <label><!-- Hours Cell Pixel Width --></label>
        <name>pse__Hours_cell_pixel_width__c</name>
    </fields>
    <fields>
        <help><!-- Maximum number of hours a resource can enter on a single timecard day. If the total number of hours for a resource exceeds this amount on a single timecard day, an error message is displayed. --></help>
        <label><!-- Max Resource Hours Per Day --></label>
        <name>pse__Max_Resource_Hours_Per_Day__c</name>
    </fields>
    <fields>
        <help><!-- Maximum number of hours a resource can enter for a timecard week. If exceeded, an error message is displayed. Enter a percentage value and symbol, eg. 90%, to specify a maximum percentage of work calendar hours. --></help>
        <label><!-- Maximum Resource Hours Per Week --></label>
        <name>pse__Maximum_Resource_Hours_Per_Week__c</name>
    </fields>
    <fields>
        <help><!-- If the Methodology field is configured to be displayed, determines whether it should be displayed to the left or the right of Hours in the timecard header line of the Timecard UI. --></help>
        <label><!-- Methodology HeaderField Position Is Left --></label>
        <name>pse__Methodology_HeaderField_Position_Is_Left__c</name>
    </fields>
    <fields>
        <help><!-- This config flag determines whether methodology field should be display on notes popup or at front page. --></help>
        <label><!-- Methodology field position is popup --></label>
        <name>pse__Methodology_field_position_is_popup__c</name>
    </fields>
    <fields>
        <help><!-- If the Milestone field is configured to be displayed, determines whether it should be displayed to the left or the right of Hours in the timecard header line of the Timecard UI. --></help>
        <label><!-- Milestone Header Field Position Is Left --></label>
        <name>pse__Milestone_Header_Field_Position_Is_Left__c</name>
    </fields>
    <fields>
        <help><!-- This config flag determines whether milestone field should be display on notes popup or at front page. --></help>
        <label><!-- Milestone field position is popup --></label>
        <name>pse__Milestone_field_position_is_popup__c</name>
    </fields>
    <fields>
        <help><!-- Minimum number of hours a resource can enter for a timecard week. If weekly hours are lower than this amount, an error message is displayed. Enter a percentage value and symbol, eg. 30%, to specify a minimum percentage of work calendar hours. --></help>
        <label><!-- Minimum Resource Hours Per Week --></label>
        <name>pse__Minimum_Resource_Hours_Per_Week__c</name>
    </fields>
    <fields>
        <help><!-- If true then assignment nickname is editable else not. --></help>
        <label><!-- Nickname Editable --></label>
        <name>pse__Nickname_Editable__c</name>
    </fields>
    <fields>
        <help><!-- If true, location field will be shown in timacard entry note popup dialog --></help>
        <label><!-- Note location allowed --></label>
        <name>pse__Note_location_allowed__c</name>
    </fields>
    <fields>
        <help><!-- If true, then primary Location drop-down will be displayed on TC UI. --></help>
        <label><!-- Note primary location allowed --></label>
        <name>pse__Note_primary_location_allowed__c</name>
    </fields>
    <fields>
        <help><!-- If the Phase field is configured to be displayed, determines whether it should be displayed to the left or the right of Hours in the timecard header line of the Timecard UI. --></help>
        <label><!-- Phase Header Field Position Is Left --></label>
        <name>pse__Phase_Header_Field_Position_Is_Left__c</name>
    </fields>
    <fields>
        <help><!-- This config flag determines whether phase field should be display on notes popup or at front page. --></help>
        <label><!-- Phase field position is popup --></label>
        <name>pse__Phase_field_position_is_popup__c</name>
    </fields>
    <fields>
        <help><!-- If selected, tasks are pre populated for the selected user and project. The default value of the checkbox is clear. --></help>
        <label><!-- Pre Populate Task --></label>
        <name>pse__Pre_Populate_Task__c</name>
    </fields>
    <fields>
        <help><!-- If the Primary Location field is configured to be displayed, determines whether it should be displayed to the left or the right of Hours in the timecard header line of the Timecard UI. --></help>
        <label><!-- Primary Loc HeaderField Position Is Left --></label>
        <name>pse__Primary_Loc_HeaderField_Position_Is_Left__c</name>
    </fields>
    <fields>
        <help><!-- When Note primary location allowed is set to true, determines whether the Primary Location field appears in the Notes section or the timecard line. --></help>
        <label><!-- Primary Location field position is popup --></label>
        <name>pse__Primary_Location_field_position_is_popup__c</name>
    </fields>
    <fields>
        <help><!-- timecard project/Assignment account drop down display value --></help>
        <label><!-- Proj assig dropdown account custom field --></label>
        <name>pse__Proj_assig_dropdown_account_custom_field__c</name>
    </fields>
    <fields>
        <help><!-- Fields in this field set define the fields shown in the tooltip for the Project Task line in the Log a Timecard with Task Time UI. --></help>
        <label><!-- Project Task Tooltip Fieldset Name --></label>
        <name>pse__Project_Task_Tooltip_Fieldset_Name__c</name>
    </fields>
    <fields>
        <help><!-- The &quot;Assignment project editable after save&quot; gives user access to change the project/assignment for the saved timecards but this require audit note to be set for timecard. Audit notes can be set by this config. --></help>
        <label><!-- Project editable after save audit Notes --></label>
        <name>pse__Project_editable_after_save_audit_Notes__c</name>
    </fields>
    <fields>
        <help><!-- List of project fields to be shown as tooltip on timecard entry ui. --></help>
        <label><!-- Project tooltip fields --></label>
        <name>pse__Project_tooltip_fields__c</name>
    </fields>
    <fields>
        <help><!-- When selected, the tasks that are pre-populated are based on the selected assignment. The &quot;Limit To Tasks with Matching Assignment&quot; field in the Select Tasks popup is then checked by default. Restrict Task Based On Assignment is deselected by default. --></help>
        <label><!-- Restrict Task Based On Assignment --></label>
        <name>pse__Restrict_Task_Based_On_Assignment__c</name>
    </fields>
    <fields>
        <help><!-- When selected, the tasks that are pre-populated are based on the selected milestone. The &quot;Limit To Tasks with Matching Milestone&quot; field in the Select Tasks popup is then checked by default. Restrict Task Based On Milestone is deselected by default. --></help>
        <label><!-- Restrict Task Based On Milestone --></label>
        <name>pse__Restrict_Task_Based_On_Milestone__c</name>
    </fields>
    <fields>
        <help><!-- If selected, customizations to width, order, and visibility of columns in Time Entry will be remembered per user. --></help>
        <label><!-- Save Column Preferences --></label>
        <name>pse__Save_Column_Preferences__c</name>
    </fields>
    <fields>
        <help><!-- API names of assignment fields to be shown when hovering over the tooltip icon in the Schedule section of a timecard. --></help>
        <label><!-- Schedule Assignment Tooltip Fields --></label>
        <name>pse__Schedule_assignment_tooltip_fields__c</name>
    </fields>
    <fields>
        <label><!-- Schedule grid weekend delta --></label>
        <name>pse__Schedule_grid_weekend_delta__c</name>
    </fields>
    <fields>
        <help><!-- API names of project fields to be shown when hovering over the tooltip icon in the Schedule section of a timecard. --></help>
        <label><!-- Schedule Project Tooltip Fields --></label>
        <name>pse__Schedule_project_tooltip_fields__c</name>
    </fields>
    <fields>
        <help><!-- Given config is used to show alert if non zero value in entered on the timecard section for the day marked as holiday (having zero hours) in the selected Resource work calendar. --></help>
        <label><!-- Show Alert For NonZero HolidayHours --></label>
        <name>pse__Show_Alert_For_NonZero_HolidayHours__c</name>
    </fields>
    <fields>
        <help><!-- Given config is used to show alert if non zero value in entered on the schedule grid section for the day having zero hours in the selected Resource work calendar days pattern. --></help>
        <label><!-- Show Alert For NonZero WeekendHours --></label>
        <name>pse__Show_Alert_For_NonZero_WeekendHours__c</name>
    </fields>
    <fields>
        <help><!-- If true, billable non-billable hours will show via hover text on each total in the totals row for all the daily columns and sum column on timecard in Time Entry. --></help>
        <label><!-- Show Billable NonBillable Hours --></label>
        <name>pse__Show_Billable_NonBillable_Hours__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether the easy entry popup appears on the Time Entry page when you double-click an editable hour cell on a timecard row or press the DOWN ARROW key while that cell is selected. --></help>
        <label><!-- Show Easy Entry Popup --></label>
        <name>pse__Show_Easy_Entry_Popup__c</name>
    </fields>
    <fields>
        <help><!-- Contains field whose value will be shown on TC UI milestone drop down list.   If these are missing/null, the TC UI just continues to show the Milestone name fields of the available Milestones in the drop-down list.  No lookup __r references are supp --></help>
        <label><!-- Show Milestone As Field --></label>
        <name>pse__Show_Milestone_As_Field__c</name>
    </fields>
    <fields>
        <label><!-- Show ProjectName Assignment Dropdown --></label>
        <name>pse__Show_ProjectName_Assignment_Dropdown__c</name>
    </fields>
    <fields>
        <help><!-- Config is used to control the display recall link on each submitted timecard. On click it, a recall overlay will be open, where we need to enter the comment and recall the approval request for timecard. --></help>
        <label><!-- Show Recall On Timecards --></label>
        <name>pse__Show_Recall_On_Timecards__c</name>
    </fields>
    <fields>
        <help><!-- Checked, means schedule grid will be shown expanded on Timecard entry by default. --></help>
        <label><!-- Show Schedule Grid Expanded By Default --></label>
        <name>pse__Show_Schedule_Grid_Expanded_By_Default__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether the Travel checkboxes on the new timecard UI are to be displayed. The default value is true. --></help>
        <label><!-- Show Travel --></label>
        <name>pse__Show_Travel__c</name>
    </fields>
    <fields>
        <help><!-- If true, schedule grid section will show on timecard ui else not --></help>
        <label><!-- Show schedule grid --></label>
        <name>pse__Show_schedule_grid__c</name>
    </fields>
    <fields>
        <help><!-- If set to true, shows the tooltip icon next to assignment name on Schedule section of TC UI.Default false (existing behavior, no tooltip column) --></help>
        <label><!-- Show Schedule Tooltips --></label>
        <name>pse__Show_schedule_tooltips__c</name>
    </fields>
    <fields>
        <help><!-- config to show or hide etc field --></help>
        <label><!-- Show timecard etc --></label>
        <name>pse__Show_timecard_etc__c</name>
    </fields>
    <fields>
        <help><!-- If true, timecard methodology field will be show on timecard UI. --></help>
        <label><!-- Show timecard methodology --></label>
        <name>pse__Show_timecard_methodology__c</name>
    </fields>
    <fields>
        <help><!-- config to show or hide milestone select list --></help>
        <label><!-- Show timecard milestone --></label>
        <name>pse__Show_timecard_milestone__c</name>
    </fields>
    <fields>
        <help><!-- If true, timecard Phase field will be show on timecard UI. --></help>
        <label><!-- Show timecard phase --></label>
        <name>pse__Show_timecard_phase__c</name>
    </fields>
    <fields>
        <help><!-- When false, the Status/recall column will display immediately to the right of the Notes column. When true, Status/recall will display to the right of any additional fields. --></help>
        <label><!-- Status/Recall Column Not Fixed --></label>
        <name>pse__Status_Recall_Column_Not_Fixed__c</name>
    </fields>
    <fields>
        <help><!-- API name of a field set used by the Timecard object. Fields in this field set define the editable fields shown to the left of Hours in the timecard header line of the Timecard UI. --></help>
        <label><!-- TC HeaderRow Left Editable Fieldset Name --></label>
        <name>pse__TC_HeaderRow_Left_Editable_Fieldset_Name__c</name>
    </fields>
    <fields>
        <help><!-- API name of a field set used by the Timecard object. Fields in this field set define the editable fields in the timecard header line in the Timecard UI. --></help>
        <label><!-- TC Header Row Editable Fieldset Name --></label>
        <name>pse__TC_Header_Row_Editable_Fieldset_Name__c</name>
    </fields>
    <fields>
        <help><!-- API name of a field set used by the Timecard object. Fields in this field set define read-only fields in the timecard header line in the Timecard UI --></help>
        <label><!-- TC Header Row Readonly Fieldset Name --></label>
        <name>pse__TC_Header_Row_Readonly_Fieldset_Name__c</name>
    </fields>
    <fields>
        <help><!-- CSV to specify Timecard Header fields to copy from schedule. Only these hour fields are valid for this config
 &quot;Monday_Hours__c,Tuesday_Hours__c ,Wednesday_Hours__c ,Thursday_Hours__c,Friday_Hours__c ,Saturday_Hours__c,Sunday_Hours__c&quot; --></help>
        <label><!-- TC Hour Fields To Copy From Schedule --></label>
        <name>pse__TC_Hour_Fields_To_Copy_From_Schedule__c</name>
    </fields>
    <fields>
        <help><!-- API name of a field set used by the Timecard object. Fields in this field set define editable fields in the Notes section in timecard lines in the Timecard UI. --></help>
        <label><!-- TC Notes Field Editable Fieldset Name --></label>
        <name>pse__TC_Notes_Field_Editable_Fieldset_Name__c</name>
    </fields>
    <fields>
        <help><!-- API name of a field set used by the Timecard object. Fields in this field set define read-only fields in the Notes section in timecard lines in the Timecard UI. --></help>
        <label><!-- TC Notes Field Readonly Fieldset Name --></label>
        <name>pse__TC_Notes_Field_Readonly_Fieldset_Name__c</name>
    </fields>
    <fields>
        <help><!-- Field is used to hold coma (,) separated list of timecard status values that will be hide on the TC UI. --></help>
        <label><!-- TC Status To Hide --></label>
        <name>pse__TC_Status_To_Hide__c</name>
    </fields>
    <fields>
        <help><!-- Specifies tasks to hide when pre-populating in Timecards unless they have associated time logged.  The value is a comma-separated list of statuses. --></help>
        <label><!-- Task Statuses To Hide --></label>
        <name>pse__Task_Statuses_To_Hide__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether the resource can only select tasks that they have an assignment for when entering time against project tasks. When this option is not selected, users can enable this feature on the Time Entry page. --></help>
        <label><!-- Task Time: Only show assigned tasks. --></label>
        <name>pse__Task_Time_Entry_Assignment_Filter__c</name>
    </fields>
    <fields>
        <help><!-- Defines the filtering behavior on task dates when choosing tasks to enter time against. Set to None or Overlap. If None, no filtering occurs. If Overlap, tasks are listed only when the timecard and selectable tasks have some overlap on dates. --></help>
        <label><!-- Task Time: Filter task dates --></label>
        <name>pse__Task_Time_Entry_Date_Filter__c</name>
    </fields>
    <fields>
        <help><!-- The time entry mode to use when entering task time for a timecard row. You can choose one of these values: Adjust up, Current, Calculated, or Freeform. The default value is Current. --></help>
        <label><!-- Task Time Entry Mode --></label>
        <name>pse__Task_Time_Entry_Mode__c</name>
    </fields>
    <fields>
        <help><!-- Number of top assignments to be loaded to show on Project/assignment overlay --></help>
        <label><!-- Top assignment to show count --></label>
        <name>pse__Top_assignment_to_show_count__c</name>
    </fields>
    <fields>
        <help><!-- Number of weeks for which top assignments/projects to loaded to show on project/assignment overlay --></help>
        <label><!-- Top_assignment_week_load_limit --></label>
        <name>pse__Top_assignment_week_load_limit__c</name>
    </fields>
    <fields>
        <help><!-- Number of top projects to be loaded to show on Project/assignment overlay --></help>
        <label><!-- Top project to show count --></label>
        <name>pse__Top_project_to_show_count__c</name>
    </fields>
    <fields>
        <help><!-- Allows you to switch your preferred method of managing visible fields on the Timecard Entry page. If False, PSA will use configuration options or custom settings to configure the UI. If True, PSA will use field sets to configure the UI. --></help>
        <label><!-- Use Field Set for Timecard UI --></label>
        <name>pse__Use_Field_Set_for_Timecard_UI__c</name>
    </fields>
    <fields>
        <help><!-- Indicates if an assignment id should be used while displaying assignments on the time card page --></help>
        <label><!-- Use assignment id --></label>
        <name>pse__Use_assignment_id__c</name>
    </fields>
    <fields>
        <help><!-- Given field/config is used to color code the date-column heading for Scheduled 0-hour days as per the Schedule Pattern (not Holidays) on the selected Resource&apos;s Work Calendar. --></help>
        <label><!-- Weekend Color Code --></label>
        <name>pse__Weekend_Color_Code__c</name>
    </fields>
    <fields>
        <help><!-- Controls the pixel width for Milestone dropdown on Timecard Entry UI. Please specify a whole number for this value, as this will directly go into width. --></help>
        <label><!-- Width_px_for_milestone_drop_down --></label>
        <name>pse__Width_px_for_milestone_drop_down__c</name>
    </fields>
    <fields>
        <help><!-- Controls the pixel width for Project/Assignment dropdown on Timecard Entry UI. Please specify a whole number for this value, as this will directly go into width. For ex a value 300 will set the dropdown width to 300px. --></help>
        <label><!-- Width px for project assig drop down --></label>
        <name>pse__Width_px_for_project_assig_drop_down__c</name>
    </fields>
</CustomObjectTranslation>
