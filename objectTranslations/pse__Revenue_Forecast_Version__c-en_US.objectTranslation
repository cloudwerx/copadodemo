<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <help><!-- The ID for the Apex job executing the revenue forecast version batch process. --></help>
        <label><!-- Apex Job ID --></label>
        <name>pse__Apex_Job_Id__c</name>
    </fields>
    <fields>
        <help><!-- A lookup to the batch process record used for the revenue forecast version. --></help>
        <label><!-- Batch Process --></label>
        <name>pse__Batch_Process__c</name>
        <relationshipLabel><!-- Revenue Forecast Versions --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- The current status of the batch job. --></help>
        <label><!-- Batch Processing Status --></label>
        <name>pse__Batch_Processing_Status__c</name>
        <picklistValues>
            <masterLabel>Complete</masterLabel>
            <translation><!-- Complete --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Error</masterLabel>
            <translation><!-- Error --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pending</masterLabel>
            <translation><!-- Pending --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Processing</masterLabel>
            <translation><!-- Processing --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The corporate currency of the org at the time the revenue forecast was run. --></help>
        <label><!-- Corp: Currency --></label>
        <name>pse__Corp_Currency__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether probabilities were excluded from opportunity revenue forecasts when this revenue forecast version was created. --></help>
        <label><!-- Exclude Probability from Opportunities --></label>
        <name>pse__Exclude_Opportunity_Probabilities__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether the opportunity best case threshold was applied when this revenue forecast version was created. --></help>
        <label><!-- Include Best Case --></label>
        <name>pse__Include_Best_Case__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether the opportunity worst case threshold was applied when this revenue forecast version was created. --></help>
        <label><!-- Include Worst Case --></label>
        <name>pse__Include_Worst_Case__c</name>
    </fields>
    <fields>
        <help><!-- If selected, the revenue forecast version is locked and adjustments can no longer be made. --></help>
        <label><!-- Locked --></label>
        <name>pse__Locked__c</name>
    </fields>
    <fields>
        <help><!-- The opportunity best case threshold that was specified on the active revenue forecast setup record at the time this revenue forecast version was created. --></help>
        <label><!-- Opportunity Best Case Threshold (%) --></label>
        <name>pse__Opportunity_Best_Case_Threshold__c</name>
    </fields>
    <fields>
        <help><!-- The opportunity expected threshold that was specified on the active revenue forecast setup record at the time this revenue forecast version was created. --></help>
        <label><!-- Opportunity Expected Threshold (%) --></label>
        <name>pse__Opportunity_Expected_Case_Threshold__c</name>
    </fields>
    <fields>
        <help><!-- The opportunity worst case threshold that was specified on the active revenue forecast setup record at the time this revenue forecast version was created. --></help>
        <label><!-- Opportunity Worst Case Threshold (%) --></label>
        <name>pse__Opportunity_Worst_Case_Threshold__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether this revenue forecast version record relates to the current version of the forecast, or to the previously generated version. --></help>
        <label><!-- Status --></label>
        <name>pse__Status__c</name>
        <picklistValues>
            <masterLabel>Current</masterLabel>
            <translation><!-- Current --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Previous</masterLabel>
            <translation><!-- Previous --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The object that projects and opportunities are grouped under on the Review Forecast Version page. --></help>
        <label><!-- Object for Version Grouping --></label>
        <name>pse__Version_Grouping_Primary__c</name>
        <picklistValues>
            <masterLabel>Group</masterLabel>
            <translation><!-- Group --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Practice</masterLabel>
            <translation><!-- Practice --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Region</masterLabel>
            <translation><!-- Region --></translation>
        </picklistValues>
    </fields>
</CustomObjectTranslation>
