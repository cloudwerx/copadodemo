<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Ajustement divers</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Ajustements divers</value>
    </caseValues>
    <fields>
        <help>Si cette case est cochée, Admin permet d&apos;apporter des modifications «globales» à l&apos;adjuvant divers, y compris aux modifications apportées au projet, à la ressource, à la devise ou à la date du jeu. Même si l&apos;option Inclure dans les états financiers est cochée. Configuration requise: le mode de calcul réel doit être défini sur &quot;Planifié&quot;.</help>
        <label><!-- Admin Global Edit --></label>
        <name>pse__Admin_Global_Edit__c</name>
    </fields>
    <fields>
        <help>Le montant monétaire de l&apos;ajustement divers. La valeur par défaut est et doit toujours être la même devise que le projet.</help>
        <label><!-- Amount --></label>
        <name>pse__Amount__c</name>
    </fields>
    <fields>
        <help>Cette case à cocher doit être cochée lorsque le réglage divers est approuvé - généralement en fonction du champ Statut.</help>
        <label><!-- Approved --></label>
        <name>pse__Approved__c</name>
    </fields>
    <fields>
        <label><!-- Approved for Billing --></label>
        <name>pse__Approved_for_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Approved for Vendor Payment --></label>
        <name>pse__Approved_for_Vendor_Payment__c</name>
    </fields>
    <fields>
        <label><!-- Approver --></label>
        <name>pse__Approver__c</name>
        <relationshipLabel><!-- Miscellaneous Adjustments --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Audit Notes --></label>
        <name>pse__Audit_Notes__c</name>
    </fields>
    <fields>
        <label><!-- Bill Date --></label>
        <name>pse__Bill_Date__c</name>
    </fields>
    <fields>
        <label><!-- Bill Transaction --></label>
        <name>pse__Bill_Transaction__c</name>
        <relationshipLabel><!-- Miscellaneous Adjustment (Bill Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Billed --></label>
        <name>pse__Billed__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Invoiced --></label>
        <name>pse__Billing_Event_Invoiced__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Item --></label>
        <name>pse__Billing_Event_Item__c</name>
        <relationshipLabel><!-- Miscellaneous Adjustments --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Billing Event Released --></label>
        <name>pse__Billing_Event_Released__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Status --></label>
        <name>pse__Billing_Event_Status__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event --></label>
        <name>pse__Billing_Event__c</name>
    </fields>
    <fields>
        <label><!-- Billing Hold --></label>
        <name>pse__Billing_Hold__c</name>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>pse__Description__c</name>
    </fields>
    <fields>
        <label><!-- Effective Date --></label>
        <name>pse__Effective_Date__c</name>
    </fields>
    <fields>
        <help>Indique que l&apos;ajustement est dans un état éligible pour la génération d&apos;événement de facturation (sauf l&apos;indicateur Approuvé pour la facturation ou ayant une catégorie de transaction éligible, qui peut également être requise par configuration globale).</help>
        <label><!-- Eligible for Billing --></label>
        <name>pse__Eligible_for_Billing__c</name>
    </fields>
    <fields>
        <help>Si cette case est cochée, ne jamais facturer cet enregistrement professionnel. Même effet que le blocage de la facturation, mais destiné à refléter une exclusion permanente de la génération de facturation.</help>
        <label><!-- Exclude from Billing --></label>
        <name>pse__Exclude_from_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Include In Financials --></label>
        <name>pse__Include_In_Financials__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Date --></label>
        <name>pse__Invoice_Date__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Number --></label>
        <name>pse__Invoice_Number__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Transaction --></label>
        <name>pse__Invoice_Transaction__c</name>
        <relationshipLabel><!-- Miscellaneous Adjustment (Invoice Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Invoiced --></label>
        <name>pse__Invoiced__c</name>
    </fields>
    <fields>
        <label><!-- Override Project Group Currency Code --></label>
        <name>pse__Override_Project_Group_Currency_Code__c</name>
    </fields>
    <fields>
        <help>Si ce champ est défini, il remplace le groupe dans lequel les transactions de réévaluation sont groupées pour les données réelles du groupe, même si son projet appartient à un groupe différent. En règle générale, les transactions de réajustement se terminent dans le groupe de projets.</help>
        <label><!-- Override Project Group --></label>
        <name>pse__Override_Project_Group__c</name>
        <relationshipLabel><!-- Override Group For Miscellaneous Adjustments --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Project Practice Currency Code --></label>
        <name>pse__Override_Project_Practice_Currency_Code__c</name>
    </fields>
    <fields>
        <help>Si ce champ est défini, il remplace la pratique à laquelle les transactions de réajustement se déroulent pour les données réelles de pratique, même si son projet est dans une pratique différente. En règle générale, les transactions de réajustements divers sont intégrées à la pratique de son projet.</help>
        <label><!-- Override Project Practice --></label>
        <name>pse__Override_Project_Practice__c</name>
        <relationshipLabel><!-- Override Practice For Miscellaneous Adjustments --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Project Region Currency Code --></label>
        <name>pse__Override_Project_Region_Currency_Code__c</name>
    </fields>
    <fields>
        <help>Si ce champ est défini, il remplace la région à laquelle les transactions avec ajustement divers seront regroupées pour les statistiques régionales, même si son projet se trouve dans une autre région. En règle générale, une transaction de réajustement se déroule dans la région de son projet.</help>
        <label><!-- Override Project Region --></label>
        <name>pse__Override_Project_Region__c</name>
        <relationshipLabel><!-- Override Region For Miscellaneous Adjustments --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Miscellaneous Adjustments --></relationshipLabel>
    </fields>
    <fields>
        <help>La méthode de comptabilisation à appliquer à cet enregistrement pour la prévision des revenus.</help>
        <label><!-- Recognition Method --></label>
        <name>pse__Recognition_Method__c</name>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>pse__Status__c</name>
        <picklistValues>
            <masterLabel>Approved</masterLabel>
            <translation>Approuvé</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Draft</masterLabel>
            <translation>Brouillon</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rejected</masterLabel>
            <translation>Rejeté</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Submitted</masterLabel>
            <translation>Soumis</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Transaction Category --></label>
        <name>pse__Transaction_Category__c</name>
        <picklistValues>
            <masterLabel>Booked Revenue</masterLabel>
            <translation>Revenu réservé</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Budgeted Cost</masterLabel>
            <translation>Coût budgété</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Budgeted Expense Cost</masterLabel>
            <translation>Coût budgété des dépenses</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Expense Budget</masterLabel>
            <translation>Budget des dépenses</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Expense Cost</masterLabel>
            <translation>Coût des dépenses</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>External Cost</masterLabel>
            <translation>Coût externe</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Internal Budget</masterLabel>
            <translation>Budget interne</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Internal Cost</masterLabel>
            <translation>Coût interne</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Other Cost</masterLabel>
            <translation>Autre coût</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pass-Through Ready-to-Bill Revenue</masterLabel>
            <translation>Chiffre d&apos;affaires Prêt-à-facturer</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pre-Billed Revenue</masterLabel>
            <translation>Recettes pré-facturées</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Ready-to-Bill Revenue</masterLabel>
            <translation>Chiffre d&apos;affaires prêt-à-facturer</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Revenue</masterLabel>
            <translation>Revenu</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Transaction --></label>
        <name>pse__Transaction__c</name>
        <relationshipLabel><!-- Miscellaneous Adjustment --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Vendor Account --></label>
        <name>pse__Vendor_Account__c</name>
        <relationshipLabel><!-- Miscellaneous Adjustments --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Vendor Invoice Item --></label>
        <name>pse__Vendor_Invoice_Item__c</name>
        <relationshipLabel><!-- Miscellaneous Adjustments --></relationshipLabel>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Nom de l&apos;ajustement divers</nameFieldLabel>
    <sharingReasons>
        <label><!-- PSE Approver Share --></label>
        <name>pse__PSE_Approver_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE Member Share --></label>
        <name>pse__PSE_Member_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE PM Share --></label>
        <name>pse__PSE_PM_Share</name>
    </sharingReasons>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <errorMessage><!-- Invoice Number or Date is not allowed on record unless it is invoiced --></errorMessage>
        <name>pse__Invoiced</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- For a Miscellaneous Adjustment to be marked as Billed, it must also be marked as Included In Financials and Approved. --></errorMessage>
        <name>pse__Misc_Adj_Billing_Requires_Inclusion</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- For a Miscellaneous Adjustment to be marked as Invoiced, it must also be marked as Billed. --></errorMessage>
        <name>pse__Misc_Adj_Invoicing_Requires_Billing</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Miscellaneous Adjustment&apos;s Project field value may not be blank and may not be updated once set (unless Admin Global Edit is checked, Audit Notes are provided, and Actuals Calculation Mode is Scheduled). --></errorMessage>
        <name>pse__Misc_Adj_Project_is_Constant</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Vendor Invoice Item cannot be changed --></errorMessage>
        <name>pse__Vendor_Invoice_Constant</name>
    </validationRules>
    <webLinks>
        <label><!-- Clear_Billing_Data --></label>
        <name>pse__Clear_Billing_Data</name>
    </webLinks>
</CustomObjectTranslation>
