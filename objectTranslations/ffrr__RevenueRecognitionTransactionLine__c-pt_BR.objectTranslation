<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Linha de transação de reconhecimento de receita</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Linhas de transação de reconhecimento de receita</value>
    </caseValues>
    <fieldSets>
        <label><!-- Summarization Fields --></label>
        <name>ffrr__Summarization_Fields</name>
    </fieldSets>
    <fields>
        <label><!-- GLA Type --></label>
        <name>ffrr__AccountType__c</name>
    </fields>
    <fields>
        <label><!-- GLA Revenue --></label>
        <name>ffrr__Account__c</name>
    </fields>
    <fields>
        <label><!-- Actual vs. Forecast Record --></label>
        <name>ffrr__ActualVsForecastRelationship__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Amortized Opening Balance Dual --></label>
        <name>ffrr__AmortizedOpeningBalanceDual__c</name>
    </fields>
    <fields>
        <label><!-- Amortized Opening Balance Home --></label>
        <name>ffrr__AmortizedOpeningBalanceHome__c</name>
    </fields>
    <fields>
        <help>Valor copiado do campo Amortizado até a data desse registro de origem antes que existisse qualquer transação de reconhecimento de receita. Normalmente, um saldo inicial é definido ao migrar os dados para o Revenue Management.</help>
        <label><!-- Amortized Opening Balance --></label>
        <name>ffrr__AmortizedOpeningBalance__c</name>
    </fields>
    <fields>
        <help>O valor acumulado amortizado até a data, na moeda do documento, para o registro de origem nesta linha de transação.</help>
        <label><!-- Amortized To Date --></label>
        <name>ffrr__AmortizedToDate__c</name>
    </fields>
    <fields>
        <help>Montante amortizado em dupla moeda.</help>
        <label><!-- Amount Amortized (Dual) --></label>
        <name>ffrr__AmountAmortizedDual__c</name>
    </fields>
    <fields>
        <help>Montante amortizado em moeda nacional.</help>
        <label><!-- Amount Amortized (Home) --></label>
        <name>ffrr__AmountAmortizedHome__c</name>
    </fields>
    <fields>
        <label><!-- Amount Amortized --></label>
        <name>ffrr__AmountAmortized__c</name>
    </fields>
    <fields>
        <help>Quantia reconhecida em moeda dupla</help>
        <label><!-- Amount Recognized (Dual) --></label>
        <name>ffrr__AmountRecognizedDual__c</name>
    </fields>
    <fields>
        <help>Quantia reconhecida na moeda local</help>
        <label><!-- Amount Recognized (Home) --></label>
        <name>ffrr__AmountRecognizedHome__c</name>
    </fields>
    <fields>
        <label><!-- Amount Recognized --></label>
        <name>ffrr__AmountRecognized__c</name>
    </fields>
    <fields>
        <help>Valor de relatório 1.</help>
        <label><!-- Analysis Item 1 --></label>
        <name>ffrr__AnalysisItem1__c</name>
    </fields>
    <fields>
        <help>Valor de relatório 2.</help>
        <label><!-- Analysis Item 2 --></label>
        <name>ffrr__AnalysisItem2__c</name>
    </fields>
    <fields>
        <help>Valor de relatório 3.</help>
        <label><!-- Analysis Item 3 --></label>
        <name>ffrr__AnalysisItem3__c</name>
    </fields>
    <fields>
        <help>Valor de relatório 4.</help>
        <label><!-- Analysis Item 4 --></label>
        <name>ffrr__AnalysisItem4__c</name>
    </fields>
    <fields>
        <label><!-- Company --></label>
        <name>ffrr__Company__c</name>
    </fields>
    <fields>
        <label><!-- GLA Cost --></label>
        <name>ffrr__CostAccount__c</name>
    </fields>
    <fields>
        <label><!-- Cost Amended --></label>
        <name>ffrr__CostAmended__c</name>
    </fields>
    <fields>
        <label><!-- Cost Center --></label>
        <name>ffrr__CostCenter__c</name>
    </fields>
    <fields>
        <label><!-- Cost Center (Cost) --></label>
        <name>ffrr__CostCostCenter__c</name>
    </fields>
    <fields>
        <label><!-- Document Currency --></label>
        <name>ffrr__Currency__c</name>
    </fields>
    <fields>
        <help>A taxa de câmbio que converte a moeda do documento para a moeda local.</help>
        <label><!-- Document Rate --></label>
        <name>ffrr__DocumentCurrencyRate__c</name>
    </fields>
    <fields>
        <help>A taxa de câmbio que converte a moeda local em moeda dupla.</help>
        <label><!-- Dual Rate --></label>
        <name>ffrr__DualCurrencyRate__c</name>
    </fields>
    <fields>
        <help>A moeda dupla.</help>
        <label><!-- Dual Currency --></label>
        <name>ffrr__DualCurrency__c</name>
    </fields>
    <fields>
        <help>A moeda local.</help>
        <label><!-- Home Currency --></label>
        <name>ffrr__HomeCurrency__c</name>
    </fields>
    <fields>
        <label><!-- Internal Amount (Amortized) --></label>
        <name>ffrr__InternalAmortizedAmount__c</name>
    </fields>
    <fields>
        <label><!-- Internal Amount --></label>
        <name>ffrr__InternalAmount__c</name>
    </fields>
    <fields>
        <help>O valor amortizado com o sinal invertido.</help>
        <label><!-- Journal Amount (Amortized) --></label>
        <name>ffrr__JournalAmortizedAmount__c</name>
    </fields>
    <fields>
        <help>O valor amortizado em moeda dupla, com sinal invertido.</help>
        <label><!-- Journal Amount Dual (Amortized) --></label>
        <name>ffrr__JournalAmortizedDualAmount__c</name>
    </fields>
    <fields>
        <help>O valor amortizado em moeda nacional, com sinal invertido.</help>
        <label><!-- Journal Amount Home (Amortized) --></label>
        <name>ffrr__JournalAmortizedHomeAmount__c</name>
    </fields>
    <fields>
        <help>O valor reconhecido em dupla moeda, com sinal invertido.</help>
        <label><!-- Journal Amount Dual --></label>
        <name>ffrr__JournalAmountDual__c</name>
    </fields>
    <fields>
        <help>O valor reconhecido na moeda nacional, com o sinal invertido.</help>
        <label><!-- Journal Amount Home --></label>
        <name>ffrr__JournalAmountHome__c</name>
    </fields>
    <fields>
        <help>O valor reconhecido com o sinal invertido.</help>
        <label><!-- Journal Amount --></label>
        <name>ffrr__JournalAmount__c</name>
    </fields>
    <fields>
        <label><!-- Level 3 Object Record Name --></label>
        <name>ffrr__Level3ObjectRecordName__c</name>
    </fields>
    <fields>
        <label><!-- Level 4 Object Record Name --></label>
        <name>ffrr__Level4ObjectRecordName__c</name>
    </fields>
    <fields>
        <label><!-- Performance Obligation --></label>
        <name>ffrr__PerformanceObligation__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Period --></label>
        <name>ffrr__Period__c</name>
    </fields>
    <fields>
        <label><!-- Primary Object Record Name --></label>
        <name>ffrr__PrimaryObjectRecordName__c</name>
    </fields>
    <fields>
        <help>Procure o produto na linha de transação.</help>
        <label><!-- Product --></label>
        <name>ffrr__Product__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Recognized Date --></label>
        <name>ffrr__RecognizedDate__c</name>
    </fields>
    <fields>
        <label><!-- Recognized Opening Balance Dual --></label>
        <name>ffrr__RecognizedOpeningBalanceDual__c</name>
    </fields>
    <fields>
        <label><!-- Recognized Opening Balance Home --></label>
        <name>ffrr__RecognizedOpeningBalanceHome__c</name>
    </fields>
    <fields>
        <help>Valor copiado do campo Reconhecido até a data desse registro de origem antes que existisse qualquer transação de reconhecimento de receita. Normalmente, um saldo inicial é definido ao migrar os dados para o Revenue Management.</help>
        <label><!-- Recognized Opening Balance --></label>
        <name>ffrr__RecognizedOpeningBalance__c</name>
    </fields>
    <fields>
        <label><!-- Recognized Record ID --></label>
        <name>ffrr__RecognizedRecordID__c</name>
    </fields>
    <fields>
        <help>O valor acumulado reconhecido até a data, na moeda do documento, para o registro de origem nesta linha de transação.</help>
        <label><!-- Recognized To Date --></label>
        <name>ffrr__RecognizedToDate__c</name>
    </fields>
    <fields>
        <label><!-- Revenue Amended --></label>
        <name>ffrr__RevenueAmended__c</name>
    </fields>
    <fields>
        <label><!-- Revenue Contract --></label>
        <name>ffrr__RevenueContract__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Revenue Recognition Transaction --></label>
        <name>ffrr__RevenueRecognitionMaster__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <fields>
        <help>Procure a conta Salesforce na linha de transação.</help>
        <label><!-- Account --></label>
        <name>ffrr__SalesforceAccount__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Level 2 Object Record Name --></label>
        <name>ffrr__SecondaryObjectRecordName__c</name>
    </fields>
    <fields>
        <label><!-- Template --></label>
        <name>ffrr__Template__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Número da linha de transação</nameFieldLabel>
</CustomObjectTranslation>
