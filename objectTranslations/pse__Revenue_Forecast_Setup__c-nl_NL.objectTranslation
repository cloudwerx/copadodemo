<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Omzetprognose Setup</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Omzetprognose Setup</value>
    </caseValues>
    <fields>
        <help>Geeft het record voor het instellen van de omzetprognose aan dat wordt gebruikt voor de berekening van de omzetprognose. U kunt slechts één actief record voor het instellen van omzetprognoses hebben.</help>
        <label><!-- Active --></label>
        <name>pse__Active__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld dat is ingesteld op het object Details omzetprognoseversie dat de project-, opportunity- en mijlpaalvelden bevat die u als aangepaste filters wilt gebruiken op de pagina Review prognoseversie.</help>
        <label><!-- Custom Filter Field Set --></label>
        <name>pse__Custom_Filter_Field_Set__c</name>
    </fields>
    <fields>
        <help>Indien geselecteerd, worden opportunities uitgesloten van versies van omzetprognoses.</help>
        <label><!-- Exclude Opportunities for Versions --></label>
        <name>pse__Exclude_Opportunities_For_Versions__c</name>
    </fields>
    <fields>
        <help>Als deze optie is geselecteerd, worden kansen uitgesloten van geplande omzetvoorspellingen.</help>
        <label><!-- Exclude Opportunities --></label>
        <name>pse__Exclude_Opportunities__c</name>
    </fields>
    <fields>
        <help>Indien geselecteerd, worden kansen niet toegepast op prognoses voor verkoopkansen.</help>
        <label><!-- Exclude Probability from Opportunities --></label>
        <name>pse__Exclude_Opportunity_Probabilities__c</name>
    </fields>
    <fields>
        <help>Selecteer dit om inkomsten uit te sluiten die betrekking hebben op vastgehouden aanvragen van middelen voor projecten en mijlpalen.</help>
        <label><!-- Exclude Resource Requests on Project --></label>
        <name>pse__Exclude_Resource_Requests_On_Project__c</name>
    </fields>
    <fields>
        <help>Indien geselecteerd, worden de werkelijke waarden voor opbrengstverantwoording uit de integratie met Opbrengstbeheer niet langer meegenomen in de berekening van de opbrengstprognose.</help>
        <label><!-- Exclude Revenue Recognition Actuals --></label>
        <name>pse__Exclude_Revenue_Recognition_Actuals__c</name>
    </fields>
    <fields>
        <help>Indien geselecteerd, worden alle ongeplande inkomsten uit projecten of mijlpalen uitgesloten van versies van inkomstenprognoses.</help>
        <label><!-- Exclude Unscheduled Revenue for Versions --></label>
        <name>pse__Exclude_Unscheduled_Revenue_For_Versions__c</name>
    </fields>
    <fields>
        <help>Aantal records dat tegelijkertijd moet worden vewerkt, zoals tijdkaarten en onkosten.</help>
        <label><!-- Forecast Factor Batch Size --></label>
        <name>pse__Forecast_Factor_Batch_Size__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld dat is ingesteld op het Milestone-object dat de hover-details bestuurt die worden weergegeven voor een mijlpaal in het Forecast Breakdown-raster. Als u geen veldset invoert, zijn de standaard hover-details van toepassing.</help>
        <label><!-- Hover Details Field Set on Milestone --></label>
        <name>pse__Hover_Details_Field_Set_On_Milestone__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld dat is ingesteld op het Opportunity-object dat de hover-details bestuurt die worden weergegeven voor een opportunity in het prognosespecificatiesraster. Als u geen veldset invoert, zijn de standaard hover-details van toepassing.</help>
        <label><!-- Hover Details Field Set on Opportunity --></label>
        <name>pse__Hover_Details_Field_Set_On_Opportunity__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld dat is ingesteld op het Project-object dat de hover-details bestuurt die voor een project worden weergegeven in het Forecast Breakdown-raster. Als u geen veldset invoert, zijn de standaard hover-details van toepassing.</help>
        <label><!-- Hover Details Field Set on Project --></label>
        <name>pse__Hover_Details_Field_Set_On_Project__c</name>
    </fields>
    <fields>
        <help>Indien geselecteerd, wordt de drempel voor het beste geval toegepast op prognoses voor verkoopkansen.</help>
        <label><!-- Include Best Case --></label>
        <name>pse__Include_Best_Case__c</name>
    </fields>
    <fields>
        <help>Indien geselecteerd, wordt de ongunstigste drempel toegepast op prognoses voor verkoopkansen.</help>
        <label><!-- Include Worst Case --></label>
        <name>pse__Include_Worst_Case__c</name>
    </fields>
    <fields>
        <help>Aantal opportunities en opportunityregelitems die tegelijkertijd moeten worden verwerkt.</help>
        <label><!-- Opportunity Batch Size --></label>
        <name>pse__Opportunity_Batch_Size__c</name>
    </fields>
    <fields>
        <help>De minimale procentuele kans dat een kans wordt opgenomen in het beste scenario.</help>
        <label><!-- Opportunity Best Case Threshold (%) --></label>
        <name>pse__Opportunity_Best_Case_Threshold__c</name>
    </fields>
    <fields>
        <help>De minimale procentuele kans dat een kans wordt opgenomen in het verwachte scenario.</help>
        <label><!-- Opportunity Expected Threshold (%) --></label>
        <name>pse__Opportunity_Expected_Case_Threshold__c</name>
    </fields>
    <fields>
        <help>De minimale procentuele kans dat een kans wordt opgenomen in het worstcasescenario.</help>
        <label><!-- Opportunity Worst Case Threshold (%) --></label>
        <name>pse__Opportunity_Worst_Case_Threshold__c</name>
    </fields>
    <fields>
        <help>Aantal projecten dat tegelijkertijd verwerkt moet worden</help>
        <label><!-- Project Batch Size --></label>
        <name>pse__Project_Batch_Size__c</name>
    </fields>
    <fields>
        <help>Voer de API-naam in van het veld op het Opportunity-object dat u wilt gebruiken als de verwachte einddatum van het project.</help>
        <label><!-- Expected Project End Date Field on Opp --></label>
        <name>pse__Project_End_Date_on_Opportunity__c</name>
    </fields>
    <fields>
        <help>Voer de API-naam in van het veld op het Opportunity-object dat u wilt gebruiken als de verwachte einddatum van het project.</help>
        <label><!-- Expected Project Start Date Field on Opp --></label>
        <name>pse__Project_Start_Date_on_Opportunity__c</name>
    </fields>
    <fields>
        <help>Indien geselecteerd wanneer u de integratie tussen PSA en Revenue Management niet gebruikt, behoudt Revenue Forecasting de werkelijke waarden van de vorige prognoserun voor elke gesloten periode. Alle resterende inkomsten die in behandeling zijn, worden verplaatst naar de volgende open periode.</help>
        <label><!-- Retain Pending Revenue in Closed Periods --></label>
        <name>pse__Retain_Pending_Revenue_In_Closed_Periods__c</name>
    </fields>
    <fields>
        <help>Selecteer de API-naam van het veld op het projectobject dat u wilt gebruiken om de waarde te berekenen in het veld% Hours Completed For Recognition. Indien nodig kunt u uw eigen veld maken en toevoegen aan de keuzelijst.</help>
        <label><!-- Total Hours Field on Project --></label>
        <name>pse__Total_Hours_Field_on_Project__c</name>
        <picklistValues>
            <masterLabel>pse__Estimated_Hours_at_Completion__c</masterLabel>
            <translation>pse__Estimated_Hours_at_Completion__c</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>pse__Planned_Hours__c</masterLabel>
            <translation>pse__Planned_Hours__c</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Aantal versiedetailrecords dat tegelijkertijd moet worden verwerkt.</help>
        <label><!-- Version Detail Batch Size --></label>
        <name>pse__Version_Detail_Batch_Size__c</name>
    </fields>
    <fields>
        <help>De prognoseperiode die wordt gedekt door de versie van de inkomstenprognose.</help>
        <label><!-- Version Forecast Period --></label>
        <name>pse__Version_Forecast_Period__c</name>
        <picklistValues>
            <masterLabel>Current Quarter</masterLabel>
            <translation>Huidige kwartaal</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Current Year</masterLabel>
            <translation>Huidige jaar</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Current and Next Quarter</masterLabel>
            <translation>Huidig en volgend kwartaal</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rolling 12 Months</masterLabel>
            <translation>Rolling 12 maanden</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rolling 3 Months</masterLabel>
            <translation>Rolling 3 maanden</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rolling 6 Months</masterLabel>
            <translation>Rolling 6 maanden</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Het object waar projecten en opportunities onder zijn gegroepeerd op de pagina Review prognoseversie.</help>
        <label><!-- Object for Version Grouping --></label>
        <name>pse__Version_Grouping_Primary__c</name>
        <picklistValues>
            <masterLabel>Group</masterLabel>
            <translation>Groep</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Practice</masterLabel>
            <translation>Praktijk</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Region</masterLabel>
            <translation>Regio</translation>
        </picklistValues>
    </fields>
    <gender>Feminine</gender>
    <nameFieldLabel>Omzetprognose Setup Naam</nameFieldLabel>
</CustomObjectTranslation>
