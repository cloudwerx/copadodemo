<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <article>Definite</article>
        <plural>false</plural>
        <value>Inställningar</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>false</plural>
        <value>Inställningar</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>false</plural>
        <value>Inställningar</value>
    </caseValues>
    <caseValues>
        <article>Definite</article>
        <plural>true</plural>
        <value>Inställningar</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>true</plural>
        <value>Inställningar</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>true</plural>
        <value>Inställningar</value>
    </caseValues>
    <fields>
        <help>API-namnet på fältet på källobjektet som innehåller information att visa om en post. Ofta kommer detta att vara det vanliga Salesforce-kontot, men det kan vara valfri text, vallista, uppslag, nummer, autonummer eller formelfält.</help>
        <label><!-- Display Information --></label>
        <name>ffrr__AccountName__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet på källobjektet som indikerar om det är aktivt. Om du vill inkludera poster i Inkomsthantering när ett fält har ett specifikt värde anger du fältnamnet i Aktivt fält, värdet i Aktivt värde och anger Inkludera Active Value to True.</help>
        <label><!-- Active Field --></label>
        <name>ffrr__ActiveField__c</name>
    </fields>
    <fields>
        <help>Värde i det aktiva fältet som anger om källposten är aktiv.</help>
        <label><!-- Active Value --></label>
        <name>ffrr__ActiveValue__c</name>
    </fields>
    <fields>
        <help>API-namnet på uppslagningsfältet (till källobjektet för den här inställningen) som din administratör har lagt till i Actuals vs Forecast Line-objektet. Det här fältet är obligatoriskt när alternativet Använd aktuell vs prognosfunktion är aktiverad.</help>
        <label><!-- Actual vs Forecast Line Lookup --></label>
        <name>ffrr__ActualVsForecastRelationship__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet Nummer eller Valuta på källobjektet som lagrar kostnaden som avskrivits hittills. Om befolkade uppdateras värdet på källposten automatiskt varje gång en intäktsgenkänningstransaktion har gjorts.</help>
        <label><!-- Amortized To Date Value --></label>
        <name>ffrr__AmortizedToDateValue__c</name>
    </fields>
    <fields>
        <help>API-namn på textfältet, uppslagning eller picklist som representerar rapporteringsvärde 1. Detta kan antingen vara ett fält på källobjektet eller genom att använda uppslagningsrelationer kan du referera till ett fält på ett objekt som är länkat med en sökväg på upp till fem sökningar.</help>
        <label><!-- Analysis Item 1 --></label>
        <name>ffrr__AnalysisItem1__c</name>
    </fields>
    <fields>
        <help>API-namnet på textfältet, uppslagning eller picklist som representerar rapporteringsvärde 2. Detta kan antingen vara ett fält på källobjektet eller genom att använda uppslagningsrelationer kan du referera till ett fält på ett objekt som är länkat med en sökväg på upp till fem sökningar.</help>
        <label><!-- Analysis Item 2 --></label>
        <name>ffrr__AnalysisItem2__c</name>
    </fields>
    <fields>
        <help>API-namnet på textfältet, uppslagning eller picklist som representerar rapporteringsvärde 3. Detta kan antingen vara ett fält på källobjektet, eller genom att använda uppslagningsrelationer kan du referera till ett fält på ett objekt som är länkat med en sökväg på upp till fem sökningar.</help>
        <label><!-- Analysis Item 3 --></label>
        <name>ffrr__AnalysisItem3__c</name>
    </fields>
    <fields>
        <help>API-namn på textfältet, uppslagning eller picklist som representerar rapporteringsvärde 4. Detta kan antingen vara ett fält på källobjektet, eller genom att använda uppslagningsrelationer kan du referera till ett fält på ett objekt länkat med en sökväg på upp till fem sökningar.</help>
        <label><!-- Analysis Item 4 --></label>
        <name>ffrr__AnalysisItem4__c</name>
    </fields>
    <fields>
        <help>API-namn på textfältet, uppslaget eller picklisten på källobjektet som representerar balansräkningen GLA till vilken intäktsjournalerna bokförs.</help>
        <label><!-- Balance Sheet GLA --></label>
        <name>ffrr__BalanceSheetAccount__c</name>
    </fields>
    <fields>
        <label><!-- Billed To Date (Reserved) --></label>
        <name>ffrr__BilledToDate__c</name>
    </fields>
    <fields>
        <label><!-- Chain ID --></label>
        <name>ffrr__ChainID__c</name>
    </fields>
    <fields>
        <help>API Namn på textfältet, uppslaget eller picklisten som representerar företaget. Detta kan antingen vara ett fält på källobjektet, eller genom att använda uppslagningsrelationer kan du referera till ett fält på ett objekt som är länkat med en sökväg på upp till fem sökningar.</help>
        <label><!-- Company --></label>
        <name>ffrr__Company__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet på källobjektet som anger dess slutförandestatus. Om en mall för leveransbarhet skapas för denna inställning, kommer intäkterna att erkännas när källposten är klar.</help>
        <label><!-- Completed Field --></label>
        <name>ffrr__CompletedField__c</name>
    </fields>
    <fields>
        <help>Värde på det aktiva fältet som anger om källposten är klar. Om en mall för leveransbarhet skapas för denna inställning, kommer intäkterna att erkännas när källposten är klar.</help>
        <label><!-- Completed Value --></label>
        <name>ffrr__CompletedValue__c</name>
    </fields>
    <fields>
        <help>API-namnet på textfältet, uppslaget eller picklisten på källobjektet som representerar balansräkningen GLA till vilken kostnadsdagarna publiceras.</help>
        <label><!-- Balance Sheet GLA (Cost) --></label>
        <name>ffrr__CostBalanceSheetAccount__c</name>
    </fields>
    <fields>
        <help>API-namn på textfältet, lookup eller picklist på källobjektet som representerar den specifika komponenten i ditt kontoplan där dina förvaltningskonton är delanalyserade.</help>
        <label><!-- Cost Center --></label>
        <name>ffrr__CostCenter__c</name>
    </fields>
    <fields>
        <help>API-namn på textfältet, lookup eller picklist på källobjektet som representerar den specifika komponenten i ditt kontoplan där dina förvaltningskonton är delanalyserade.</help>
        <label><!-- Cost Center (Cost) --></label>
        <name>ffrr__CostCostCenter__c</name>
    </fields>
    <fields>
        <help>API-namn på textfältet, uppslaget eller plocklistan på källobjektet som representerar resultaträkningen GLA till vilken kostnadsdagarna publiceras.</help>
        <label><!-- Income Statement GLA (Cost) --></label>
        <name>ffrr__CostIncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet på källobjektet som representerar hastigheten vid vilken kostnadsenheter debiteras.</help>
        <label><!-- Cost Rate --></label>
        <name>ffrr__CostRate__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet Nummer på källobjektet som representerar det totala antalet kostnadsenheter som debiteras. Kostnadsenheternas värde kan vara det totala antalet timmar eller dagar som arbetat på ett projekt till exempel.</help>
        <label><!-- Total Cost Units --></label>
        <name>ffrr__CostTotalUnits__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet Text, Lookup eller Picklist på källobjektet som representerar källpostens valuta.</help>
        <label><!-- Document Currency --></label>
        <name>ffrr__Currency__c</name>
    </fields>
    <fields>
        <help>Fältmappningsdefinitionen Populär på Prestationsförpliktelse Linjeposter som standard när du länkar dem till ett källpost och dessa inställningar.</help>
        <label><!-- Default Field Mapping Definition --></label>
        <name>ffrr__DefaultFieldMappingDefinition__c</name>
        <relationshipLabel><!-- Settings --></relationshipLabel>
    </fields>
    <fields>
        <help>API-namn för nummerfältet på källobjektet som lagrar den uppskjutna intäkten hittills i dubbel valuta.</help>
        <label><!-- FOR FUTURE USE Deferred Rev (Dual) --></label>
        <name>ffrr__DeferredRevenueToDateDualCurrency__c</name>
    </fields>
    <fields>
        <help>API-namn för fältet Number på källobjektet som lagrar den uppskjutna intäkten hittills i hemvaluta.</help>
        <label><!-- FOR FUTURE USE Deferred Rev (Home) --></label>
        <name>ffrr__DeferredRevenueToDateHomeCurrency__c</name>
    </fields>
    <fields>
        <help>API-namn för fältet Number på källobjektet som lagrar den uppskjutna intäkten hittills i rapporteringsvaluta.</help>
        <label><!-- FOR FUTURE USE Deferred Rev (Reporting) --></label>
        <name>ffrr__DeferredRevenueToDateReportingCurrency__c</name>
    </fields>
    <fields>
        <help>API-namn för fältet Number på källobjektet som lagrar den uppskjutna intäkten hittills i dokumentvaluta.</help>
        <label><!-- FOR FUTURE USE Deferred Rev --></label>
        <name>ffrr__DeferredRevenueToDate__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet Text eller Långt textområde på källobjektet som innehåller en beskrivning.</help>
        <label><!-- Description --></label>
        <name>ffrr__Description__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet Dubbel eller Valuta på källobjektet som representerar dokumentet till hemvalutakurs.</help>
        <label><!-- Document Rate --></label>
        <name>ffrr__DocumentCurrencyRate__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet Dubbel eller Valuta på källobjektet som representerar bokföringsföretagets hem till dubbelvalutakurs.</help>
        <label><!-- Dual Rate --></label>
        <name>ffrr__DualCurrencyRate__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet Text, Lookup eller Picklist på källobjektet som representerar bokföringsföretagets dubbla valuta. Dubbelvaluta är vanligtvis en företagsvaluta som används för rapporteringsändamål.</help>
        <label><!-- Dual Currency --></label>
        <name>ffrr__DualCurrency__c</name>
    </fields>
    <fields>
        <help>API-namnet på datum- eller datumtidsfältet på källobjektet som representerar sitt slutdatum.</help>
        <label><!-- End Date/Deliverable Date --></label>
        <name>ffrr__EndDate__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Filter 1 --></label>
        <name>ffrr__Filter1__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Filter 2 --></label>
        <name>ffrr__Filter2__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Filter 3 --></label>
        <name>ffrr__Filter3__c</name>
    </fields>
    <fields>
        <help>Välj att ange att motsvarande värde är en fast kod, inte ett API-namn på det relaterade källobjektet. Dessa kryssrutor kan ställas in oberoende av varandra.</help>
        <label><!-- Fixed Balance Sheet --></label>
        <name>ffrr__FixedBalanceSheetAccountCode__c</name>
    </fields>
    <fields>
        <help>Välj att ange att motsvarande värde är en fast kod, inte ett API-namn på det relaterade källobjektet. Dessa kryssrutor kan ställas in oberoende av varandra.</help>
        <label><!-- Fixed Balance Sheet (Cost) --></label>
        <name>ffrr__FixedCostBalanceSheetAccountCode__c</name>
    </fields>
    <fields>
        <help>Välj att ange att motsvarande värde är en fast kod, inte ett API-namn på det relaterade källobjektet. Dessa kryssrutor kan ställas in oberoende av varandra.</help>
        <label><!-- Fixed Cost Center --></label>
        <name>ffrr__FixedCostCenterCode__c</name>
    </fields>
    <fields>
        <help>Välj att ange att motsvarande värde är en fast kod, inte ett API-namn på det relaterade källobjektet. Dessa kryssrutor kan ställas in oberoende av varandra.</help>
        <label><!-- Fixed Cost Center (Cost) --></label>
        <name>ffrr__FixedCostCostCenterCode__c</name>
    </fields>
    <fields>
        <help>Välj att ange att motsvarande värde är en fast kod, inte ett API-namn på det relaterade källobjektet. Dessa kryssrutor kan ställas in oberoende av varandra.</help>
        <label><!-- Fixed Income Statement (Cost) --></label>
        <name>ffrr__FixedCostIncomeStatementAccountCode__c</name>
    </fields>
    <fields>
        <help>Välj att ange att motsvarande värde är en fast kod, inte ett API-namn på det relaterade källobjektet. Dessa kryssrutor kan ställas in oberoende av varandra.</help>
        <label><!-- Fixed Income Statement --></label>
        <name>ffrr__FixedIncomeStatementAccountCode__c</name>
    </fields>
    <fields>
        <help>Välj för att ange att motsvarande värde är en fast kod, inte ett API-namn på det relaterade källobjektet. Dessa kryssrutor kan ställas in oberoende av varandra.</help>
        <label><!-- FOR FUTURE USE Fixed True-up (Dual) --></label>
        <name>ffrr__FixedTrueUpDualAccountCode__c</name>
    </fields>
    <fields>
        <help>Välj för att ange att motsvarande värde är en fast kod, inte ett API-namn på det relaterade källobjektet. Dessa kryssrutor kan ställas in oberoende av varandra.</help>
        <label><!-- FOR FUTURE USE Fixed True-up (Home) --></label>
        <name>ffrr__FixedTrueUpHomeAccountCode__c</name>
    </fields>
    <fields>
        <help>API-namnet på sökfältet (till källobjektet på den här inställningsnivån) som administratören har lagt till i transaktionsobjektet. Detta måste fyllas i om inställningstypen är Prognos och inställningsnivån är Primär.</help>
        <label><!-- Revenue Forecast Transaction Lookup --></label>
        <name>ffrr__ForecastHeaderPrimaryRelationship__c</name>
    </fields>
    <fields>
        <help>API-namnet på uppslagningsfältet (till källobjektet på den här inställningsnivån) som administratören har lagt till i objektet Prognostprognos Transaktionslinje. Detta måste fyllas i om inställningstypen är Prognos.</help>
        <label><!-- Revenue Forecast Transaction Line Lookup --></label>
        <name>ffrr__ForecastTransactionLineRelationship__c</name>
    </fields>
    <fields>
        <label><!-- Group Name --></label>
        <name>ffrr__GroupName__c</name>
    </fields>
    <fields>
        <label><!-- Group --></label>
        <name>ffrr__Group__c</name>
    </fields>
    <fields>
        <help>API-namnet på textfältet, uppslaget eller plocklistan på källobjektet som representerar den specifika komponenten du vill gruppera efter processen Recognize All.</help>
        <label><!-- Grouped By --></label>
        <name>ffrr__GroupedBy__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet Text, Lookup eller Picklist på källobjektet som representerar bokföringsföretagets hemvaluta. Hemvaluta är företagets huvudsakliga arbete och rapportering, valuta.</help>
        <label><!-- Home Currency --></label>
        <name>ffrr__HomeCurrency__c</name>
    </fields>
    <fields>
        <help>Inkludera eller uteslut källkataloger med värdet definierat i fältet Aktivt värde.</help>
        <label><!-- Include Active Value --></label>
        <name>ffrr__IncludeActiveValue__c</name>
        <picklistValues>
            <masterLabel>Exclude</masterLabel>
            <translation>Utesluta</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Include</masterLabel>
            <translation>Omfatta</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Inkludera eller uteslut källordningar med värdet definierat i fältet Slutfört värde.</help>
        <label><!-- Include Completed Value --></label>
        <name>ffrr__IncludeCompletedValue__c</name>
        <picklistValues>
            <masterLabel>Exclude</masterLabel>
            <translation>Utesluta</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Include</masterLabel>
            <translation>Omfatta</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>API-namn på textfältet, uppslaget eller plocklistan på källobjektet som representerar resultaträkningen GLA till vilken intäktsjournalerna bokförs.</help>
        <label><!-- Income Statement GLA --></label>
        <name>ffrr__IncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>API-namnet på objektet som fungerar som datakälla på den här nivån. Detta kan inte ändras när inställningen används på en mall. För icke-primära inställningar måste objektet vara i en huvuddetalj eller uppslagsrelation med objektet i dess föräldrainställning.</help>
        <label><!-- Object --></label>
        <name>ffrr__Object__c</name>
    </fields>
    <fields>
        <help>API-namnet på uppslagningsfältet (till källobjektet för den här inställningen) som administratören har lagt till i objektobjektet Obligationslinjeobjekt.</help>
        <label><!-- POLI Source Lookup --></label>
        <name>ffrr__POLISourceField__c</name>
    </fields>
    <fields>
        <help>API-namn på uppslagsfält på källobjektet som beskrivs av denna inställningspost, eller länkat till källobjektet med en relationsbana på upp till 5 uppslag, vars värden fyller i intäktsplaneringsraden. När du anger flera fält måste de vara kommaseparerade och i samma ordning som fälten i Revenue Schedule Line Lookups.</help>
        <label><!-- Parent Relationship Paths --></label>
        <name>ffrr__ParentRelationshipPaths__c</name>
    </fields>
    <fields>
        <help>Välj föräldrainställningsrekordet (inställningsrekordet för källobjektet en nivå ovanför det här i hierarkin).</help>
        <label><!-- Parent Settings --></label>
        <name>ffrr__ParentSettings__c</name>
        <relationshipLabel><!-- ParentSettings --></relationshipLabel>
    </fields>
    <fields>
        <help>API-namnet på fältet Antal eller Procent på källobjektet som representerar hur komplett källkoden är i procentuella termer.</help>
        <label><!-- % Complete --></label>
        <name>ffrr__PercentageComplete__c</name>
    </fields>
    <fields>
        <help>Ange API-namnet på uppslagningsfältet till källobjektet för moderinställningen.</help>
        <label><!-- Parent Lookup --></label>
        <name>ffrr__PrimaryRelationship__c</name>
    </fields>
    <fields>
        <help>API-namn på sökningen som representerar produkten. Detta kan antingen vara en uppslagning på källobjektet, eller genom att använda uppslagningsrelationer kan du referera till en uppslagning på ett objekt länkat med en sökväg på upp till fem sökningar.</help>
        <label><!-- Product --></label>
        <name>ffrr__Product__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet på källobjektet som representerar hastigheten vid vilken enheter laddas.</help>
        <label><!-- Rate --></label>
        <name>ffrr__Rate__c</name>
    </fields>
    <fields>
        <help>API-namn för fältet Number på källobjektet som lagrar den intäkt som hittills redovisats i dubbel valuta. Om det fylls i uppdateras källposten automatiskt varje gång en intäktsigenkänningstransaktion åtagits.</help>
        <label><!-- FOR FUTURE USE Rec To Date Value (Dual) --></label>
        <name>ffrr__RecognizedToDateValueDual__c</name>
    </fields>
    <fields>
        <help>API-namn för fältet Number på källobjektet som lagrar den intäkt som hittills redovisats i hemvaluta. Om det fylls i uppdateras källposten automatiskt varje gång en intäktsigenkänningstransaktion åtagits.</help>
        <label><!-- FOR FUTURE USE Rec To Date Value (Home) --></label>
        <name>ffrr__RecognizedToDateValueHome__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet Nummer eller Valuta på källobjektet som lagrar mängden intäkter som hittills är erkända. Om befolkade uppdateras värdet på källposten automatiskt varje gång en intäktsgenkänningstransaktion har gjorts.</help>
        <label><!-- Recognized To Date Value --></label>
        <name>ffrr__RecognizedToDateValue__c</name>
    </fields>
    <fields>
        <help>API-namnet på det booleska fältet på källobjektet som anger alla intäkter är fullt erkänt och alla kostnader skrivs av fullt ut. Om befolkade aktiveras källkodens kryssrutan automatiskt när intäkterna är fullt erkända och kostnadsavskrivs helt.</help>
        <label><!-- Fully Recognized and Amortized --></label>
        <name>ffrr__RevenueRecognitionCompleted__c</name>
    </fields>
    <fields>
        <help>API-namn på uppslagsfält (till källobjektet som beskrivs av denna inställningspost eller relaterade objekt) som din administratör har lagt till i Intäktsschemaläggningsobjektet. Dessa fält fylls i från de sökningar som identifierats i föräldrarelationsvägarna. När du anger flera fält måste de vara kommaseparerade och i samma ordning som fälten i föräldrarelationsvägarna.</help>
        <label><!-- Revenue Schedule Line Lookups --></label>
        <name>ffrr__RevenueScheduleLineLookups__c</name>
    </fields>
    <fields>
        <help>API-namn för uppslagsfältet (till källobjektet på denna inställningsnivå) som din administratör har lagt till i Intäktsschema-objektet.</help>
        <label><!-- Revenue Schedule Lookup --></label>
        <name>ffrr__RevenueScheduleSourceLookup__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet på källobjektet som lagrar det fristående försäljningspriset för en prestationsförpliktelsepost.</help>
        <label><!-- SSP --></label>
        <name>ffrr__SSP__c</name>
    </fields>
    <fields>
        <help>API-namn på sökningen som representerar Salesforce-kontot. Detta kan antingen vara en sökning på källobjektet, eller genom att använda uppslagningsrelationer kan du referera till en sökning på ett objekt som är länkat med en sökväg på upp till fem sökningar.</help>
        <label><!-- Account --></label>
        <name>ffrr__SalesforceAccount__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Setting Uniqueness --></label>
        <name>ffrr__SettingUniqueness__c</name>
    </fields>
    <fields>
        <help>Välj nivån för den här källans objektinställningsrekordet.</help>
        <label><!-- Settings Level --></label>
        <name>ffrr__SettingsLevel__c</name>
        <picklistValues>
            <masterLabel>Level 2</masterLabel>
            <translation>Nivå 2</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Level 3</masterLabel>
            <translation>Nivå 3</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Level 4</masterLabel>
            <translation>Nivå 4</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Primary</masterLabel>
            <translation>Primär</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Den typ av process som denna inställningsrekord kommer att användas för: Faktisk, Prognos eller båda. Om mappningarna är identiska för både faktiska och prognosprocesser, skapa en inställningsrekord för användning av båda.</help>
        <label><!-- Settings Type --></label>
        <name>ffrr__SettingsType__c</name>
        <picklistValues>
            <masterLabel>Actual</masterLabel>
            <translation>Faktisk</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Forecast</masterLabel>
            <translation>Prognos</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>API-namnet på datum- eller datumtidsfältet på källobjektet som representerar startdatumet.</help>
        <label><!-- Start Date --></label>
        <name>ffrr__StartDate__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet Nummer eller Valuta på källobjektet som representerar den totala kostnaden.</help>
        <label><!-- Total Cost --></label>
        <name>ffrr__TotalCost__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet Nummer eller Valuta på källobjektet som representerar den totala intäkten.</help>
        <label><!-- Total Revenue --></label>
        <name>ffrr__TotalRevenue__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet Nummer på källobjektet som representerar det totala antalet laddade enheter. Den totala enhetens värde kan vara antalet timmar eller dagar som arbetat på ett projekt till exempel.</help>
        <label><!-- Total Units --></label>
        <name>ffrr__TotalUnits__c</name>
    </fields>
    <fields>
        <help>API-namnet på uppslagningsfältet (till källobjektet på den här inställningsnivån) som administratören har lagt till i objektet för intäktsinkomsttransaktionslinje. Detta måste fyllas i om inställningstypen är aktuell.</help>
        <label><!-- Revenue Recognition Txn Line Lookup --></label>
        <name>ffrr__TransactionLineRelationship__c</name>
    </fields>
    <fields>
        <help>API-namn för textfältet, uppslag eller plocklista på källobjektet som representerar GLA som ska användas för sanna uppvärden i dubbel valuta.</help>
        <label><!-- FOR FUTURE USE True-Up GLA (Dual) --></label>
        <name>ffrr__TrueUpDualAccount__c</name>
    </fields>
    <fields>
        <help>API-namn för textfältet, uppslagning eller plocklista på källobjektet som representerar GLA som ska användas för sanna uppvärden i hemvaluta.</help>
        <label><!-- FOR FUTURE USE True-Up GLA (Home) --></label>
        <name>ffrr__TrueUpHomeAccount__c</name>
    </fields>
    <fields>
        <help>Används denna inställning i intäktskontrakt.</help>
        <label><!-- Use in Revenue Contract --></label>
        <name>ffrr__UseInRevenueContract__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet Nummer eller procentandel på källobjektet som representerar VSOE-talet (Vendor Specific Objective Evidence).</help>
        <label><!-- VSOE % --></label>
        <name>ffrr__VSOEPercent__c</name>
    </fields>
    <fields>
        <help>API-namnet på fältet Nummer eller Valuta på källobjektet som representerar VSOE-talet (Vendor Specific Objective Evidence).</help>
        <label><!-- VSOE Rate --></label>
        <name>ffrr__VSOERate__c</name>
    </fields>
    <fields>
        <help>Den här typen av bearbetning av denna inställningsrekord används för: Inkomster, Kostnad eller båda.</help>
        <label><!-- Value Type --></label>
        <name>ffrr__ValueType__c</name>
        <picklistValues>
            <masterLabel>Cost</masterLabel>
            <translation>Kosta</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Revenue</masterLabel>
            <translation>Inkomst</translation>
        </picklistValues>
    </fields>
    <gender>Feminine</gender>
    <nameFieldLabel>Inställningsnamn</nameFieldLabel>
    <webLinks>
        <label><!-- CreateMappings --></label>
        <name>ffrr__CreateMappings</name>
    </webLinks>
    <webLinks>
        <label><!-- CreateMappingsList --></label>
        <name>ffrr__CreateMappingsList</name>
    </webLinks>
    <webLinks>
        <label><!-- DeleteSettings --></label>
        <name>ffrr__DeleteSettings</name>
    </webLinks>
</CustomObjectTranslation>
