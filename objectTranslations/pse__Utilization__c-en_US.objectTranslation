<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <help><!-- The number of assignments to process in a single batch when calculating utilization --></help>
        <label><!-- Assignment Batch Size --></label>
        <name>pse__Assignment_Batch_Size__c</name>
    </fields>
    <fields>
        <help><!-- Sets the maximum number of child records above which a batch deletion is used instead of cascading deletion. --></help>
        <label><!-- Batch Deletion Cut-Off Point --></label>
        <name>pse__Batch_Deletion_Cutoff__c</name>
    </fields>
    <fields>
        <help><!-- Allows the calculation of cross product combination of region, practice and group.
Such as Region by Practice, or Group by Region --></help>
        <label><!-- Calculate Cross Products --></label>
        <name>pse__Calculate_Cross_Products__c</name>
    </fields>
    <fields>
        <help><!-- If true, calculate the amount of scheduled time for held (unassigned) resource requests. --></help>
        <label><!-- Calculate Held Resource Request Time --></label>
        <name>pse__Calculate_Held_Resource_Request_Time__c</name>
    </fields>
    <fields>
        <label><!-- Calculate Summary By Role --></label>
        <name>pse__Calculate_Summary_By_Role__c</name>
    </fields>
    <fields>
        <label><!-- Calculate Unheld Resource Request Time --></label>
        <name>pse__Calculate_Unheld_Resource_Request_Time__c</name>
    </fields>
    <fields>
        <help><!-- Controls the fields on the Resource Request object to include for utilization calculations of unheld resource requests. This setting accepts custom and standard field values in SOQL queries. If not defined, calculations use default criteria. --></help>
        <label><!-- Custom Filters for Unheld RR Utilization --></label>
        <name>pse__CustomFiltersforUnheldRRUtilization__c</name>
    </fields>
    <fields>
        <label><!-- Default Opportunity Probability --></label>
        <name>pse__Default_Opportunity_Probability__c</name>
    </fields>
    <fields>
        <help><!-- When batch deleting previous utilization calculations, sets the maximum number of utilization calculation records to process per batch. --></help>
        <label><!-- Deletion Batch Size --></label>
        <name>pse__Deletion_Batch_Size__c</name>
    </fields>
    <fields>
        <help><!-- When batch deleting previous utilization calculations, sets the maximum number of utilization detail and summary records to process per batch. --></help>
        <label><!-- Deletion Child Record Batch Size --></label>
        <name>pse__Deletion_Child_Record_Batch_Size__c</name>
    </fields>
    <fields>
        <help><!-- When true, historical utilization uses the start day based on the work calendar instead of the user&apos;s locale. --></help>
        <label><!-- Historical UTE Work Cal Start Day --></label>
        <name>pse__Historical_UTE_Work_Cal_Start_Day__c</name>
    </fields>
    <fields>
        <label><!-- Minimum Opportunity Probability --></label>
        <name>pse__Minimum_Opportunity_Probability__c</name>
    </fields>
    <fields>
        <help><!-- The number of resources to process in a single batch when calculating utilization --></help>
        <label><!-- Resource Batch Size --></label>
        <name>pse__Resource_Batch_Size__c</name>
    </fields>
    <fields>
        <label><!-- Resource Request Batch Size --></label>
        <name>pse__Resource_Request_Batch_Size__c</name>
    </fields>
    <fields>
        <label><!-- Target Utilization Approach --></label>
        <name>pse__Target_Utilization_Approach__c</name>
    </fields>
    <fields>
        <help><!-- The number of timecards to process in a single batch when calculating utilization --></help>
        <label><!-- Timecard Batch Size --></label>
        <name>pse__Timecard_Batch_Size__c</name>
    </fields>
    <fields>
        <help><!-- A comma delimited list of timecard status values that are used to determine which timecards are used for utilization calculations. --></help>
        <label><!-- Timecard Statuses --></label>
        <name>pse__Timecard_Statuses__c</name>
    </fields>
    <fields>
        <help><!-- Determines whether the &quot;Is Report Master&quot; flag is reset based on the Calculation Type and Parent object (checked) or based on the Utilization Calculation Name (unchecked). --></help>
        <label><!-- Uncheck Master Based On Type/Parent --></label>
        <name>pse__Uncheck_Master_Based_On_Time_Parent__c</name>
    </fields>
    <fields>
        <help><!-- When true, utilization details do not calculate before the Start Date or beyond the Last Date on the resource. --></help>
        <label><!-- Use Start Date and Last Date on Resource --></label>
        <name>pse__Use_Last_Date_on_Resource__c</name>
    </fields>
    <fields>
        <help><!-- If true, the Utilization Engine is used for calculating utilization with Shift Management. The default is false. --></help>
        <label><!-- Use Utilization Engine for Shift Mgmt --></label>
        <name>pse__Use_Utilization_Engine_TOD__c</name>
    </fields>
    <fields>
        <label><!-- Use Utilization Engine --></label>
        <name>pse__Use_Utilization_Engine__c</name>
    </fields>
    <fields>
        <label><!-- Utilization Engine Batch Size --></label>
        <name>pse__Utilization_Engine_Batch_Size__c</name>
    </fields>
    <fields>
        <help><!-- Specifies the number of days into the future to maintain real time utilization data for Shift Management. The default value is 30. --></help>
        <label><!-- Utilization Eng Max Range Shift Mgmt --></label>
        <name>pse__Utilization_Engine_Max_Range_TOD__c</name>
    </fields>
    <fields>
        <help><!-- Maximum number of days into the future for which to maintain real time utilization data --></help>
        <label><!-- Utilization Engine Max Range --></label>
        <name>pse__Utilization_Engine_Max_Range__c</name>
    </fields>
    <fields>
        <help><!-- Specifies the number of days prior to the current date to calculate real time utilization data for Shift Management.  The default value is 5. --></help>
        <label><!-- Utilization Eng Start Range Shift Mgmt --></label>
        <name>pse__Utilization_Engine_Start_Range_TOD__c</name>
    </fields>
    <fields>
        <help><!-- Defines the number of days prior to the current date to calculate real time utilization data. Set to 0 to start calculation from the current date. The default value is 50. --></help>
        <label><!-- Utilization Engine Start Range --></label>
        <name>pse__Utilization_Engine_Start_Range__c</name>
    </fields>
</CustomObjectTranslation>
