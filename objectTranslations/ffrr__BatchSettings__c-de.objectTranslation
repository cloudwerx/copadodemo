<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <caseType>Nominative</caseType>
        <plural>false</plural>
        <value><!-- Revenue Management Stapeleinstellungen --></value>
    </caseValues>
    <caseValues>
        <caseType>Nominative</caseType>
        <plural>true</plural>
        <value><!-- Revenue Management Stapeleinstellungen --></value>
    </caseValues>
    <caseValues>
        <caseType>Accusative</caseType>
        <plural>false</plural>
        <value><!-- Revenue Management Stapeleinstellungen --></value>
    </caseValues>
    <caseValues>
        <caseType>Accusative</caseType>
        <plural>true</plural>
        <value><!-- Revenue Management Stapeleinstellungen --></value>
    </caseValues>
    <caseValues>
        <caseType>Genitive</caseType>
        <plural>false</plural>
        <value><!-- Revenue Management Stapeleinstellungen --></value>
    </caseValues>
    <caseValues>
        <caseType>Genitive</caseType>
        <plural>true</plural>
        <value><!-- Revenue Management Stapeleinstellungen --></value>
    </caseValues>
    <caseValues>
        <caseType>Dative</caseType>
        <plural>false</plural>
        <value><!-- Revenue Management Stapeleinstellungen --></value>
    </caseValues>
    <caseValues>
        <caseType>Dative</caseType>
        <plural>true</plural>
        <value><!-- Revenue Management Stapeleinstellungen --></value>
    </caseValues>
    <fields>
        <help>Die Anzahl der festgeschriebenen Umsatzrealisierungs-Transaktionszeilen pro verarbeiteten Stapel, für die Ist-gegen-Prognose-Zeilen erstellt werden (Standard: 200).</help>
        <label><!-- Actual vs. Forecast Actuals Batch Size --></label>
        <name>ffrr__AVFCreateFromActualsBatchSize__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Umsatzprognosetransaktionspositionen pro verarbeiteten Stapel, für die Ist- oder Prognosepositionen erstellt werden (Standard: 200).</help>
        <label><!-- Actual vs. Forecast Forecast Batch Size --></label>
        <name>ffrr__AVFCreateFromForecastBatchSize__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der verarbeiteten Ist- und Prognosepositionen pro Stapel (Standard: 1000).</help>
        <label><!-- Actual vs. Forecast Delete Batch Size --></label>
        <name>ffrr__AVFDeleteLinesBatchSize__c</name>
    </fields>
    <fields>
        <help>Veraltet: Die Anzahl der Quell- / Staging-Datensätze, die pro verarbeitetem Stapel verarbeitet / gelöscht werden sollen (Standard: 1000).</help>
        <label><!-- Deprecated: Old Staging Batch Size --></label>
        <name>ffrr__ActualsRetrieveLinesBatchSize__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Einnahmenerfassungstransaktionen, die pro Stapel verarbeitet wurden (Standard: 1).</help>
        <label><!-- Actuals Transaction Commit Batch Size --></label>
        <name>ffrr__ActualsTransactionCommitBatchSize__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Quelldatensätze, für die Umsatzrealisierungs-Transaktionszeilen pro Stapelverarbeitung erstellt werden (Standard: 500).</help>
        <label><!-- Actuals Transaction Save Batch Size --></label>
        <name>ffrr__ActualsTransactionSaveBatchSize__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Umsatzrealisierungstransaktionen, die pro verarbeiteter Charge zusammengefasst wurden (Standard: 1).</help>
        <label><!-- Actuals Transaction Summary Batch Size --></label>
        <name>ffrr__ActualsTransactionSummaryBatchSize__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Entwurfsprognosedatensätze, die pro Stapel gelöscht werden, der beim Ausführen des DeleteDraftForecasts-Stapelprozesses verarbeitet wird (Standard: 50).</help>
        <label><!-- Forecast Draft Transaction Delete Batch --></label>
        <name>ffrr__ForecastDraftTransactionDeleteBatchSize__c</name>
    </fields>
    <fields>
        <help>Dies ist die Stapelgröße für den Sicherungsstapel, mit dem Prognose- und Prognosezeilen aus Entwurfszeilen erstellt werden (Standard: 200).</help>
        <label><!-- Forecast Transaction Save Batch Size --></label>
        <name>ffrr__ForecastTransactionSaveBatchSize__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Prognosedatensätze, die beim Generieren aus Quelldatensätzen pro Stapel verarbeitet werden (Standard: 200).</help>
        <label><!-- Generate Forecasts Batch Size --></label>
        <name>ffrr__GenerateForecastsBatchSize__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Stapelverfolgungssteuerungsdatensätze mit der Aktion &quot;Gruppierung erstellen&quot;, die pro Stapel gelöscht werden, wenn der Stapelprozess &quot;GroupingDeleteBatch&quot; ausgeführt wird (Standard: 1000).</help>
        <label><!-- Grouping BTC Delete Batch Size --></label>
        <name>ffrr__GroupingBTCDeleteBatchSize__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Staging-Datensätze, die verarbeitet werden, um die Gruppierungszusammenfassungsdatensätze pro verarbeitetem Stapel zu erstellen (Standard: 1000).</help>
        <label><!-- Grouping Create Batch Size --></label>
        <name>ffrr__GroupingCreateBatchSize__c</name>
    </fields>
    <fields>
        <help>Die maximale Anzahl von Quelldatensätzen, die synchron gruppiert werden können (Standard: 3000).</help>
        <label><!-- Grouping Create Synchronous Limit --></label>
        <name>ffrr__GroupingCreateSynchronousLimit__c</name>
    </fields>
    <fields>
        <help>Die maximale Anzahl von Gruppierungsdatensätzen, die synchron gelöscht werden können (Standard: 2000).</help>
        <label><!-- Grouping Delete Synchronous Limit --></label>
        <name>ffrr__GroupingDeleteSynchronousLimit__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Gruppierungszusammenfassungsdatensätze, die pro Stapel gelöscht werden, der beim Ausführen des GroupingDeleteBatch-Stapelprozesses verarbeitet wird (Standard: 1000).</help>
        <label><!-- Grouping Summary Delete Batch Size --></label>
        <name>ffrr__GroupingSummaryDeleteBatchSize__c</name>
    </fields>
    <fields>
        <label><!-- Opportunity Product Scheduler Date --></label>
        <name>ffrr__LastOpportunityProductMirrorBatch__c</name>
    </fields>
    <fields>
        <help>Die maximale Anzahl von Quelldatensätzen, die in einem einzelnen Stapel verarbeitet werden, wenn ein Umsatzvertrag asynchron erstellt oder aktualisiert wird (Standard: 500).</help>
        <label><!-- Manage RC Batch Size --></label>
        <name>ffrr__ManageRevenueContractBatchSize__c</name>
    </fields>
    <fields>
        <help>Die maximale Anzahl von Quelldatensätzen, die beim Erstellen oder Aktualisieren eines Umsatzvertrags synchron verarbeitet werden können. Wenn die Anzahl der Datensätze gleich oder größer als dieser Wert ist, werden alle Datensätze im Hintergrund verarbeitet. Der Standardwert ist 100.</help>
        <label><!-- Manage RC Synchronous Limit --></label>
        <name>ffrr__ManageRevenueContractSynchronousLimit__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Opportunity-Produktdatensätze, für die Opportunity-Produktspiegeldatensätze pro Stapelverarbeitung erstellt oder aktualisiert werden (Standard: 1000).</help>
        <label><!-- Opportunity Product Mirror Batch Size --></label>
        <name>ffrr__OpportunityProductMirrorBatchSize__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Opportunity Product Mirror-Datensätze, die pro Stapelverarbeitung gelöscht werden sollen (Standard: 2000).</help>
        <label><!-- Opp Product Mirror Delete Batch Size --></label>
        <name>ffrr__OpportunityProductMirrorDeleteBatchSize__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Prozesssteuerungsdatensätze, die pro Stapel gelöscht werden, der beim Ausführen des Staging-Lösch-Stapelprozesses verarbeitet wird (Standard: 2000).</help>
        <label><!-- Process Control Delete Batch Size --></label>
        <name>ffrr__ProcessControlDeleteBatchSize__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Umsatzrealisierungstransaktionen pro Stapel, die beim Erstellen von FinancialForce Accounting-Journalen verarbeitet wurden (Standard: 1).</help>
        <label><!-- RRTs for Journal Creation Batch Size --></label>
        <name>ffrr__RRTToJournalCreationBatchSize__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Stapelverfolgungssteuerungsdatensätze mit der Aktion &quot;Staging erstellen&quot;, die pro Stapel gelöscht werden, der beim Ausführen des StagingDeleteBatch-Stapelprozesses verarbeitet wird (Standard: 1000).</help>
        <label><!-- Staging BTC Delete Batch Size --></label>
        <name>ffrr__StagingBTCDeleteBatchSize__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Staging-Datensätze, die pro Stapel verarbeitet werden, um Gruppierungszusammenfassungen zu erstellen (Standard: 1000).</help>
        <label><!-- Staging Create Batch Size --></label>
        <name>ffrr__StagingCreateBatchSize__c</name>
    </fields>
    <fields>
        <help>Die maximale Anzahl von Quelldatensätzen, die synchron verarbeitet werden können (Standard: 1000).</help>
        <label><!-- Staging Create Synchronous Limit --></label>
        <name>ffrr__StagingCreateSynchronousLimit__c</name>
    </fields>
    <fields>
        <help>Die maximale Anzahl von Staging-Datensätzen und Gruppierungsdatensätzen, die synchron gelöscht werden können. Ab diesem Grenzwert wird Batch Apex verwendet (Standard: 1000).</help>
        <label><!-- Staging Delete Synchronous Limit --></label>
        <name>ffrr__StagingDeleteSynchronousLimit__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Staging-Detaildatensätze, die pro Stapel gelöscht werden, der beim Ausführen des StagingDeleteBatch-Stapelprozesses verarbeitet wird (Standard: 1000).</help>
        <label><!-- Staging Detail Delete Batch Size --></label>
        <name>ffrr__StagingDetailDeleteBatchSize__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Datensätze, die verarbeitet werden, um eine Umsatzrealisierungstransaktion aus Staging-Daten pro verarbeitetem Stapel zu erstellen (Standard: 1000).</help>
        <label><!-- Staging Save Batch Size --></label>
        <name>ffrr__StagingSaveBatchSize__c</name>
    </fields>
    <fields>
        <help>Die maximale Anzahl von Staging-Datensätzen, die synchron gespeichert werden können (Standard: 500).</help>
        <label><!-- Staging Save Synchronous Limit --></label>
        <name>ffrr__StagingSaveSynchronousLimit__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Staging-Zusammenfassungsdatensätze, die pro Stapel gelöscht werden, der beim Ausführen des StagingDeleteBatch-Stapelprozesses verarbeitet wird (Standard: 600).</help>
        <label><!-- Staging Summary Delete Batch Size --></label>
        <name>ffrr__StagingSummaryDeleteBatchSize__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Staging-Versionsdatensätze, die pro Stapel gelöscht werden, der beim Ausführen des StagingDeleteBatch-Stapelprozesses verarbeitet wird (Standard: 1000).</help>
        <label><!-- Staging Version Delete Batch Size --></label>
        <name>ffrr__StagingVersionDeleteBatchSize__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Leistungsverpflichtungsposten, die pro Stapel verarbeitet werden, wenn der Prozess &quot;Zuvor erkannt übertragen&quot; ausgeführt wird (Standard: 1000).</help>
        <label><!-- Transfer Prev. Recognized Batch Size --></label>
        <name>ffrr__TransferPreviouslyRecognizedBatchSize__c</name>
    </fields>
    
</CustomObjectTranslation>
