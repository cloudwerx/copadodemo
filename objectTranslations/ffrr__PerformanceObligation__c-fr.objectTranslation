<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Obligation de performance</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Obligations de performance</value>
    </caseValues>
    <fields>
        <help>Informations sur l’obligation de performance. Ce sera souvent le nom du compte, mais il peut s’agir de n’importe quelle information.</help>
        <label><!-- Display Information --></label>
        <name>ffrr__AccountName__c</name>
    </fields>
    <fields>
        <help>Indique si l&apos;enregistrement source est actif.</help>
        <label><!-- Active --></label>
        <name>ffrr__Active__c</name>
    </fields>
    <fields>
        <help>Ajustement dû à une différence entre les revenus et les revenus alloués sur le contrat de revenus en raison d’erreurs d’arrondi cumulatives sur les obligations de performance.</help>
        <label><!-- Allocated Revenue Rounding Adjustment --></label>
        <name>ffrr__AllocatedRevenueAdjustment__c</name>
    </fields>
    <fields>
        <help>Remplacez le montant calculé alloué à cette obligation de performance en saisissant une valeur différente ici.</help>
        <label><!-- Allocated Revenue Override --></label>
        <name>ffrr__AllocatedRevenueOverride__c</name>
    </fields>
    <fields>
        <help>Le montant alloué à cette obligation de performance par le calcul de l’allocation des revenus. Cette valeur est utilisée pour la reconnaissance des revenus.</help>
        <label><!-- Allocated Revenue --></label>
        <name>ffrr__AllocatedRevenue__c</name>
    </fields>
    <fields>
        <help>Le ratio utilisé pour répartir les revenus sur le contrat de revenus.</help>
        <label><!-- Allocation Ratio --></label>
        <name>ffrr__AllocationRatio__c</name>
    </fields>
    <fields>
        <help>Indique si le produit de cette obligation de performance a été entièrement attribué ou doit être réaffecté.</help>
        <label><!-- Allocation Status --></label>
        <name>ffrr__AllocationStatus__c</name>
    </fields>
    <fields>
        <label><!-- Amortized to Date --></label>
        <name>ffrr__AmortizedToDate__c</name>
    </fields>
    <fields>
        <label><!-- Balance Sheet GLA --></label>
        <name>ffrr__BalanceSheetAccount__c</name>
    </fields>
    <fields>
        <help>Indique si l&apos;enregistrement source est complet.</help>
        <label><!-- Completed --></label>
        <name>ffrr__Completed__c</name>
    </fields>
    <fields>
        <help>La ligne de commande Obligation de performance des coûts de contrôle. Utilisé pour renseigner les champs de l&apos;obligation de performance.</help>
        <label><!-- Controlling POLI (Cost) --></label>
        <name>ffrr__ControllingCostPOLI__c</name>
        <relationshipLabel><!-- Controlling POLIs (Cost) --></relationshipLabel>
    </fields>
    <fields>
        <help>La ligne de contrôle de la performance des obligations de revenus. Utilisé pour renseigner les champs de l&apos;obligation de performance.</help>
        <label><!-- Controlling POLI (Revenue) --></label>
        <name>ffrr__ControllingPOLI__c</name>
        <relationshipLabel><!-- Controlling POLIs (Revenue) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Balance Sheet GLA (Cost) --></label>
        <name>ffrr__CostBalanceSheetAccount__c</name>
    </fields>
    <fields>
        <label><!-- Cost Center --></label>
        <name>ffrr__CostCenter__c</name>
    </fields>
    <fields>
        <label><!-- Cost Center (Cost) --></label>
        <name>ffrr__CostCostCenter__c</name>
    </fields>
    <fields>
        <label><!-- Income Statement GLA (Cost) --></label>
        <name>ffrr__CostIncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>Coût pour cette obligation de performance</help>
        <label><!-- Cost --></label>
        <name>ffrr__Cost__c</name>
    </fields>
    <fields>
        <help>Nombre de décimales dans la devise de l’enregistrement.</help>
        <label><!-- Currency Decimal Places --></label>
        <name>ffrr__CurrencyDP__c</name>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>ffrr__Description__c</name>
    </fields>
    <fields>
        <label><!-- End Date --></label>
        <name>ffrr__EndDate__c</name>
    </fields>
    <fields>
        <help>Indique que cette obligation de performance concerne les revenus.</help>
        <label><!-- Has Revenue --></label>
        <name>ffrr__HasRevenue__c</name>
    </fields>
    <fields>
        <label><!-- Income Statement GLA --></label>
        <name>ffrr__IncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>Le nombre d&apos;éléments de ligne d&apos;obligation de performance relatifs à des revenus ayant une valeur de fournisseur de services réseau nulle.</help>
        <label><!-- Null SSP Count --></label>
        <name>ffrr__NullSSPCount__c</name>
    </fields>
    <fields>
        <label><!-- % Complete --></label>
        <name>ffrr__PercentComplete__c</name>
    </fields>
    <fields>
        <help>Ceci est vérifié si le revenu a été alloué et que l&apos;enregistrement source est actif. Si décoché, cet enregistrement ne peut pas être inclus dans la reconnaissance des revenus.</help>
        <label><!-- Ready for Revenue Recognition --></label>
        <name>ffrr__ReadyForRevenueRecognition__c</name>
    </fields>
    <fields>
        <label><!-- Recognized to Date --></label>
        <name>ffrr__RecognizedToDate__c</name>
    </fields>
    <fields>
        <label><!-- Revenue Contract --></label>
        <name>ffrr__RevenueContract__c</name>
        <relationshipLabel><!-- Performance Obligations --></relationshipLabel>
    </fields>
    <fields>
        <help>Le nombre d&apos;éléments de ligne d&apos;obligation de performance liés aux produits.</help>
        <label><!-- Revenue Count --></label>
        <name>ffrr__RevenueCount__c</name>
    </fields>
    <fields>
        <help>Lorsqu&apos;elle est définie, indique que tous les revenus de cette obligation de performance ont été intégralement comptabilisés et que tous les coûts sont entièrement amortis.</help>
        <label><!-- Fully Recognized and Amortized --></label>
        <name>ffrr__RevenueRecognitionComplete__c</name>
    </fields>
    <fields>
        <help>Revenus pour cette obligation de performance</help>
        <label><!-- Revenue --></label>
        <name>ffrr__Revenue__c</name>
    </fields>
    <fields>
        <help>Remplacez le prix de vente autonome de l&apos;enregistrement source en saisissant une valeur différente ici.</help>
        <label><!-- SSP Override --></label>
        <name>ffrr__SSPOverride__c</name>
    </fields>
    <fields>
        <help>Le prix de vente autonome qui sera utilisé pour cette obligation de performance. Utilise Total SSP ou SSP Override si rempli.</help>
        <label><!-- SSP --></label>
        <name>ffrr__SSP__c</name>
    </fields>
    <fields>
        <label><!-- Start Date --></label>
        <name>ffrr__StartDate__c</name>
    </fields>
    <fields>
        <help>Somme des prix de vente autonomes de tous les éléments de ligne liés aux obligations de performance.</help>
        <label><!-- Total SSP --></label>
        <name>ffrr__TotalSSP__c</name>
    </fields>
    <fields>
        <label><!-- Template --></label>
        <name>ffrr__ffrrTemplate__c</name>
        <relationshipLabel><!-- Performance Obligations --></relationshipLabel>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Numéro d&apos;obligation de performance</nameFieldLabel>
    <startsWith>Vowel</startsWith>
    <webLinks>
        <label><!-- AllocateRevenue --></label>
        <name>ffrr__AllocateRevenue</name>
    </webLinks>
    <webLinks>
        <label><!-- Update --></label>
        <name>ffrr__Update</name>
    </webLinks>
</CustomObjectTranslation>
