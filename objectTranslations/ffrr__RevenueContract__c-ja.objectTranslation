<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>収益契約</value>
    </caseValues>
    <fields>
        <help>収益契約に関する情報。多くの場合、これはアカウント名になりますが、どのような情報でもかまいません。</help>
        <label><!-- Display Information --></label>
        <name>ffrr__AccountName__c</name>
    </fields>
    <fields>
        <label><!-- Account --></label>
        <name>ffrr__Account__c</name>
        <relationshipLabel><!-- Revenue Contracts --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Active --></label>
        <name>ffrr__Active__c</name>
    </fields>
    <fields>
        <help>この収益契約に対して配分を正常に実行できるかどうかを示します。</help>
        <label><!-- Ready for Allocation --></label>
        <name>ffrr__Allocatable__c</name>
    </fields>
    <fields>
        <help>Allocated Revenue値が上書きされていない場合、リンクされた履行義務に収益を割り当てるために使用される比率。</help>
        <label><!-- Allocation Ratio --></label>
        <name>ffrr__AllocationRatio__c</name>
    </fields>
    <fields>
        <help>この契約の収益が完全に割り当てられているか、または再割り当てが必要かを示します。</help>
        <label><!-- Allocation Status --></label>
        <name>ffrr__AllocationStatus__c</name>
    </fields>
    <fields>
        <help>会社オブジェクトへのルックアップ。</help>
        <label><!-- Company --></label>
        <name>ffrr__Company__c</name>
        <relationshipLabel><!-- Revenue Contracts --></relationshipLabel>
    </fields>
    <fields>
        <help>レコードの通貨の小数点以下の桁数。</help>
        <label><!-- Currency Decimal Places --></label>
        <name>ffrr__CurrencyDP__c</name>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>ffrr__Description__c</name>
    </fields>
    <fields>
        <label><!-- End Date --></label>
        <name>ffrr__EndDate__c</name>
    </fields>
    <fields>
        <help>少なくとも1つのヌルSSP値を持つ、収益に関する履行義務の数。</help>
        <label><!-- Null SSP Count --></label>
        <name>ffrr__NullSSPCount__c</name>
    </fields>
    <fields>
        <help>Allocated Revenue Override値が入力された、リンクされた履行義務の数。</help>
        <label><!-- PO Allocated Revenue Override Count --></label>
        <name>ffrr__POAllocatedRevenueOverrideCount__c</name>
    </fields>
    <fields>
        <help>この契約に関する履行義務の数。</help>
        <label><!-- Performance Obligations Count --></label>
        <name>ffrr__PerformanceObligationsCount__c</name>
    </fields>
    <fields>
        <help>この契約の収益が正常に割り当てられたかどうかを示します。</help>
        <label><!-- Revenue Allocated --></label>
        <name>ffrr__RevenueAllocated__c</name>
    </fields>
    <fields>
        <help>この項目は、契約の収益が履行義務から計算されるのではなく契約レベルで定義されている場合にのみ入力されます。データが入力されると、この値が[収益]フィールドで使用されます。</help>
        <label><!-- Revenue Override --></label>
        <name>ffrr__RevenueOverride__c</name>
    </fields>
    <fields>
        <label><!-- Fully Recognized and Amortized --></label>
        <name>ffrr__RevenueRecognitionComplete__c</name>
    </fields>
    <fields>
        <help>履行義務に割り当てられる値。 Total Revenueを使用するか、データが取り込まれている場合はRevenue Overrideを使用します。</help>
        <label><!-- Revenue --></label>
        <name>ffrr__Revenue__c</name>
    </fields>
    <fields>
        <label><!-- Start Date --></label>
        <name>ffrr__StartDate__c</name>
    </fields>
    <fields>
        <help>リンクされているすべての履行義務からの割り当てられた収入オーバーライド値の合計。</help>
        <label><!-- Total Allocated Revenue Override --></label>
        <name>ffrr__TotalAllocatedRevenueOverride__c</name>
    </fields>
    <fields>
        <help>リンクされているすべての履行義務からの配分収益値の合計。</help>
        <label><!-- Total Allocated Revenue --></label>
        <name>ffrr__TotalAllocatedRevenue__c</name>
    </fields>
    <fields>
        <help>リンクされているすべての履行義務からの、現在までの償却額の合計。</help>
        <label><!-- Total Amortized To Date --></label>
        <name>ffrr__TotalAmortizedToDate__c</name>
    </fields>
    <fields>
        <help>リンクされているすべての履行義務からのコスト値の合計。</help>
        <label><!-- Total Cost --></label>
        <name>ffrr__TotalCost__c</name>
    </fields>
    <fields>
        <help>Allocation RatioフィールドとAllocated Revenue Overrideフィールドがnullの場合の、リンクされている履行義務の数。</help>
        <label><!-- Null Allocation Ratio Count --></label>
        <name>ffrr__TotalNullAllocationRatioCount__c</name>
    </fields>
    <fields>
        <help>リンクされているすべての履行義務から認識された日付までの値の合計。</help>
        <label><!-- Total Recognized To Date --></label>
        <name>ffrr__TotalRecognizedToDate__c</name>
    </fields>
    <fields>
        <help>リンクされているすべての履行義務からの収益の合計値。</help>
        <label><!-- Total Revenue --></label>
        <name>ffrr__TotalRevenue__c</name>
    </fields>
    <fields>
        <help>Allocated Revenue値がオーバーライドされていない、リンクされた履行義務のSSP値の合計。</help>
        <label><!-- Total SSP for Allocation --></label>
        <name>ffrr__TotalSSPForAllocation__c</name>
    </fields>
    <fields>
        <help>リンクされているすべての履行義務からの独立販売価格の合計。</help>
        <label><!-- Total SSP --></label>
        <name>ffrr__TotalSSP__c</name>
    </fields>
    <fields>
        <help>SSPがゼロの場合の収益に関する履行義務の数。</help>
        <label><!-- Zero SSP Count --></label>
        <name>ffrr__ZeroSSPCount__c</name>
    </fields>
    <fields>
        <label><!-- Template --></label>
        <name>ffrr__ffrrtemplate__c</name>
        <relationshipLabel><!-- Revenue Contracts --></relationshipLabel>
    </fields>
    <nameFieldLabel>収益契約番号</nameFieldLabel>
    <webLinks>
        <label><!-- AllocateRevenue --></label>
        <name>ffrr__AllocateRevenue</name>
    </webLinks>
    <webLinks>
        <label><!-- ListAllocateRevenue --></label>
        <name>ffrr__ListAllocateRevenue</name>
    </webLinks>
    <webLinks>
        <label><!-- ListUpdatePerformanceObligations --></label>
        <name>ffrr__ListUpdatePerformanceObligations</name>
    </webLinks>
    <webLinks>
        <label><!-- ManageObligations --></label>
        <name>ffrr__ManageObligations</name>
    </webLinks>
    <webLinks>
        <label><!-- UpdatePerformanceObligations --></label>
        <name>ffrr__UpdatePerformanceObligations</name>
    </webLinks>
</CustomObjectTranslation>
