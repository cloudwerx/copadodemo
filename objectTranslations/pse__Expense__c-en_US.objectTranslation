<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldSets>
        <label><!-- Expense Columns For Combine Attachment PDF --></label>
        <name>pse__ExpenseColumnsForCombineAttachmentPDF</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Columns For Combine Attachment Page --></label>
        <name>pse__ExpenseColumnsForCombineAttachmentPage</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Header Row Editable Columns --></label>
        <name>pse__ExpenseHeaderRowEditableColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Header Row Read Only Columns --></label>
        <name>pse__ExpenseHeaderRowReadOnlyColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Notes Fields Editable Columns --></label>
        <name>pse__ExpenseNotesFieldsEditableColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Notes Fields Read Only Columns --></label>
        <name>pse__ExpenseNotesFieldsReadOnlyColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Mobile Expenses: Additional Fields in New and Edit Mode --></label>
        <name>pse__Mobile_Expenses_Additional_Fields</name>
    </fieldSets>
    <fields>
        <label><!-- Expense Notification --></label>
        <name>FFX_Expense_Notification__c</name>
    </fields>
    <fields>
        <help><!-- If checked, allows Admin to make &apos;global&apos; changes to the Expense, including edits to the Expense&apos;s Project, Resource, Currency, or Date, even if Include In Financials is checked. Config requirement: Actuals Calculation Mode must be set to &apos;Scheduled&apos;. --></help>
        <label><!-- Admin Global Edit --></label>
        <name>pse__Admin_Global_Edit__c</name>
    </fields>
    <fields>
        <label><!-- Amount To Bill --></label>
        <name>pse__Amount_To_Bill__c</name>
    </fields>
    <fields>
        <label><!-- Amount To Reimburse --></label>
        <name>pse__Amount_To_Reimburse__c</name>
    </fields>
    <fields>
        <label><!-- Amount --></label>
        <name>pse__Amount__c</name>
    </fields>
    <fields>
        <help><!-- Field will hold the Expense Rate applied on Expense. --></help>
        <label><!-- Applied Expense Rate --></label>
        <name>pse__Applied_Expense_Rate__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- This checkbox should be checked when the Expense is approved, which should be based on whether its parent Expense Report (if any) is approved. --></help>
        <label><!-- Approved --></label>
        <name>pse__Approved__c</name>
    </fields>
    <fields>
        <label><!-- Approved for Billing --></label>
        <name>pse__Approved_for_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Approved for Vendor Payment --></label>
        <name>pse__Approved_for_Vendor_Payment__c</name>
    </fields>
    <fields>
        <label><!-- Assignment --></label>
        <name>pse__Assignment__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Automatically checked by configuration to indicate whether expense attachments have been moved from the expense line to the expense report. --></help>
        <label><!-- Attachments moved to ER --></label>
        <name>pse__Attachments_moved_to_ER__c</name>
    </fields>
    <fields>
        <help><!-- Stores audit notes history. When a user changes a project or assignment using the Timecard UI and the audit notes exceed 255 characters, older audit notes are moved and appended here. --></help>
        <label><!-- Audit Notes History --></label>
        <name>pse__Audit_Notes_History__c</name>
    </fields>
    <fields>
        <label><!-- Audit Notes --></label>
        <name>pse__Audit_Notes__c</name>
    </fields>
    <fields>
        <label><!-- Bill Date --></label>
        <name>pse__Bill_Date__c</name>
    </fields>
    <fields>
        <label><!-- Bill Transaction --></label>
        <name>pse__Bill_Transaction__c</name>
        <relationshipLabel><!-- Expense (Bill Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- This formula equals Amount (minus Non-Billable Incurred Amount and Tax if any) for billable Expenses and zero for non-billable Expenses:  all in the incurred currency. --></help>
        <label><!-- Billable Amount --></label>
        <name>pse__Billable_Amount__c</name>
    </fields>
    <fields>
        <help><!-- Flat fee in Project currency by which to increase the billable amount of the Expense, defaulted from the Project. The calculation will apply any percentage expense fee*before* this flat expense fee amount. --></help>
        <label><!-- Billable Fee Flat Amount --></label>
        <name>pse__Billable_Fee_Flat_Amount__c</name>
    </fields>
    <fields>
        <help><!-- Text formula showing flat fee in Project currency by which to increase the billable amount of the Expense, defaulted from Project along with Project currency. The calculation will apply any percentage expense fee*before* this flat expense fee amount. --></help>
        <label><!-- Billable Fee Flat --></label>
        <name>pse__Billable_Fee_Flat__c</name>
    </fields>
    <fields>
        <help><!-- Fee percentage by which to increase the billable amount of the Expense, defaulted from the Project. 0% means no increase (default), whereas 100% means double. The calculation will apply the percentage expense fee*before* any flat expense fee. --></help>
        <label><!-- Billable Fee Percentage --></label>
        <name>pse__Billable_Fee_Percentage__c</name>
    </fields>
    <fields>
        <label><!-- Billable --></label>
        <name>pse__Billable__c</name>
    </fields>
    <fields>
        <label><!-- Billed --></label>
        <name>pse__Billed__c</name>
    </fields>
    <fields>
        <label><!-- Billing Amount (Pre-Fee Subtotal) --></label>
        <name>pse__Billing_Amount_Pre_Fee_Subtotal__c</name>
    </fields>
    <fields>
        <label><!-- Billing Amount --></label>
        <name>pse__Billing_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Billing Currency --></label>
        <name>pse__Billing_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Invoiced --></label>
        <name>pse__Billing_Event_Invoiced__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Item --></label>
        <name>pse__Billing_Event_Item__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Billing Event Released --></label>
        <name>pse__Billing_Event_Released__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Status --></label>
        <name>pse__Billing_Event_Status__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event --></label>
        <name>pse__Billing_Event__c</name>
    </fields>
    <fields>
        <label><!-- Billing Hold --></label>
        <name>pse__Billing_Hold__c</name>
    </fields>
    <fields>
        <label><!-- Cost Transaction --></label>
        <name>pse__Cost_Transaction__c</name>
        <relationshipLabel><!-- Expense (Cost Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>pse__Description__c</name>
    </fields>
    <fields>
        <help><!-- Distance traveled for auto mileage reimbursement. --></help>
        <label><!-- Distance --></label>
        <name>pse__Distance__c</name>
    </fields>
    <fields>
        <help><!-- Indicates Expense is in a state that is eligible for Billing Event Generation (not including the Approved for Billing flag, which may also be required per global config). --></help>
        <label><!-- Eligible for Billing --></label>
        <name>pse__Eligible_for_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Exchange Rate (Billing Currency) --></label>
        <name>pse__Exchange_Rate_Billing_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Exchange Rate (Incurred Currency) --></label>
        <name>pse__Exchange_Rate_Incurred_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Exchange Rate (Reimbursement Currency) --></label>
        <name>pse__Exchange_Rate_Reimbursement_Currency__c</name>
    </fields>
    <fields>
        <help><!-- When set, this field sets the *relative* exchange rate between Incurred Rate and Reimbursement Rate that is experienced by the Resource incurring the Expense. --></help>
        <label><!-- Exchange Rate (Resource-Defined) --></label>
        <name>pse__Exchange_Rate_Resource_Defined__c</name>
    </fields>
    <fields>
        <help><!-- If checked, never bill this business record.  Same effect as Billing Hold, but intended to reflect a permanent exclusion from Billing Generation. --></help>
        <label><!-- Exclude from Billing --></label>
        <name>pse__Exclude_from_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Expense Date --></label>
        <name>pse__Expense_Date__c</name>
    </fields>
    <fields>
        <label><!-- Expense Report --></label>
        <name>pse__Expense_Report__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Expense Split Parent --></label>
        <name>pse__Expense_Split_Parent__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Include In Financials --></label>
        <name>pse__Include_In_Financials__c</name>
    </fields>
    <fields>
        <help><!-- If checked for a billable Expense, this means that the portion of the Expense amount specified in the Incurred Tax field is not billable.  (This is separate from and in addition to any amount in the Non-Billable Incurred Amount field). --></help>
        <label><!-- Incurred Tax Non-Billable --></label>
        <name>pse__Incurred_Tax_Non_Billable__c</name>
    </fields>
    <fields>
        <help><!-- The amount of tax incurred --></help>
        <label><!-- Incurred Tax --></label>
        <name>pse__Incurred_Tax__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Date --></label>
        <name>pse__Invoice_Date__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Number --></label>
        <name>pse__Invoice_Number__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Transaction --></label>
        <name>pse__Invoice_Transaction__c</name>
        <relationshipLabel><!-- Expense (Invoice Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Invoiced --></label>
        <name>pse__Invoiced__c</name>
    </fields>
    <fields>
        <help><!-- This field is visible on UI if user requires attachment on expense lines if Expense report is submitted. If this field is set true then user does not need to add attachment although the config is set to true. --></help>
        <label><!-- Lost Receipt --></label>
        <name>pse__Lost_Receipt__c</name>
    </fields>
    <fields>
        <label><!-- Milestone --></label>
        <name>pse__Milestone__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Mobile Expense Reference ID --></label>
        <name>pse__Mobile_Expense_Reference_ID__c</name>
    </fields>
    <fields>
        <help><!-- This formula equals Amount for non-billable Expenses and Non-Billable Incurred Amount and Tax (anywhere from zero to Expense Amount) for billable Expenses: all in the incurred currency. --></help>
        <label><!-- Non-Billable Amount --></label>
        <name>pse__Non_Billable_Amount__c</name>
    </fields>
    <fields>
        <help><!-- If provided for a billable Expense, this is subtracted from the Expense Amount (incurred currency) to calculate the Expense&apos;s billable amount.  Min. value zero, max. value Expense amount.  Default value:  zero or null.  No effect on non-billable Expenses. --></help>
        <label><!-- Non-Billable Incurred Amount --></label>
        <name>pse__Non_Billable_Incurred_Amount__c</name>
    </fields>
    <fields>
        <help><!-- This formula adds any Non-Billable Incurred Tax (only if Incurred Tax Non-Billable is checked) to any additional Non-Billable Incurred Amount to get a Non-Billable Incurred Subtotal, with a minimum of 0.00 and a maximum of the Expense Amount. --></help>
        <label><!-- Non-Billable Incurred Subtotal --></label>
        <name>pse__Non_Billable_Incurred_Subtotal__c</name>
    </fields>
    <fields>
        <help><!-- If this checkbox is checked, the Reimbursement amount for the Expense will be zero regardless of the incurred amount. --></help>
        <label><!-- Non-Reimbursable --></label>
        <name>pse__Non_Reimbursible__c</name>
    </fields>
    <fields>
        <label><!-- Notes --></label>
        <name>pse__Notes__c</name>
    </fields>
    <fields>
        <label><!-- Override Group Currency Code --></label>
        <name>pse__Override_Group_Currency_Code__c</name>
    </fields>
    <fields>
        <help><!-- Overrides the Group to which the Expense Transactions will roll up for Group Actuals, even if the Project is in a different Group. Typically an Expense&apos;s Transactions roll up to its Project&apos;s or Resource&apos;s Group based on &apos;follows&apos; rules. --></help>
        <label><!-- Override Group --></label>
        <name>pse__Override_Group__c</name>
        <relationshipLabel><!-- Override Group For Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Practice Currency Code --></label>
        <name>pse__Override_Practice_Currency_Code__c</name>
    </fields>
    <fields>
        <help><!-- Overrides the Practice to which the Expense Transactions will roll up for Practice Actuals, even if the Project is in a different Practice. Typically an Expense&apos;s Transactions roll up to its Project&apos;s or Resource&apos;s Practice based on &apos;follows&apos; rules. --></help>
        <label><!-- Override Practice --></label>
        <name>pse__Override_Practice__c</name>
        <relationshipLabel><!-- Override Practice For Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Rate (Billing Currency) --></label>
        <name>pse__Override_Rate_Billing_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Override Rate (Incurred Currency) --></label>
        <name>pse__Override_Rate_Incurred_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Override Rate (Reimbursement Currency) --></label>
        <name>pse__Override_Rate_Reimbursement_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Override Region Currency Code --></label>
        <name>pse__Override_Region_Currency_Code__c</name>
    </fields>
    <fields>
        <help><!-- Overrides the Region to which the Expense Transactions will roll up for Regional Actuals, even if the Project is in a different Region. Typically an Expense&apos;s Transactions roll up to its Project&apos;s or Resource&apos;s Region based on &apos;follows&apos; rules. --></help>
        <label><!-- Override Region --></label>
        <name>pse__Override_Region__c</name>
        <relationshipLabel><!-- Override Region For Expenses --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Lookup  to Project Methodology --></help>
        <label><!-- Project Methodology --></label>
        <name>pse__Project_Methodology__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Lookup to Project Phase --></help>
        <label><!-- Project Phase --></label>
        <name>pse__Project_Phase__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Rate Unit --></label>
        <name>pse__Rate_Unit__c</name>
        <picklistValues>
            <masterLabel>Kilometer</masterLabel>
            <translation><!-- Kilometer --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Mile</masterLabel>
            <translation><!-- Mile --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The recognition method to apply to this record for revenue forecasting. --></help>
        <label><!-- Recognition Method --></label>
        <name>pse__Recognition_Method__c</name>
    </fields>
    <fields>
        <help><!-- This field stores a numeric value for the Reimbursement Amount converted to the Project (billing) currency using system (not overridden) rates, for later use in vendor invoicing. --></help>
        <label><!-- Reimbursement Amount In Project Currency --></label>
        <name>pse__Reimbursement_Amount_In_Project_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Reimbursement Amount --></label>
        <name>pse__Reimbursement_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Reimbursement Currency --></label>
        <name>pse__Reimbursement_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Resource --></label>
        <name>pse__Resource__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Revenue Transaction --></label>
        <name>pse__Revenue_Transaction__c</name>
        <relationshipLabel><!-- Expense (Revenue Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Split Expense --></label>
        <name>pse__Split_Expense__c</name>
    </fields>
    <fields>
        <label><!-- Split Notes --></label>
        <name>pse__Split_Notes__c</name>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>pse__Status__c</name>
        <picklistValues>
            <masterLabel>Approved</masterLabel>
            <translation><!-- Approved --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Draft</masterLabel>
            <translation><!-- Draft --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rejected</masterLabel>
            <translation><!-- Rejected --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Submitted</masterLabel>
            <translation><!-- Submitted --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Submitted --></label>
        <name>pse__Submitted__c</name>
    </fields>
    <fields>
        <help><!-- This checkbox is for system use.  It is intended to be checked in conjunction with any insert/update of the Timecard Split executed within a future method, and will then ensure that no downstream future methods are called. --></help>
        <label><!-- Synchronous Update Required --></label>
        <name>pse__Synchronous_Update_Required__c</name>
    </fields>
    <fields>
        <help><!-- An optional type of tax that has been included in the expense --></help>
        <label><!-- Tax Type --></label>
        <name>pse__Tax_Type__c</name>
        <picklistValues>
            <masterLabel>GST</masterLabel>
            <translation><!-- GST --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>VAT 1</masterLabel>
            <translation><!-- VAT 1 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>VAT 2</masterLabel>
            <translation><!-- VAT 2 --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The Expense ID from the Third-Party Expenses Application. --></help>
        <label><!-- Third-Party Expenses App Expense ID --></label>
        <name>pse__Third_Party_Expenses_App_Expense_ID__c</name>
    </fields>
    <fields>
        <label><!-- Type --></label>
        <name>pse__Type__c</name>
        <picklistValues>
            <masterLabel>Airfare</masterLabel>
            <translation><!-- Airfare --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Auto Mileage</masterLabel>
            <translation><!-- Auto Mileage --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Business Meals</masterLabel>
            <translation><!-- Business Meals --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Car Rental</masterLabel>
            <translation><!-- Car Rental --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Employee Relations</masterLabel>
            <translation><!-- Employee Relations --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Gasoline</masterLabel>
            <translation><!-- Gasoline --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>General and Admin Expenses</masterLabel>
            <translation><!-- General and Admin Expenses --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>IT Equipment and Services</masterLabel>
            <translation><!-- IT Equipment and Services --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Lodging (Room and Tax)</masterLabel>
            <translation><!-- Lodging (Room and Tax) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Miscellaneous</masterLabel>
            <translation><!-- Miscellaneous --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Office Supplies and Services</masterLabel>
            <translation><!-- Office Supplies and Services --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Personal Meals</masterLabel>
            <translation><!-- Personal Meals --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Phone</masterLabel>
            <translation><!-- Phone --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Postage and Shipping</masterLabel>
            <translation><!-- Postage and Shipping --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Recruiting</masterLabel>
            <translation><!-- Recruiting --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Subway</masterLabel>
            <translation><!-- Subway --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Taxi</masterLabel>
            <translation><!-- Taxi --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Vendor Invoice Item --></label>
        <name>pse__Vendor_Invoice_Item__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <sharingReasons>
        <label><!-- PSE Approver Share --></label>
        <name>pse__PSE_Approver_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE Member Share --></label>
        <name>pse__PSE_Member_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE PM Share --></label>
        <name>pse__PSE_PM_Share</name>
    </sharingReasons>
    <validationRules>
        <errorMessage><!-- Assignment Project must match Expense Project. --></errorMessage>
        <name>pse__Expense_Asgnmt_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Assignment Resource must match Expense Resource. --></errorMessage>
        <name>pse__Expense_Asgnmt_Resource_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Expense Line Project must match Expense Report Project. --></errorMessage>
        <name>pse__Expense_ER_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Expense Line Resource must match Expense Report Resource. --></errorMessage>
        <name>pse__Expense_ER_Resource_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- For an Expense Line to be marked as Billed, it must also be marked as Included in Financials and Approved. --></errorMessage>
        <name>pse__Expense_Line_Billing_Requires_Inclusion</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- For an Expense Line to be marked as Invoiced, it must also be marked as Billed. --></errorMessage>
        <name>pse__Expense_Line_Invoicing_Requires_Billing</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- An Expense Line may not be submitted or included in financials unless it belongs to an Expense Report. --></errorMessage>
        <name>pse__Expense_Line_Needs_Parent_For_Submit</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Expense Line&apos;s Project field value may not be updated once set (unless parent Expense Report is changed, Admin Global Edit is checked, Audit Notes are provided, and Actuals Calculation Mode is Scheduled). --></errorMessage>
        <name>pse__Expense_Line_Project_is_Constant</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Expense Line&apos;s Expense Report may not be changed if it is submitted or included in financials. --></errorMessage>
        <name>pse__Expense_Line_Report_Fixed_If_Submitted</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Expense Line&apos;s Resource field value may not be blank and may not be updated once set (unless Admin Global Edit is checked, Audit Notes are provided, and Actuals Calculation Mode is Scheduled). --></errorMessage>
        <name>pse__Expense_Line_Resource_is_Constant</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- An Expense may only be marked as Billable if its Project is Billable and its Expense Report and Assignment, if any, are Billable. --></errorMessage>
        <name>pse__Expense_May_Not_Be_Billable</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Milestone Project must match Expense Project. --></errorMessage>
        <name>pse__Expense_Milestone_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Expense Report is required --></errorMessage>
        <name>pse__Expense_Requires_Expense_Report</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- If provided, the incurred tax amount for an Expense must not be less than zero or greater than the Expense amount itself. --></errorMessage>
        <name>pse__Incurred_Tax_Amount_Invalid</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Invoice Number or Date is not allowed on record unless it is invoiced --></errorMessage>
        <name>pse__Invoiced</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- If provided, the non-billable incurred amount for an Expense must not be less than zero or greater than the Expense amount itself. --></errorMessage>
        <name>pse__Non_Billable_Incurred_Amount_Invalid</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The non-billable incurred subtotal for an Expense (any non-billable amount plus any non-billable tax) must not be greater than the Expense amount itself. --></errorMessage>
        <name>pse__Non_Billable_Subtotal_Invalid</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Resource-defined Exchange Rate can only be set if Incurred and Resource currencies are different.  If the currencies are the same, a relative rate of 1.0 is implied. --></errorMessage>
        <name>pse__Res_Defined_Rate_Diff_Currencies_Only</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Resource-defined Exchange Rate must either be greater than 0.0 (zero) or null/empty. --></errorMessage>
        <name>pse__Res_Defined_Rate_may_not_be_Negative</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The vendor invoice cannot change once set. --></errorMessage>
        <name>pse__Vendor_Invoice_Constant</name>
    </validationRules>
    <webLinks>
        <label><!-- Admin_Edit --></label>
        <name>pse__Admin_Edit</name>
    </webLinks>
    <webLinks>
        <label><!-- Clear_Billing_Data --></label>
        <name>pse__Clear_Billing_Data</name>
    </webLinks>
    <webLinks>
        <label><!-- Edit_Expenses --></label>
        <name>pse__Edit_Expenses</name>
    </webLinks>
    <webLinks>
        <label><!-- Multiple_Expense_Entry_UI --></label>
        <name>pse__Multiple_Expense_Entry_UI</name>
    </webLinks>
    <webLinks>
        <label><!-- Ready_to_Submit --></label>
        <name>pse__Ready_to_Submit</name>
    </webLinks>
</CustomObjectTranslation>
