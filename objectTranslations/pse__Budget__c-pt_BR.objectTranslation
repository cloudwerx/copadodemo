<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Despesas</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Orçamentos</value>
    </caseValues>
    <fieldSets>
        <label><!-- Create Project From Opp And Template Budget Columns --></label>
        <name>pse__CreateProjectFromOppAndTempBudgetColumns</name>
    </fieldSets>
    <fields>
        <label><!-- Account --></label>
        <name>pse__Account__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <help>Se marcado, permite que o Admin faça mudanças ’globais’ no orçamento, incluindo edições no projeto, recurso, moeda ou data do orçamento, mesmo se Incluir nas finanças estiver marcado. Requisito de configuração: o modo de cálculo de reais deve ser definido como ’Programado’.</help>
        <label><!-- Admin Global Edit --></label>
        <name>pse__Admin_Global_Edit__c</name>
    </fields>
    <fields>
        <help>O valor monetário do orçamento, sem incluir qualquer valor inserido separadamente no campo Valor da despesa. O padrão é e deve sempre ser a mesma moeda do Projeto.</help>
        <label><!-- Amount --></label>
        <name>pse__Amount__c</name>
    </fields>
    <fields>
        <help>Esta caixa de seleção deve ser marcada quando o orçamento é aprovado - normalmente com base no campo Status.</help>
        <label><!-- Approved --></label>
        <name>pse__Approved__c</name>
    </fields>
    <fields>
        <label><!-- Approved for Billing --></label>
        <name>pse__Approved_for_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Approver --></label>
        <name>pse__Approver__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Audit Notes --></label>
        <name>pse__Audit_Notes__c</name>
    </fields>
    <fields>
        <label><!-- Bill Date --></label>
        <name>pse__Bill_Date__c</name>
    </fields>
    <fields>
        <label><!-- Billable --></label>
        <name>pse__Billable__c</name>
    </fields>
    <fields>
        <label><!-- Billed --></label>
        <name>pse__Billed__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Invoiced --></label>
        <name>pse__Billing_Event_Invoiced__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Item --></label>
        <name>pse__Billing_Event_Item__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Billing Event Released --></label>
        <name>pse__Billing_Event_Released__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Status --></label>
        <name>pse__Billing_Event_Status__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event --></label>
        <name>pse__Billing_Event__c</name>
    </fields>
    <fields>
        <label><!-- Billing Hold --></label>
        <name>pse__Billing_Hold__c</name>
    </fields>
    <fields>
        <label><!-- Budget Header --></label>
        <name>pse__Budget_Header__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>pse__Description__c</name>
    </fields>
    <fields>
        <label><!-- Effective Date --></label>
        <name>pse__Effective_Date__c</name>
    </fields>
    <fields>
        <help>Indica que o orçamento está em um estado que é elegível para geração de evento de cobrança (não incluindo o sinalizador aprovado para cobrança, que também pode ser exigido por configuração global).</help>
        <label><!-- Eligible for Billing --></label>
        <name>pse__Eligible_for_Billing__c</name>
    </fields>
    <fields>
        <help>Se marcada, nunca fature esse registro comercial. O mesmo efeito da suspensão de faturamento, mas pretende refletir uma exclusão permanente da geração de faturamento.</help>
        <label><!-- Exclude from Billing --></label>
        <name>pse__Exclude_from_Billing__c</name>
    </fields>
    <fields>
        <help>A quantia monetária (se houver) explicitamente reservada para despesas no orçamento. Este é separado e adicional ao campo Valor do orçamento. O padrão é e deve sempre ser a mesma moeda do Projeto.</help>
        <label><!-- Expense Amount --></label>
        <name>pse__Expense_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Expense Transaction --></label>
        <name>pse__Expense_Transaction__c</name>
        <relationshipLabel><!-- Budget (Expense Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Include In Financials --></label>
        <name>pse__Include_In_Financials__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Date --></label>
        <name>pse__Invoice_Date__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Number --></label>
        <name>pse__Invoice_Number__c</name>
    </fields>
    <fields>
        <label><!-- Invoiced --></label>
        <name>pse__Invoiced__c</name>
    </fields>
    <fields>
        <label><!-- Opportunity --></label>
        <name>pse__Opportunity__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Project Group Currency Code --></label>
        <name>pse__Override_Project_Group_Currency_Code__c</name>
    </fields>
    <fields>
        <help>Se este campo for definido, ele substituirá o grupo para o qual as transações de orçamento serão acumuladas para os reais do grupo, mesmo se seu projeto estiver em um grupo diferente. Normalmente, as transações de um orçamento são acumuladas até o grupo do projeto.</help>
        <label><!-- Override Project Group --></label>
        <name>pse__Override_Project_Group__c</name>
        <relationshipLabel><!-- Override Group For Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Project Practice Currency Code --></label>
        <name>pse__Override_Project_Practice_Currency_Code__c</name>
    </fields>
    <fields>
        <help>Se este campo for definido, ele substitui a prática para a qual as transações de um orçamento serão acumuladas para reais da prática, mesmo se seu projeto estiver em uma prática diferente. Normalmente, as transações de um orçamento são acumuladas na prática do projeto.</help>
        <label><!-- Override Project Practice --></label>
        <name>pse__Override_Project_Practice__c</name>
        <relationshipLabel><!-- Override Practice For Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Project Region Currency Code --></label>
        <name>pse__Override_Project_Region_Currency_Code__c</name>
    </fields>
    <fields>
        <help>Se este campo for definido, ele substitui a região para a qual as transações de orçamento serão acumuladas para reais regionais, mesmo se seu projeto estiver em uma região diferente. Normalmente, as transações de um orçamento são acumuladas para a região do projeto.</help>
        <label><!-- Override Project Region --></label>
        <name>pse__Override_Project_Region__c</name>
        <relationshipLabel><!-- Override Region For Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- DEPRECATED: Pre-Billed Amount --></label>
        <name>pse__PreBilledAmount__c</name>
    </fields>
    <fields>
        <help>A parte dos valores combinados de orçamento / despesa a ser pré-faturada. O padrão é e deve sempre ser a mesma moeda do Projeto. Este campo é uma substituição para o antigo campo Pre-Billed Amount (PreBilledAmount__c), que não permitia números decimais não inteiros.</help>
        <label><!-- Pre-Billed Amount --></label>
        <name>pse__Pre_Billed_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Pre-Billed Transaction --></label>
        <name>pse__Pre_Billed_Transaction__c</name>
        <relationshipLabel><!-- Budget (Pre-Billed Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>pse__Status__c</name>
        <picklistValues>
            <masterLabel>Approved</masterLabel>
            <translation>Aprovado</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Draft</masterLabel>
            <translation>Esboço, projeto</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Open</masterLabel>
            <translation>Abrir</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rejected</masterLabel>
            <translation>Rejeitado</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Total Amount --></label>
        <name>pse__Total_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Transaction --></label>
        <name>pse__Transaction__c</name>
        <relationshipLabel><!-- Budget --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Type --></label>
        <name>pse__Type__c</name>
        <picklistValues>
            <masterLabel>Customer Purchase Order</masterLabel>
            <translation>Pedido de Compra do Cliente</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Customer Purchase Order Change Request</masterLabel>
            <translation>Solicitação de mudança de ordem de compra do cliente</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Internal Budget</masterLabel>
            <translation>Orçamento Interno</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Internal Budget Change Request</masterLabel>
            <translation>Solicitação de mudança de orçamento interno</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Vendor Purchase Order</masterLabel>
            <translation>Pedido de compra do fornecedor</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Vendor Purchase Order Change Request</masterLabel>
            <translation>Solicitação de mudança de ordem de compra do fornecedor</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Work Order</masterLabel>
            <translation>Ordem de Serviço</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Work Order Change Request</masterLabel>
            <translation>Solicitação de mudança de ordem de serviço</translation>
        </picklistValues>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Nome do orçamento</nameFieldLabel>
    <sharingReasons>
        <label><!-- PSE Approver Share --></label>
        <name>pse__PSE_Approver_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE Member Share --></label>
        <name>pse__PSE_Member_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE PM Share --></label>
        <name>pse__PSE_PM_Share</name>
    </sharingReasons>
    <validationRules>
        <errorMessage><!-- The budget header&apos;s account must match the account on this budget --></errorMessage>
        <name>pse__Budget_Header_Account_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The budget header&apos;s project does not match the project on this budget --></errorMessage>
        <name>pse__Budget_Header_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The budget header&apos;s type must match the type on this budget --></errorMessage>
        <name>pse__Budget_Header_Type_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Budget&apos;s Project field value may not be blank and may not be updated once set (unless Admin Global Edit is checked, Audit Notes are provided, and Actuals Calculation Mode is Scheduled). --></errorMessage>
        <name>pse__Budget_Project_is_Constant</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Amount should be a positive value --></errorMessage>
        <name>pse__Has_non_negative_budget_amount</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Pre Billed Amount should be a positive value --></errorMessage>
        <name>pse__Has_non_negative_pre_billed_amount</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Master Budget cannot be changed once set. --></errorMessage>
        <name>pse__Master_Budget_is_Constant</name>
    </validationRules>
    <webLinks>
        <label><!-- Clear_Billing_Data --></label>
        <name>pse__Clear_Billing_Data</name>
    </webLinks>
</CustomObjectTranslation>
