<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <article>Definite</article>
        <plural>false</plural>
        <value>Budget</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>false</plural>
        <value>Budget</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>false</plural>
        <value>Budget</value>
    </caseValues>
    <caseValues>
        <article>Definite</article>
        <plural>true</plural>
        <value>Budgetter</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>true</plural>
        <value>Budgetter</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>true</plural>
        <value>Budgetter</value>
    </caseValues>
    <fieldSets>
        <label><!-- Create Project From Opp And Template Budget Columns --></label>
        <name>pse__CreateProjectFromOppAndTempBudgetColumns</name>
    </fieldSets>
    <fields>
        <label><!-- Account --></label>
        <name>pse__Account__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <help>Hvis markeret, tillader Admin at lave globale ændringer til budgettet, herunder redigeringer til budgetprojektet, Ressource, Valuta eller Dato, selvom Inkluder i Finans er markeret. Config krav: Aktuals Beregning Mode skal indstilles til &apos;Planlagt&apos;.</help>
        <label><!-- Admin Global Edit --></label>
        <name>pse__Admin_Global_Edit__c</name>
    </fields>
    <fields>
        <help>Det monetære beløb for budgettet, ikke inklusive et beløb, der er angivet separat i feltet Udgiftsbeløb. Standard og skal altid være samme valuta som Project.</help>
        <label><!-- Amount --></label>
        <name>pse__Amount__c</name>
    </fields>
    <fields>
        <help>Denne afkrydsningsfelt skal kontrolleres, når budgettet er godkendt - typisk baseret på statusfeltet.</help>
        <label><!-- Approved --></label>
        <name>pse__Approved__c</name>
    </fields>
    <fields>
        <label><!-- Approved for Billing --></label>
        <name>pse__Approved_for_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Approver --></label>
        <name>pse__Approver__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Audit Notes --></label>
        <name>pse__Audit_Notes__c</name>
    </fields>
    <fields>
        <label><!-- Bill Date --></label>
        <name>pse__Bill_Date__c</name>
    </fields>
    <fields>
        <label><!-- Billable --></label>
        <name>pse__Billable__c</name>
    </fields>
    <fields>
        <label><!-- Billed --></label>
        <name>pse__Billed__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Invoiced --></label>
        <name>pse__Billing_Event_Invoiced__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Item --></label>
        <name>pse__Billing_Event_Item__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Billing Event Released --></label>
        <name>pse__Billing_Event_Released__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Status --></label>
        <name>pse__Billing_Event_Status__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event --></label>
        <name>pse__Billing_Event__c</name>
    </fields>
    <fields>
        <label><!-- Billing Hold --></label>
        <name>pse__Billing_Hold__c</name>
    </fields>
    <fields>
        <label><!-- Budget Header --></label>
        <name>pse__Budget_Header__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>pse__Description__c</name>
    </fields>
    <fields>
        <label><!-- Effective Date --></label>
        <name>pse__Effective_Date__c</name>
    </fields>
    <fields>
        <help>Angiver, at budgettet er i en stat, der er berettiget til fakturering af generering af fakturaer (ikke inklusive flag for godkendt for fakturering, hvilket også kan kræves pr. Global konfiguration).</help>
        <label><!-- Eligible for Billing --></label>
        <name>pse__Eligible_for_Billing__c</name>
    </fields>
    <fields>
        <help>Hvis markeret, skal du aldrig fakturere denne virksomhedsfortegnelse. Samme virkning som Billing Hold, men beregnet til at afspejle en permanent udelukkelse fra Billing Generation.</help>
        <label><!-- Exclude from Billing --></label>
        <name>pse__Exclude_from_Billing__c</name>
    </fields>
    <fields>
        <help>Det monetære beløb (hvis nogen) udtrykkeligt afsat til udgifter i budgettet. Dette er adskilt fra og ud over feltet Budgetbeløb. Standard og skal altid være samme valuta som Project.</help>
        <label><!-- Expense Amount --></label>
        <name>pse__Expense_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Expense Transaction --></label>
        <name>pse__Expense_Transaction__c</name>
        <relationshipLabel><!-- Budget (Expense Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Include In Financials --></label>
        <name>pse__Include_In_Financials__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Date --></label>
        <name>pse__Invoice_Date__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Number --></label>
        <name>pse__Invoice_Number__c</name>
    </fields>
    <fields>
        <label><!-- Invoiced --></label>
        <name>pse__Invoiced__c</name>
    </fields>
    <fields>
        <label><!-- Opportunity --></label>
        <name>pse__Opportunity__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Project Group Currency Code --></label>
        <name>pse__Override_Project_Group_Currency_Code__c</name>
    </fields>
    <fields>
        <help>Hvis dette felt er indstillet, tilsidesætter det den gruppe, som et budgets transaktioner vil rulle op for Gruppelovgivninger, selvom projektet er i en anden gruppe. Typisk ruller et Budgets Transaktioner op til Projektets Gruppe.</help>
        <label><!-- Override Project Group --></label>
        <name>pse__Override_Project_Group__c</name>
        <relationshipLabel><!-- Override Group For Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Project Practice Currency Code --></label>
        <name>pse__Override_Project_Practice_Currency_Code__c</name>
    </fields>
    <fields>
        <help>Hvis dette felt er indstillet, tilsidesætter det den praksis, som et budgets transaktioner vil rulle til for praksishandlinger, selvom projektet er i en anden praksis. Et budgets transaktioner går typisk op til projektets praksis.</help>
        <label><!-- Override Project Practice --></label>
        <name>pse__Override_Project_Practice__c</name>
        <relationshipLabel><!-- Override Practice For Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Project Region Currency Code --></label>
        <name>pse__Override_Project_Region_Currency_Code__c</name>
    </fields>
    <fields>
        <help>Hvis dette felt er indstillet, tilsidesætter det det område, hvortil et budgets Transaktioner vil kaste op for regionale aktører, selvom projektet er i en anden region. Typisk ruller et Budgets Transaktioner op til Projektets Region.</help>
        <label><!-- Override Project Region --></label>
        <name>pse__Override_Project_Region__c</name>
        <relationshipLabel><!-- Override Region For Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- DEPRECATED: Pre-Billed Amount --></label>
        <name>pse__PreBilledAmount__c</name>
    </fields>
    <fields>
        <help>Den del af de samlede budget / udgiftsbeløb, der skal forudregistreres. Standard og skal altid være samme valuta som Project. Dette felt er en erstatning for det gamle Faktura for forudbetaling (PreBilledAmount__c), som ikke tillod ikke-integer-decimaler.</help>
        <label><!-- Pre-Billed Amount --></label>
        <name>pse__Pre_Billed_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Pre-Billed Transaction --></label>
        <name>pse__Pre_Billed_Transaction__c</name>
        <relationshipLabel><!-- Budget (Pre-Billed Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>pse__Status__c</name>
        <picklistValues>
            <masterLabel>Approved</masterLabel>
            <translation>Godkendt</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Draft</masterLabel>
            <translation>Udkast</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Open</masterLabel>
            <translation>Åben</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rejected</masterLabel>
            <translation>Afvist</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Total Amount --></label>
        <name>pse__Total_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Transaction --></label>
        <name>pse__Transaction__c</name>
        <relationshipLabel><!-- Budget --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Type --></label>
        <name>pse__Type__c</name>
        <picklistValues>
            <masterLabel>Customer Purchase Order</masterLabel>
            <translation>Kunde Indkøbsordre</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Customer Purchase Order Change Request</masterLabel>
            <translation>Forespørgsel om købsordreændring</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Internal Budget</masterLabel>
            <translation>Internt budget</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Internal Budget Change Request</masterLabel>
            <translation>Intern budgetændringsanmodning</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Vendor Purchase Order</masterLabel>
            <translation>Leverandør Indkøbsordre</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Vendor Purchase Order Change Request</masterLabel>
            <translation>Forhandler Indkøbsordre Ændring Request</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Work Order</masterLabel>
            <translation>Arbejdsordre</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Work Order Change Request</masterLabel>
            <translation>Arbejdsordreændringsanmodning</translation>
        </picklistValues>
    </fields>
    <gender>Feminine</gender>
    <nameFieldLabel>Budgetnavn</nameFieldLabel>
    <sharingReasons>
        <label><!-- PSE Approver Share --></label>
        <name>pse__PSE_Approver_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE Member Share --></label>
        <name>pse__PSE_Member_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE PM Share --></label>
        <name>pse__PSE_PM_Share</name>
    </sharingReasons>
    <validationRules>
        <errorMessage><!-- The budget header&apos;s account must match the account on this budget --></errorMessage>
        <name>pse__Budget_Header_Account_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The budget header&apos;s project does not match the project on this budget --></errorMessage>
        <name>pse__Budget_Header_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The budget header&apos;s type must match the type on this budget --></errorMessage>
        <name>pse__Budget_Header_Type_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Budget&apos;s Project field value may not be blank and may not be updated once set (unless Admin Global Edit is checked, Audit Notes are provided, and Actuals Calculation Mode is Scheduled). --></errorMessage>
        <name>pse__Budget_Project_is_Constant</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Amount should be a positive value --></errorMessage>
        <name>pse__Has_non_negative_budget_amount</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Pre Billed Amount should be a positive value --></errorMessage>
        <name>pse__Has_non_negative_pre_billed_amount</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Master Budget cannot be changed once set. --></errorMessage>
        <name>pse__Master_Budget_is_Constant</name>
    </validationRules>
    <webLinks>
        <label><!-- Clear_Billing_Data --></label>
        <name>pse__Clear_Billing_Data</name>
    </webLinks>
</CustomObjectTranslation>
