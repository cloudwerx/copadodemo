<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>予算</value>
    </caseValues>
    <fieldSets>
        <label><!-- Create Project From Opp And Template Budget Columns --></label>
        <name>pse__CreateProjectFromOppAndTempBudgetColumns</name>
    </fieldSets>
    <fields>
        <label><!-- Account --></label>
        <name>pse__Account__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <help>オンにすると、[財務に含める]がオンになっていても、管理者は予算のプロジェクト、リソース、通貨、または日付の編集を含め、予算に対して「グローバル」な変更を加えることができます。構成要件：実績計算モードを「スケジュール済み」に設定する必要があります。</help>
        <label><!-- Admin Global Edit --></label>
        <name>pse__Admin_Global_Edit__c</name>
    </fields>
    <fields>
        <help>予算の金額で、[費用額]フィールドに個別に入力した金額は含まれません。既定の設定で、常にProjectと同じ通貨である必要があります。</help>
        <label><!-- Amount --></label>
        <name>pse__Amount__c</name>
    </fields>
    <fields>
        <help>このチェックボックスは、予算が承認されたとき（通常は[ステータス]フィールドに基づく）にチェックする必要があります。</help>
        <label><!-- Approved --></label>
        <name>pse__Approved__c</name>
    </fields>
    <fields>
        <label><!-- Approved for Billing --></label>
        <name>pse__Approved_for_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Approver --></label>
        <name>pse__Approver__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Audit Notes --></label>
        <name>pse__Audit_Notes__c</name>
    </fields>
    <fields>
        <label><!-- Bill Date --></label>
        <name>pse__Bill_Date__c</name>
    </fields>
    <fields>
        <label><!-- Billable --></label>
        <name>pse__Billable__c</name>
    </fields>
    <fields>
        <label><!-- Billed --></label>
        <name>pse__Billed__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Invoiced --></label>
        <name>pse__Billing_Event_Invoiced__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Item --></label>
        <name>pse__Billing_Event_Item__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Billing Event Released --></label>
        <name>pse__Billing_Event_Released__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Status --></label>
        <name>pse__Billing_Event_Status__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event --></label>
        <name>pse__Billing_Event__c</name>
    </fields>
    <fields>
        <label><!-- Billing Hold --></label>
        <name>pse__Billing_Hold__c</name>
    </fields>
    <fields>
        <label><!-- Budget Header --></label>
        <name>pse__Budget_Header__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>pse__Description__c</name>
    </fields>
    <fields>
        <label><!-- Effective Date --></label>
        <name>pse__Effective_Date__c</name>
    </fields>
    <fields>
        <help>予算が請求イベント生成に適した状態にあることを示します（グローバル設定ごとにも必要な場合がある[Approved for Billing]フラグは含まれません）。</help>
        <label><!-- Eligible for Billing --></label>
        <name>pse__Eligible_for_Billing__c</name>
    </fields>
    <fields>
        <help>チェックした場合、このビジネス記録に請求しないでください。 Billing Holdと同じ効果ですが、Billing Generationからの永久的な除外を反映することを目的としています。</help>
        <label><!-- Exclude from Billing --></label>
        <name>pse__Exclude_from_Billing__c</name>
    </fields>
    <fields>
        <help>予算内の費用として明示的に確保されている金額（ある場合）。これは、[予算金額]フィールドとは別のものです。既定の設定で、常にProjectと同じ通貨である必要があります。</help>
        <label><!-- Expense Amount --></label>
        <name>pse__Expense_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Expense Transaction --></label>
        <name>pse__Expense_Transaction__c</name>
        <relationshipLabel><!-- Budget (Expense Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Include In Financials --></label>
        <name>pse__Include_In_Financials__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Date --></label>
        <name>pse__Invoice_Date__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Number --></label>
        <name>pse__Invoice_Number__c</name>
    </fields>
    <fields>
        <label><!-- Invoiced --></label>
        <name>pse__Invoiced__c</name>
    </fields>
    <fields>
        <label><!-- Opportunity --></label>
        <name>pse__Opportunity__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Project Group Currency Code --></label>
        <name>pse__Override_Project_Group_Currency_Code__c</name>
    </fields>
    <fields>
        <help>このフィールドが設定されていると、そのプロジェクトが別のグループに属していても、予算の取引がグループ実績にロールアップするグループをオーバーライドします。通常、予算取引はそのプロジェクトのグループにロールアップします。</help>
        <label><!-- Override Project Group --></label>
        <name>pse__Override_Project_Group__c</name>
        <relationshipLabel><!-- Override Group For Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Project Practice Currency Code --></label>
        <name>pse__Override_Project_Practice_Currency_Code__c</name>
    </fields>
    <fields>
        <help>このフィールドが設定されていると、そのプロジェクトが別のプラクティスに属していても、予算のトランザクションがプラクティス実績にロールアップするプラクティスをオーバーライドします。通常、予算取引はそのプロジェクトの実務にロールアップします。</help>
        <label><!-- Override Project Practice --></label>
        <name>pse__Override_Project_Practice__c</name>
        <relationshipLabel><!-- Override Practice For Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Project Region Currency Code --></label>
        <name>pse__Override_Project_Region_Currency_Code__c</name>
    </fields>
    <fields>
        <help>このフィールドが設定されている場合、そのプロジェクトが別の地域にある場合でも、予算の取引が地域の実績にロールアップする地域をオーバーライドします。通常、予算取引はそのプロジェクトの地域にロールアップします。</help>
        <label><!-- Override Project Region --></label>
        <name>pse__Override_Project_Region__c</name>
        <relationshipLabel><!-- Override Region For Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- DEPRECATED: Pre-Billed Amount --></label>
        <name>pse__PreBilledAmount__c</name>
    </fields>
    <fields>
        <help>予算/支出額の合計のうち、事前請求される金額。既定の設定で、常にProjectと同じ通貨である必要があります。このフィールドは、整数以外の小数点以下桁数を許可していなかった古いPre-Billed Amountフィールド（PreBilledAmount__c）に代わるものです。</help>
        <label><!-- Pre-Billed Amount --></label>
        <name>pse__Pre_Billed_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Pre-Billed Transaction --></label>
        <name>pse__Pre_Billed_Transaction__c</name>
        <relationshipLabel><!-- Budget (Pre-Billed Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>pse__Status__c</name>
        <picklistValues>
            <masterLabel>Approved</masterLabel>
            <translation>承認済み</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Draft</masterLabel>
            <translation>ドラフト</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Open</masterLabel>
            <translation>開いた</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rejected</masterLabel>
            <translation>拒否されました</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Total Amount --></label>
        <name>pse__Total_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Transaction --></label>
        <name>pse__Transaction__c</name>
        <relationshipLabel><!-- Budget --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Type --></label>
        <name>pse__Type__c</name>
        <picklistValues>
            <masterLabel>Customer Purchase Order</masterLabel>
            <translation>顧客発注書</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Customer Purchase Order Change Request</masterLabel>
            <translation>得意先発注変更要求</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Internal Budget</masterLabel>
            <translation>内部予算</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Internal Budget Change Request</masterLabel>
            <translation>内部予算変更依頼</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Vendor Purchase Order</masterLabel>
            <translation>仕入先購買発注</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Vendor Purchase Order Change Request</masterLabel>
            <translation>仕入先購買発注変更依頼</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Work Order</masterLabel>
            <translation>作業命令</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Work Order Change Request</masterLabel>
            <translation>作業指示変更依頼</translation>
        </picklistValues>
    </fields>
    <nameFieldLabel>予算名</nameFieldLabel>
    <sharingReasons>
        <label><!-- PSE Approver Share --></label>
        <name>pse__PSE_Approver_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE Member Share --></label>
        <name>pse__PSE_Member_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE PM Share --></label>
        <name>pse__PSE_PM_Share</name>
    </sharingReasons>
    <validationRules>
        <errorMessage><!-- The budget header&apos;s account must match the account on this budget --></errorMessage>
        <name>pse__Budget_Header_Account_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The budget header&apos;s project does not match the project on this budget --></errorMessage>
        <name>pse__Budget_Header_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The budget header&apos;s type must match the type on this budget --></errorMessage>
        <name>pse__Budget_Header_Type_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Budget&apos;s Project field value may not be blank and may not be updated once set (unless Admin Global Edit is checked, Audit Notes are provided, and Actuals Calculation Mode is Scheduled). --></errorMessage>
        <name>pse__Budget_Project_is_Constant</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Amount should be a positive value --></errorMessage>
        <name>pse__Has_non_negative_budget_amount</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Pre Billed Amount should be a positive value --></errorMessage>
        <name>pse__Has_non_negative_pre_billed_amount</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Master Budget cannot be changed once set. --></errorMessage>
        <name>pse__Master_Budget_is_Constant</name>
    </validationRules>
    <webLinks>
        <label><!-- Clear_Billing_Data --></label>
        <name>pse__Clear_Billing_Data</name>
    </webLinks>
</CustomObjectTranslation>
