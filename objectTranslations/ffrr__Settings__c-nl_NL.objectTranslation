<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Instellingen</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Instellingen</value>
    </caseValues>
    <fields>
        <help>API-naam van het veld op het bronobject dat informatie bevat over een record. Vaak is dit het standaard Salesforce-account, maar het kan elk tekst-, keuzelijst-, zoek-, nummer-, automatisch nummer- of formuleveld zijn.</help>
        <label><!-- Display Information --></label>
        <name>ffrr__AccountName__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld op het bronobject dat aangeeft of het actief is. Als u records in Revenue Management wilt opnemen wanneer een veld een specifieke waarde heeft, stelt u de veldnaam in Actief Veld in, de waarde in Actief Waarde en stelt u Inclusief actief waarde in op Waar.</help>
        <label><!-- Active Field --></label>
        <name>ffrr__ActiveField__c</name>
    </fields>
    <fields>
        <help>Waarde in het veld actief dat aangeeft of de bronrecord actief is.</help>
        <label><!-- Active Value --></label>
        <name>ffrr__ActiveValue__c</name>
    </fields>
    <fields>
        <help>API-naam van het opzoekveld (naar het bronobject van deze instelling) dat uw beheerder heeft toegevoegd aan het regelobject Werkelijk vs Forecast. Dit veld is verplicht als de optie Werkelijk vs Forecast optie is ingeschakeld.</help>
        <label><!-- Actual vs Forecast Line Lookup --></label>
        <name>ffrr__ActualVsForecastRelationship__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld Nummer of Valuta van het bronobject waarin het tot nu toe afgeschreven bedrag is opgeslagen. Indien ingevuld, wordt de waarde in het bronrecord automatisch bijgewerkt telkens wanneer een omzetverantwoording transactie wordt vastgelegd.</help>
        <label><!-- Amortized To Date Value --></label>
        <name>ffrr__AmortizedToDateValue__c</name>
    </fields>
    <fields>
        <help>API-naam van het tekstveld, de zoekopdracht of de keuzelijst die rapportwaarde 1 vertegenwoordigt. Dit kan een veld op het bronobject zijn of met behulp van opzoekrelaties kunt u verwijzen naar een veld op een object dat is gekoppeld via een pad van maximaal vijf opzoekopdrachten.</help>
        <label><!-- Analysis Item 1 --></label>
        <name>ffrr__AnalysisItem1__c</name>
    </fields>
    <fields>
        <help>API-naam van het tekstveld, de zoekopdracht of de keuzelijst die rapportwaarde 2 vertegenwoordigt. Dit kan een veld op het bronobject zijn of met behulp van opzoekrelaties kunt u verwijzen naar een veld op een object dat is gekoppeld via een pad van maximaal vijf opzoekopdrachten.</help>
        <label><!-- Analysis Item 2 --></label>
        <name>ffrr__AnalysisItem2__c</name>
    </fields>
    <fields>
        <help>API-naam van het tekstveld, de zoekopdracht of de keuzelijst die rapportagewaarde 3 vertegenwoordigt. Dit kan een veld op het bronobject zijn of met behulp van opzoekrelaties kunt u verwijzen naar een veld op een object dat is gekoppeld via een pad van maximaal vijf opzoekopdrachten.</help>
        <label><!-- Analysis Item 3 --></label>
        <name>ffrr__AnalysisItem3__c</name>
    </fields>
    <fields>
        <help>API-naam van het tekstveld, de zoekopdracht of de keuzelijst die rapportwaarde 4 vertegenwoordigt. Dit kan een veld op het bronobject zijn of met behulp van opzoekrelaties kunt u verwijzen naar een veld op een object dat is gekoppeld via een pad van maximaal vijf opzoekopdrachten.</help>
        <label><!-- Analysis Item 4 --></label>
        <name>ffrr__AnalysisItem4__c</name>
    </fields>
    <fields>
        <help>API-naam van het tekstveld, de zoeklijst of de keuzelijst op het bronobject dat de GLA van de balans vertegenwoordigt waarnaar de inkomstenjournalen worden gepost.</help>
        <label><!-- Balance Sheet GLA --></label>
        <name>ffrr__BalanceSheetAccount__c</name>
    </fields>
    <fields>
        <label><!-- Billed To Date (Reserved) --></label>
        <name>ffrr__BilledToDate__c</name>
    </fields>
    <fields>
        <label><!-- Chain ID --></label>
        <name>ffrr__ChainID__c</name>
    </fields>
    <fields>
        <help>API Naam van het tekstveld, de zoekopdracht of de keuzelijst die het bedrijf vertegenwoordigt. Dit kan een veld op het bronobject zijn of met behulp van opzoekrelaties kunt u verwijzen naar een veld op een object dat is gekoppeld via een pad van maximaal vijf opzoekopdrachten.</help>
        <label><!-- Company --></label>
        <name>ffrr__Company__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld op het bronobject dat de voltooiingsstatus aangeeft. Als voor deze instelling een sjabloon van het type &apos;Te leveren&apos; wordt gemaakt, wordt omzet verantwoord wanneer het bronrecord is voltooid.</help>
        <label><!-- Completed Field --></label>
        <name>ffrr__CompletedField__c</name>
    </fields>
    <fields>
        <help>Waarde op het veld actief dat aangeeft of het bronrecord voltooid is. Als voor deze instelling een sjabloon van het type Op Levering wordt gemaakt, wordt omzet verantwoord wanneer het bronrecord is voltooid.</help>
        <label><!-- Completed Value --></label>
        <name>ffrr__CompletedValue__c</name>
    </fields>
    <fields>
        <help>API-naam van het tekstveld, de opzoeklijst of de keuzelijst op het bronobject dat de GLA van de balans vertegenwoordigt waarnaar de kostendagboeken worden geboekt.</help>
        <label><!-- Balance Sheet GLA (Cost) --></label>
        <name>ffrr__CostBalanceSheetAccount__c</name>
    </fields>
    <fields>
        <help>API-naam van het tekstveld, de opzoeking of de keuzelijst op het bronobject dat de specifieke component van uw rekeningschema vertegenwoordigt waarmee uw management accounts worden geanalyseerd.</help>
        <label><!-- Cost Center --></label>
        <name>ffrr__CostCenter__c</name>
    </fields>
    <fields>
        <help>API-naam van het tekstveld, de opzoeking of de keuzelijst op het bronobject dat de specifieke component van uw rekeningschema vertegenwoordigt waarmee uw management accounts worden geanalyseerd.</help>
        <label><!-- Cost Center (Cost) --></label>
        <name>ffrr__CostCostCenter__c</name>
    </fields>
    <fields>
        <help>API-naam van het tekstveld, lookup of keuzelijst op het bronobject dat de GLA van de winst-en-verliesrekening vertegenwoordigt waarnaar de kostendagboeken worden gepost.</help>
        <label><!-- Income Statement GLA (Cost) --></label>
        <name>ffrr__CostIncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld op het bronobject dat de koers weergeeft waarvoor kosteneenheden worden berekend.</help>
        <label><!-- Cost Rate --></label>
        <name>ffrr__CostRate__c</name>
    </fields>
    <fields>
        <help>API-naam van het Nummer veld op het bronobject dat het totale aantal in rekening gebrachte kosteneenheden vertegenwoordigt. De waarde van de kostendragers kan bijvoorbeeld het totale aantal uren of dagen zijn dat aan een project is gewerkt.</help>
        <label><!-- Total Cost Units --></label>
        <name>ffrr__CostTotalUnits__c</name>
    </fields>
    <fields>
        <help>API-naam van het Tekst,Opzoeking of Keuzelijst veld op het bronobject dat de valuta van het bronrecord vertegenwoordigt.</help>
        <label><!-- Document Currency --></label>
        <name>ffrr__Currency__c</name>
    </fields>
    <fields>
        <help>De Veldtoewijzingsdefinitie wordt standaard ingevuld op prestatieverplichtingsregels wanneer deze worden gekoppeld aan een bronrecord en deze instellingen.</help>
        <label><!-- Default Field Mapping Definition --></label>
        <name>ffrr__DefaultFieldMappingDefinition__c</name>
        <relationshipLabel><!-- Settings --></relationshipLabel>
    </fields>
    <fields>
        <help>API-naam van het veld Getal op het bronobject waarin het tot nu toe uitgestelde omzetbedrag is opgeslagen, in dubbele valuta.</help>
        <label><!-- FOR FUTURE USE Deferred Rev (Dual) --></label>
        <name>ffrr__DeferredRevenueToDateDualCurrency__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld Getal op het bronobject waarin het tot nu toe uitgestelde bedrag aan inkomsten wordt opgeslagen, in de eigen valuta.</help>
        <label><!-- FOR FUTURE USE Deferred Rev (Home) --></label>
        <name>ffrr__DeferredRevenueToDateHomeCurrency__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld Getal op het bronobject waarin het tot nu toe uitgestelde omzetbedrag in aangiftevaluta wordt opgeslagen.</help>
        <label><!-- FOR FUTURE USE Deferred Rev (Reporting) --></label>
        <name>ffrr__DeferredRevenueToDateReportingCurrency__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld Getal op het bronobject waarin het bedrag van de tot nu toe uitgestelde omzet wordt opgeslagen, in documentvaluta.</help>
        <label><!-- FOR FUTURE USE Deferred Rev --></label>
        <name>ffrr__DeferredRevenueToDate__c</name>
    </fields>
    <fields>
        <help>API-naam van het Tekst of Lange Tekst veld op het bronobject dat een beschrijving bevat.</help>
        <label><!-- Description --></label>
        <name>ffrr__Description__c</name>
    </fields>
    <fields>
        <help>API-naam van het Dubbele of Valuta veld voor het bronobject dat de wisselkoers van het document naar de eigen valuta vertegenwoordigt.</help>
        <label><!-- Document Rate --></label>
        <name>ffrr__DocumentCurrencyRate__c</name>
    </fields>
    <fields>
        <help>API-naam van het dubbele of Valuta veld voor het bronobject dat de boekhoud entiteit&apos;s basis of rapportage wisselkoers vertegenwoordigd</help>
        <label><!-- Dual Rate --></label>
        <name>ffrr__DualCurrencyRate__c</name>
    </fields>
    <fields>
        <help>API-naam van het Tekst, Opzoeking of Keuzelijst veld op het bronobject dat de rapportagevaluta van de boekhoudentiteit vertegenwoordigt. Rapportage valuta is doorgaans een entiteitsvaluta die wordt gebruikt voor rapportagedoeleinden.</help>
        <label><!-- Dual Currency --></label>
        <name>ffrr__DualCurrency__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld Datum of DatumTijd in het bronobject dat de einddatum vertegenwoordigt.</help>
        <label><!-- End Date/Deliverable Date --></label>
        <name>ffrr__EndDate__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Filter 1 --></label>
        <name>ffrr__Filter1__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Filter 2 --></label>
        <name>ffrr__Filter2__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Filter 3 --></label>
        <name>ffrr__Filter3__c</name>
    </fields>
    <fields>
        <help>Selecteer om aan te geven dat de bijbehorende waarde een vaste code is en geen API-naam op het gerelateerde bronobject. Deze selectievakjes kunnen onafhankelijk van elkaar worden ingesteld.</help>
        <label><!-- Fixed Balance Sheet --></label>
        <name>ffrr__FixedBalanceSheetAccountCode__c</name>
    </fields>
    <fields>
        <help>Selecteer om aan te geven dat de bijbehorende waarde een vaste code is en geen API-naam op het gerelateerde bronobject. Deze selectievakjes kunnen onafhankelijk van elkaar worden ingesteld.</help>
        <label><!-- Fixed Balance Sheet (Cost) --></label>
        <name>ffrr__FixedCostBalanceSheetAccountCode__c</name>
    </fields>
    <fields>
        <help>Selecteer om aan te geven dat de bijbehorende waarde een vaste code is en geen API-naam op het gerelateerde bronobject. Deze selectievakjes kunnen onafhankelijk van elkaar worden ingesteld.</help>
        <label><!-- Fixed Cost Center --></label>
        <name>ffrr__FixedCostCenterCode__c</name>
    </fields>
    <fields>
        <help>Selecteer om aan te geven dat de bijbehorende waarde een vaste code is en geen API-naam op het gerelateerde bronobject. Deze selectievakjes kunnen onafhankelijk van elkaar worden ingesteld.</help>
        <label><!-- Fixed Cost Center (Cost) --></label>
        <name>ffrr__FixedCostCostCenterCode__c</name>
    </fields>
    <fields>
        <help>Selecteer om aan te geven dat de bijbehorende waarde een vaste code is en geen API-naam op het gerelateerde bronobject. Deze selectievakjes kunnen onafhankelijk van elkaar worden ingesteld.</help>
        <label><!-- Fixed Income Statement (Cost) --></label>
        <name>ffrr__FixedCostIncomeStatementAccountCode__c</name>
    </fields>
    <fields>
        <help>Selecteer om aan te geven dat de bijbehorende waarde een vaste code is en geen API-naam op het gerelateerde bronobject. Deze selectievakjes kunnen onafhankelijk van elkaar worden ingesteld.</help>
        <label><!-- Fixed Income Statement --></label>
        <name>ffrr__FixedIncomeStatementAccountCode__c</name>
    </fields>
    <fields>
        <help>Selecteer om aan te geven dat de bijbehorende waarde een vaste code is, niet een API-naam voor het gerelateerde bronobject. Deze selectievakjes kunnen onafhankelijk van elkaar worden ingesteld.</help>
        <label><!-- FOR FUTURE USE Fixed True-up (Dual) --></label>
        <name>ffrr__FixedTrueUpDualAccountCode__c</name>
    </fields>
    <fields>
        <help>Selecteer om aan te geven dat de bijbehorende waarde een vaste code is, niet een API-naam voor het gerelateerde bronobject. Deze selectievakjes kunnen onafhankelijk van elkaar worden ingesteld.</help>
        <label><!-- FOR FUTURE USE Fixed True-up (Home) --></label>
        <name>ffrr__FixedTrueUpHomeAccountCode__c</name>
    </fields>
    <fields>
        <help>API-naam van het opzoekveld (naar het bronobject op dit instellingsniveau) dat uw beheerder heeft toegevoegd aan het Omzet Forecast Transactie Regel-object. Dit moet worden ingevuld als het type instelling Forecast is en het instellingsniveau Primair is.</help>
        <label><!-- Revenue Forecast Transaction Lookup --></label>
        <name>ffrr__ForecastHeaderPrimaryRelationship__c</name>
    </fields>
    <fields>
        <help>API-naam van het opzoekveld (naar het bronobject op dit instellingsniveau) dat uw beheerder heeft toegevoegd aan het Omzet Forecast Transactie Regel-object. Dit moet worden ingevuld als het type instelling Forecast is.</help>
        <label><!-- Revenue Forecast Transaction Line Lookup --></label>
        <name>ffrr__ForecastTransactionLineRelationship__c</name>
    </fields>
    <fields>
        <label><!-- Group Name --></label>
        <name>ffrr__GroupName__c</name>
    </fields>
    <fields>
        <label><!-- Group --></label>
        <name>ffrr__Group__c</name>
    </fields>
    <fields>
        <help>API-naam van het tekstveld, de opzoeking of de keuzelijst op het bronobject dat de specifieke component vertegenwoordigt waarop u wilt groeperen voor het proces Alles verantwoorden.</help>
        <label><!-- Grouped By --></label>
        <name>ffrr__GroupedBy__c</name>
    </fields>
    <fields>
        <help>API-naam van het Tekst, Opzoeking of Keuzelijst veld op het bronobject dat de basis valuta van de boekhoudentiteit vertegenwoordigt. Basis valuta is de belangrijkste werk- en rapportagevaluta van het bedrijf.</help>
        <label><!-- Home Currency --></label>
        <name>ffrr__HomeCurrency__c</name>
    </fields>
    <fields>
        <help>Bronrecords opnemen of uitsluiten met de waarde die is gedefinieerd in het veld Actieve waarde.</help>
        <label><!-- Include Active Value --></label>
        <name>ffrr__IncludeActiveValue__c</name>
        <picklistValues>
            <masterLabel>Exclude</masterLabel>
            <translation>Uitsluiten</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Include</masterLabel>
            <translation>Opnemen</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Bronrecords opnemen of uitsluiten met de waarde die is gedefinieerd in het veld Voltooide waarde.</help>
        <label><!-- Include Completed Value --></label>
        <name>ffrr__IncludeCompletedValue__c</name>
        <picklistValues>
            <masterLabel>Exclude</masterLabel>
            <translation>Uitsluiten</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Include</masterLabel>
            <translation>Opnemen</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>API-naam van het tekstveld, de zoeklijst of de keuzelijst op het bronobject dat de GLA van de winst-en-verliesrekening vertegenwoordigt waarnaar de inkomstenbladen worden gepost.</help>
        <label><!-- Income Statement GLA --></label>
        <name>ffrr__IncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>API-naam van het object dat op dit niveau als gegevensbron fungeert. Dit kan niet veranderen zodra de instelling op een sjabloon wordt gebruikt. Voor niet-primaire instellingen moet het object een masterdetail- of opzoekrelatie hebben met het object op de bovenliggende instelling.</help>
        <label><!-- Object --></label>
        <name>ffrr__Object__c</name>
    </fields>
    <fields>
        <help>API-naam van het opzoekveld (naar het bronobject van deze instelling) dat uw beheerder heeft toegevoegd aan het regelitem Prestatieverplichting Regel.</help>
        <label><!-- POLI Source Lookup --></label>
        <name>ffrr__POLISourceField__c</name>
    </fields>
    <fields>
        <help>API-namen van opzoekvelden op het bronobject dat wordt beschreven door dit instellingsrecord, of gekoppeld aan het bronobject door een relatiepad van maximaal 5 zoekacties, waarvan de waarden de Opzoekopdrachten voor de regel Opbrengstenplanning vullen. Bij het specificeren van meerdere velden moeten ze door komma’s worden gescheiden en in dezelfde volgorde als de velden in de Opzoekopdrachten voor omzetplanningsregels.</help>
        <label><!-- Parent Relationship Paths --></label>
        <name>ffrr__ParentRelationshipPaths__c</name>
    </fields>
    <fields>
        <help>Selecteer de record met de ouderinstellingen (de instellingenrecord voor het bronobject één niveau hoger in de hiërarchie dan deze).</help>
        <label><!-- Parent Settings --></label>
        <name>ffrr__ParentSettings__c</name>
        <relationshipLabel><!-- ParentSettings --></relationshipLabel>
    </fields>
    <fields>
        <help>API-naam van het Nummer of Percentage veld in het bronobject dat aangeeft hoe volledig in procenten het bronrecord is.</help>
        <label><!-- % Complete --></label>
        <name>ffrr__PercentageComplete__c</name>
    </fields>
    <fields>
        <help>Voer de API-naam van het opzoekveld in voor het bronobject van de bovenliggende instelling.</help>
        <label><!-- Parent Lookup --></label>
        <name>ffrr__PrimaryRelationship__c</name>
    </fields>
    <fields>
        <help>API-naam van de zoekopdracht die het product vertegenwoordigt. Dit kan een opzoeking op het bronobject zijn, of met behulp van opzoekrelaties kunt u verwijzen naar een opzoeking op een object dat is gekoppeld via een pad van maximaal vijf opzoekingen.</help>
        <label><!-- Product --></label>
        <name>ffrr__Product__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld op het bronobject dat de koers weergeeft waarvoor eenheden worden berekend.</help>
        <label><!-- Rate --></label>
        <name>ffrr__Rate__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld Getal op het bronobject waarin het tot nu toe erkende omzetbedrag in dubbele valuta wordt opgeslagen. Indien ingevuld, wordt de waarde in het bronrecord automatisch bijgewerkt telkens wanneer een omzetherkenningstransactie wordt vastgelegd.</help>
        <label><!-- FOR FUTURE USE Rec To Date Value (Dual) --></label>
        <name>ffrr__RecognizedToDateValueDual__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld Getal op het bronobject waarin het tot nu toe erkende omzetbedrag in de eigen valuta wordt opgeslagen. Indien ingevuld, wordt de waarde in het bronrecord automatisch bijgewerkt telkens wanneer een omzetherkenningstransactie wordt vastgelegd.</help>
        <label><!-- FOR FUTURE USE Rec To Date Value (Home) --></label>
        <name>ffrr__RecognizedToDateValueHome__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld Nummer of Valuta op het bronobject waarin het tot nu toe verantwoorde omzet bedrag is opgeslagen. Indien ingevuld, wordt de waarde in het bronrecord automatisch bijgewerkt telkens wanneer een omzetverantwoording transactie wordt vastgelegd.</help>
        <label><!-- Recognized To Date Value --></label>
        <name>ffrr__RecognizedToDateValue__c</name>
    </fields>
    <fields>
        <help>API-naam van het Boolean veld op het bronobject dat aangeeft dat alle omzet volledig worden verantwoord en alle kosten volledig worden afgeschreven. Indien ingevuld, wordt het selectievakje van het bronrecord automatisch ingeschakeld wanneer de omzet volledig wordt verantwoord en de kosten volledig worden afgeschreven.</help>
        <label><!-- Fully Recognized and Amortized --></label>
        <name>ffrr__RevenueRecognitionCompleted__c</name>
    </fields>
    <fields>
        <help>API-namen van opzoekvelden (naar het bronobject beschreven door dit instellingenrecord of gerelateerde objecten) die uw beheerder heeft toegevoegd aan het Revenue Schedule Line-object. Deze velden worden ingevuld op basis van de zoekacties die zijn geïdentificeerd in de paden voor bovenliggende relaties. Bij het specificeren van meerdere velden moeten ze door komma’s worden gescheiden en in dezelfde volgorde als de velden in de paden voor bovenliggende relatie.</help>
        <label><!-- Revenue Schedule Line Lookups --></label>
        <name>ffrr__RevenueScheduleLineLookups__c</name>
    </fields>
    <fields>
        <help>API-naam van het opzoekveld (naar het bronobject op dit instellingsniveau) dat uw beheerder heeft toegevoegd aan het Revenue Schedule-object.</help>
        <label><!-- Revenue Schedule Lookup --></label>
        <name>ffrr__RevenueScheduleSourceLookup__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld in het bronobject dat de zelfstandige verkoopprijs voor een regelitem met prestatieverplichtingen opslaat.</help>
        <label><!-- SSP --></label>
        <name>ffrr__SSP__c</name>
    </fields>
    <fields>
        <help>API-naam van de zoekopdracht die het Salesforce-account vertegenwoordigt. Dit kan een zoekopdracht zijn op het bronobject of met behulp van zoekrelaties kunt u verwijzen naar een zoekopdracht op een object dat is gekoppeld via een pad van maximaal vijf zoekopdrachten.</help>
        <label><!-- Account --></label>
        <name>ffrr__SalesforceAccount__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Setting Uniqueness --></label>
        <name>ffrr__SettingUniqueness__c</name>
    </fields>
    <fields>
        <help>Selecteer het niveau van deze bronobjectinstellingenrecord.</help>
        <label><!-- Settings Level --></label>
        <name>ffrr__SettingsLevel__c</name>
        <picklistValues>
            <masterLabel>Level 2</masterLabel>
            <translation>Niveau 2</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Level 3</masterLabel>
            <translation>Niveau 3</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Level 4</masterLabel>
            <translation>Niveau 4</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Primary</masterLabel>
            <translation>Primair</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Het type proces waarvoor dit instellingen record wordt gebruikt: Werkelijk, Forecast of beide. Als de toewijzingen identiek zijn voor zowel werkelijke als Forecast processen, maak dan één instellingen record voor gebruik door beide.</help>
        <label><!-- Settings Type --></label>
        <name>ffrr__SettingsType__c</name>
        <picklistValues>
            <masterLabel>Actual</masterLabel>
            <translation>Werkelijk</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Forecast</masterLabel>
            <translation>Forecast</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>API-naam van het veld Datum of DatumTijd in het bronobject dat de begindatum vertegenwoordigt.</help>
        <label><!-- Start Date --></label>
        <name>ffrr__StartDate__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld Aantal of Valuta voor het bronobject dat de totale kosten vertegenwoordigt.</help>
        <label><!-- Total Cost --></label>
        <name>ffrr__TotalCost__c</name>
    </fields>
    <fields>
        <help>API-naam van het veld Aantal of Valuta voor het bronobject dat de totale omzet vertegenwoordigt.</help>
        <label><!-- Total Revenue --></label>
        <name>ffrr__TotalRevenue__c</name>
    </fields>
    <fields>
        <help>API-naam van het Nummer veld op het bronobject dat het totale aantal in rekening gebrachte eenheden vertegenwoordigt. De totale waarde van de eenheden kan bijvoorbeeld het aantal uren of dagen zijn dat aan een project is gewerkt.</help>
        <label><!-- Total Units --></label>
        <name>ffrr__TotalUnits__c</name>
    </fields>
    <fields>
        <help>API-naam van het opzoekveld (naar het bronobject op dit instellingsniveau) dat uw beheerder heeft toegevoegd aan het Omzetverantwoording Transactie Regel-object. Dit moet worden ingevuld als het type instelling Werkelijk is.</help>
        <label><!-- Revenue Recognition Txn Line Lookup --></label>
        <name>ffrr__TransactionLineRelationship__c</name>
    </fields>
    <fields>
        <help>API-naam van het tekstveld, de zoeklijst of de keuzelijst op het bronobject dat de GLA vertegenwoordigt die moet worden gebruikt voor waarheidsgetrouwe waarden in dubbele valuta.</help>
        <label><!-- FOR FUTURE USE True-Up GLA (Dual) --></label>
        <name>ffrr__TrueUpDualAccount__c</name>
    </fields>
    <fields>
        <help>API-naam van het tekstveld, de zoeklijst of de keuzelijst op het bronobject dat de GLA vertegenwoordigt die moet worden gebruikt voor waarheidsgetrouwe waarden in de eigen valuta.</help>
        <label><!-- FOR FUTURE USE True-Up GLA (Home) --></label>
        <name>ffrr__TrueUpHomeAccount__c</name>
    </fields>
    <fields>
        <help>Wordt deze instelling gebruikt in omzetcontracten.</help>
        <label><!-- Use in Revenue Contract --></label>
        <name>ffrr__UseInRevenueContract__c</name>
    </fields>
    <fields>
        <help>API-naam van het Nummer of Percentage veld voor het bronobject dat de LSOB-percentage (Leveranciers Specifieke Objectief Bewijs) weergeeft.</help>
        <label><!-- VSOE % --></label>
        <name>ffrr__VSOEPercent__c</name>
    </fields>
    <fields>
        <help>API-naam van het Nummer of Valuta veld voor het bronobject dat de LSOB-waarde (Leveranciers Specifieke Objectief Bewijs) weergeeft.</help>
        <label><!-- VSOE Rate --></label>
        <name>ffrr__VSOERate__c</name>
    </fields>
    <fields>
        <help>Het type verwerking van dit instellingen record wordt gebruikt voor: Omzet, Kosten of beide.</help>
        <label><!-- Value Type --></label>
        <name>ffrr__ValueType__c</name>
        <picklistValues>
            <masterLabel>Cost</masterLabel>
            <translation>Kosten</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Revenue</masterLabel>
            <translation>Omzet</translation>
        </picklistValues>
    </fields>
    <gender>Feminine</gender>
    <nameFieldLabel>Instelling naam</nameFieldLabel>
    <webLinks>
        <label><!-- CreateMappings --></label>
        <name>ffrr__CreateMappings</name>
    </webLinks>
    <webLinks>
        <label><!-- CreateMappingsList --></label>
        <name>ffrr__CreateMappingsList</name>
    </webLinks>
    <webLinks>
        <label><!-- DeleteSettings --></label>
        <name>ffrr__DeleteSettings</name>
    </webLinks>
</CustomObjectTranslation>
