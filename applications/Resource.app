<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Cloudwerx_Opp_Record_Page</content>
        <formFactor>Small</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Cloudwerx_Opp_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
    </actionOverrides>
    <brand>
        <headerColor>#0070D2</headerColor>
        <shouldOverrideOrgTheme>false</shouldOverrideOrgTheme>
    </brand>
    <formFactors>Small</formFactors>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Resource</label>
    <navType>Standard</navType>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>ResourceProjectLightningPage</content>
        <formFactor>Small</formFactor>
        <pageOrSobjectType>pse__Proj__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>ResourceProjectLightningPage</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>pse__Proj__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Resource_Assignment_Record_Page</content>
        <formFactor>Small</formFactor>
        <pageOrSobjectType>pse__Assignment__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Resource_Assignment_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>pse__Assignment__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Resource_Account_Record_Page</content>
        <formFactor>Small</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Resource_Account_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Resource_Contact_Record_Page</content>
        <formFactor>Small</formFactor>
        <pageOrSobjectType>Contact</pageOrSobjectType>
        <recordType>Contact.CRM_Contact</recordType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Resource_Contact_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Contact</pageOrSobjectType>
        <recordType>Contact.CRM_Contact</recordType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Resource_Contact_Record_Page</content>
        <formFactor>Small</formFactor>
        <pageOrSobjectType>Contact</pageOrSobjectType>
        <recordType>Contact.PSA_Resource</recordType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Resource_Contact_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Contact</pageOrSobjectType>
        <recordType>Contact.PSA_Resource</recordType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>CW_Resource_Skill_Certification_Record_Page</content>
        <formFactor>Small</formFactor>
        <pageOrSobjectType>pse__Skill__c</pageOrSobjectType>
        <recordType>pse__Skill__c.pse__Category</recordType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>CW_Resource_Skill_Certification_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>pse__Skill__c</pageOrSobjectType>
        <recordType>pse__Skill__c.pse__Category</recordType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>CW_Resource_Skill_Certification_Record_Page</content>
        <formFactor>Small</formFactor>
        <pageOrSobjectType>pse__Skill__c</pageOrSobjectType>
        <recordType>pse__Skill__c.pse__Certification</recordType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>CW_Resource_Skill_Certification_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>pse__Skill__c</pageOrSobjectType>
        <recordType>pse__Skill__c.pse__Certification</recordType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>CW_Resource_Skill_Certification_Record_Page</content>
        <formFactor>Small</formFactor>
        <pageOrSobjectType>pse__Skill__c</pageOrSobjectType>
        <recordType>pse__Skill__c.pse__Skill</recordType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>CW_Resource_Skill_Certification_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>pse__Skill__c</pageOrSobjectType>
        <recordType>pse__Skill__c.pse__Skill</recordType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Region_Resource_Record_Page</content>
        <formFactor>Small</formFactor>
        <pageOrSobjectType>pse__Region__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Region_Resource_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>pse__Region__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Practice_Resource_Record_Page</content>
        <formFactor>Small</formFactor>
        <pageOrSobjectType>pse__Practice__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Practice_Resource_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>pse__Practice__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Cloudwerx Resource Platform User</profile>
    </profileActionOverrides>
    <tabs>Services_Delivery_Workspace</tabs>
    <tabs>pse__Time_Entry</tabs>
    <tabs>pse__Timecard_Header__c</tabs>
    <tabs>pse__Missing_Timecard__c</tabs>
    <tabs>Approvals</tabs>
    <tabs>pse__Assignment__c</tabs>
    <tabs>pse__Skill__c</tabs>
    <uiType>Lightning</uiType>
    <utilityBar>Resource_UtilityBar</utilityBar>
</CustomApplication>
