/**
* @name         Onboarding_FinalEmailBodyCtrlTest 
* @author       Girish Baviskar
* @date         20-02-2021
* @description  Test class
* @testClass    
**/ 
@isTest
public inherited sharing class Onboarding_FinalEmailBodyCtrlTest {
   
    @TestSetup
    static void makeData(){
        List<Account> accList = Onboarding_UtilityClass.createAccounts(1, true);
        Onboarding_UtilityClass.createContacts(1, true, accList[0].Id);
    }
    @IsTest
    static void testEmailController(){
        
        contact cnt = [select name from contact limit 1];
        // PageReference pageRef = Page.success;
        // Test.setCurrentPage(pageRef);
        Onboarding_FinalEmailBodyController controller = new Onboarding_FinalEmailBodyController();
        
        Test.startTest();
        controller.conID = cnt.Id;
        List<Task> employeeTask = controller.employeeTasks;
        List<Task> employerTask = controller.employerTasks;
        Test.stopTest();
        
    }

}