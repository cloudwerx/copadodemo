/**
* @name         Onboarding_loginControllerTest 
* @author       Girish Baviskar
* @date         11-02-2021
* @description  Test class
* @testClass    
**/ 
@isTest
public with sharing class Onboarding_loginControllerTest {
    
   
    @TestSetup
    static void makeData(){

        RecordType recType = [SELECT Name FROM RecordType WHERE Name = 'India Employee'];
        Contact con = new Contact();
        con.FirstName = 'Yash';
        con.LastName = 'Bhalerao';
        con.Email = 'ybhalerao@cloudwerx.co';
        //con.Work_Location__c = 'Pune Office';
        con.Employee_Role__c = 'Salesforce Developer';
        con.Employee_Type__c = 'Employee';
        con.Date_Of_Joining__c = date.parse('11/02/2021');
        con.Is_Slack_User__c = true;
        con.Slack_Profile_Id__c = '293458998'; 
        con.RecordTypeId = recType.Id;
        insert con;
        
    }

    @IsTest
    static void testLoginSuccess(){
            
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new Onboarding_SlackResponseMock());   
            Contact objContact = [select Name, Email from Contact];
            System.debug(objContact);
            
            objContact.Status__c = 'Active Employee';
            update objContact;
            
            Onboarding_loginController.LoginResponse res = Onboarding_loginController.generateAndEmailOtp('ybhalerao@cloudwerx.co');
            // System.assertEquals(objContact, res.objContact, 'success');
        Test.stopTest();   
        
    }

    @IsTest
    static void testLoginFail(){
        // Test.setMock(HttpCalloutMock.class, new Onboarding_SlackResponseMock());
        Test.startTest();
        Onboarding_loginController.LoginResponse res = Onboarding_loginController.generateAndEmailOtp('testemail@cloudwerx.co');
        System.assertEquals(null, res, 'failed');
        Test.stopTest();
    }
}