@isTest
public with sharing class CandidateEvaluator_TestDataFactory {
    public CandidateEvaluator_TestDataFactory() {

    }

    public static void createTestRecords(Integer numCandidates) {
        List<Candidate__c> candidateList = new List<Candidate__c>();
        List<Candidate_Skill__c> candidateSkillList = new List<Candidate_Skill__c>();
        List<Skillset__c> skillset = new List<Skillset__c>();
        Candidate_Skill__c candidateSkill;
        Skillset__c skill;
        
        skill = new Skillset__c(SkillSetType__c='Salesforce customization', Skill__c='Javascript', Profile_Type__c='Salesforce Developer');
		skillset.add(skill);
        skill = new Skillset__c(SkillSetType__c='Salesforce tools', Skill__c='Git', Profile_Type__c='Salesforce Developer;Salesforce Administrator');
		skillset.add(skill);
        skill = new Skillset__c(SkillSetType__c='Salesforce customization', Skill__c='Integration', Profile_Type__c='Salesforce Developer;Salesforce Administrator');
		skillset.add(skill);
        skill = new Skillset__c(SkillSetType__c='Salesforce customization', Skill__c='Apex', Profile_Type__c='Salesforce Developer');
        skillset.add(skill);
        skill = new Skillset__c(SkillSetType__c='Salesforce customization', Skill__c='HTML', Profile_Type__c='Salesforce Developer');
		skillset.add(skill);
        skill = new Skillset__c(SkillSetType__c='Salesforce tools', Skill__c='Jira', Profile_Type__c='Salesforce Developer');
        skillset.add(skill);
        skill = new Skillset__c(SkillSetType__c='Salesforce tools', Skill__c='Bitbucket', Profile_Type__c='Salesforce Developer;Salesforce Administrator');
		skillset.add(skill);
        skill = new Skillset__c(SkillSetType__c='Salesforce customization', Skill__c='CSS', Profile_Type__c='Salesforce Developer');
		skillset.add(skill);
        skill = new Skillset__c(SkillSetType__c='Salesforce customization', Skill__c='Lightning Web Component', Profile_Type__c='Salesforce Developer');
        skillset.add(skill);

        insert skillset;

        for(Integer i=0; i<numCandidates; i++) {
            Candidate__c candidate = new Candidate__c(First_Name__c='Test',
                                                      Last_Name__c='Candidate ' + i,
                                                      Job_Role__c='Salesforce Developer',
                                                      Notice_Period__c=80,
                                                      Probable_Date_Of_Joining__c=System.today() + i,
                                                      Skills__c='Javascript;Integration;Git;Apex;Flows',
                                                      Experience_Years__c=5,
                                                      Score__c=40,
                                                      Email__c='jain.abhinav14@gmail.com'
                                                     );

            candidateList.add(candidate);
        }

        insert candidateList;

        for(Integer i=0; i<candidateList.size(); i++) {
            for(Integer j=0; j<4; j++) {
                candidateSkill = new Candidate_Skill__c(Skillset__c=skillset[(j+1)].Id, Candidate__c=candidateList[i].Id);
                candidateSkillList.add(candidateSkill);
            }
        }

        insert candidateSkillList;
    }

}