@isTest()
public without sharing class Onboarding_FileUploaderTest
{

    @testSetup
    public static void setup()
    {
        Test.setMock(HttpCalloutMock.class, new Onboarding_SlackResponseMock());
        
        Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body to be insert in test class for testing the');

        ContentVersion contentVersion_1 = new ContentVersion(
            Title='SampleTitle', 
            PathOnClient ='SampleTitle.jpg',
            VersionData = bodyBlob, 
            origin = 'H',
            isMajorVersion = true
            //FirstPublishLocationId = newTask.Id
        );

        insert contentVersion_1;
        
        ContentVersion cv1 = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_1.Id];
        
        //system.debug('contentVersion_1.Id : ' + contentVersion_1.Id);
        system.debug('cv.ContentDocumentId : ' + cv1.ContentDocumentId);
        
        /*list<Onboarding_Task__mdt> MetadataTaskList = [SELECT Id, AssignedToId__c, Description_of_Task__c, Employee_Role__c, Employer_Comments__c, IsRequired__c, Is_Slack_msg_Required__c, Is_Upload_Required__c, Is_Information_Form_required__c, Office_Location__c, Subject_of_Task__c, Task_Sequence_Number__c, Type_of_Document__c, Type_of_Task__c, Help_Doc_URL__c, Help_Doc_ID__c, Detail_Button__c, Is_Downloadable__c FROM Onboarding_Task__mdt ORDER BY Task_Sequence_Number__c ASC LIMIT 10000];
        
        for(Onboarding_Task__mdt mdt : MetadataTaskList)
        {
            mdt.Help_Doc_ID__c = cv1.ContentDocumentId;
            system.debug('mdt.Help_Doc_ID__c : '+ mdt.Help_Doc_ID__c);
        }
        
        system.debug('MetadataTaskList : ' + MetadataTaskList);
        insert MetadataTaskList;*/
        
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
        User newUser = new User();
        newUser.Username = 'ybhalerao@cloudwerx.c';
        newUser.LastName = 'Baviskar';
        newUser.Email = 'gbaviskar@cloudwerx.co';
        newUser.Alias = 'gbavi';
        newUser.TimeZoneSidKey = 'Australia/Sydney';
        newUser.LocaleSidKey = 'en_AU';
        newUser.EmailEncodingKey = 'ISO-8859-1';
        newUser.ProfileId = p.Id;
        newUser.LanguageLocaleKey = 'en_US';
        
        insert newUser;
        
        RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'India Employee' AND isActive = true LIMIT 1];

        Contact con = new Contact();
        con.FirstName = 'Yash';
        con.LastName = 'Bhalerao';
        con.Email = 'ybhalerao@cloudwerx.c';
        con.Work_Location__c = 'Pune Office';
        con.Employee_Role__c = 'Salesforce Developer';
        con.Employee_Type__c = 'Employee';
        con.Date_Of_Joining__c = date.parse('11/02/2021');
        con.RecordTypeId = rt.Id;
        
        insert con;
        
        List<Task> taskList = [SELECT Id, Help_Doc_ID__c FROM Task WHERE WhoId =: con.Id];
        
        for(Task t : taskList)
        {
            t.Help_Doc_ID__c = cv1.ContentDocumentId;
        }
        
        update taskList;
        
        /*Task newTask = new Task();
        newTask.OwnerId = newUser.Id;
        newTask.WhoId = con.Id;
        newTask.Subject = 'ascascac';
        newTask.Status = 'In Progress';
        newTask.Is_Completed__c = false;
        newTask.Type_of_Document__c = 'Profile Photo';
        newTask.Uploaded_File_Id__c = 'xaqwscw';
        //newTask.Help_Doc_ID__c = '0690T000000N4oVQAS';

        insert newTask;*/

        

        /*ContentVersion cv = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_1.Id LIMIT 1];
        Id cdId = contentVersion_1.ContentDocumentId;

        Task t = [SELECT Id,Help_Doc_ID__c  FROM Task WHERE Id=: newTask.Id LIMIT 1];
        t.Help_Doc_ID__c = cdId;
        update t;*/
    }

    @isTest
    public static void fetchAttachmentTest()
    {
        Test.setMock(HttpCalloutMock.class, new Onboarding_SlackResponseMock());
        
        Task t = [SELECT Id, Help_Doc_ID__c FROM Task WHERE Help_Doc_ID__c != null LIMIT 1];
        
        system.debug('test : ' + t.Help_Doc_ID__c);

        Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body to be insert in test class for testing the');

        ContentVersion contentVersion_1 = new ContentVersion(
            Title='SampleTitle', 
            PathOnClient ='SampleTitle.jpg',
            VersionData = bodyBlob, 
            origin = 'H',
            ContentDocumentId = t.Help_Doc_ID__c
        );

        insert contentVersion_1;

        string s = FileUploader.fetchAttachment(t.Id);
        system.debug('s : ' + s);
    }
}