/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class FeatureConsoleInteractionService {
global class BillingCentralSetupService implements fferpcore.IFeatureConsoleCustomApexRunner {
    global BillingCentralSetupService() {

    }
    global void run(fferpcore.FeatureConsoleService.StepRunnerConfig config) {

    }
}
global class BillingCentralUpdateService implements fferpcore.IFeatureConsoleCustomApexRunner {
    global BillingCentralUpdateService() {

    }
    global void run(fferpcore.FeatureConsoleService.StepRunnerConfig config) {

    }
}
global class RRTToJournalCheckDependency implements fferpcore.IFeatureConsoleCustomApexRunner {
    global RRTToJournalCheckDependency() {

    }
}
global class RRTToJournalCheckDependentPackages implements fferpcore.IFeatureConsoleCustomApexRunner {
    global RRTToJournalCheckDependentPackages() {

    }
}
global class RegisterRRTCostToJournalWithERP implements fferpcore.IFeatureConsoleCustomApexRunner {
    global RegisterRRTCostToJournalWithERP() {

    }
}
global class RegisterSummarizedRRTToJournalWithERP implements fferpcore.IFeatureConsoleCustomApexRunner {
    global RegisterSummarizedRRTToJournalWithERP() {

    }
}
global class RegisterWithERP implements fferpcore.IFeatureConsoleCustomApexRunner {
    global RegisterWithERP() {

    }
}
global class RevenueCloudCostAmortizationSetupService implements fferpcore.IFeatureConsoleCustomApexRunner {
    global RevenueCloudCostAmortizationSetupService() {

    }
    global void run(fferpcore.FeatureConsoleService.StepRunnerConfig config) {

    }
}
global class RevenueCloudSetupService implements fferpcore.IFeatureConsoleCustomApexRunner {
    global RevenueCloudSetupService() {

    }
    global void run(fferpcore.FeatureConsoleService.StepRunnerConfig config) {

    }
}
global class RevenueContractsSetupService implements fferpcore.IFeatureConsoleCustomApexRunner {
    global RevenueContractsSetupService() {

    }
    global void run(fferpcore.FeatureConsoleService.StepRunnerConfig config) {

    }
}
global class SummarizedRRTToJournalCheckDependency implements fferpcore.IFeatureConsoleCustomApexRunner {
    global SummarizedRRTToJournalCheckDependency() {

    }
}
}
