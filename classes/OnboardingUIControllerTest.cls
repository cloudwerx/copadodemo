/************************************************************************************************
* @Name         OnboardingUIControllerTest 
* @Author       Yash Bhalerao
* @Date         10-02-2021
* @Description  Test Class for OnboardingUIController
* @TestClass    
*************************************************************************************************/

@isTest
public class OnboardingUIControllerTest //implements HttpCalloutMock implements WebServiceMock
{
    @testSetup
    public static void setup()
    {
        User newUser = new User();
        newUser.Username = 'ybhalerao@cloudwerx.c';
        newUser.LastName = 'Baviskar';
        newUser.Email = 'gbaviskar@cloudwerx.co';
        newUser.Alias = 'gbavi';
        newUser.TimeZoneSidKey = 'Australia/Sydney';
        newUser.LocaleSidKey = 'en_AU';
        newUser.EmailEncodingKey = 'ISO-8859-1';
        newUser.ProfileId = '00e6F000003YSGmQAO';
        newUser.LanguageLocaleKey = 'en_US';
        
        insert newUser;
        
        RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'India Employee' AND isActive = true LIMIT 1];

        Contact con = new Contact();
        con.FirstName = 'Yash';
        con.LastName = 'Bhalerao';
        con.Email = 'ybhalerao@cloudwerx.co';
        con.Work_Location__c = 'Pune Office';
        con.Employee_Role__c = 'Salesforce Developer';
        con.Employee_Type__c = 'Employee';
        con.Date_Of_Joining__c = date.parse('11/02/2021');
        con.RecordTypeId = rt.Id;
        
        insert con;
        
        Task newTask = new Task();
        newTask.OwnerId = newUser.Id;
        newTask.WhoId = con.Id;
        newTask.Subject = 'ascascac';
        newTask.Status = 'In Progress';
        newTask.Is_Completed__c = false;
        newTask.Type_of_Document__c = 'Profile Photo';
        newTask.Uploaded_File_Id__c = 'xaqwscw';

        Task newTask1 = new Task();
        newTask1.OwnerId = newUser.Id;
        newTask1.WhoId = con.Id;
        newTask1.Subject = 'ascasac';
        newTask1.Status = 'In Progress';
        newTask1.Is_Completed__c = true;
        newTask1.Uploaded_File_Id__c = 'xaqwscw';
        
        insert newTask;
        insert newTask1;

        Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body to be insert in test class for testing the'); 
            
        ContentVersion contentVersion_1 = new ContentVersion(
            Title='SampleTitle', 
            PathOnClient ='SampleTitle.jpg',
            VersionData = bodyBlob, 
            origin = 'H',
            FirstPublishLocationId = newTask.Id
        );

        insert contentVersion_1;

        List<Onboarding_App_Google_Drive_Folder_Id__mdt> allFolders = [SELECT Folder_Id__c, Folder_Name__c FROM Onboarding_App_Google_Drive_Folder_Id__mdt LIMIT 10000];
        system.debug('allFolders : ' + allFolders);
        Onboarding_App_Google_Drive_Folder_Id__mdt other = [SELECT Folder_Id__c, Folder_Name__c FROM Onboarding_App_Google_Drive_Folder_Id__mdt WHERE Folder_Name__c = 'Other Documents'];
        string otherFolderId = other.Folder_Id__c;

        /*Onboarding_App_Google_Drive_Folder_Id__mdt f = new Onboarding_App_Google_Drive_Folder_Id__mdt();
        f.Folder_Id__c = 'asscaca';
        f.Folder_Name__c = 'Profile Photo';
        insert f;

        Onboarding_App_Google_Drive_Folder_Id__mdt f1 = new Onboarding_App_Google_Drive_Folder_Id__mdt();
        f1.Folder_Id__c = 'asscaca';
        f1.Folder_Name__c = 'Other Documents';
        insert f1;*/

        //updateTaskStatusTest();
        //Onboarding_SlackWebApi.postMessageAsUserViaBot('hi', con);
        //OnboardingUIController.sendSlackMessage('hi', con);
        //OnboardingUIController.updateTaskStatus(newTask.Id);
        //OnboardingUIController.updateTaskStatus(newTask1.Id);

    }

    @isTest
    public static void updateTaskStatusTest()
    {
        Task t1 = [SELECT Status, Is_Completed__c FROM Task WHERE Is_Completed__c = false LIMIT 1];
        Task t2 = [SELECT Status, Is_Completed__c FROM Task WHERE Is_Completed__c = true LIMIT 1]; 

        OnboardingUIController.updateTaskStatus(t1.Id);
        OnboardingUIController.updateTaskStatus(t2.Id);
        Contact res = OnboardingUIController.updateTaskStatus('0120T0000005nNCQAY');
        system.assertEquals(null, res);

        /*for(Task t : taskList)
        {
            system.debug('t : ' + t);
            OnboardingUIController.updateTaskStatus(t.Id);
        }*/

        //update taskList;
    }

    @isTest
    public static void sendSlackMessageTest()
    {
        Contact con = [SELECT Id, Slack_Display_Name__c, Slack_Profile_Picture_Url__c, Work_Location__c FROM Contact LIMIT 1];
        boolean res1 = OnboardingUIController.sendSlackMessage('hi', con.Id);
        system.assertEquals(true, res1);
        boolean res2 = OnboardingUIController.sendSlackMessage('hi', '0120T0000005nNCQAY');
        system.assertEquals(false, res2);
    }

    @isTest
    public static void showAllTasksTest()
    {
        Contact con = [SELECT Id FROM Contact LIMIT 1];
        OnboardingUIController.showAllTasks(con.Id);
    }

    @isTest
    public static void saveOnboardingFeedbackTest()
    {
        Contact con = [SELECT Id FROM Contact LIMIT 1];

        boolean res1 = OnboardingUIController.saveOnboardingFeedback(con);
        system.assertEquals(true, res1);

        con.LastName = '';
        
        boolean res = OnboardingUIController.saveOnboardingFeedback(con);
        system.assertEquals(false, res);
    }

    @isTest
    public static void OnboardingStatusTest()
    {
        Contact con = [SELECT Id FROM Contact LIMIT 1];
        OnboardingUIController.OnboardingStatus(con.Id);
    }

    @isTest
    public static void saveFileTest()
    {
        Contact con = [SELECT Id FROM Contact LIMIT 1];

        OnboardingUIController.saveFile(con.Id, 'test', 'test', con.Id);
    }

    /*global HTTPResponse respond(HttpRequest req)
    {
        HttpResponse resp = new HttpResponse();
        resp.setHeader('Content-Type', 'application/pdf');
        resp.setBody('Apex');
        resp.setStatusCode(200);
        return resp;   
    }*/

    @isTest 
    static void testCallout() 
    {
        ContentVersion cv = [SELECT Id FROM ContentVersion LIMIT 1];
        Contact con = [SELECT Id FROM Contact LIMIT 1];

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new Onboarding_MockResponseGenerator());
        
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        OnboardingUIController.getAccessToken(cv.Id, con.Id,'panda.jpg');
        
        // Verify response received contains fake values
        /*String contentType = res.getHeader('Content-Type');
        System.assert(contentType == 'application/json');
        String actualValue = res.getBody();
        String expectedValue = '{"example":"test"}';
        System.assertEquals(actualValue, expectedValue);
        System.assertEquals(200, res.getStatusCode());*/
    }
}