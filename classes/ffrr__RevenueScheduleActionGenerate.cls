/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RevenueScheduleActionGenerate {
    global RevenueScheduleActionGenerate() {

    }
    @InvocableMethod(label='Sync - Generate Revenue Schedules from source records.' description='Generate Revenue Schedules synchronously from source records.')
    global static void generateRevenueSchedulesFromSourceRecords(List<ffrr.RevenueScheduleActionGenerate.SourceRecord> input) {

    }
global class SourceRecord {
    @InvocableVariable(label='Source Record ID' description='Source record ID' required=true)
    global Id sourceId;
    global SourceRecord() {

    }
}
}
