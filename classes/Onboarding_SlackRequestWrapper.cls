/**
* @name         Onboarding_SlackRequestWrapper 
* @author       Girish Baviskar
* @date         15-01-2021
* @description  Wrapper class to hold HttpRequest fields (need b'coz cannot have httprequest in queueable class as class variable serialization exception) 
* @testClass    
**/
public inherited sharing class Onboarding_SlackRequestWrapper {
    
    public  string url;
    public  string method;
    public  String body;
    public  Contact objContact; 

    public Onboarding_SlackRequestWrapper() {

    }
    
    
}