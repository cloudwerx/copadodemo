/************************************************************************************************
* @Name         Onboarding_UtilityClass 
* @Author       Girish Baviskar
* @Date         10-02-2021
* @Description  Classs to generate test data
* @TestClass    
*************************************************************************************************/
@isTest
public with sharing class Onboarding_UtilityClass {
    
    public static List<Account> createAccounts(Integer count, Boolean performDml) {
        List<Account> accounts = new List<Account>();
        /* for(Integer i = 0; i < count; i++) {
            Account a = new Account();
            a.Name = 'Cloudwerx';
            a.BillingStreet = '4 Privet Drive';
            a.BillingCity = 'Little Whinging';
            a.BillingState = 'Surrey';
            a.BillingCountry = 'Australia';
            a.BillingPostalCode = 'SW1A 1AA';
            accounts.add(a);
        } */
        Account a = new Account();
            a.Name = 'Cloudwerx';
            a.BillingStreet = '4 Privet Drive';
            a.BillingCity = 'Little Whinging';
            a.BillingState = 'Surrey';
            a.BillingCountry = 'Australia';
            a.BillingPostalCode = 'SW1A 1AA';
            accounts.add(a);
        if(performDml) {
            insert accounts;
        }
        return accounts;
    }

    public static List<Contact> createContacts(Integer count, Boolean performDml, Id accountId) {
        RecordType indiaRecType = [SELECT Name FROM RecordType WHERE Name = 'India Employee'];
        RecordType australiaRecType = [SELECT Name FROM RecordType WHERE Name = 'Australia Employee'];
        List<Contact> contacts = new List<Contact>();
        for(Integer i = 0; i < count; i++) {
            Contact c = new Contact();
            c.FirstName = 'test';
            c.LastName = 'Contact'+i;
            c.AccountId = accountId;
            c.Email = 'test' + i + '@onboarding.com';
            c.Official_Email__c = 'test' + i + '@onboarding.com';
            
            if (Math.mod(i, 2) == 0) { 
                c.WorkCountry__c = 'India';
                c.Work_Location__c = 'Pune Office';
                c.Employee_Type__c = 'Employee';
                c.Employee_Role__c = 'Salesforce Developer';
                c.Date_Of_Joining__c = Date.today().addDays(1);
                c.RecordTypeId = indiaRecType.Id;

            } else {
                c.WorkCountry__c = 'Australia';
                c.Work_Location__c = 'Sydney Office';
                c.Employee_Type__c = 'Employer';
                c.Employee_Role__c = 'Salesforce Administrator';
                c.Date_Of_Joining__c = Date.today();
                c.RecordTypeId = australiaRecType.Id;
            }
            contacts.add(c);
        }
        if(performDml) {
            insert contacts;
        }
        return contacts;
    }

    public static List<Candidate__c> createCandidates(Integer count, Boolean performDml) {
        List<Candidate__c> candidates = new List<Candidate__c>();
        for(Integer i=0; i<count; i++) {
            Candidate__c candidate = new Candidate__c();
            candidate.First_Name__c='Test';
            candidate.Last_Name__c='Candidate ' + i;
            candidate.Notice_Period__c=80;
            candidate.Probable_Date_Of_Joining__c=System.today() + i;
            if (Math.mod(i, 2) == 0) { 
                candidate.Work_Location__c = 'Pune Office';
                candidate.Job_Role__c = 'Salesforce Developer';
                candidate.Date_Of_Joining__c = Date.today().addDays(1);
                candidate.Status__c  = 'Interview In Progress';  

            } else {
                candidate.Work_Location__c = 'Sydney Office';
                candidate.Job_Role__c = 'Salesforce Administrator';
                candidate.Date_Of_Joining__c = Date.today();
                candidate.Status__c   = 'Hired';
            }
            candidate.Date_Of_Joining__c = Date.today().addDays(1);
            candidate.Skills__c='Javascript;Integration;Git;Apex;Flows';
            candidate.Experience_Years__c=5;
            candidate.Score__c=40;
            candidate.Email__c='test@onboarding.com';
            candidate.Phone_No__c = '9283749817243';
                                                   

        candidates.add(candidate);
        }
        if(performDml) {
            insert candidates;
        }
        return candidates;
    }
    public static Contact createContact(Account objAccount){
        /* Contact objContact = new Contact(
            FirstName = 'Test',
            LastName = 'Contact',
            AccountId = objAccount.Id,
            Email = 'test@cloudwerx.co',
            Official_email__C = 'gbaviskar@cloudwerx.co',
            Work_Location__c = 'Pune Office',
            Employee_Type__c = 'Employee',
            Employee_Role__c = 'Salesforce Developer',
            Date_Of_Joining__c = Date.today(),  
            recordTypeId = '0120T0000005nNCQAY'  
        ); */

        Contact con = new Contact();
        con.FirstName = 'Yash';
        con.LastName = 'Bhalerao';
        con.Official_email__C = 'ybhalerao@cloudwerx.co';
        con.WorkCountry__c = 'India';
        con.Work_Location__c = 'Pune Office';
        con.Employee_Role__c = 'Salesforce Developer';
        con.Employee_Type__c = 'Employee';
        con.Date_Of_Joining__c = date.parse('11/02/2021');
        RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'India Employee' AND isActive = true LIMIT 1];
        con.RecordTypeId = rt.Id;
        con.AccountId = objAccount.Id;
        insert con;
        return con;
    }

    public static User createUser() {
        String uniqueUserName = 'communityuser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Onboarding Community Profile'];
        User u = new User(Alias = 'commuser', Email='communityuser@testorg.com',
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles',
         UserName=uniqueUserName);
         return u;
    }
}