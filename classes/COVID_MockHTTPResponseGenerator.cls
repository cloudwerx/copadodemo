@isTest
global class COVID_MockHTTPResponseGenerator implements HttpCalloutMock {
 global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"result":true,"phone_number":"918793238010","template_name":"payment_success","parameteres":[{"name":"total_price","value":"total_price"},{"name":"shop_whatsapp_url","value":"shop_whatsapp_url"}],"contact":{"id":"609e04a370aadc72795b2ed5","wAid":"918793238010","firstName":"Akshay Waikar","fullName":"Akshay Waikar","phone":"918793238010","source":null,"contactStatus":"VALID","photo":null,"created":"05-14-2021","tags":[],"customParams":[{"name":"name","value":"Akshay Waikar"},{"name":"phone","value":"918793238010"},{"name":"total_price","value":"total_price"},{"name":"shop_whatsapp_url","value":"shop_whatsapp_url"}],"optedIn":false,"isDeleted":false,"lastUpdated":"2021-05-14T05:03:33.703Z","allowBroadcast":true,"allowSMS":true,"teamIds":["609cdff9f019aee6784918a6"],"isInFlow":false,"lastFlowId":null,"currentFlowNodeId":null},"model":{"ids":["609e04a370aadc72795b2ed5"]},"validWhatsAppNumber":true}');
        res.setStatusCode(200);
        return res;
    }
}