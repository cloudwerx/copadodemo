/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RevenueScheduleService {
    global static Id generateAsync() {
        return null;
    }
    global static Id generateAsync(Set<Id> sourceRecordIds) {
        return null;
    }
    global static List<Id> generate(Set<Id> sourceRecordIds) {
        return null;
    }
    global static Map<Id,ffrr.RevenueScheduleService.GenerateResult> generate(Set<Id> sourceRecordIds, ffrr.RevenueScheduleService.GenerateConfig config) {
        return null;
    }
global class GenerateConfig {
    global Boolean allOrNone {
        get;
        set;
    }
    global GenerateConfig() {

    }
}
global class GenerateResult {
    global List<String> errors {
        get;
    }
    global Id scheduleId {
        get;
    }
    global Id sourceRecordId {
        get;
    }
    global Boolean success {
        get;
    }
}
}
