/************************************************************************************************
* @Name         Onboarding_loginControllertTest 
* @Author       Girish Baviskar
* @Date         20-02-2021
* @Description  controller for EmployeeList LWC.
* @TestClass    
*************************************************************************************************/ 
@isTest
public without sharing class Onboarding_FaqsControllerTest {
    
    @TestSetup
    static void makeData(){
        List<Onboarding_Faq__c> faqsToInsert = new List<Onboarding_Faq__c>();
        for (Integer i = 0; i < 10; i++) {
            Onboarding_Faq__c faq = new Onboarding_Faq__c();
            faq.Question_Number__c = i;
            faq.Question__c = 'test question';
            faq.Answer__c = 'test answers';
            if (Math.mod(i, 2) == 0) { 
                faq.Office_Location__c = 'Pune Office';
            } else {
                faq.Office_Location__c = 'Sydney Office';
            }
            faqsToInsert.add(faq);
        }
        insert faqsToInsert;
    }
    @IsTest
    static void testGetListOfFaqs(){

        List<Onboarding_Faq__c> lstOfFaqs = [SELECT     Question_Number__c,
                                                            Question__c,
                                                            Answer__c
                                                FROM Onboarding_Faq__c
                                                WITH SECURITY_ENFORCED
                                                ORDER BY Question_Number__c ASC
                                                LIMIT 5000];
        System.debug(lstOfFaqs.size());
        list<Account> accList = Onboarding_UtilityClass.createAccounts(1, true);
        List<Contact> cntList =  Onboarding_UtilityClass.createContacts(1, true, accList[0].Id);
        Test.startTest();
        List<Onboarding_Faq__c> faqList = Onboarding_FaqsController.getListOfFaqs(cntList[0].id);
        System.debug(faqList.size());
        System.assertNotEquals(0, faqList.size() , 'success');
        Test.stopTest();
    }
    
  
}