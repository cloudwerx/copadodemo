/*public inherited sharing class Onboarding_MockResponseGenerator {
    public Onboarding_MockResponseGenerator() {

    }
}*/


@isTest
global class Onboarding_MockResponseGenerator implements HttpCalloutMock 
{
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) 
    {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('https://www.googleapis.com/oauth2/v4/token', req.getEndpoint());
        //System.assertEquals('https://www.googleapis.com/drive/v2/files/xaqwscw', req.getEndpoint());
        //System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"access_token":"test", "expires_in":123, "token_type":"test"}');
        res.setStatusCode(200);
        return res;
    }
}