/**
* @name         ReviewTimeSheetSlackMthReminderBatch
* @author       Girish
* @date         30/08/2021
* @description  This is batch class for sending a reminder messages to slack users to review their time-sheets for this month.
* @testClass    ReviewTimeSheetSlackReminderWkBatchTest
**/
global with sharing class ReviewTimeSheetSlackMthReminderBatch implements Database.Batchable<sObject>, Database.Stateful, Schedulable, Database.AllowsCallouts {
    
    public Integer msgCount = 0;
    public Integer contactsWithNoSlackId = 0;
    global static Review_Timesheet_Slack_Reminder_Settings__mdt mdtRec = TimeSheetReminderUtils.getTimesheetReminderConfigDetails();
    boolean isWeekly = false;

    global void execute(SchedulableContext sc) {
        ReviewTimeSheetSlackMthReminderBatch b = new ReviewTimeSheetSlackMthReminderBatch(); 
        database.executebatch(b, 100);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        List<String> contactsToExclude = new List<String>();
        if (mdtRec.Users_to_Exclude_from_reminders__c != null) {
            contactsToExclude = mdtRec.Users_to_Exclude_from_reminders__c.split(',');
        } 

        String query = TimeSheetReminderUtils.startInit(mdtRec);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List <Contact> listOfReceivers) {
        if (mdtRec.Send_Monthly_Reminders__c) {
            for (Contact con : listOfReceivers) {
                if (con.Slack_Profile_Id__c != NULL) {
                    Http http = new Http();
                    HttpRequest req = TimeSheetReminderUtils.formHttpRequestToSend(mdtRec, con, isWeekly);
                    HttpResponse res = http.send(req);

                    if (res.getStatusCode() == 200) {
                        SlackPostMessageResponseWrapper postMessageParsedResponse =  SlackPostMessageResponseWrapper.parse(res.getBody());
                        if (postMessageParsedResponse.ok == true) {
                            msgCount++;
                        }
                    }
                } else {
                    contactsWithNoSlackId ++;
                }
            }
        }
    }

    global void finish(Database.BatchableContext bc){
        AsyncApexJob asyncJob = [Select Id, Status, ExtendedStatus, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedDate, CompletedDate
                            from AsyncApexJob where Id =:bc.getJobId()];                          
        TimeSheetReminderUtils.sendConfirmationEmail(asyncJob, mdtRec, msgCount, contactsWithNoSlackId);  
                           
    }
}