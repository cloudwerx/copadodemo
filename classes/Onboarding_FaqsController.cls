/**
* @name         Onboarding_loginController 
* @author       Girish Baviskar
* @date         20-02-2021
* @description  controller for faq LWC.
* @testClass    Onboarding_FaqsControllerTest
**/ 
public without sharing class Onboarding_FaqsController {
    
    /**
    * @description This method returns list of all the relevant faqs for the contact
    * @param       contactId id of contact for whome faqs are returned
    * @return     `List<Onboarding_Faq__c>`
    **/
    @AuraEnabled
    public static List<Onboarding_Faq__c> getListOfFaqs(Id contactId)
    {
        

            Contact objContact =   [SELECT Work_Location__c  
                                    FROM Contact  
                                    WHERE ID = :contactId
                                    WITH SECURITY_ENFORCED
                                    LIMIT 1];
            if (objContact != NULL) {
                List<Onboarding_Faq__c> lstOfFaqs = [SELECT     Question_Number__c,
                                    Question__c,
                                    Answer__c
                        FROM Onboarding_Faq__c
                        WHERE Office_Location__c = :objContact.Work_Location__c
                        OR Office_Location__c = 'All Offices'
                        WITH SECURITY_ENFORCED
                        ORDER BY Question_Number__c ASC
                        LIMIT 5000];
                System.debug('lstOfFaqs : ' + lstOfFaqs);
                return lstOfFaqs;
            }
        else 
        {
                return null;
            }
            
        
    }
}