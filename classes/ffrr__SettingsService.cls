/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SettingsService {
    global static ffrr.SettingsService.UpsertResult createMappingsToPOLI(List<Id> settingIds) {
        return null;
    }
    global static ffrr.SettingsService.UpsertResult createMappingsToPOLI(List<Id> settingIds, ffrr.SettingsService.UpsertOptions options) {
        return null;
    }
global class UpsertOptions {
    global Boolean UpdateMappings {
        get;
        set;
    }
    global UpsertOptions() {

    }
}
global class UpsertResult {
    global List<Id> FieldMappingDefinitionIds {
        get;
        set;
    }
    global List<Id> SettingIds {
        get;
        set;
    }
    global UpsertResult() {

    }
}
}
