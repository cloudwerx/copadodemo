/**
* @name         Onboarding_SlackBulkResponseWrapper 
* @author       Girish Baviskar
* @date         15-01-2021
* @description  This class is generated via jsonToApex to catch response from slack callout. 
* @testClass    
**/
@SuppressWarnings('PMD.pmdVariableNamingConventions')
public with sharing class Onboarding_SlackBulkResponseWrapper {

	public class Profile_Z {
		public String title;
		public String phone;
		public String skype;
		public String real_name;
		public String real_name_normalized;
		public String display_name;
		public String display_name_normalized;
		public Object fields;
		public String status_text;
		public String status_emoji;
		public Integer status_expiration;
		public String avatar_hash;
		public String image_original;
		public Boolean is_custom_image;
		public String email;
		public String first_name;
		public String last_name;
		public String image_24;
		public String image_32;
		public String image_48;
		public String image_72;
		public String image_192;
		public String image_512;
		public String image_1024;
		public String status_text_canonical;
		public String team;
	}

	public Boolean ok;
	public List<Members> members;
	public Integer cache_ts;
	public Response_metadata response_metadata;

	public class Profile_V {
		public String title;
		public String phone;
		public String skype;
		public String real_name;
		public String real_name_normalized;
		public String display_name;
		public String display_name_normalized;
		public Object fields;
		public String status_text;
		public String status_emoji;
		public Integer status_expiration;
		public String avatar_hash;
		public String image_original;
		public Boolean is_custom_image;
		public String email;
		public String image_24;
		public String image_32;
		public String image_48;
		public String image_72;
		public String image_192;
		public String image_512;
		public String image_1024;
		public String status_text_canonical;
		public String team;
	}

	public class Profile_W {
		public String title;
		public String phone;
		public String skype;
		public String real_name;
		public String real_name_normalized;
		public String display_name;
		public String display_name_normalized;
		public Object fields;
		public String status_text;
		public String status_emoji;
		public Integer status_expiration;
		public String avatar_hash;
		public String api_app_id;
		public Boolean always_active;
		public String image_original;
		public Boolean is_custom_image;
		public String bot_id;
		public String image_24;
		public String image_32;
		public String image_48;
		public String image_72;
		public String image_192;
		public String image_512;
		public String image_1024;
		public String status_text_canonical;
		public String team;
	}

	public class Profile_X {
		public String title;
		public String phone;
		public String skype;
		public String real_name;
		public String real_name_normalized;
		public String display_name;
		public String display_name_normalized;
		public Object fields;
		public String status_text;
		public String status_emoji;
		public Integer status_expiration;
		public String avatar_hash;
		public String email;
		public String image_24;
		public String image_32;
		public String image_48;
		public String image_72;
		public String image_192;
		public String image_512;
		public String status_text_canonical;
		public String team;
	}

	public class Profile_Y {
		public String title;
		public String phone;
		public String skype;
		public String real_name;
		public String real_name_normalized;
		public String display_name;
		public String display_name_normalized;
		public Object fields;
		public String status_text;
		public String status_emoji;
		public Integer status_expiration;
		public String avatar_hash;
		public String api_app_id;
		public Boolean always_active;
		public String image_original;
		public Boolean is_custom_image;
		public String bot_id;
		public String first_name;
		public String last_name;
		public String image_24;
		public String image_32;
		public String image_48;
		public String image_72;
		public String image_192;
		public String image_512;
		public String image_1024;
		public String status_text_canonical;
		public String team;
	}

	public class Profile {
		public String title;
		public String phone;
		public String skype;
		public String real_name;
		public String real_name_normalized;
		public String display_name;
		public String display_name_normalized;  
		public Object fields;
		public String status_text;
		public String status_emoji;
		public Integer status_expiration;
		public String avatar_hash;
		public String image_original;
		public Boolean is_custom_image;
		public String email;
		public String first_name;
		public String last_name;
		public String image_24;
		public String image_32;
		public String image_48;
		public String image_72;
		public String image_192;
		public String image_512;
		public String image_1024;
		public String status_text_canonical;
		public String team;
	}

	public class Response_metadata {
		public String next_cursor;
	}

	public class Profile_R {
		public String title;
		public String phone;
		public String skype;
		public String real_name;
		public String real_name_normalized;
		public String display_name;
		public String display_name_normalized;
		public Object fields;
		public String status_text;
		public String status_emoji;
		public Integer status_expiration;
		public String avatar_hash;
		public String guest_invited_by;
		public String email;
		public String first_name;
		public String last_name;
		public String image_24;
		public String image_32;
		public String image_48;
		public String image_72;
		public String image_192;
		public String image_512;
		public String status_text_canonical;
		public String team;
	}

	public class Profile_S {
		public String title;
		public String phone;
		public String skype;
		public String real_name;
		public String real_name_normalized;
		public String display_name;
		public String display_name_normalized;
		public Object fields;
		public String status_text;
		public String status_emoji;
		public Integer status_expiration;
		public String avatar_hash;
		public String guest_invited_by;
		public String image_original;
		public Boolean is_custom_image;
		public String email;
		public String first_name;
		public String last_name;
		public String image_24;
		public String image_32;
		public String image_48;
		public String image_72;
		public String image_192;
		public String image_512;
		public String image_1024;
		public String status_text_canonical;
		public String team;
	}

	public class Profile_T {
		public String title;
		public String phone;
		public String skype;
		public String real_name;
		public String real_name_normalized;
		public String display_name;
		public String display_name_normalized;
		public Object fields;
		public String status_text;
		public String status_emoji;
		public Integer status_expiration;
		public String avatar_hash;
		public String email;
		public String first_name;
		public String last_name;
		public String image_24;
		public String image_32;
		public String image_48;
		public String image_72;
		public String image_192;
		public String image_512;
		public String status_text_canonical;
		public String team;
	}

	public class Profile_U {
		public String title;
		public String phone;
		public String skype;
		public String real_name;
		public String real_name_normalized;
		public String display_name;
		public String display_name_normalized;
		public Object fields;
		public String status_text;
		public String status_emoji;
		public Integer status_expiration;
		public String avatar_hash;
		public String guest_invited_by;
		public String email;
		public String image_24;
		public String image_32;
		public String image_48;
		public String image_72;
		public String image_192;
		public String image_512;
		public String status_text_canonical;
		public String team;
	}

	public class Members {
		public String id;
		public String team_id;
		public String name;
		public Boolean deleted;
		public String color;
		public String real_name;
		public String tz;
		public String tz_label;
		public Integer tz_offset;
		public Profile profile;
		public Boolean is_admin;
		public Boolean is_owner;
		public Boolean is_primary_owner;
		public Boolean is_restricted;
		public Boolean is_ultra_restricted;
		public Boolean is_bot;
		public Boolean is_app_user;
		public Integer updated;
		public Boolean is_invited_user;
		public Boolean is_workflow_bot;
		public Boolean has_2fa;
	}

	public class Profile_Q {
		public String title;
		public String phone;
		public String skype;
		public String real_name;
		public String real_name_normalized;
		public String display_name;
		public String display_name_normalized;
		public Object fields;
		public String status_text;
		public String status_emoji;
		public Integer status_expiration;
		public String avatar_hash;
		public String api_app_id;
		public Boolean always_active;
		public String bot_id;
		public String image_24;
		public String image_32;
		public String image_48;
		public String image_72;
		public String image_192;
		public String image_512;
		public String status_text_canonical;
		public String team;
	}


	
	public static Onboarding_SlackBulkResponseWrapper parse(String json) {
		return (Onboarding_SlackBulkResponseWrapper) System.JSON.deserialize(json, Onboarding_SlackBulkResponseWrapper.class);
	}

}