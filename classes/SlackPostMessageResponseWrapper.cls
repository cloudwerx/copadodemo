public with sharing class SlackPostMessageResponseWrapper {
    
        public class Response_metadata {
            public List<String> warnings;
        }
    
        public class Message {
            public String type;
            public String subtype;
            public String text;
            public String ts;
            public String username;
            public Icons icons;
            public String bot_id;
        }
    
        public Boolean ok;
        public String channel;
        public String ts;
        public Message message;
        public String warning;
        public Response_metadata response_metadata;
    
        public class Icons {
            public String image_48;
        }
    
        
        public static SlackPostMessageResponseWrapper parse(String json) {
            return (SlackPostMessageResponseWrapper) System.JSON.deserialize(json, SlackPostMessageResponseWrapper.class);
        }
    
}