/**
* @name         Onboarding_EmployeeListControllerTest  
* @author       Girish Baviskar
* @date         20-01-2021
* @description  Test class for controller for Onboarding_EmployeeListControlle.
* @testClass    
**/ 
@isTest
public with sharing class Onboarding_EmployeeListControllerTest {
    
    @TestSetup
    static void makeData(){
        List<Account> accountList = Onboarding_UtilityClass.createAccounts(1, true);
        List<Contact> employeeList = Onboarding_UtilityClass.createContacts(10, true, accountList[0].Id);
    }
    @IsTest
    static void getOnboardingEmployees(){

        
        List<Contact> employeeList = [SELECT id, Employee_Type__c FROM contact ];
        Contact contactToPass = new Contact();
        for (Contact objContact : employeeList) {
            if (objContact.Employee_Type__c == 'Employer') {
                contactToPass = objContact;
                break;
            }
        }
        Test.startTest();
        list<Contact> returnedContacts = Onboarding_EmployeeListController.getCurrentOnboardingEmployees(contactToPass.Id);
        System.assertEquals( 5 , returnedContacts.size(), 'lists matched');
        System.debug('returned list : ' + returnedContacts);
        System.debug(employeeList.size());
        Test.stopTest();
        
    }

    @IsTest
    static void exceptionGetOnboardingEmployees(){
        
        Test.startTest();
        list<Contact> returnedContacts = Onboarding_EmployeeListController.getCurrentOnboardingEmployees('00T0T000002OIldUAG');
        System.assertEquals( null , returnedContacts, 'lists matched');
        Test.stopTest();
        
    }
}