/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RRTCreateJournalsService {
    global static Set<Id> createRRTToJournalAsync(ffrr.RRTCreateJournalsService.CreateJournalRequest integrationRequest) {
        return null;
    }
    global static Map<Id,ffrr.RRTCreateJournalsService.CreateJournalResponse> createRRTToJournal(ffrr.RRTCreateJournalsService.CreateJournalRequest integrationRequest) {
        return null;
    }
    global static Map<Id,String> validateRevenueRecognitionTransactions(ffrr.RRTCreateJournalsService.CreateJournalRequest integrationRequest) {
        return null;
    }
global class CreateJournalRequest {
    global Set<Id> revenueRecognitionIds;
    global Boolean shouldCreateJournalFromSummary;
    global CreateJournalRequest(Set<Id> revenueRecognitionIds) {

    }
    global CreateJournalRequest(Set<Id> revenueRecognitionIds, Boolean shouldCreateJournalFromSummary) {

    }
}
global class CreateJournalResponse {
    global String costJournalCreationError;
    global String costJournalCreationStatus;
    global Id costJournalId;
    global String revenueJournalCreationError;
    global String revenueJournalCreationStatus;
    global Id revenueJournalId;
    global CreateJournalResponse() {

    }
}
}
