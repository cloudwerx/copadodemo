/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global abstract class MappingDescriber {
    global MappingDescriber() {

    }
    global virtual List<fferpcore.MappingService.CustomMapping> getCustomMappings(String process) {
        return null;
    }
    global virtual List<fferpcore.TransformMapping> getManagedMappings() {
        return null;
    }
    global abstract fferpcore.Context getSourceContext();
    global abstract fferpcore.Context getTargetContext();
    global virtual void saveCustomMappings(String process, List<fferpcore.MappingService.CustomMapping> customMappings) {

    }
    global virtual Boolean supportsCustomMappings() {
        return null;
    }
}
