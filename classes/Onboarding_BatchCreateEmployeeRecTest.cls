/************************************************************************************************
* @Name         Onboarding_BatchCreateEmployeeRec 
* @Author       Girish Baviskar
* @Date         12-02-2021
* @Description  This batch class creates employee(Contact) records for hired candidates.
* @TestClass    
*************************************************************************************************/
@isTest
public inherited sharing class Onboarding_BatchCreateEmployeeRecTest {
    @TestSetup
    static void makeData(){
        /* List<Candidate__c> insertedCandidates = Onboarding_UtilityClass.createCandidates(4, true); */
        List<Account> accounts = Onboarding_UtilityClass.createAccounts(1, true);
        CandidateEvaluator_TestDataFactory.createTestRecords(4);

        
    }

    @IsTest
    static void checkContactInsert(){
        
        List<candidate__C> cnts = [select status__c from candidate__C];
        for (Integer i = 0; i < cnts.size() ; i++){
            if (Math.mod(i, 2) == 0) {
                cnts[i].Work_Location__c = 'Pune Office';
                cnts[i].Job_Role__c = 'Salesforce Developer';
                cnts[i].Date_Of_Joining__c = Date.today().addDays(1);
                cnts[i].Status__c  = 'Hired';  
            } else {
                cnts[i].Work_Location__c = 'Sydney Office';
                cnts[i].Job_Role__c = 'Salesforce Administrator';
                cnts[i].Date_Of_Joining__c = Date.today();
                cnts[i].Status__c   = 'Hired';
            }

        }
        
        update cnts;
        Test.startTest();
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() + 1); 
        String ss = String.valueOf(Datetime.now().second());
        String oneMinChron = ss + ' ' + min + ' ' + hour + ' 1/1 * ? *';
        Onboarding_ScheduleEmployeeCreate schobj = new Onboarding_ScheduleEmployeeCreate();
        System.schedule('Create Employee', oneMinChron, schobj);
        schobj.execute(null);
        Test.stopTest();
        /* 
        Onboarding_BatchCreateEmployeeRec obj = new Onboarding_BatchCreateEmployeeRec();
        Id batchId = Database.executeBatch(obj); */
        
        Integer insertedContacts = [SELECT Count() FROM Contact];
        System.debug('inserted contacts: ' + insertedContacts);
        // System.debug('inserted contacts size' + insertedContacts.size());
        System.assertEquals(2, insertedContacts, 'it runs fine');
        
    }

    /* @IsTest
    static void checkContactInsertFail(){
        Account acc = [Select name from Account];
        acc.Name = 'cloudwerx pvt ltd';
        update acc;
        Test.startTest();
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() + 1); 
        String ss = String.valueOf(Datetime.now().second());
        String oneMinChron = ss + ' ' + min + ' ' + hour + ' 1/1 * ? *';
        Onboarding_ScheduleEmployeeCreate schobj = new Onboarding_ScheduleEmployeeCreate();
        System.schedule('Create Employee', oneMinChron, schobj);
        Test.stopTest();
        List<Contact> insertedContacts = [SELECT id FROM Contact];
        System.debug('inseted contacts: ' + insertedContacts);
        System.assertEquals(0, insertedContacts.size(), 'it runs fine');
        
    } */
}