/**
* @name         Onboarding_FinalEmailBodyController 
* @author       Girish Baviskar
* @date         20-02-2021
* @description  controller for visualforce email template.
*               returns all the employee and employer tasks.
* @testClass    Onboarding_FinalEmailBodyCtrlTest
**/ 
public without sharing class Onboarding_FinalEmailBodyController {
	public String conID {get; set;}
    public List<Task> employerTasks {
        get {
            List<Task> employerTaskForContact = [SELECT  Id,
                            Subject,
                            Status,
                            Priority,
                            Description,
                            Is_Completed__c
                    FROM Task 
                    WHERE whoId =: conID
                    AND Task_Type__c = 'Employer'
                    ORDER BY Task_Sequence_Number__c ASC
                    LIMIT 10000];
            return employerTaskForContact;
        }
        set;
    }
    
    public List<Task> employeeTasks {
        get {
            List<Task> employeeTaskForContact = [SELECT  Id,
                            Subject,
                            Status,
                            Priority,
                            Description,
                            Is_Completed__c
                    FROM Task 
                    WHERE whoId =: conID
                    AND Task_Type__c = 'Employee'
                    ORDER BY Task_Sequence_Number__c ASC
                    LIMIT 10000];
            return employeeTaskForContact;
        }
        set;
    }

}