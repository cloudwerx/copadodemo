/**
* @name         Onboarding_EmployerTaskViewController 
* @author       Girish Baviskar
* @date         11-02-2021
* @description  controller for EmployerTaskView LWC.
* @testClass    Onboarding_EmployerTaskViewCntlrTest
**/ 
public without sharing class Onboarding_EmployerTaskViewController {
    
    /**
    * @description This method gets all the employers onboarding tasks for the employee.SELECT  FROM ${1||}
    * @param       contactId contact id for whose employer tasks are returned   
    * @return     `List<Task>`
    **/
    @AuraEnabled
    public static List<Task> getEmployerTaskDetails(Id contactId)
    {
        System.debug('inside apex controller getEmployerTaskDetails, contactId:' + contactId);
        
        List<Task> lstOfTask = [SELECT  Id,
                            Subject,
                            Status,
                            Priority,
                            Description,
                            Is_Completed__c
                    FROM Task 
                    WHERE whoId =: contactId 
                    AND Task_Type__c = 'Employer'
                    WITH SECURITY_ENFORCED
                    ORDER BY Task_Sequence_Number__c ASC
                    LIMIT 10000];
                    System.debug('lst of task  : ' + lstOfTask);
        if(lstOfTask.size() > 0){
            return lstOfTask;
        } else {
            return null;
        }
           
    }

    /**
    * @description This method inserts new task from ui employers task view
    * @param       taskToInsert Task object to insert
    * @param       contactId To which contact this task should be for.SELECT  FROM ${1||}
    * @return     ``
    **/
    @AuraEnabled
    public static void insertTask(Task taskToInsert, Id contactId)
    {
        Id EmployeeId;
        Id EmployerId;

        List<Onboarding_Task_Assigned_Id__mdt> UserIds = [SELECT Label, User_Id__c FROM Onboarding_Task_Assigned_Id__mdt LIMIT 100];

        for(Onboarding_Task_Assigned_Id__mdt u : UserIds)
        {
            if(u.Label == 'Platform Integration User')
            {
                EmployeeId = u.User_Id__c;
            }
            else 
            {
                EmployerId = u.User_Id__c;
            }
        }

        if(taskToInsert.Task_Type__c == 'Employee')
        {
            taskToInsert.OwnerId = EmployeeId;
        }
        else
        {
            taskToInsert.OwnerId = EmployerId;
        }

        taskToInsert.WhoId = contactId;

        insert taskToInsert;
    }
}