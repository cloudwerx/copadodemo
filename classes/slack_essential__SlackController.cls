/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SlackController {
    @AuraEnabled
    global static String checkIfConnectedToChannel(String recordId) {
        return null;
    }
    @AuraEnabled
    global static String checkIfOauthed() {
        return null;
    }
    @AuraEnabled
    global static String createNewChannel(String recordId, String channelName) {
        return null;
    }
    @RemoteAction
    global static void doOauth(String code) {

    }
    @AuraEnabled
    global static String getSlackChannels() {
        return null;
    }
    @AuraEnabled
    global static String linkChannel(String slackChannelId, String recordId) {
        return null;
    }
    @AuraEnabled
    global static String unlinkChannel(String recordId, String slackChannelId) {
        return null;
    }
}
