/************************************************************************************************
* @Name         Onboarding_EmployerTaskViewCntlrTest 
* @Author       Yash Bhalerao
* @Date         12-02-2021
* @Description  Test class for Onboarding_EmployerTaskViewController
* @TestClass    
*************************************************************************************************/ 

@isTest
public class Onboarding_EmployerTaskViewCntlrTest 
{
    @TestSetup
    static void makeData()
    {
        RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'India Employee' AND isActive = true LIMIT 1];

        Contact con = new Contact();
        con.FirstName = 'Yash';
        con.LastName = 'Bhalerao';
        con.Email = 'ybhalerao@cloudwerx.co';
        con.Work_Location__c = 'Pune Office';
        con.Employee_Role__c = 'Salesforce Developer';
        con.Employee_Type__c = 'Employee';
        con.Date_Of_Joining__c = date.parse('11/02/2021');
        con.RecordTypeId = rt.Id;
        
        insert con;

        User newUser = new User();
        newUser.Username = 'ybhalerao@cloudwerx.c';
        newUser.LastName = 'Baviskar';
        newUser.Email = 'gbaviskar@cloudwerx.co';
        newUser.Alias = 'gbavi';
        newUser.TimeZoneSidKey = 'Australia/Sydney';
        newUser.LocaleSidKey = 'en_AU';
        newUser.EmailEncodingKey = 'ISO-8859-1';
        newUser.ProfileId = '00e6F000003YSGmQAO';
        newUser.LanguageLocaleKey = 'en_US';
        
        insert newUser;

        Task newTask = new Task();
        newTask.OwnerId = newUser.Id;
        newTask.WhoId = con.Id;
        newTask.Subject = 'ascascac';
        newTask.Status = 'In Progress';
        newTask.Is_Completed__c = false;
        newTask.Type_of_Document__c = 'Profile Photo';
        newTask.Uploaded_File_Id__c = 'xaqwscw';

        insert newTask;
    }

    @isTest
    public static void getEmployerTaskDetailsTest()
    {
        Contact con = [SELECT Id FROM Contact LIMIT 1];

        Onboarding_EmployerTaskViewController.getEmployerTaskDetails(con.Id);
    }

    @isTest
    public static void insertTaskTest()
    {
        Contact con = [SELECT Id FROM Contact LIMIT 1];
        User newUser = [SELECT Id FROM User LIMIT 1];

        Task newTask = new Task();
        newTask.OwnerId = newUser.Id;
        newTask.WhoId = con.Id;
        newTask.Subject = 'ascascac';
        newTask.Status = 'In Progress';
        newTask.Is_Completed__c = false;
        newTask.Type_of_Document__c = 'Profile Photo';
        newTask.Uploaded_File_Id__c = 'xaqwscw';

        //Task t = [SELECT ID,Task_Type__c FROM Task LIMIT 1];
        

        Onboarding_EmployerTaskViewController.insertTask(newTask, con.Id);
    }
}