/**
* @name         Onboarding_SlackReminder 
* @author       Girish Baviskar
* @date         15-01-2021
* @description  Reminds employee and employer if their tasks are not completed. 
* @testClass    Onboarding_SlackReminderTest
**/
public with sharing class Onboarding_SlackReminder implements Schedulable
{
    
    public void execute(SchedulableContext sc) {
        List<Onboarding_App_Slack_Setting__mdt> metaDataSettings = [SELECT  Notify_Employee__c,
                                                                            Office_Location__c,
                                                                            Notify_Employer__c, 
                                                                            Employers_Channel__c,
                                                                            Reminder_Message_To_Employee__c,
                                                                            Reminder_Message_To_Employer__c,
                                                                            Channels__c,
                                                                            Bot_Named_Credentials__c,
                                                                            User_Named_Credentials__c
                                                                    FROM Onboarding_App_Slack_Setting__mdt
                                                                    WITH SECURITY_ENFORCED
                                                                    LIMIT 1000];  
        
        Map< Id, Onboarding_App_Slack_Setting__mdt > mapOfContactAndMetadata = new Map<Id, Onboarding_App_Slack_Setting__mdt>();
         
        Date todayDate = Date.today();
        List<contact> onboardingNotCompletedList = [SELECT  Id,
                                                            FirstName,
                                                            LastName,
                                                            Is_Employee_Onboarding_Complete__c,
                                                            Is_Employer_Onboarding_Complete__c,
                                                            Work_Location__c,
                                                            Slack_Display_Name__c,
                                                            Slack_Profile_Picture_Url__c,
                                                            Slack_Profile_Id__c,
                                                            Is_Slack_User__c,
                                                            Date_Of_Joining__c
                                                    FROM Contact
                                                    WHERE (Is_Employee_Onboarding_Complete__c = false
                                                    OR Is_Employer_Onboarding_Complete__c = false)
                                                    AND Date_Of_Joining__c = :todayDate
                                                    WITH SECURITY_ENFORCED
                                                    LIMIT 10000];
        System.debug('onboardingNotCompletedList : ' + onboardingNotCompletedList);
        for (Contact objContact : onboardingNotCompletedList) {
        
            for (Onboarding_App_Slack_Setting__mdt metaData : metaDataSettings) {
                if(metadata.Office_Location__c == objContact.Work_Location__c) {
                    mapOfContactAndMetadata.put(objContact.Id, metaData); // creating map of contact and slack setting applicable for it.
                }
            }
        }
       
        List<contact> employeeOnboardingIncompleteLst = new List<contact>();
        List<contact> employerOnboardingIncompleteLst = new List<contact>();    
        
        for (contact objContact : onboardingNotCompletedList) {
            if (objContact.Is_Employee_Onboarding_Complete__c == false) {
                employeeOnboardingIncompleteLst.add(objContact);
            } 
            if(objContact.Is_Employer_Onboarding_Complete__c == false) {
                employerOnboardingIncompleteLst.add(objContact);
            }
        }
        // if (!Test.isRunningTest())
        // {
            Onboarding_ContactTriggerHandler.sendSlackMessageToEmployee(employeeOnboardingIncompleteLst,mapOfContactAndMetadata, 'taskReminder');
            Onboarding_ContactTriggerHandler.sendSlackMessageToEmployer(employerOnboardingIncompleteLst,mapOfContactAndMetadata, 'taskReminder');
        // }
    }

    /*public void execute(schedulableContext SC) 
    {
        system.debug('inside scheduler');
        Onboarding_BatchSlackReminder obj = new Onboarding_BatchSlackReminder();
        Database.executeBatch(obj);
    }*/
}