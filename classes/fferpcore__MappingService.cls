/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MappingService {
    global static String MAPPING_TYPE_RELATIONSHIP;
    global static String MAPPING_TYPE_STANDARD;
    global MappingService() {

    }
    global static List<fferpcore.MappingService.CustomMapping> getCustomMappings(String process) {
        return null;
    }
    global static Map<String,List<fferpcore.MappingService.CustomMapping>> getCustomMappings(Set<String> processes) {
        return null;
    }
global class CustomMapping {
    global CustomMapping(String process, List<List<String>> sourceKeys, List<List<String>> targetKeys) {

    }
    global String getAdditionalLookupField() {
        return null;
    }
    global List<String> getCorrelationSourceKey() {
        return null;
    }
    global String getCorrelationStrategy() {
        return null;
    }
    global String getCorrelationTargetKey() {
        return null;
    }
    global List<String> getFilters() {
        return null;
    }
    global String getName() {
        return null;
    }
    global String getProcess() {
        return null;
    }
    global String getSingleSourceKey() {
        return null;
    }
    global String getSingleTargetKey() {
        return null;
    }
    global List<String> getSourceKey() {
        return null;
    }
    global List<List<String>> getSourceKeys() {
        return null;
    }
    global List<String> getSourcePath() {
        return null;
    }
    global String getStaticData() {
        return null;
    }
    global String getStaticDescription() {
        return null;
    }
    global String getTargetChildType() {
        return null;
    }
    global List<List<String>> getTargetKeys() {
        return null;
    }
    global List<String> getTargetPath() {
        return null;
    }
    global Id getTransformationTable() {
        return null;
    }
    global String getType() {
        return null;
    }
    global fferpcore.MappingService.CustomMapping withAdditionalLookupField(String additionalLookupField) {
        return null;
    }
    global fferpcore.MappingService.CustomMapping withCorrelationSourceKey(List<String> correlationSourceKey) {
        return null;
    }
    global fferpcore.MappingService.CustomMapping withCorrelationStrategy(String correlationStrategy) {
        return null;
    }
    global fferpcore.MappingService.CustomMapping withCorrelationTargetKey(String correlationTargetKey) {
        return null;
    }
    global fferpcore.MappingService.CustomMapping withFilters(List<String> filters) {
        return null;
    }
    global fferpcore.MappingService.CustomMapping withName(String name) {
        return null;
    }
    global fferpcore.MappingService.CustomMapping withSourcePath(List<String> path) {
        return null;
    }
    global fferpcore.MappingService.CustomMapping withStaticData(String staticData) {
        return null;
    }
    global fferpcore.MappingService.CustomMapping withStaticDescription(String staticDescription) {
        return null;
    }
    global fferpcore.MappingService.CustomMapping withTargetChildType(String targetChildType) {
        return null;
    }
    global fferpcore.MappingService.CustomMapping withTargetPath(List<String> path) {
        return null;
    }
    global fferpcore.MappingService.CustomMapping withTransformationTable(Id transformationTable) {
        return null;
    }
    global fferpcore.MappingService.CustomMapping withType(String type) {
        return null;
    }
}
}
