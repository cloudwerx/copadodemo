/************************************************************************************************
* @Name         MyFieldSetApexClass
* @Author       Yash Bhalerao
* @Date         19-01-2021
* @Description  Controller for onboarding_App_Contact_Form
* @TestClass    Onboarding_MyFieldSetApexClassTest
*************************************************************************************************/

public without sharing class MyFieldSetApexClass 
{
    /*public MyFieldSetApexClass() 
    {

    }*/

    public static List<Schema.FieldSetMember> getFields() 
    {
        system.debug('Field set : '+SObjectType.Contact.FieldSets.FieldSet.getFields());
        return SObjectType.Contact.FieldSets.FieldSet.getFields();
    }

    @AuraEnabled
    public static Contact getContactList(Id conid) 
    {
        System.debug('conid : ' + conid );
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : MyFieldSetApexClass.getFields()) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id FROM Contact WHERE ID=\''+ conid +'\'';
        System.debug('query : ' + query );
        return Database.query(query);
    }

    @AuraEnabled
    public static boolean updateContact(Contact contactToUpdate)
    {
        system.debug('contactToUpdate : ' + contactToUpdate);
        try 
        {
            
            update contactToUpdate;
            return true;

        }
        catch (Exception e) 
        {
            return false;
            //system.debug('unable to update record'+e.getMessage());
        }
    }
}