<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <content>pse__Utilization_Summary_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>pse__Utilization_Summary_Compact_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>pse__Utilization_Summary_Compact_Layout</fullName>
        <fields>Name</fields>
        <fields>pse__Region__c</fields>
        <fields>pse__Practice__c</fields>
        <fields>pse__Group__c</fields>
        <fields>pse__Actual_Billable_Hours__c</fields>
        <fields>pse__Target_Hours__c</fields>
        <label>Utilization Summary Compact Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>If Calculate Summary By Role, in the Utilization custom setting, is set to True, FinancialForce PSA creates Utilization Summary records when calculating scheduled utilization. In contrast to Utilization Detail, this object summarizes utilization by role rather than resource; it can therefore summarize hours from unheld resource requests (these are never linked to a resource). Therefore, utilization summaries give a fuller picture of capacity because these hours are summarized alongside those from held resource requests, assignments, and calendars. You can use this information to plan for future capacity by role.

Optionally, total unheld hours can be weighted by the percentage probability of closing an opportunity linked to the resource request. Utilization calculations have a master-detail relationship with utilization summaries.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>FFX_Period_Ending__c</fullName>
        <externalId>false</externalId>
        <formula>pse__Time_Period__r.pse__End_Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Period Ending</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>FFX_Remaining_Capacity_Hours__c</fullName>
        <externalId>false</externalId>
        <formula>IF(pse__Calendar_Hours__c=0,
0,
(pse__Calendar_Hours__c - pse__Assigned_Billable_Hours__c - pse__Assigned_Excluded_Hours__c - pse__Assigned_Credited_Hours__c - pse__Assigned_Non_Billable_Hours__c - pse__Held_Hours__c - pse__Unheld_Hours__c))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Remaining Capacity Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Actual_Billable_Hours__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Actual Billable Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Actual_Credited_Hours__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Actual Credited Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Actual_Excluded_Hours__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Actual Excluded Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Actual_Non_Billable_Hours__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Actual Non-Billable Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Assigned_Billable_Hours__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Assigned Billable Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Assigned_Credited_Hours__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Assigned Credited Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Assigned_Excluded_Hours__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Assigned Excluded Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Assigned_Non_Billable_Hours__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Assigned Non-Billable Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Calendar_Hours__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Calendar Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Group__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Group</label>
        <referenceTo>pse__Grp__c</referenceTo>
        <relationshipLabel>Utilization Summary</relationshipLabel>
        <relationshipName>Utilization_Summary</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>pse__Held_Hours_Opp_Pct_Weighted__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Held Hours Opp Pct Weighted</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Held_Hours__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Held Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Practice__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Practice</label>
        <referenceTo>pse__Practice__c</referenceTo>
        <relationshipLabel>Utilization Summary</relationshipLabel>
        <relationshipName>Utilization_Summary</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>pse__Region__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Region</label>
        <referenceTo>pse__Region__c</referenceTo>
        <relationshipLabel>Utilization Summary</relationshipLabel>
        <relationshipName>Utilization_Summary</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>pse__Remaining_Capacity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(pse__Calendar_Hours__c=0,
0,
(pse__Calendar_Hours__c - pse__Assigned_Billable_Hours__c - pse__Assigned_Excluded_Hours__c - pse__Assigned_Credited_Hours__c - pse__Assigned_Non_Billable_Hours__c - pse__Held_Hours__c -  pse__Unheld_Hours__c) / pse__Calendar_Hours__c
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Remaining Capacity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>pse__Resource_Role__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Resource Role</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Program Manager - AU</fullName>
                    <default>false</default>
                    <label>Program Manager - AU</label>
                </value>
                <value>
                    <fullName>Project Manager/Scrum Master - AU</fullName>
                    <default>false</default>
                    <label>Project Manager/Scrum Master - AU</label>
                </value>
                <value>
                    <fullName>Salesforce CTA - AU</fullName>
                    <default>false</default>
                    <label>Salesforce CTA - AU</label>
                </value>
                <value>
                    <fullName>Salesforce Program Architect - AU</fullName>
                    <default>false</default>
                    <label>Salesforce Program Architect - AU</label>
                </value>
                <value>
                    <fullName>Salesforce Principal Architect - AU</fullName>
                    <default>false</default>
                    <label>Salesforce Principal Architect - AU</label>
                </value>
                <value>
                    <fullName>Salesforce Architect - AU</fullName>
                    <default>false</default>
                    <label>Salesforce Architect - AU</label>
                </value>
                <value>
                    <fullName>Salesforce Principal Consultant - AU</fullName>
                    <default>false</default>
                    <label>Salesforce Principal Consultant - AU</label>
                </value>
                <value>
                    <fullName>Salesforce Consultant - AU</fullName>
                    <default>false</default>
                    <label>Salesforce Consultant - AU</label>
                </value>
                <value>
                    <fullName>Salesforce Technical Lead - AU</fullName>
                    <default>false</default>
                    <label>Salesforce Technical Lead - AU</label>
                </value>
                <value>
                    <fullName>Salesforce Developer - AU</fullName>
                    <default>false</default>
                    <label>Salesforce Developer - AU</label>
                </value>
                <value>
                    <fullName>Salesforce Architect - IN</fullName>
                    <default>false</default>
                    <label>Salesforce Architect - IN</label>
                </value>
                <value>
                    <fullName>Salesforce Consultant - IN</fullName>
                    <default>false</default>
                    <label>Salesforce Consultant - IN</label>
                </value>
                <value>
                    <fullName>Salesforce Technical Lead - IN</fullName>
                    <default>false</default>
                    <label>Salesforce Technical Lead - IN</label>
                </value>
                <value>
                    <fullName>Salesforce Developer - IN</fullName>
                    <default>false</default>
                    <label>Salesforce Developer - IN</label>
                </value>
                <value>
                    <fullName>Salesforce QA Lead - IN</fullName>
                    <default>false</default>
                    <label>Salesforce QA Lead - IN</label>
                </value>
                <value>
                    <fullName>Salesforce QA Consultant - IN</fullName>
                    <default>false</default>
                    <label>Salesforce QA Consultant - IN</label>
                </value>
                <value>
                    <fullName>Salesforce UI Lead - IN</fullName>
                    <default>false</default>
                    <label>Salesforce UI Lead - IN</label>
                </value>
                <value>
                    <fullName>Salesforce UI Developer - IN</fullName>
                    <default>false</default>
                    <label>Salesforce UI Developer - IN</label>
                </value>
                <value>
                    <fullName>Salesforce Release Manager - IN</fullName>
                    <default>false</default>
                    <label>Salesforce Release Manager - IN</label>
                </value>
                <value>
                    <fullName>Salesforce Data Migration Consultant - IN</fullName>
                    <default>false</default>
                    <label>Salesforce Data Migration Consultant - IN</label>
                </value>
                <value>
                    <fullName>Automation Architect - AU</fullName>
                    <default>false</default>
                    <label>Automation Architect - AU</label>
                </value>
                <value>
                    <fullName>Automation Principal Consultant - AU</fullName>
                    <default>false</default>
                    <label>Automation Principal Consultant - AU</label>
                </value>
                <value>
                    <fullName>Automation Consultant - AU</fullName>
                    <default>false</default>
                    <label>Automation Consultant - AU</label>
                </value>
                <value>
                    <fullName>Automation Technical Lead - AU</fullName>
                    <default>false</default>
                    <label>Automation Technical Lead - AU</label>
                </value>
                <value>
                    <fullName>Automation Developer - AU</fullName>
                    <default>false</default>
                    <label>Automation Developer - AU</label>
                </value>
                <value>
                    <fullName>Automation QA Consultant - AU</fullName>
                    <default>false</default>
                    <label>Automation QA Consultant - AU</label>
                </value>
                <value>
                    <fullName>Automation Principal Consultant - IN</fullName>
                    <default>false</default>
                    <label>Automation Principal Consultant - IN</label>
                </value>
                <value>
                    <fullName>Automation Consultant - IN</fullName>
                    <default>false</default>
                    <label>Automation Consultant - IN</label>
                </value>
                <value>
                    <fullName>Automation Technical Lead - IN</fullName>
                    <default>false</default>
                    <label>Automation Technical Lead - IN</label>
                </value>
                <value>
                    <fullName>Automation Developer - IN</fullName>
                    <default>false</default>
                    <label>Automation Developer - IN</label>
                </value>
                <value>
                    <fullName>Automation QA Consultant - IN</fullName>
                    <default>false</default>
                    <label>Automation QA Consultant - IN</label>
                </value>
                <value>
                    <fullName>Integration Architect - AU</fullName>
                    <default>false</default>
                    <label>Integration Architect - AU</label>
                </value>
                <value>
                    <fullName>Integration Technical Lead - AU</fullName>
                    <default>false</default>
                    <label>Integration Technical Lead - AU</label>
                </value>
                <value>
                    <fullName>Integration Developer - AU</fullName>
                    <default>false</default>
                    <label>Integration Developer - AU</label>
                </value>
                <value>
                    <fullName>Integration QA Consultant - AU</fullName>
                    <default>false</default>
                    <label>Integration QA Consultant - AU</label>
                </value>
                <value>
                    <fullName>Integration Technical Lead - IN</fullName>
                    <default>false</default>
                    <label>Integration Technical Lead - IN</label>
                </value>
                <value>
                    <fullName>Integration Developer - IN</fullName>
                    <default>false</default>
                    <label>Integration Developer - IN</label>
                </value>
                <value>
                    <fullName>Integration QA Consultant - IN</fullName>
                    <default>false</default>
                    <label>Integration QA Consultant - IN</label>
                </value>
                <value>
                    <fullName>Integration Junior Developer - IN</fullName>
                    <default>false</default>
                    <label>Integration Junior Developer - IN</label>
                </value>
                <value>
                    <fullName>Delivery Manager - IN</fullName>
                    <default>false</default>
                    <label>Delivery Manager - IN</label>
                </value>
                <value>
                    <fullName>Salesforce Administrator - IN</fullName>
                    <default>false</default>
                    <label>Salesforce Administrator - IN</label>
                </value>
                <value>
                    <fullName>Salesforce Junior Developer - IN</fullName>
                    <default>false</default>
                    <label>Salesforce Junior Developer - IN</label>
                </value>
                <value>
                    <fullName>Engagement Manager - AU</fullName>
                    <default>false</default>
                    <label>Engagement Manager - AU</label>
                </value>
                <value>
                    <fullName>Salesforce UX Lead - AU</fullName>
                    <default>false</default>
                    <label>Salesforce UX Lead - AU</label>
                </value>
                <value>
                    <fullName>Automation Administrator - IN</fullName>
                    <default>false</default>
                    <label>Automation Administrator - IN</label>
                </value>
                <value>
                    <fullName>Release Manager - IN</fullName>
                    <default>false</default>
                    <label>Release Manager - IN</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>pse__Target_Hours__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Target Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Template_Key__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Template Key</label>
        <precision>8</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Time_Period__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Time Period</label>
        <referenceTo>pse__Time_Period__c</referenceTo>
        <relationshipLabel>Utilization Summary</relationshipLabel>
        <relationshipName>Utilization_Summary</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>pse__Total_Hours_Opp_Pct_Weighted__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>pse__Assigned_Billable_Hours__c + pse__Assigned_Excluded_Hours__c + pse__Assigned_Credited_Hours__c + pse__Assigned_Non_Billable_Hours__c + pse__Held_Hours_Opp_Pct_Weighted__c + pse__Unheld_Hours_Opp_Pct_Weighted__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Hours Opp Pct Weighted</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Total_Hours__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>pse__Assigned_Billable_Hours__c + pse__Assigned_Excluded_Hours__c + pse__Assigned_Credited_Hours__c + pse__Assigned_Non_Billable_Hours__c +  pse__Held_Hours__c + pse__Unheld_Hours__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Group</fullName>
                    <default>false</default>
                    <label>Group</label>
                </value>
                <value>
                    <fullName>Group by Practice</fullName>
                    <default>false</default>
                    <label>Group by Practice</label>
                </value>
                <value>
                    <fullName>Group by Region</fullName>
                    <default>false</default>
                    <label>Group by Region</label>
                </value>
                <value>
                    <fullName>Practice</fullName>
                    <default>false</default>
                    <label>Practice</label>
                </value>
                <value>
                    <fullName>Practice by Group</fullName>
                    <default>false</default>
                    <label>Practice by Group</label>
                </value>
                <value>
                    <fullName>Practice by Region</fullName>
                    <default>false</default>
                    <label>Practice by Region</label>
                </value>
                <value>
                    <fullName>Region</fullName>
                    <default>false</default>
                    <label>Region</label>
                </value>
                <value>
                    <fullName>Region by Group</fullName>
                    <default>false</default>
                    <label>Region by Group</label>
                </value>
                <value>
                    <fullName>Region by Practice</fullName>
                    <default>false</default>
                    <label>Region by Practice</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>pse__Unheld_Hours_Opp_Pct_Weighted__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unheld Hours Opp Pct Weighted</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Unheld_Hours__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unheld Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Utilization_Calculation__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Utilization Calculation</label>
        <referenceTo>pse__Utilization_Calculation__c</referenceTo>
        <relationshipLabel>Utilization Summary</relationshipLabel>
        <relationshipName>Utilization_Summary</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <label>Utilization Summary</label>
    <listViews>
        <fullName>pse__All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>US-{0000000000}</displayFormat>
        <label>Utilization Summary Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Utilization Summary</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
