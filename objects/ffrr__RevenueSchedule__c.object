<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Header information associated with revenue schedule lines for a given source record.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>ffrr__Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Lookup to the Salesforce account on the revenue schedule.</description>
        <externalId>false</externalId>
        <inlineHelpText>Lookup to the Salesforce account on the revenue schedule.</inlineHelpText>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Revenue Schedules</relationshipLabel>
        <relationshipName>RevenueSchedules</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ffrr__Currency__c</fullName>
        <deprecated>false</deprecated>
        <description>Deprecated and no longer in use.</description>
        <externalId>false</externalId>
        <inlineHelpText>Deprecated and no longer in use.</inlineHelpText>
        <label>Deprecated: Document Currency</label>
        <length>50</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__EndDate__c</fullName>
        <deprecated>false</deprecated>
        <description>The end date of the revenue schedule, derived from the source record.</description>
        <externalId>false</externalId>
        <inlineHelpText>The end date of the revenue schedule, derived from the source record.</inlineHelpText>
        <label>End Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ffrr__PerformanceObligation__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Performance Obligation</label>
        <referenceTo>ffrr__PerformanceObligation__c</referenceTo>
        <relationshipLabel>Revenue Schedules</relationshipLabel>
        <relationshipName>RevenueSchedules</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ffrr__RecognitionMethod__c</fullName>
        <deprecated>false</deprecated>
        <description>The method used to calculate the revenue to recognize for each period. For example, Equal Split where equal amounts are recognized each period.</description>
        <externalId>false</externalId>
        <inlineHelpText>The method used to calculate the revenue to recognize for each period. For example, Equal Split where equal amounts are recognized each period.</inlineHelpText>
        <label>Recognition Method</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>ffrr__RecognitionMethods</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>ffrr__ScheduleNumber__c</fullName>
        <deprecated>false</deprecated>
        <displayFormat>RS-{000000000}</displayFormat>
        <externalId>false</externalId>
        <label>Schedule Number</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>AutoNumber</type>
    </fields>
    <fields>
        <fullName>ffrr__SourceId__c</fullName>
        <caseSensitive>false</caseSensitive>
        <deprecated>false</deprecated>
        <description>ID of the related source record.</description>
        <externalId>false</externalId>
        <inlineHelpText>ID of the related source record.</inlineHelpText>
        <label>Source ID</label>
        <length>18</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>ffrr__SourceType__c</fullName>
        <deprecated>false</deprecated>
        <description>The source record&apos;s object type.</description>
        <externalId>false</externalId>
        <inlineHelpText>The source record&apos;s object type.</inlineHelpText>
        <label>Source Type</label>
        <length>60</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__StartDate__c</fullName>
        <deprecated>false</deprecated>
        <description>The start date of the revenue schedule, derived from the source record.</description>
        <externalId>false</externalId>
        <inlineHelpText>The start date of the revenue schedule, derived from the source record.</inlineHelpText>
        <label>Start Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ffrr__TotalRevenue__c</fullName>
        <deprecated>false</deprecated>
        <description>Total revenue amount that can be recognized for the source record.</description>
        <externalId>false</externalId>
        <inlineHelpText>Total revenue amount that can be recognized for the source record.</inlineHelpText>
        <label>Total Revenue</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__TotalScheduledRevenue__c</fullName>
        <deprecated>false</deprecated>
        <description>Amount of revenue scheduled to be recognized.</description>
        <externalId>false</externalId>
        <inlineHelpText>Amount of revenue scheduled to be recognized.</inlineHelpText>
        <label>Total Scheduled Revenue</label>
        <summarizedField>ffrr__RevenueScheduleLine__c.ffrr__Revenue__c</summarizedField>
        <summaryForeignKey>ffrr__RevenueScheduleLine__c.ffrr__RevenueSchedule__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <label>Revenue Schedule</label>
    <listViews>
        <fullName>ffrr__All</fullName>
        <columns>NAME</columns>
        <columns>ffrr__Account__c</columns>
        <columns>ffrr__StartDate__c</columns>
        <columns>ffrr__EndDate__c</columns>
        <columns>ffrr__TotalRevenue__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Revenue Schedule Name</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Revenue Schedules</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
