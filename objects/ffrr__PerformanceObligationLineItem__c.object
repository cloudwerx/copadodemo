<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>ffrr__PerformanceObligationLineItemCompactLayout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>ffrr__PerformanceObligationLineItemCompactLayout</fullName>
        <fields>Name</fields>
        <fields>ffrr__Description__c</fields>
        <fields>ffrr__Revenue__c</fields>
        <fields>ffrr__Cost__c</fields>
        <fields>ffrr__SSP__c</fields>
        <fields>ffrr__StartDate__c</fields>
        <fields>ffrr__EndDate__c</fields>
        <label>Performance Obligation Line Item Compact Layout</label>
    </compactLayouts>
    <customHelpPage>ffrr__helprevenuecontract</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Source record/s of a performance obligation</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>ffrr__AccountName__c</fullName>
        <deprecated>false</deprecated>
        <description>Information about the Performance Obligation Line Item. Often this will be the account name, but it can be any information.</description>
        <externalId>false</externalId>
        <inlineHelpText>Information about the Performance Obligation Line Item. Often this will be the account name, but it can be any information.</inlineHelpText>
        <label>Display Information</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__Active__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Indicates whether the source record is active.</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates whether the source record is active.</inlineHelpText>
        <label>Active</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ffrr__BalanceSheetAccount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Balance Sheet GLA</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__Completed__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Indicates whether the source record is complete.</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates whether the source record is complete.</inlineHelpText>
        <label>Completed</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ffrr__CostBalanceSheetAccount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Balance Sheet GLA (Cost)</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__CostCenterAccount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Cost Center</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__CostCostCenterAccount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Cost Center (Cost)</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__CostIncomeStatementAccount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Income Statement GLA (Cost)</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__Cost__c</fullName>
        <deprecated>false</deprecated>
        <description>Cost for this Performance Obligation Line Item.</description>
        <externalId>false</externalId>
        <label>Cost</label>
        <precision>16</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ffrr__Description__c</fullName>
        <deprecated>false</deprecated>
        <description>The description of this Performance Obligation Line Item.</description>
        <externalId>false</externalId>
        <label>Description</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__EndDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ffrr__FieldMapping__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The Field Mapping Definition used to map the values from the source record to this record.</description>
        <externalId>false</externalId>
        <inlineHelpText>The Field Mapping Definition used to map the values from the source record to this record.</inlineHelpText>
        <label>Field Mapping Definition</label>
        <referenceTo>ffrr__FieldMappingDefinition__c</referenceTo>
        <relationshipLabel>Performance Obligation Line Items</relationshipLabel>
        <relationshipName>PerformanceObligationLineItems</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ffrr__IncomeStatementAccount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Income Statement GLA</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__IsControllingCostPOLI__c</fullName>
        <deprecated>false</deprecated>
        <description>Is this a controlling cost POLI?</description>
        <externalId>false</externalId>
        <formula>ffrr__PerformanceObligation__r.ffrr__ControllingCostPOLI__c  =  Id</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Is this a controlling cost POLI?</inlineHelpText>
        <label>Is Controlling POLI (Cost)</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ffrr__IsControllingPOLI__c</fullName>
        <deprecated>false</deprecated>
        <description>Is this a controlling revenue POLI?</description>
        <externalId>false</externalId>
        <formula>ffrr__PerformanceObligation__r.ffrr__ControllingPOLI__c  =  Id</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Is this a controlling revenue POLI?</inlineHelpText>
        <label>Is Controlling POLI (Revenue)</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ffrr__PercentageComplete__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>% Complete</label>
        <precision>18</precision>
        <required>false</required>
        <scale>10</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>ffrr__PerformanceObligation__c</fullName>
        <deprecated>false</deprecated>
        <description>The Performance Obligation this Performance Obligation Line Item belongs to.</description>
        <externalId>false</externalId>
        <label>Performance Obligation</label>
        <referenceTo>ffrr__PerformanceObligation__c</referenceTo>
        <relationshipLabel>Performance Obligation Line Items</relationshipLabel>
        <relationshipName>PerformanceObligationLineItems</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ffrr__Revenue__c</fullName>
        <deprecated>false</deprecated>
        <description>Revenue for this Performance Obligation Line Item.</description>
        <externalId>false</externalId>
        <label>Revenue</label>
        <precision>16</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ffrr__SSP__c</fullName>
        <deprecated>false</deprecated>
        <description>The Standalone Selling Price of this Performance Obligation Line Item.</description>
        <externalId>false</externalId>
        <label>SSP</label>
        <precision>16</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ffrr__SourceRecordSetting__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The Settings used by the source record related to this Performance Obligation Line Item.</description>
        <externalId>false</externalId>
        <inlineHelpText>The Settings used by the source record related to this Performance Obligation Line Item.</inlineHelpText>
        <label>Source Record Settings</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>The selected settings record does not exist, or Use in Revenue Contract is not enabled on it.</errorMessage>
            <filterItems>
                <field>ffrr__Settings__c.ffrr__UseInRevenueContract__c</field>
                <operation>equals</operation>
                <value>True</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>ffrr__Settings__c</referenceTo>
        <relationshipLabel>Performance Obligation Line Items</relationshipLabel>
        <relationshipName>PerformanceObligationLineItems</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ffrr__StartDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ffrr__ValueType__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The type of value held on this performance obligation line item: Revenue, Cost, or both.</inlineHelpText>
        <label>Value Type</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Performance Obligation Line Item</label>
    <nameField>
        <displayFormat>POLI-{00000000}</displayFormat>
        <label>Performance Obligation Line Item Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Performance Obligation Line Items</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>ffrr__Update</fullName>
        <availability>online</availability>
        <description>Updates the Performance Obligation Line Item based on its Field Mapping Definition</description>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Update from Source</masterLabel>
        <openType>sidebar</openType>
        <page>ffrr__PopulatePolis</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
