<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <content>pse__Expense_Limit_Rate_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>pse__Expense_Limit_Rate_Compact_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>pse__Expense_Limit_Rate_Compact_Layout</fullName>
        <fields>Name</fields>
        <fields>pse__Type__c</fields>
        <fields>pse__Amount__c</fields>
        <fields>pse__Is_Rate__c</fields>
        <fields>pse__Rate_Unit__c</fields>
        <fields>pse__Currency_Effective_Date__c</fields>
        <fields>pse__Is_Active__c</fields>
        <label>Expense Limit / Rate Compact Layout</label>
    </compactLayouts>
    <customHelpPage>pse__ExpenseLimitRateHelp</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Depending on the type of expense incurred, either enforces a maximum amount (for example, Personal Meals)  or calculates an amount based on a per-unit rate (for example, Auto Mileage). You can create a limit or rate for a particular time period or role. Also depends on the project&apos;s region, practice, or group (RPG). Expense limits/rates have a lookup relationship to one of the following: RPG or project.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>pse__Amount__c</fullName>
        <defaultValue>0.00</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The amount of the limit or rate</inlineHelpText>
        <label>Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>pse__Cascading__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, this checkbox determines whether the limit or rate applies to the child Projects/Regions/Practices/Groups of the specified Project/Region/Practice/Group.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, this checkbox determines whether the limit or rate applies to the child Projects/Regions/Practices/Groups of the specified Project/Region/Practice/Group.</inlineHelpText>
        <label>Cascading</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>pse__Currency_Effective_Date__c</fullName>
        <defaultValue>Today()</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The date to use for the exchange rate when performing currency conversion on this limit/rate amount.</inlineHelpText>
        <label>Currency Effective Date</label>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>pse__Description__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>pse__End_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>Expense Limit/Rate end date</description>
        <externalId>false</externalId>
        <inlineHelpText>Expense Limit/Rate end date</inlineHelpText>
        <label>End Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>pse__Group__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Group</label>
        <referenceTo>pse__Grp__c</referenceTo>
        <relationshipLabel>Expense Limits / Rates</relationshipLabel>
        <relationshipName>Expense_Limits_Rates</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>pse__Is_Active__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Active</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>pse__Is_Rate__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Rate</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>pse__Practice__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Practice</label>
        <referenceTo>pse__Practice__c</referenceTo>
        <relationshipLabel>Expense Limits / Rates</relationshipLabel>
        <relationshipName>Expense_Limits_Rates</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>pse__Project__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Project</label>
        <referenceTo>pse__Proj__c</referenceTo>
        <relationshipLabel>Expense Limits / Rates</relationshipLabel>
        <relationshipName>Expense_Limits_Rates</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>pse__Rate_Unit__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Rate Unit</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Mile</fullName>
                    <default>false</default>
                    <label>Mile</label>
                </value>
                <value>
                    <fullName>Kilometer</fullName>
                    <default>false</default>
                    <label>Kilometer</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>pse__Region__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Region</label>
        <referenceTo>pse__Region__c</referenceTo>
        <relationshipLabel>Expense Limits / Rates</relationshipLabel>
        <relationshipName>Expense_Limits_Rates</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>pse__Resource_Role__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Resource Role</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Program Manager - AU</fullName>
                    <default>false</default>
                    <label>Program Manager - AU</label>
                </value>
                <value>
                    <fullName>Project Manager/Scrum Master - AU</fullName>
                    <default>false</default>
                    <label>Project Manager/Scrum Master - AU</label>
                </value>
                <value>
                    <fullName>Salesforce CTA - AU</fullName>
                    <default>false</default>
                    <label>Salesforce CTA - AU</label>
                </value>
                <value>
                    <fullName>Salesforce Program Architect - AU</fullName>
                    <default>false</default>
                    <label>Salesforce Program Architect - AU</label>
                </value>
                <value>
                    <fullName>Salesforce Principal Architect - AU</fullName>
                    <default>false</default>
                    <label>Salesforce Principal Architect - AU</label>
                </value>
                <value>
                    <fullName>Salesforce Architect - AU</fullName>
                    <default>false</default>
                    <label>Salesforce Architect - AU</label>
                </value>
                <value>
                    <fullName>Salesforce Principal Consultant - AU</fullName>
                    <default>false</default>
                    <label>Salesforce Principal Consultant - AU</label>
                </value>
                <value>
                    <fullName>Salesforce Consultant - AU</fullName>
                    <default>false</default>
                    <label>Salesforce Consultant - AU</label>
                </value>
                <value>
                    <fullName>Salesforce Technical Lead - AU</fullName>
                    <default>false</default>
                    <label>Salesforce Technical Lead - AU</label>
                </value>
                <value>
                    <fullName>Salesforce Developer - AU</fullName>
                    <default>false</default>
                    <label>Salesforce Developer - AU</label>
                </value>
                <value>
                    <fullName>Salesforce Architect - IN</fullName>
                    <default>false</default>
                    <label>Salesforce Architect - IN</label>
                </value>
                <value>
                    <fullName>Salesforce Consultant - IN</fullName>
                    <default>false</default>
                    <label>Salesforce Consultant - IN</label>
                </value>
                <value>
                    <fullName>Salesforce Technical Lead - IN</fullName>
                    <default>false</default>
                    <label>Salesforce Technical Lead - IN</label>
                </value>
                <value>
                    <fullName>Salesforce Developer - IN</fullName>
                    <default>false</default>
                    <label>Salesforce Developer - IN</label>
                </value>
                <value>
                    <fullName>Salesforce QA Lead - IN</fullName>
                    <default>false</default>
                    <label>Salesforce QA Lead - IN</label>
                </value>
                <value>
                    <fullName>Salesforce QA Consultant - IN</fullName>
                    <default>false</default>
                    <label>Salesforce QA Consultant - IN</label>
                </value>
                <value>
                    <fullName>Salesforce UI Lead - IN</fullName>
                    <default>false</default>
                    <label>Salesforce UI Lead - IN</label>
                </value>
                <value>
                    <fullName>Salesforce UI Developer - IN</fullName>
                    <default>false</default>
                    <label>Salesforce UI Developer - IN</label>
                </value>
                <value>
                    <fullName>Salesforce Release Manager - IN</fullName>
                    <default>false</default>
                    <label>Salesforce Release Manager - IN</label>
                </value>
                <value>
                    <fullName>Salesforce Data Migration Consultant - IN</fullName>
                    <default>false</default>
                    <label>Salesforce Data Migration Consultant - IN</label>
                </value>
                <value>
                    <fullName>Automation Architect - AU</fullName>
                    <default>false</default>
                    <label>Automation Architect - AU</label>
                </value>
                <value>
                    <fullName>Automation Principal Consultant - AU</fullName>
                    <default>false</default>
                    <label>Automation Principal Consultant - AU</label>
                </value>
                <value>
                    <fullName>Automation Consultant - AU</fullName>
                    <default>false</default>
                    <label>Automation Consultant - AU</label>
                </value>
                <value>
                    <fullName>Automation Technical Lead - AU</fullName>
                    <default>false</default>
                    <label>Automation Technical Lead - AU</label>
                </value>
                <value>
                    <fullName>Automation Developer - AU</fullName>
                    <default>false</default>
                    <label>Automation Developer - AU</label>
                </value>
                <value>
                    <fullName>Automation QA Consultant - AU</fullName>
                    <default>false</default>
                    <label>Automation QA Consultant - AU</label>
                </value>
                <value>
                    <fullName>Automation Principal Consultant - IN</fullName>
                    <default>false</default>
                    <label>Automation Principal Consultant - IN</label>
                </value>
                <value>
                    <fullName>Automation Consultant - IN</fullName>
                    <default>false</default>
                    <label>Automation Consultant - IN</label>
                </value>
                <value>
                    <fullName>Automation Technical Lead - IN</fullName>
                    <default>false</default>
                    <label>Automation Technical Lead - IN</label>
                </value>
                <value>
                    <fullName>Automation Developer - IN</fullName>
                    <default>false</default>
                    <label>Automation Developer - IN</label>
                </value>
                <value>
                    <fullName>Automation QA Consultant - IN</fullName>
                    <default>false</default>
                    <label>Automation QA Consultant - IN</label>
                </value>
                <value>
                    <fullName>Integration Architect - AU</fullName>
                    <default>false</default>
                    <label>Integration Architect - AU</label>
                </value>
                <value>
                    <fullName>Integration Technical Lead - AU</fullName>
                    <default>false</default>
                    <label>Integration Technical Lead - AU</label>
                </value>
                <value>
                    <fullName>Integration Developer - AU</fullName>
                    <default>false</default>
                    <label>Integration Developer - AU</label>
                </value>
                <value>
                    <fullName>Integration QA Consultant - AU</fullName>
                    <default>false</default>
                    <label>Integration QA Consultant - AU</label>
                </value>
                <value>
                    <fullName>Integration Technical Lead - IN</fullName>
                    <default>false</default>
                    <label>Integration Technical Lead - IN</label>
                </value>
                <value>
                    <fullName>Integration Developer - IN</fullName>
                    <default>false</default>
                    <label>Integration Developer - IN</label>
                </value>
                <value>
                    <fullName>Integration QA Consultant - IN</fullName>
                    <default>false</default>
                    <label>Integration QA Consultant - IN</label>
                </value>
                <value>
                    <fullName>Integration Junior Developer - IN</fullName>
                    <default>false</default>
                    <label>Integration Junior Developer - IN</label>
                </value>
                <value>
                    <fullName>Delivery Manager - IN</fullName>
                    <default>false</default>
                    <label>Delivery Manager - IN</label>
                </value>
                <value>
                    <fullName>Salesforce Administrator - IN</fullName>
                    <default>false</default>
                    <label>Salesforce Administrator - IN</label>
                </value>
                <value>
                    <fullName>Salesforce Junior Developer - IN</fullName>
                    <default>false</default>
                    <label>Salesforce Junior Developer - IN</label>
                </value>
                <value>
                    <fullName>Engagement Manager - AU</fullName>
                    <default>false</default>
                    <label>Engagement Manager - AU</label>
                </value>
                <value>
                    <fullName>Salesforce UX Lead - AU</fullName>
                    <default>false</default>
                    <label>Salesforce UX Lead - AU</label>
                </value>
                <value>
                    <fullName>Automation Administrator - IN</fullName>
                    <default>false</default>
                    <label>Automation Administrator - IN</label>
                </value>
                <value>
                    <fullName>Release Manager - IN</fullName>
                    <default>false</default>
                    <label>Release Manager - IN</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>pse__Start_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>Expense Limit/Rate start date.</description>
        <externalId>false</externalId>
        <inlineHelpText>Expense Limit/Rate start date.</inlineHelpText>
        <label>Start Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>pse__Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Airfare</fullName>
                    <default>true</default>
                    <label>Airfare</label>
                </value>
                <value>
                    <fullName>Lodging (Room and Tax)</fullName>
                    <default>false</default>
                    <label>Lodging (Room and Tax)</label>
                </value>
                <value>
                    <fullName>Car Rental</fullName>
                    <default>false</default>
                    <label>Car Rental</label>
                </value>
                <value>
                    <fullName>Gasoline</fullName>
                    <default>false</default>
                    <label>Gasoline</label>
                </value>
                <value>
                    <fullName>Taxi</fullName>
                    <default>false</default>
                    <label>Taxi</label>
                </value>
                <value>
                    <fullName>Subway</fullName>
                    <default>false</default>
                    <label>Subway</label>
                </value>
                <value>
                    <fullName>Auto Mileage</fullName>
                    <default>false</default>
                    <label>Auto Mileage</label>
                </value>
                <value>
                    <fullName>Personal Meals</fullName>
                    <default>false</default>
                    <label>Personal Meals</label>
                </value>
                <value>
                    <fullName>Business Meals</fullName>
                    <default>false</default>
                    <label>Business Meals</label>
                </value>
                <value>
                    <fullName>Phone</fullName>
                    <default>false</default>
                    <label>Phone</label>
                </value>
                <value>
                    <fullName>Office Supplies and Services</fullName>
                    <default>false</default>
                    <label>Office Supplies and Services</label>
                </value>
                <value>
                    <fullName>IT Equipment and Services</fullName>
                    <default>false</default>
                    <label>IT Equipment and Services</label>
                </value>
                <value>
                    <fullName>Postage and Shipping</fullName>
                    <default>false</default>
                    <label>Postage and Shipping</label>
                </value>
                <value>
                    <fullName>General and Admin Expenses</fullName>
                    <default>false</default>
                    <label>General and Admin Expenses</label>
                </value>
                <value>
                    <fullName>Employee Relations</fullName>
                    <default>false</default>
                    <label>Employee Relations</label>
                </value>
                <value>
                    <fullName>Recruiting</fullName>
                    <default>false</default>
                    <label>Recruiting</label>
                </value>
                <value>
                    <fullName>Miscellaneous</fullName>
                    <default>false</default>
                    <label>Miscellaneous</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Expense Limit / Rate</label>
    <listViews>
        <fullName>pse__All</fullName>
        <columns>NAME</columns>
        <columns>pse__Type__c</columns>
        <columns>pse__Amount__c</columns>
        <columns>pse__Cascading__c</columns>
        <columns>pse__Is_Active__c</columns>
        <columns>pse__Is_Rate__c</columns>
        <columns>pse__Group__c</columns>
        <columns>pse__Practice__c</columns>
        <columns>pse__Project__c</columns>
        <columns>pse__Region__c</columns>
        <columns>pse__Resource_Role__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>EL-{0}</displayFormat>
        <label>Expense Limit / Rate Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Expense Limits / Rates</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>pse__Type__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>pse__Is_Active__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>pse__Is_Rate__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>pse__Amount__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>pse__Cascading__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>pse__Practice__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>pse__Project__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>pse__Region__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>pse__Group__c</customTabListAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <sharingReasons>
        <fullName>pse__PSE_Member_Share__c</fullName>
        <label>PSE Member Share</label>
    </sharingReasons>
    <sharingReasons>
        <fullName>pse__PSE_PM_Share__c</fullName>
        <label>PSE PM Share</label>
    </sharingReasons>
    <validationRules>
        <fullName>pse__Limit_Rate_May_Not_Have_Multiple_Targets</fullName>
        <active>true</active>
        <description>Expense Limit / Rate must not reference more than one of the following: Project, Region, Practice, Group.</description>
        <errorConditionFormula>OR(AND(NOT(OR(ISNULL(pse__Project__c),(pse__Project__c=&apos;&apos;))),NOT(AND(OR(ISNULL(pse__Region__c),(pse__Region__c=&apos;&apos;)),OR(ISNULL(pse__Practice__c),(pse__Practice__c=&apos;&apos;)),OR(ISNULL(pse__Group__c),(pse__Group__c=&apos;&apos;))))),AND(NOT(OR(ISNULL(pse__Region__c),(pse__Region__c=&apos;&apos;))),NOT(AND(OR(ISNULL(pse__Project__c),(pse__Project__c=&apos;&apos;)),OR(ISNULL(pse__Practice__c),(pse__Practice__c=&apos;&apos;)),OR(ISNULL(pse__Group__c),(pse__Group__c=&apos;&apos;))))),AND(NOT(OR(ISNULL(pse__Practice__c),(pse__Practice__c=&apos;&apos;))),NOT(AND(OR(ISNULL(pse__Region__c),(pse__Region__c=&apos;&apos;)),OR(ISNULL(pse__Project__c),(pse__Project__c=&apos;&apos;)),OR(ISNULL(pse__Group__c),(pse__Group__c=&apos;&apos;))))),AND(NOT(OR(ISNULL(pse__Group__c),(pse__Group__c=&apos;&apos;))),NOT(AND(OR(ISNULL(pse__Region__c),(pse__Region__c=&apos;&apos;)),OR(ISNULL(pse__Practice__c),(pse__Practice__c=&apos;&apos;)),OR(ISNULL(pse__Project__c),(pse__Project__c=&apos;&apos;))))))</errorConditionFormula>
        <errorMessage>Expense Limit / Rate must not reference more than one of the following: Project, Region, Practice, Group.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>pse__Must_have_project_or_RPG_set</fullName>
        <active>true</active>
        <errorConditionFormula>AND(LEN(pse__Project__c)=0,LEN(pse__Region__c)=0,
LEN(pse__Practice__c)=0,
LEN(pse__Group__c)=0)</errorConditionFormula>
        <errorMessage>A project, region, practice, or group must be set.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
