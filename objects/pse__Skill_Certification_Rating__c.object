<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <content>pse__Skill_Certification_Rating_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>pse__Skill_Certification_Rating_Compact_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>pse__Skill_Certification_Rating_Compact_Layout</fullName>
        <fields>Name</fields>
        <fields>pse__Skill_Certification__c</fields>
        <fields>pse__Resource__c</fields>
        <fields>pse__Rating__c</fields>
        <fields>pse__Evaluation_Date__c</fields>
        <label>Skill Certification Rating Compact Layout</label>
    </compactLayouts>
    <customHelpPage>pse__SkillCertificationRatingHelp</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Records a resource&apos;s proficiency rating in a skill/certification.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>psaws__Is_Resource_Current_User__c</fullName>
        <deprecated>false</deprecated>
        <description>The formula field checks whether the resource is a current user or not.</description>
        <externalId>false</externalId>
        <formula>pse__Resource__r.pse__Salesforce_User__c = $User.Id</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Is Resource Current User</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>psaws__Skill_Or_Certification__c</fullName>
        <deprecated>false</deprecated>
        <description>The formula field displays the list of Skills and Certifications ratings.</description>
        <externalId>false</externalId>
        <formula>pse__Skill_Certification__r.RecordType.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Skill or Certification</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Approval_Status__c</fullName>
        <deprecated>false</deprecated>
        <description>Indicates the approval status of the rating.</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the approval status of the rating.</inlineHelpText>
        <label>Approval Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Saved</fullName>
                    <default>false</default>
                    <label>Saved</label>
                </value>
                <value>
                    <fullName>Submitted</fullName>
                    <default>false</default>
                    <label>Submitted</label>
                </value>
                <value>
                    <fullName>Approved</fullName>
                    <default>false</default>
                    <label>Approved</label>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                    <label>Rejected</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>pse__Aspiration__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If selected, indicates the resource&apos;s interest in acquiring or gaining additional knowledge of this skill.</description>
        <externalId>false</externalId>
        <inlineHelpText>If selected, indicates the resource&apos;s interest in acquiring or gaining additional knowledge of this skill.</inlineHelpText>
        <label>Aspiration</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>pse__Certified__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If selected, indicates that the resource&apos;s certification is accredited and active. If deselected, indicates that the certification is inactive.</description>
        <externalId>false</externalId>
        <inlineHelpText>If selected, indicates that the resource&apos;s certification is accredited and active. If deselected, indicates that the certification is inactive.</inlineHelpText>
        <label>Certified</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>pse__Date_Achieved__c</fullName>
        <deprecated>false</deprecated>
        <description>The date when the rating was achieved.</description>
        <externalId>false</externalId>
        <inlineHelpText>The date when the rating was achieved.</inlineHelpText>
        <label>Date Achieved</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>pse__Evaluation_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>Date on which the rating was evaluated.</description>
        <externalId>false</externalId>
        <inlineHelpText>Date on which the rating was evaluated.</inlineHelpText>
        <label>Evaluation Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>pse__Expiration_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>Date on which the skill or certification rating expires.</description>
        <externalId>false</externalId>
        <inlineHelpText>Date on which the skill or certification rating expires.</inlineHelpText>
        <label>Expiration Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>pse__Last_Used_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>The date when the resource last used the skill.</description>
        <externalId>false</externalId>
        <inlineHelpText>The date when the resource last used the skill.</inlineHelpText>
        <label>Last Used Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>pse__Notes__c</fullName>
        <deprecated>false</deprecated>
        <description>You can enter information about the skill or certification rating.</description>
        <externalId>false</externalId>
        <inlineHelpText>You can enter information about the skill or certification rating.</inlineHelpText>
        <label>Notes</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>pse__Numerical_Rating__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(
        		ISNUMBER(
        			LEFT(TEXT(pse__Rating__c),2)
        		),
				VALUE(LEFT(TEXT(pse__Rating__c),2)),
				VALUE(LEFT(TEXT(pse__Rating__c),1))
			)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Numerical Rating</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Rating__c</fullName>
        <deprecated>false</deprecated>
        <description>The level of exposure or ability that the resource has for the skill or certification.</description>
        <externalId>false</externalId>
        <inlineHelpText>The level of exposure or ability that the resource has for the skill or certification.</inlineHelpText>
        <label>Rating</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>1 - Novice</fullName>
                    <default>false</default>
                    <label>1 - Novice</label>
                </value>
                <value>
                    <fullName>2 - Competent</fullName>
                    <default>false</default>
                    <label>2 - Competent</label>
                </value>
                <value>
                    <fullName>3 - Expert</fullName>
                    <default>false</default>
                    <label>3 - Expert</label>
                </value>
                <value>
                    <fullName>Acquired</fullName>
                    <default>true</default>
                    <label>Acquired</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>pse__Resource__c</fullName>
        <deprecated>false</deprecated>
        <description>The resource to which the rating applies.</description>
        <externalId>false</externalId>
        <inlineHelpText>The resource to which the rating applies.</inlineHelpText>
        <label>Resource</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Skill and Certification Ratings</relationshipLabel>
        <relationshipName>Skill_Certification_Ratings</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>pse__Skill_Certification__c</fullName>
        <deprecated>false</deprecated>
        <description>The skill or certification to which the rating applies.</description>
        <externalId>false</externalId>
        <inlineHelpText>The skill or certification to which the rating applies.</inlineHelpText>
        <label>Skill / Certification</label>
        <referenceTo>pse__Skill__c</referenceTo>
        <relationshipLabel>Resource Ratings</relationshipLabel>
        <relationshipName>Resource_Ratings</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>pse__Years_Experience__c</fullName>
        <deprecated>false</deprecated>
        <description>The number of years&apos; experience a resource has in a skill.</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of years&apos; experience a resource has in a skill.</inlineHelpText>
        <label>Years of Experience</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Skill / Certification Rating</label>
    <listViews>
        <fullName>psaws__My_Certifications_To_Expire_This_Year</fullName>
        <columns>NAME</columns>
        <columns>pse__Skill_Certification__c</columns>
        <columns>pse__Expiration_Date__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>pse__Expiration_Date__c</field>
            <operation>lessOrEqual</operation>
            <value>THIS_YEAR</value>
        </filters>
        <filters>
            <field>psaws__Skill_Or_Certification__c</field>
            <operation>equals</operation>
            <value>Certification</value>
        </filters>
        <filters>
            <field>psaws__Is_Resource_Current_User__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <label>My Certifications To Expire This Year</label>
    </listViews>
    <listViews>
        <fullName>pse__All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>Rating{0000000}</displayFormat>
        <label>Rating Id</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Skill and Certification Ratings</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>pse__Check_Resource</fullName>
        <active>true</active>
        <errorConditionFormula>AND(LEN(pse__Resource__c) &gt;  0,  NOT( pse__Resource__r.pse__Is_Resource__c))</errorConditionFormula>
        <errorDisplayField>pse__Resource__c</errorDisplayField>
        <errorMessage>Resource lookup must be set to an Contact with Is Resource = true</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
