<!--
	Description  : Certification App which withholds the certification lightning component
	Author		 : Cloudwerx
 	Created Date : 09 December 2020	
-->
<aura:application access="GLOBAL"  extends="force:slds" implements="force:appHostable,ltng:allowGuestAccess"><!--extends="ltng:outApp",implements="ltng:allowGuestAccess"-->
    <c:Cloudwerx_Certifications />
    <!--<c:Cloudwerx_fileUpload_XMLToJSON />-->
</aura:application>