({
	fetchCertificationsHelper : function(component, event, helper) {
		component.set('v.existingCertificationMessage', '');
        //console.log(component.get('v.recordId')); 
        var action = component.get("c.getCertifications");
        action.setParams({"userId" : component.get('v.recordId')});
        action.setCallback(this, function(response){
            if(response.getState() === "SUCCESS"){
                var result = response.getReturnValue();
                if(result.isSuccess){
                    component.set("v.existingCertifications", result.data.existingCerts);
                    if(result.data.existingCerts.length == 0){
                        component.set('v.existingCertificationMessage', result.data.ErrorMessage);
                    }
                    component.set("v.newCertifications", result.data.remainingCerts);
                }
            }
            else if(response.getState() === "ERROR"){
                let errors = response.getError();
                if (errors){
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        component.set('v.existingCertificationMessage', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
	},
    
    newCertificationHelper : function(component, event, helper) {
        //console.log('im inside newCertificationHelper');
        var action = component.get("c.insertNewCertifications");
        action.setParams({"certificationList" : component.get('v.newCertifications'),
                          "userId" : component.get('v.recordId')});
        action.setCallback(this, function(response){
            if(response.getState() === "SUCCESS"){
                var result = response.getReturnValue();
                if(result.isSuccess){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'SUCCESS',
                        message: 'Certifications have been added successfully!',
                        duration:' 4000',
                        key: 'info_alt',
                        type: 'info',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                    component.set("v.existingCertifications", result.data.existingCerts);
                    component.set("v.newCertifications", result.data.remainingCerts);
                }
            }
            else if(response.getState() === "ERROR"){
                let errors = response.getError();
                if (errors){
                    if (errors[0] && errors[0].message) {
                        component.set('v.existingCertificationMessage', errors[0].message);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Error',
                            message:errors[0].message,
                            duration:' 4000',
                            key: 'info_alt',
                            type: 'error',
                            mode: 'dismissible'
                        });
                        toastEvent.fire();
                    }
                } else {
                    console.log("Unknown error");
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Error',
                            message:'Please check with administrator. Something went wrong!',
                            duration:' 4000',
                            key: 'info_alt',
                            type: 'error',
                            mode: 'dismissible'
                        });
                        toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    }
})