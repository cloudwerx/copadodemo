<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>FFX_PSA_Set_TC_Exclude_from_Billing_True</fullName>
        <field>pse__Exclude_from_Billing__c</field>
        <literalValue>1</literalValue>
        <name>PSA Set TC Exclude from Billing True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSA_Set_Rec_Method_to_Deliverable</fullName>
        <field>pse__Recognition_Method__c</field>
        <literalValue>Deliverable</literalValue>
        <name>PSA Set Rec Method to Deliverable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>FFX PSA Exclude 0 Billable Amounts on Timecard Splits</fullName>
        <actions>
            <name>FFX_PSA_Set_TC_Exclude_from_Billing_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>pse__Timecard__c.pse__Total_Billable_Amount__c</field>
            <operation>equals</operation>
            <value>AUD 0</value>
        </criteriaItems>
        <description>Removes 0 Timecard Splits From Billing Event Generation</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FFX PSA Rev Forecast Set Deliverable Recognition Method on Timecard Splits</fullName>
        <actions>
            <name>PSA_Set_Rec_Method_to_Deliverable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sets recognition method to deliverable on timecard split for t/m projects</description>
        <formula>ISPICKVAL( pse__Project__r.pse__Billing_Type__c , &quot;Time and Materials&quot;) &amp;&amp; pse__Billable__c = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
